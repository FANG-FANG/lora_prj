/*******************************************************************************
	修改：
********************************************************************************/

#include "Stm32f10x_s.h"


#ifndef _hw_hxxzs_h

	#define _hw_hxxzs_h
	//#define _DebugCode



//7、35、55、56、57
	/*******************************************************************************************
					PLC系统参数定义区
	********************************************************************************************/
	#define		_ProductID		73	//产品ID
	#define		_ProductVer		0x14	//产品版本
	
	#define		HiFilter		(108000000/(8*HSCmaxFreq)-10) //高速滤波时间
	#define		EdgeCycle		(int)(1000/106.666) //边沿发生频度	

	#define		_BuildYY		18		//编译年份
	#define		_BuildMM		5		//编译月份
	#define		_BuildDD		15		//编译日
	#define		_BuildHH		14		//编译小时
//	#define		_FPGA_YY		17		//编译年份
//	#define		_FPGA_MM		5		//编译月份
//	#define		_FPGA_DD		9		//编译日
//	#define		_FPGA_HH		16		//编译小时
	
//	#define _FlashProtect
//	#define	_ConfigFPGA
//	#define _FreqCounter
//	#define	_DebugFPGA
//	#define _NewFPGA


	#define		AccessFlashTime	75    //访问敏感Flash超时时间，单位:0.1s
	#define		Hseries_Speed	12		//8->12
	#define		Sseries_Speed	25

	#define		eModLimit		15		//扩展模块数
	#define		ePortLimit		3		//扩展端口数	
	#define		_PIDLimit		64		//PID条数

	#define 	SPIwaitTimes 	5000
	#define 	SPIfailTimes	50

	#define		CodeBitsSeg		6		//位内码段数
	#define		CodeRegsSeg		7		//寄存器内码段数
	#define		ModrBitsSeg		5		//Modbus 可读位段数
	#define		ModrRegsSeg		7		//Modbus 可读寄存器段数6+1

	#define		WritableSMSeg	24		//Modbus 可写SM段数
	#define		WritableSVSeg	16		//Modbus 可写SV段数



	#define		Xpoints		1024		//开关量输入点数
	#define		Ypoints		1024		//开关量输出点数
	#define		Mpoints		12288		//中间继电器点数
	#define		SMpoints	8192		//系统状态位点数
	#define		SSpoints	2048		//步进指令点数
	#define		LMpoints	(64*32)		//局部中间继电器点数
	#define		Tpoints		1024		//定时器点数
	#define		Cpoints		256			//计数器点数
	#define		AIpoints	256			//模拟量输入点数
	#define		AQpoints	256			//模拟量输出点数
	#define		Vpoints		14848		//V点数
	#define		SVpoints	601			//SV点数,SV0-SV899用户可见
	#define		SVEpoints	(5120-SVpoints)//SVE点数,SV900-SV5119用户不可见
	#define		LVpoints	(64*32) 	//局部寄存器点数
	#define		DebugVpoints 200		//调试V访问长度

	#define		Xbase		0x0000		//X内码基址
	#define		Ybase		0x0600		//Y内码基址
	#define		Mbase		0x0c00  	//M内码基址
	#define		Tbase		0x3c00		//T内码基址
	#define		Cbase		0x4000		//C内码基址
	#define		SMbase		0x4200		//SM内码基址
	#define		SSbase		0x7000		//SS内码基址
	#define		LMbase		0x7800		//LM内码基址
	#define		AIbase		0x8000		//AI内码基址
	#define		AQbase		0x8100		//AQ内码基址
	#define		Vbase		0x8200		//V内码基址
	#define		TCVbase		0xbc00		//TCV内码基址
	#define		CCVbase		0xc000		//CCV内码基址
	#define		SVbase		0xc400		//SV内码基址
	#define		SVEbase		(0xc400+SVpoints)//扩展SV内码基址
	#define		LVbase		0xf700		//LV内码基址
	#define		DebugVbase	0xf000		//DebugV基址
	
	#define		_PLCsize	65536		//用户PLC容量
	
	#define		PI			3.1415926535//π常数定义
	
	/*******************************************************************************************
					故障代码常数表	
	********************************************************************************************/
	#define		_FirmwareErr	1	//B: CPU固件不完整
	#define		_FlashErr		2	//A: CPU存储器1访问异常
	#define		_SRAMErr		3	//A: CPU存储器2访问异常
	#define		_RTCErr			4	//A: RTC访问异常
	#define		_CPUIOErr		5	//A: CPU I/O访问失败
	#define		_SysParaErr		6	//A: CPU存储器3访问异常(系统参数异常)
	#define		_IOBErr			7	//A:	I/O板访问失败
	#define		_eBusErr		8	//A:	增强总线工作异常

	#define		_eFirmwareErr	60	//B:	1#扩展模块固件不完整(60-74:1#-15#)
	#define		_eModErr		75	//B:	扩展模块硬件故障

	
	#define		_ProgVerErr		89	//B:	编程软件版本过低
	#define		_uCodeErr		90	//B:	用户程序损坏
	#define		_STLOverflow	91	//B:	步地址超出范围
	#define		_SFROMOverflow	92	//B:	步合并超出范围
	#define		_TABOverflow	93	//B:	表格记录项数超出范围
	#define		_EdgeOverflow	94	//B:	指令取边沿数超出范围
	#define		_BwnIndexErr	95	//B:	掉电配置数据非法
	#define		_VoidFunErr		96	//B:	非法功能码
	#define		_VoidParaErr	97	//B:	非法操作数
	#define		_InsOverflow	98	//B:	同类指令条数超出范围
	#define		_NoEndErr		99	//B:	无结束指令
	#define		_eComErr		100	//C:	访问1# 扩展模块I/O失败(100-114:1#-15#)

	#define		_BatteryErr		131	//C:	RTC电池失效
	#define		_PowerFail		132	//C:	扩展模块供电不正常

	#define		_CfgMatchErr	140	//D	硬件不匹配
	#define		_WDTOverflow	141	//B	扫描超时，看门狗溢出
	#define		_LockedDat		142	//D	有锁定数据
	#define		_SSlimit		143 //D 当前运行的步进号超范围


	/*******************************************************************************************
					内核运行代码常数表	
	********************************************************************************************/
	#define		_Compiling		1
	#define		_ConfigModule	2
	#define		_RunCode		3

	/*******************************************************************************************
					FPGA外设地址
	********************************************************************************************/
	#define		aRegBase		0x0000								//操作寄存段地址
	#define		aFPGAVer		0x0000								//FPGA版本
	#define		aHSCF			0x0001								//HSC中断标志
	#define		aPLSF			0x0002								//PLS中断标志
	#define		aEIRQF			0x0003								//eIRQ中断标志
	#define		aEdgeF			0x0004								//边沿中断标志
	#define		aFPGADat		0x000c								//FPGA编译日期

	#define		aLEDline		0x000f								//Out LED显示行数
	#define		aLoFilter		0x0010								//低速滤波
	#define		aHiFilter		0x0011								//高速滤波
	#define		aEdgeFreq		0x0012								//边沿产生频度
	#define		aEdgeEna		0x0013								//边沿捕获使能D0-D7:下降
	#define		aReleasePLS		0x0014								//释放脉冲输出
	#define		debugFPGAi		0x0100								//FPGA输入信息
	#define		debugFPGAo		0x0110								//FPGA输出信息
	
	#define		aDI0			0x0018								//DI0-15
	#define		aDI1			0x0019								//DI16-31
	#define		aDI2			0x001a								//DI32-47
	#define		aDI3			0x001b								//DI48-63
	#define		aDO0			0x001c								//DO0-15
	#define		aDO1			0x001d								//DO16-31
	#define		aDO2			0x001e								//DO32-47
	#define		aDO3			0x001f								//DO48-63

#ifndef _NewFPGA

	//每组HSC占用0x80地址
	#define		aHSCbase		0x0400								//HSC外设基址
	#define		aTnP0			0x003								//SPD0闸门参数
	#define		aNTd0			0x004								//SPD0统计时间余数
	#define		aNTD0			0x005								//SPD0统计时间
	#define		aNP0			0x006								//SPD0统计脉冲个数

	#define		aTnP1			0x00b								//SPD1闸门参数
	#define		aNTd1			0x00c								//SPD1统计时间余数
	#define		aNTD1			0x00d								//SPD1统计时间
	#define		aNP1			0x00e								//SPD1统计脉冲个数
	
	#define		aHSCmode		0x010								//HSC0模式配置
	#define		aHSCsta			0x011								//HSC0状态
	#define		aHSCcnt			0x012								//HSC0当前值
	#define		aHSCpv			0x014								//HSC0比较值首址

	#define		aSHCx1cnt		0x052								//SHCx1当前值
	#define		aSHCx1PV		0x054								//SHCx1PV值

	#define		aPLS0base		0x0c00								//PLS0-3外设基址
	#define		aPLS1base		0x1000								//PLS4-7外设基址
	#define		aPWMcycle		0x001								//PWM高电平周期时间(1-2)
	#define		aBacklash		0x003								//间隙补偿
	#define		aOutRMN			0x005								//段剩余脉冲数(1-2)
	#define		aOutCNT			0x007								//当前位置(1-2)
	#define		aPLSsta			0x009								//运行状态
	#define		aPLSmode		0x00a								//模式与段信息
	#define		aSeg0Number		0x00f								//当前段脉冲数(0f-10)
	#define		aSeg0Cycle		0x011								//当前段周期(11-12)
	#define		aSeg0Mode		0x013								//当前子段信息
	#define		aSeg0SubT		0x014								//当前子段时间
	#define		aSeg1Number		0x015								//段1脉冲数(15-16)
	#define		aSeg1Cycle		0x017								//段1周期(17-18)
	#define		aSeg1Mode		0x019								//段1子段信息
	#define		aSeg1SubT		0x01a								//段1子段时间	
#endif

	/*******************************************************************************************
					GPIOA引脚定义
	********************************************************************************************/	
	#define		DRE2		1		/*COM2发送使能控制线*/
	#define		TX2			2		/*COM2发送信号线*/
	#define		RX2			3		/*COM2接收信号线*/
	#define		MAQ0		4		/*主模拟量输出*/
	#define		SCK1		5		/*从CPU通信口SPI之SCK信号线*/
	#define		MISO1		6		/*从CPU通信口SPI之MISO信号线*/
	#define		MOSI1		7		/*从CPU通信口SPI之MOSI信号线*/
	#define		MCO			8		/*36MHZ时钟信号输出线*/
	#define		TX1			9		/*COM1发送信号线*/
	#define		RX1			10		/*COM1接收信号线*/
	#define		USB_DM		11		/*USB口 D-通信线*/
	#define		USB_DP		12		/*USB口 D+通信线*/
	#define		TMS			13		/*JTAG口 TMS信号线*/
	#define		TCK			14		/*JTAG口 TCK信号线*/
	#define		TDI			15		/*JTAG口 TDI信号线*/

	/*******************************************************************************************
					GPIOB引脚定义
	********************************************************************************************/
	#define		INT			0		/*从CPU中断申请信号*/
	#define		NSS3		1		/*从CPU通信口SPI之NSS信号线*/
	#define		NSS1		2		/*从CPU配置SPI之NSS信号线*/
	#define		TDO			3		/*JTAG口 TDO信号线*/
	#define		nTRST		4		/*JTAG口 nTRST信号线*/
	#define		IOctr1		5		/*IO板控制数据线1*/
	#define		WorkCtrl	6		/*工作电源控制信号线*/
	#define		NADV		7		/*外扩SRAM地址锁存线*/
	#define		CANRX		8		/*CAN 接收线*/
	#define		CANTX		9		/*CAN 发送线*/
	#define		TX3			10		/*USART3发送信号线*/
	#define		RX3			11		/*USART3接收信号线*/
	#define		NSS2		12		/*IO板SPI口之NSS信号线*/
	#define		SCK2		13		/*IO板SPI口之SCK信号线*/
	#define		MISO2		14		/*IO板SPI口之MISO信号线*/
	#define		MOSI2		15		/*IO板SPI口之MOSI信号线*/

	/*******************************************************************************************
					GPIOC引脚定义
	********************************************************************************************/
	#define		BatteryV	0		/*后备电池电压输入信号*/
	#define		AdjV0		1		/*电位器输入0*/
	#define		AdjV1		2		/*电位器输入1*/
	#define		SRST		4		/*从CPU通信口复位信号线*/
	#define		CDONE		5		/*从CPU配置完成信号线*/
	#define		SW0			6		/*地址拨码开关0输入线*/
	#define		SW1			7		/*地址拨码开关1输入线*/
	#define		SW2			8		/*地址拨码开关2输入线*/
	#define		SW3			9		/*地址拨码开关3输入线*/
	#define		TX4			10		/*USART4发送信号线*/
	#define		RX4			11		/*USART4接收信号线*/
	
	#define		RTCO		13		/*RTC校准时钟输出*/

	/*******************************************************************************************
					GPIOD引脚定义
	********************************************************************************************/\
	#define		D02			0		/*外扩SRAM 数据线D02*/
	#define		D03			1		/*外扩SRAM 数据线D03*/
	
	#define		BOD			3		/*系统掉电检测线*/
	#define		OE			4		/*外扩SRAM 读控制线*/
	#define		WE			5		/*外扩SRAM 写控制线*/
	#define		SW7			6		/*地址拨码开关7输入线*/
	#define		NE1			7		/*外扩SRAM 片选信号线*/
	#define		D13			8		/*外扩SRAM 数据线D13*/
	#define		D14			9		/*外扩SRAM 数据线D14*/
	#define		D15			10		/*外扩SRAM 数据线D15*/
	#define		A16			11		/*外扩SRAM 地址线A16*/
	#define		A17			12		/*外扩SRAM 地址线A17*/
	#define		DRE3		13		/*USART3 发送使能控制线*/
	#define		D00			14		/*外扩SRAM 数据线D00*/
	#define		D01			15		/*外扩SRAM 数据线D01*/

	/*******************************************************************************************
					GPIOE引脚定义
	********************************************************************************************/
	#define		UB			0		/*外扩SRAM 高字节选通信号线*/
	#define		LB			1		/*外扩SRAM 低字节选通信号线*/
	#define		RunLED		2		/*运行指示灯输出信号线*/
	#define		ComLED		3		/*通信指示灯输出信号线*/
	#define		ErrGrn		4		/*错误指示灯输出绿灯*/
	#define		ErrRed		5		/*错误指示灯输出红灯*/
	#define		RunSW		6		/*运行开关输入信号线*/
	#define		D04			7		/*外扩SRAM 数据线D04*/
	#define		D05			8		/*外扩SRAM 数据线D05*/
	#define		D06			9		/*外扩SRAM 数据线D06*/
	#define		D07			10		/*外扩SRAM 数据线D07*/
	#define		D08			11		/*外扩SRAM 数据线D08*/
	#define		D09			12		/*外扩SRAM 数据线D09*/
	#define		D10			13		/*外扩SRAM 数据线D10*/
	#define		D11			14		/*外扩SRAM 数据线D11*/
	#define		D12			15		/*外扩SRAM 数据线D12*/

	/*******************************************************************************************
					GPIOF引脚定义
	********************************************************************************************/
	#define		Charge		6		/*后备电池充电控制线*/
	#define		IO62		7		/*IO62*/
//	#define		IO63		8		/*IO63*/
	#define		DRE1		8		/*COM1发送使能信号线*/

	#define		BootPara	(unsigned char *)0x08023800	//引导参数区存储地址

	/*******************************************************************************************
					RTC位定义
	********************************************************************************************/
	#define		IsLeapYear	0

	/*******************************************************************************************
		uStatus  用户程序状态定义
	********************************************************************************************/
	#define		CR			0
	#define		ENO			1
	#define		uI			2

	/*******************************************************************************************
		SM0-7  特殊状态位定义
	********************************************************************************************/
	#define		RP1			0		//Run:1 Stop:0
	#define		PR0			1		//Run:0 Stop:1
	#define		FirstScan	2		//首次运行:1
	#define		T10ms		3		//10ms周期信号 50%占空比
	#define		T100ms		4		//100ms周期信号 50%占空比
	#define		T1s			5		//1s周期信号 50%占空比

	/*******************************************************************************************
		SM8-15  特殊状态位定义
	********************************************************************************************/
	#define		WDTovr		0		//看门狗溢出
	#define		runSW		1		//运行开关
	#define		Running		2		//运行状态
	#define		SysFail		3		//系统故障
	#define		NoMatch		4		//项目与硬件不匹配
	#define		BatLow		5		//电池电压低
	#define		DivZero		6		//除零标志
	#define		Overflow	7		//运行溢出标志

	/*******************************************************************************************
		BootReg.TickTmS寄存器位定义
	********************************************************************************************/
	#define		Tick10ms	0		//Tick of 10ms
	#define		Tick100ms	1		//Tick of 100ms
	#define		Tick1s		2		//Tick of 1s
	#define		Cycle10ms	3		//10ms周期信号
	#define		Cycle100ms	4		//100ms周期信号
	#define		Cycle1s		5		//1s周期信号
	
	
	/*******************************************************************************************
		INTmsg.RunSWFlag 寄存器位定义
	********************************************************************************************/	
	#define		RunSWcur	5		//前次采样值
	#define		RunSWchg	6		//运行开关有效跳变
	#define	  	RunSWval	7		//滤波结果值

	/*******************************************************************************************
		SysMsg.Ctrl 寄存器位定义
	********************************************************************************************/
	#define		RunST	 	0		//0-停止运行用户程序 1-运行用户程序
	#define		FirstST		1		//1-首次扫描用户程序
	#define		NoCalScanT	2		//1-不统计扫描周期
	#define		ReCompile	3		//1-重编译用户程序
	#define		cRunValue	4		//1-清除用户程序运行变量
	#define		RunST_		5		//1-前次运行状态
	#define		RunChg		6		//1-运行状态跳变
	#define		LinkEmod	7		//1-链接扩展模块
	#define		ClearPLCu	8		//1-清除PLC程序
	#define		OTstop		9		//1-停止一周期
		
	/*******************************************************************************************
		SysMsg.SCW 寄存器位定义
	********************************************************************************************/
	
	/*******************************************************************************************
		SysMsg.iMsg 寄存器位定义
	********************************************************************************************/
	#define		RefOprCode	0		//更新操作代码

	/*******************************************************************************************
		PortPara.Status 寄存器位定义
	********************************************************************************************/
	#define		IsASCII		12		//ASCII标志
	#define		RCVbusy		13		//RCV忙标志,次标志
	#define		TxDelay		14		//发送延时
	#define		PortBusy	15		//端口忙标志

	/*******************************************************************************************
		SysTmsg.iMsg 寄存器位定义
	********************************************************************************************/
	#define		AccessOvrT	0

	/*******************************************************************************************
		sCPUcomm.aMission寄存器位定义
	********************************************************************************************/
	#define		PushPend	0		//从CPU中断挂起
	#define		aFrameN_	1		//前一任务自动组帧标志

	/*******************************************************************************************
		Com.Mission寄存器位定义
	********************************************************************************************/
	#define		FreeRx		2		//自由接收
	#define		TxOrRx		3		//收发标志
	#define		UserOrSys	7		//系统控制位

	/*******************************************************************************************
		uPLC.KernelStatus
	********************************************************************************************/
	#define		DisConfigModule		0	//禁止配置扩展模块
	#define		DisRunuCode			1	//禁止运行微代码
	#define		DisCompile			2	//禁止编译微代码


	/*******************************************************************************************
		uPLC.ePortAddrP寄存器位定义
	********************************************************************************************/
	#define		PortIndex	0		//子端口号
	#define		SorH		2		//0:S 1:H
	
	/*******************************************************************************************
		T1ms.iFlag寄存器位定义
	********************************************************************************************/
	#define		T252		0		//定时器252中断标志
	#define		T253		1		//定时器253中断标志
	#define		T254		2		//定时器254中断标志
	#define		T255		3		//定时器255中断标志



	#define		CLI()		__disable_irq()  //关闭中断
	#define		SEI()		__enable_irq()  //开启中断

	typedef struct 
	{	unsigned char Mission; //协议[0-2]：0：自由协议  1：MODBUS RTU  2：MODBUS ASCII  3:自由被动从接收  收发标志[3]：0收，1发
		                       //任务号[4-6]  端口占用标志[7]：0系统控制，1用户控制
		unsigned char CheckSum; //累加和
		unsigned short Len; //收发长度
		unsigned char *Pointer; //收发指针
		unsigned int OverTime; //超时定时器,单位16us
		volatile unsigned char ProceNo; //进程任务器 0:空闲  1:解析命令   2:等待协作任务完成  3:协作任务完成组回复帧 8:主动发送数据 9:主动接收完成等待处理
		unsigned char Reserved; //状态寄存器
		unsigned char Error; //错误码
		unsigned char PortNo; //端口号
		unsigned char Temp; //ASCII暂存寄存器
		unsigned char Buffer[520];
	} COMSTRUCT;


	typedef struct
	{ 	unsigned char Mission; //任务号 0:空闲 1:任务数据准备就绪 2:发送  3:接收延时 4:接收  5:接收正确 6:接收异常 7:连续占用资源
	    unsigned char CallID; //父进程ID
		unsigned char aMission; //辅助任务 D0:有中断挂起
		unsigned char ErrCnt; //错误计数器
		unsigned short TxLen; //发送字数
		unsigned short RxLen; //接收字数
		unsigned short *P; //发送起始地址
		unsigned short Buffer[260]; //数据缓存
		unsigned short IntBuffer[16]; //中断数据缓存
	}SPICOMSTRUCT;


	typedef struct
	{	unsigned char AckAddr; //应答地址
		unsigned char FunID; //功能码
		unsigned char AckLen; //响应字节数
		unsigned short *OutP;	//out位地址指针
		unsigned short *ErrP; //Err位地址指针
	}ACTIVECOMSTRUCT;

	typedef __packed struct
	{	unsigned short Year; //年
		unsigned short Month; //月
		unsigned short Day; //日
		unsigned short Hour; //时
		unsigned short Minute;	//分
		unsigned short Second; //秒
		unsigned short Week; //星期
	}TIMESTRUCT;

	typedef __packed struct
	{	unsigned char ModuleID; //模块ID
		unsigned char Ver; //模块版本
	}EMODULESTRUCT;

	typedef __packed struct
	{	unsigned short SegNo; //HHSC当前段号
		int CNT; //HHSC当前值
		unsigned short Error; //HHSC错误码
	}HSCSTRUCT;	

	typedef __packed struct
	{	unsigned short SegNo; //PLS当前段号
		int OutNum; //PLS已输出值
		int CurPos; //PLS当前值
		unsigned short Error; //PLS错误码
	}PLSSTRUCT;
	
	typedef __packed struct
	{	int MachinePoint; //机械原点
		short Backlash; //间隙补偿
		short FollowFactor; //跟随因子
	}PLS1STRUCT;

	typedef __packed struct
	{	unsigned short FailCode;
		unsigned short Time;
	}FailLogSTRUCT;

	typedef struct
	{	signed short	CurScanTime;		//当前扫描时间,单位0.1ms
		unsigned short	MinScanTime;		//最小扫描时间,单位0.1ms
		unsigned short	MaxScanTime;		//最大扫描时间,单位0.1ms
		unsigned short	ErrorCode;			//系统错误代码
		unsigned short	Com1ErrCode;		//Com1通讯错误代码
		unsigned short	Com2ErrCode;		//Com2通讯错误代码
		unsigned short	Port3ErrCode;		//Port3通讯错误代码
		unsigned short	Port4ErrCode;		//Port4通讯错误代码
		unsigned short	Port5ErrCode;		//Port5通讯错误代码
		unsigned short	ClientPortNo;		//错误的指令号
		unsigned short	ROMcpyID;
		unsigned short	SV11;				//保留字节
		TIMESTRUCT		CurTime;			//当前系统时间,格式(年 月 日 时 分 秒 星期)
		unsigned char	PLCName[12];		//PLC站名
		short	uWDT;				//扫描超时定时器,单位ms
		unsigned short	Address;			//主机地址
		unsigned short	eModNum;			//高字节为CPU类型,低字节扩展模块数
		EMODULESTRUCT	ModuleInf[16];		//模块ID号
		unsigned short	Com1Mode;			//Com1通讯模式
		unsigned short	Com1OverTime;		//Com1等待回复超时值,单位ms
		unsigned short	Com2Mode;			//Com2通讯模式
		unsigned short	Com2OverTime;		//Com2等待回复超时值,单位ms
		unsigned short	PLCSize;			//PLC程序容量
		__packed int	SysCLK;				//系统时钟 32位(单位16us)
		unsigned char ProductNo1[6];		//产品序列号
		unsigned short	Com3Mode;			//Com3通讯模式
		unsigned short	Com3OverTime;		//Com3等待回复超时值,单位ms
		unsigned short	Com4Mode;			//Com4通讯模式
		unsigned short	Com4OverTime;		//Com4等待回复超时值,单位ms
		unsigned short	Com5Mode;			//Com5通讯模式
		unsigned short	Com5OverTime;		//Com5等待回复超时值,单位ms
		HSCSTRUCT	HSCData[8];				//高速计数器0-7的当前段号,当前值,错误代码
		PLSSTRUCT	PLSData[8];				//高速脉冲输出0-7的当前段号,当前位置,当前速度,错误代码
		unsigned short	DisOutDO;			//禁止刷新DO输出
		unsigned short	ComInterval;		//Com主动通讯间隔
		unsigned short	SoftAddr;			//软地址
		unsigned short	DIPaddr;			//DIP地址
		unsigned short	ProductNo2[2];		//产品流水号		
		unsigned short 	PLSdirDelay;		//脉冲输出方向提前时间
		unsigned short	SV147_150[4]; 		//保留
		unsigned short	LockNumber;			//锁定数据个数
		unsigned short	IP[2];				//IP地址
		unsigned short	NET_Mask[2];		//子网掩码
		PLS1STRUCT	PLSData1[8];			//高速脉冲输出0-7的机械原点，间隙补偿
		unsigned short SV188_600[413]; 		//保留
	}SVSTRUCT;

	typedef struct
	{	unsigned char *OprAddr; //Flash写地址
		unsigned short Len; //Flash写长度
		unsigned char Buffer[2112]; //Flash读写缓存器		
	}WriteFlashSTRUCT;

	typedef struct
	{	unsigned char Mission; /*  0:空闲 0x01:下载PLC程序  0x02:清除PLC程序 0x03,写升级固件 0x04:上载PLC程序
						    	 0x05:编译用户程序		D4-D7:操作Flash的端口号*/
		unsigned char Flag; //D0:硬件配置 D1:程序代码
		unsigned char OprCode; //操作码
		unsigned short PreFrameNo; //前一帧信息节帧号
		unsigned short OverTimer; //写Flash超时井
		unsigned char *WRP; //code读写指针		
	}AccessSTRUCT;

	typedef struct
	{	unsigned char Mission; //功能码
		unsigned char Error; //错误计数器
		unsigned short OverT; //超时计数器
		unsigned short Status; //状态字D0:并口发送完成 D1:收到响应字
							   //D2:并口接收完成 D3:eIRQ
		unsigned char *RxP;
		unsigned short RxLen;
		unsigned short Buffer[128]; //收发缓存
	}ParallelSTRUCT;
	
	typedef struct	
	{   unsigned int Mission; /*中断处理任务号
		D0-D3:PLS中断子任务  D4-D7:HSC中断子任务	D8-D11:Edge子任务	 D12-D15:T1ms子任务 D28-D31:主任务号*/
		unsigned short iHSC; //高速计数器中断
		unsigned short iPLS; //高速脉冲中断
		unsigned short iParallel; //并口中断(no delete)
		unsigned short iEdge; //边沿中断
		unsigned short iT1mS; //1ms定时中断
		unsigned short mHSC;
		unsigned short mPLS;
		unsigned short mEdge;
		unsigned short uPLS;
		unsigned short mT1mS;
	}uINTSTRUCT;
		
	typedef struct
	{	COMSTRUCT Com1,Com2,PortCom;
		unsigned char ComMode_n[8]; //串口通信模式变化检测寄存器
		unsigned int TickTmS;
		unsigned char *P1,CodeKey;
		unsigned short WrSearchPacket; //0xa659:写搜索回复信息至Flash
		unsigned short WaitTaskID; //等待任务ID号
		unsigned char *UpdataP;
		unsigned char UpdataFlag; //升级中标志D0-D3:被升级的模块地址,0为主机  D4-D7:升级数据源串口号
		unsigned char HostAddr; //主机地址
		unsigned char RandomCode; //随机码
		WriteFlashSTRUCT WriteFlash; //写Flash控制消息
		AccessSTRUCT Access; //ROM访问控制消息
		unsigned char UpdataInfo[200]; //升级信息索引表0-39:升级索引 40-49:通信参数
		SPICOMSTRUCT sCPUcomm; //从CPU通信控制消息
		ParallelSTRUCT Parallel; //并行口控制消息
		unsigned char PortAck[30]; //端口回复缓存
		unsigned short SPIfailCNT; //20
		unsigned short FrameNo;
		unsigned short Resreved[18];
	}BootRegSTRUCT;

	
	typedef struct
	{	unsigned char Password[8]; //PLC伪密码
		unsigned char SoftVer1,SoftVer2,SoftVer3; //PLC主版本,次版本
		unsigned char FrameLen; //信息包长度
		unsigned char uCodeOkFlag; //PLC程序信息完整标志 0xaf:信息完整
		unsigned char ConfigFrames; //硬件配置信息帧数
		unsigned char ConfigRemain; //硬件配置信息尾数
		unsigned char InsFrames[2]; //指令信息帧数
		unsigned char InsRemain; //指令信息尾数
		unsigned char ProjectFrames[2]; //项目信息帧数
		unsigned char ProjectRemain; //项目信息尾数
		unsigned char TextFrames[2]; //注释信息帧数
		unsigned char TextRemain; //注释信息尾数
		unsigned char MasterVer; //CPU支持的编程软件版本S:2 H:
		unsigned char UpLoadFlag; //上载标志,0xaf:禁止上载
		unsigned char CPUtype; //硬件CPU类型
		unsigned char FactoryID; //厂家代码
		unsigned char Reserved1[3]; //保留字节
		unsigned char FirmwareOkFlag; //固件完整标志 0xaf:固件完整
	} PLCHeaderSTRUCT;


	typedef struct
	{	unsigned char	PLCName[12];		//PLC站名
		unsigned short	uWDT;				//扫描超时定时器,单位ms
		unsigned short	Address;			//主机地址
		unsigned short	eModNum;			//高字节为CPU类型,低字节扩展模块数
		EMODULESTRUCT	ModuleName[16];		//模块ID号
		unsigned short	Com1Mode;			//Com1通讯模式
		unsigned short	Com1OverTime;		//Com1等待回复超时值,单位ms
		unsigned short	Com2Mode;			//Com2通讯模式
		unsigned short	Com2OverTime;		//Com2等待回复超时值,单位ms
		unsigned short	PLCSize;			//PLC程序容量
		__packed unsigned int	SysCLK;		//系统时钟 32位(单位16us)
		unsigned char ProductNo1[6];		//产品序列号
		unsigned short	Com3Mode;			//Com3通讯模式
		unsigned short	Com3OverTime;		//Com3等待回复超时值,单位ms
		unsigned short	Com4Mode;			//Com4通讯模式
		unsigned short	Com4OverTime;		//Com4等待回复超时值,单位ms
		unsigned short	Com5Mode;			//Com5通讯模式
		unsigned short	Com5OverTime;		//Com5等待回复超时值,单位ms
		HSCSTRUCT	HSCData[8];				//高速计数器0-7的当前段号,当前值,错误代码
		PLSSTRUCT	PLSData[8];				//高速脉冲输出0-7的当前段号,当前位置,当前速度,错误代码
		unsigned short	DisOutDO;			//禁止刷新DO输出
		unsigned short	ComInterval;		//Com主动通讯间隔
		unsigned short	SoftAddr;			//软地址
		unsigned short	DIPaddr;			//DIP地址
		unsigned short	ProductNo2[2];		//产品流水号		
		unsigned short 	PLSdirDelay;		//脉冲输出方向提前时间
		unsigned short	SV147_150[4]; 		//保留
		unsigned short	LockNumber;			//锁定数据个数
		unsigned short	IP[2];				//IP地址
		unsigned short	NET_Mask[2];		//子网掩码
		PLS1STRUCT	PLSData1[8];			//高速脉冲输出0-7的机械原点，间隙补偿
		unsigned short SV188_600[413]; 		//保留
	} PPDsvSTRUCT;

	typedef struct
	{	unsigned short *P; //MSP指针
		void * const FunP[75];
	} STM32VectorSTRUCT;

	typedef struct
	{	unsigned char Company[8]; //公司名称
		unsigned char BuildData[4]; //编译日期
		unsigned char CPUID; //CPU物理ID
		unsigned char Reserved[3]; //保留
		unsigned char ProductID; //产品ID
		unsigned char ProductVer; //产品版本
		unsigned char CPUindex; //低半字节CPU数,高半字节CPU号
		unsigned char CPU2Offset; //CPU2起始段号
		unsigned int * Seg1Base; //段1起始地址
		unsigned int * Seg1Len; //段1长度
		unsigned int * Seg2Base; //段2起始地址
		unsigned int * Seg2Len; //段2长度
		unsigned int * Seg3Base; //段3起始地址
		unsigned int * Seg3Len; //段3长度
		unsigned int * Seg4Base; //段4起始地址
		unsigned int * Seg4Len; //段4长度
		unsigned int * Seg5Base; //段5起始地址
		unsigned int * Seg5Len; //段5长度
	} UpdataIndexSTRUCT;

	typedef struct
	{	unsigned short CodeAddrBase; //内码地址基址
		unsigned short Len; //长度
	} CodeAddrSTRUCT;
	
	typedef struct
	{	unsigned short Alain; //对齐数0表示已对齐
		unsigned short Len; //实际写长度
		unsigned short SegPos; //搜索到的段位置
	}SearchSTRUCT;
	
	typedef struct 
	{	unsigned char *PhyBase; //物理地址基址
		unsigned short PhyLen; //物理地址长度
		unsigned char Position; //物理地址所在段号
		unsigned char Avail; //有效标志0:无效
	}PhyAddressSTRUCT;

	typedef struct
	{   unsigned short LockAddr[100]; //锁定逻辑地址
		unsigned char *LockPhy[100]; //锁定的物理地址
		unsigned short LockValue[100]; //锁定值
	}LockSTRUCT;

	typedef struct
	{   unsigned char ComRefFlag; //串行通信口通信格式更新标志D0:Com1 D1:Com2 D2:Com3 D3:Com4 D4:Com5 D5:Com6 D6:Com7 D7:Com8		
		unsigned char SpeMask; //特殊操作掩码 0xa6-有效
		unsigned char mRemoteCtrl; //D0-D3下载类型主程序消息	D4-D7：远程启停消息
		unsigned char ErrCtrl;
		unsigned short Ctrl; //PLC运行控制消息
		unsigned short SCW; //系统控制字
		unsigned short iMsg; //中断标志
		unsigned short SysDelayCNT; //系统延时寄存器
		unsigned short ErrLEDCNT;
		unsigned short RandomCode;
		unsigned short RandomCNT; //更新计数器
	}SysMsgSTRUCT;

	typedef struct
	{	unsigned char X[Xpoints/8];	//开关量输入
		unsigned char Y[Ypoints/8];	//开关量输出
		unsigned char M[Mpoints/8];	//中间继电器
		unsigned char T[Tpoints/8];	//定时器状态位
		unsigned char C[Cpoints/8];	//计数器状态位
		unsigned char SS[SSpoints/8];	//步进状态位
		unsigned char SM[SMpoints/8];	//系统状态位
		unsigned char LM[LMpoints/8];	//局部中间继电器
	} PLCBITSSTRUCT;

	typedef struct
	{	signed short AI[AIpoints]; //AI寄存器
		signed short AQ[AQpoints]; //AQ寄存器
		signed short TCV[Tpoints]; //定时器当前值
		signed short CCV[(Cpoints+32)]; //计数器当前值
		signed short LV[LVpoints]; //局部寄存器
		unsigned int Tclk[Tpoints]; //定时器启动时钟
		signed int CNTSV[Cpoints]; //计数器复位操作表
	}iPLCREGSSTRUCT;


	typedef struct
	{	signed short V[Vpoints]; //V寄存器
		signed short SVE[200]; //601
		__packed int HSCfreq[8]; //高速计数器频率801
		FailLogSTRUCT FailLog[8]; //故障日志817
		__packed signed short Com2_6Interval[7]; //Com2-8通信间隔
		unsigned short WDTstatus[2]; //WDT触发细分原因
		unsigned short YYMM; //版本高字842
        unsigned short DDHH;
		unsigned short FPGA_YYMM; //FPGA版本高字
		unsigned short FPGA_DDHH;
		unsigned short Gateway[2]; //网关IP 846
		unsigned short MAC[3]; //MAC地址
		unsigned short ComCharT[5];//SV851
		signed short SVE1[SVEpoints-255]; //SV856-601以上的系统寄存器
		unsigned char * iVector[53]; //中断向量表
		unsigned char * PIDtask[64]; //PID任务指针
	}ePLCREGSSTRUCT;

	typedef struct
	{	signed short SVE[200]; //601
		__packed int HSCfreq[8]; //高速计数器频率801
		FailLogSTRUCT FailLog[8]; //故障日志817
		__packed signed short Com2_6Interval[7]; //Com2-8通信间隔
		unsigned short WDTstatus[2]; //WDT触发细分原因
		unsigned short YYMM; //版本高字842
        unsigned short DDHH;
		unsigned short FPGA_YYMM; //FPGA版本高字
		unsigned short FPGA_DDHH;
		unsigned short Gateway[2]; //网关IP 846
		unsigned short MAC[3]; //MAC地址
		unsigned short ComCharT[5];//SV851
		unsigned short Reserved[50]; //SV901
	}DummySVESTRUCT;

	union MixSTRUCT{
		struct{
			SVSTRUCT DummySV; //SV0-600
			DummySVESTRUCT DummySVE; //SV601-951
			unsigned short PortBaud[8];
			unsigned char UpdataData[300];
			}Boot;
		iPLCREGSSTRUCT uRegs; //AI,AQ,TCV,CCV,LV
	};

	typedef struct
	{	unsigned char Bit[(Xpoints+Ypoints+Mpoints+Tpoints+Cpoints+SSpoints)/8]; //位锁定标志
		unsigned char iReg[(AIpoints+AQpoints+Tpoints+Cpoints+32)/8]; //内部寄存器锁定标志
		unsigned char eReg[Vpoints/8]; //外部寄存器锁定标志
	}LockFlagSTRUCT;


	typedef struct
	{	unsigned int RTCn_; //计算标准实时时钟前时刻值
		unsigned int Flag; //D0:当前年为闰年标志,1代表闰年
		unsigned short Days; //距2000-1-1的总天数
	}RTCSTRUCT;

	typedef struct
	{	unsigned char RemoteCtrl; /*D0-D3:下载类型0xa：正常下载  0xd:不停机下载  D4-D7:0xa0：远程运行 0x60:远程停止
									0xb6:清除PLC程序 0xa5:看门狗溢出*/
		unsigned char RunSWFlag; //运行开关采样 D0-D3:采样计数器
		unsigned char AddrSWsmp; //地址开关采样值
		unsigned char AddrSWcur; //地址开关当前值
		unsigned char AddrSWflt; //地址开关滤波计数器
	}IntMsgSTRUCT;

	typedef struct
	{	unsigned int T100msCNT; //3.125ms时基
		unsigned int T1sCNT; //31.25ms时基
		volatile unsigned short TIM2_CNTH; //TIM2当前值高16位
		unsigned short TIM2_CCRH[3]; //TIM2之OCC1，OCC2，OCC3，匹配值高16位
		unsigned short TIM3_CNTH; //TIM3当前值高16位
		unsigned char TickT1s; //31.25ms计时器
		unsigned char Tick50ms; //50ms计时器
		unsigned char Tick500ms; //500ms计时器
	    unsigned char iMsg; //中断定时消息 D0:敏感操作超时 D1:Com1加载匹配值 D2:Com2加载匹配值
		unsigned char mMsg; //主程序定时消息
		unsigned int ScanCNT; //扫描计数器D0-D23:当前时刻值 D24-D31:统计任务号
	}SysTmsgSTRUCT;

	typedef struct
	{ 	unsigned short SegResolution; //跳段数
		unsigned short StrSeg;
		unsigned short StpSeg;
		unsigned short LoadTMS;
		int Pn; //脉冲数
		int HiCycle; //输出周期
		int LoCycle;
	}PLSParaSTRUCT;
	
	typedef struct
	{	int OutN_n;
		int RMN_n[2]; //剩余脉冲数
		int RunOffset; //插补运行初值
	    unsigned short Status; //D0:0忙标志 D1:1忙标志 D2:1-影响绝对位置  D4-D7:输出模式  D8:0-1个点 1-2个点
		unsigned short LoadSeg; //D0-D7:当前加载起始段号 D8-D15:总载总段号
		PLSParaSTRUCT Para; //Zn脉冲输出参数
	}CtrlPLSSTRUCT;

	typedef struct
	{	unsigned short Mode; //D0-D7:比较段数 D10-D11:比较模式 D12-D15:脉冲模式
		unsigned char OutMask[48]; //输出掩码
		unsigned int BitOut; //位输出地址
		int *PV;	//PV指针
	}CtrlHSCSTRUCT;

	typedef __packed struct
	{	unsigned char ID; //配置ID
		unsigned short Reserved; //保留
		unsigned short CfgOffset; //配置信息偏移
		unsigned char DIfilter; //DI滤波系数
		unsigned char DInum;  //DI点数
		unsigned short DIaddr; //DI位地址
		unsigned char DOnum; //DO点数
		unsigned short DOaddr; //DO位地址
		unsigned char AInum; //AI通道数
		unsigned short AIaddr; //AI位地址
		unsigned char AOnum; //AO通道数
		unsigned short AOaddr; //AO地址
	}MapIOSTRUCT;

	typedef __packed struct
	{	unsigned char Len; //配置表长度
		unsigned char Flag; //配置字：D0:DI  D1:DO  D2:AI  D3:AO  D4-D7:0-无参数 1-HSC  2-PLS 3-HSC+PLS
		unsigned short ParaOffset;
	}MapParaSTRUCT;
		
	typedef struct
	{	unsigned short Status; //步运行状态
		unsigned short Index; //步索引信息
		unsigned short LastIndex; //D0-D7最迟进表索引 D8-D15:距SFROM_LD字节数
		unsigned int uAddr[100]; //运行步号微地址
	}SSSTRUCT;

	typedef struct
	{	unsigned int Timer1ms[4]; //D0-23:定时器1ms启动值 D24-31:1启动中
		unsigned char *Timer1msP[4]; //1mS定时器指针
		unsigned short TIM4CCRH[4]; //定时器1ms高16位匹配值
		unsigned short TIM4CNTH; //定时器的高16位		
	}Timer1msSTRUCT;

	typedef struct
	{	unsigned short DINumber;
		unsigned short DONumber;
		unsigned short yLEDLine;
		unsigned short CheckSum;
	}IOCFGSTRUCT;

	typedef struct
	{	unsigned int KernelStatus; //内核状态
		unsigned int RePC; //重新运行PC
		unsigned int uStatus;  //用户程序状态指针
		unsigned int LDstack;	//LD栈
		unsigned int Mstack; //逻辑位栈
		unsigned int uStack[18]; //用户软堆栈		
		unsigned int *uSP; //用户软栈指针
		unsigned int *uCR;  //指向位CR
		unsigned int *uENO; //指向位ENO
		unsigned char *HSCindex; //高速脉冲输入通道索引指针
		unsigned char *PLSindex; //高速脉冲输出通道索引指针
		unsigned char *MapIO; //IO映射信息指针
		unsigned char *RunSTA; //主程序起始地址		
		unsigned int BitLockOffset; //位锁定位偏移
		unsigned int EdgeBuffer; //边沿缓存
		unsigned int BitTempOut;
		unsigned short RegTempOut[8];
		signed short InsBuffer[130]; //指令运算缓存
		unsigned short RunID; //运行ID
		unsigned short uWDTCNT; //用户看门狗主计数器
		unsigned short FastSysCode; //快速系统处理码D0:Void
		unsigned short BwnTimerStr; //掉电保持定时器起始号
		unsigned short BwnTimerEnd; //掉电保持定时器结束号				
		unsigned short RunStatus; //运行状态D0-D7:内部运行错误码
		CtrlPLSSTRUCT CtrlPLS[8]; //脉冲输出控制寄存器
		CtrlHSCSTRUCT CtrlHSC[8]; //脉冲计数控制寄存器
		unsigned short PLSoutMask; //脉冲输出掩码
		unsigned short PIDnumber; //PID任务条数
		IOCFGSTRUCT IOCFG; //IO配置
		uINTSTRUCT uINT; //用户中断标志
		SSSTRUCT CtrlSS; //步进控制寄存器
		Timer1msSTRUCT T1mS; //1ms定时器控制寄存器
	}uPLCSTRUCT;

	typedef struct
	{	unsigned char *CodePC; //源程序指针
		unsigned char *uPC; //运行程序指针
		unsigned char *P; //当前指令首址
		unsigned char Avail; //错误代码
		unsigned char Align; //对齐长度
	}CompileSTRUCT;

	typedef struct
	{	unsigned short k;
		unsigned short b;
		unsigned int Check;
	}AIcaliSTRUCT;

	typedef struct
	{	unsigned short ParaCRC; //参数区CRC16
		unsigned char Password[8];	//系统密码
		unsigned int FPGAcrc32; //配置数据CRC32
		LockSTRUCT  Lock; //锁定数据
		unsigned short ROMcopyID;
		unsigned short OTIflag; //0xa596:初始化系统参数
		AIcaliSTRUCT CaliPara[3]; //校准参数
		unsigned int FPGAVer;
		unsigned int ID[3];  //0
		unsigned int FactoryID; 
		unsigned short BKP_DR[42];
		unsigned short LockNumber;
		unsigned short Align;
		unsigned short CaliData[80];
		unsigned int Reserve[73]; //100
		unsigned short FTCrate[32]; //FTC
		float FTCfactor[32]; //FTC模糊系数		
	}SysParaSTRUCT;

	typedef struct
	{	unsigned short CMDcode; //操作码
	}BootSTRUCT;
	
	typedef struct
	{	unsigned short CMDcode; //操作码
		unsigned int FPGAver;
		unsigned int SPIOverT;
	}ApplicationSTRUCT;	
	
	typedef struct
	{	PLCHeaderSTRUCT Header; //PLC程序头信息
		unsigned char *InsP; //PLC指令节起始地址
		unsigned char *ProjectP; //PLC项目节起始地址
		unsigned char *TextP; //PLC注释节起始地址
		unsigned short ConfigLen; //配置信息长度
		unsigned int CodeLen; //代码信息长度
		unsigned int uCodeCRC; //用户程序CRC32校验码
	}uCodeIndexSTRUCT;

	typedef struct
	{	unsigned int Opr;
		unsigned char *uPC;
	}GetRegOprSTRUCT;

	typedef struct{
		unsigned char *P;
		unsigned short Flag;
		unsigned short N;
	}GetLockSTRUCT;
	
	typedef struct{
		unsigned short *QueueP; //任务指针
		unsigned short QueueNumber; //队列数量
		unsigned short CurTask; //当前任务
		unsigned int IntervalCLK; //间隔时钟
		unsigned int OverflowCNT; //溢出定时器
		unsigned short Status; /*D0-D3:任务命令种类 0-comm  1-modr  2-modw	3-hwrd	4-hwwr 5-rcv 6-xmt
								 D4: 0-位读写  1-字读写
							   D12:0-RTU 1:ASCII   D13:RCVbusy  D14：1-发送延时中  D15:0-空闲  1-忙 */
		unsigned short HaltT; //端口异常超时井
		unsigned short DREctrl; //DRE切换字
		unsigned short RxLen; //接收长度
		unsigned short Schar; //起始符
		unsigned short Echar; //结束符
		unsigned char *RxdP; //接收数据指针
		unsigned int BitOut1; //输出状态1指针
		unsigned int BitOut2; //输出状态2指针
	}PortParaSTRUCT;

	typedef struct{
		unsigned short *QueueP; //任务指针
		unsigned short QueueNumber; //队列数量
		unsigned short CurTask; //当前任务
		unsigned int IntervalCLK; //间隔时钟
		unsigned int OverflowCNT; //溢出定时器
		unsigned short ComNumber; //通信数量
		unsigned short clientMsgAmount;
		unsigned short Status; /*D0-D3:任务命令种类 0-modr  1-modw	2-hwrd	3-hwwr
								 D4: 0-位读写  1-字读写
							   D12:0-RTU 1:ASCII   D13:RCVbusy  D14：1-发送延时中  D15:0-空闲  1-忙 */
		unsigned short ActiveFlag; //主动超时激活标志
		unsigned short HaltT; //端口异常超时井
		unsigned short RxLen; //接收长度
		unsigned char *RxdP; //接收数据指针
		unsigned int BitOut1; //输出状态1指针		
		unsigned char Buffer[300];
	}TCPParaSTRUCT;
	
	typedef struct{
		unsigned char uINTmsg; //代码中断标志
		unsigned char reserved[9];
	}ABSSTRUCT;

	typedef struct{
		unsigned char *uPC; //程序运程PC指针
		unsigned int uID; //指令ID
	}RunParaSTRUCT;

	typedef struct{
		unsigned short Mission; /*D0-D7:任务号 0:校准 1：等待校准完成 2:启动规则转换 3：等待规则转换扫描结束
		                                       4：转换扫描结束*/
		unsigned short Buffer[9];
		unsigned short uMax[5]; //微采集最大值
		unsigned short uMin[5]; //微采集最小值
		unsigned short AI[5]; //滤波有效值
		unsigned int uSum[5]; //微采集累积和
	}ADCSTRUCT;

	typedef struct
	{	unsigned short Flag; //D0：运行 D1:ticking  D2:initT 
		unsigned char AdjDelay; //整定延时
		unsigned char AdjCyc; //整定周期数
		unsigned char AdjEndC; //整定结束周期数
		unsigned char SurgeCnt; //振荡次数
		unsigned char SurgeCyc; //振荡周期
		unsigned char IDnumber;  //任务序列号
		unsigned short OnT;
		unsigned short CycT;
		short SVn;
		short AT_SV; //自整定SV
		short En; //当前偏差
		short En_1;
		short En_2;
		int AdjSum; //整定偏差的偏差累计和
		float Un_1;
		float RunFactor; //运行模糊因子
		float FTC_P;
		float FTC_I;
		float FTC_D;
	}FTCSTRUCT;

	typedef struct
	{	unsigned short Flag; //D0：运行 D1:ticking  D2:initT 
		unsigned char AdjDelay; //整定延时
		unsigned char AdjCyc; //整定周期数
		unsigned char AdjEndC; //整定结束周期数
		unsigned char SurgeCnt; //振荡次数
		unsigned char SurgeCyc; //振荡周期
		unsigned short CycT; //时钟5ms
		short SVn;
		short AT_SV; //自整定SV
		short En; //当前偏差
		short En_1;
		short En_2;
		int AdjSum; //整定偏差的偏差累计和
		float Un_1;
		float FixFactor; //修正系数
		float P; //实际运算比例
	}APIDSTRUCT;



#endif

