
#include "STM32F10x_S.h"

void LoraGpioInit(void);
void SPI3Init(void);
void Tim4Init(void);

/*注：此前需先初始化系统时钟RCCInitialize();*/
void LoRa_Init(void)
{
	AFIO_MAPR = (AFIO_MAPR&0XF8FFFFFF)|(1<<25);//禁用JTAG功能 开放其引脚(pb3,pb4,pa15）
	RCC_AHBENR |=(1<<1);//DMA2时钟使能	
	RCC_APB1ENR |=0X8000;//SPI3时钟
	RCC_APB1ENR |=1<<2;//TIM4时钟

	RCC_APB2ENR|=(1<<6);
	GPIOE->CRL = 0X44434444;//PE4用于调试 lora接收数据指示灯

	LoraGpioInit();
	SPI3Init();
	Tim4Init();
}

/*lora六个引脚初始化*/
void LoraGpioInit(void)
{
	GPIOB->CRH = (GPIOB->CRH&0xFFFFFFF0)|0x00000008;	//lora中断DIO0-->PB8 上下拉输入模式
	GPIOB->CRH = (GPIOB->CRH&0xFFFFFF0F)|0x00000030; // loraRESET-->PB9 通用推挽输出 
	GPIOB->CRL =0X44BBB444;//loraSPI3复用推挽输出 PB3-sck pb4-miso pb5-mosi 
	GPIOA->CRH =0X34444444;//loraSPI3 PA15-cs 通用推挽输出
	GPIOA->BSRR |=(1<<15);//cs引脚拉高		
	GPIOB->BSRR |=(1<<9);//复位引脚拉高

	EXTI_IMR |= 0x100;//开放来自线8上的事件请求。
	AFIO_EXTICR3 |= 1;//线八的中断源为pb8
	EXTI_RTSR |= 0x100;//上升沿触发中断

	NVIC_SETENA0 |= (1<<NVIC_EnableEXTI5_9);	//NVIC_EnableIRQ(EXTI9_5_IRQn); pb8中断使能
	NVIC_SETENA1 |= (1<<NVIC_EnableDMA21); //	a_NVIC_SetIRQEnable(DMA2_Channel1_IRQChannel,1,0x25);//开DAM2中断
	
}

/*lorasx1276\78的SPI通信配置*/
void SPI3Init(void)
{
  /*禁止CRC,8位数据格式，软件从设备管理，先高位MSB ，波特率111：fpclk/256(可改)，从配置模式，空闲下sck低，第一时钟沿开始采样数据*/
  SPI3->CR1 = 0x33c;
  SPI3->I2SCFGR &= 0xF7FF;	//spi3选择spi模式	

  /* Write to SPIx CRCPOLY */
  SPI3->CRCPR = 7;
  SPI3->CR1 |= 0x0040;//开启SPI
}
/*((99+1)*(719+1)/72mhz)=1 000us=1ms
 *主机发送完成等待接收数据超时计数
 */
void Tim4Init(void)
{
  TIM4->ARR = 99;
	TIM4->PSC = 719;
	TIM4->DIER |=1<<0;
	//TIM4->CR1 |=1<<0;//开中断
	NVIC_SETENA0 |= (1<<NVIC_EnableTIM4);
	
}
 
void USART2_INIT(void)
{
	  unsigned int temp;
	//PA1-RE高发TX-低收RX，PA2-TX,PA3-RX
	GPIOA->CRL&=0XFFFF000F; //IO 状态设置
  GPIOA->CRL|=0X00008B30; //IO 状态设置 

  RCC_APB1ENR|=1<<17; //使能串口时钟
  RCC_APB1RSTR|=1<<17; //复位串口 2
  RCC_APB1RSTR&=~(1<<17); //停止复位

////波特率设置
//USART2->BRR=0x0EA6; // 波特率设置
//USART2->CR1|=0X200C; //1 位停止,无校验位.

////使能接收中断
//USART2->CR1|=1<<8; //PE 中断使能
//USART2->CR1|=1<<5; //接收缓冲区非空中断使能 
////MY_NVIC_Init(3,3,USART2_IRQn,2);//组 2，最低优先级
//GPIOA->BRR|=(1<<1);//默认为接收模式	

  //enable UE(13)
  USART2->CR1|=0X2000;
  //M(12)=0;PCE(10)=0;PS(9)=0;  TE(3)=1;RE(2)=1;
  temp=USART2->CR1&(~0X160C);
  USART2->CR1=temp|0X000C;
  USART2->CR3&=0XFCFF;//USART1->CR3:CTSE(9)=0 Disable,RTSE(8)=0 Disable
  //开中断 RXNEIE(5)=1   关中断 TXEIE(7)=0，TXCIE(6)=0
  USART2->CR1|=0X020;          
  USART2->CR1&=~(0X00C0);
  USART2->CR1|=0X000C;//enable USART1 UE(13) TE(3),RE(2)
  USART2->SR&=~0X40;//清除TC(6) 
	
	
}
//unsigned char RX_485[271];
//unsigned short RX_CNT=0;
//void L_USART2_IRQHandler(void)
//{
//  unsigned char rx_data;

//  /**************接收中断******************************************************/
//  if (USART2->SR&(1<<5))
//  {
//		rx_data=USART2->DR;
//		if(RX_CNT<129)
//		{
//      RX_485[RX_CNT]=rx_data;
//			RX_CNT++;
//    }
//	}

//}



