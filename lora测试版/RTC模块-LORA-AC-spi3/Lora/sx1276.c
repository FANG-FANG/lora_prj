
#include "sx1276.h"
#include "STM32F10x_S.h"




typedef struct 
	{	unsigned char Mission; //协议[0-2]：0：自由协议  1：MODBUS RTU  2：MODBUS ASCII  3:自由被动从接收  收发标志[3]：0收，1发
		                       //任务号[4-6]  端口占用标志[7]：0系统控制，1用户控制
		unsigned char CheckSum; //累加和
		unsigned short Len; //收发长度
		unsigned char *Pointer; //收发指针
		unsigned int OverTime; //超时定时器,单位16us
		volatile unsigned char ProceNo; //进程任务器 0:空闲  1:解析命令   2:等待协作任务完成  3:协作任务完成组回复帧 8:主动发送数据 9:主动接收完成等待处理
		unsigned char Reserved; //状态寄存器
		unsigned char Error; //错误码
		unsigned char PortNo; //端口号
		unsigned char Temp; //ASCII暂存寄存器
		unsigned char Buffer[520];
	} COMSTRUCT;
	extern COMSTRUCT LoRaBuf;

extern unsigned char * FillRAM(unsigned char *P, unsigned int Len,unsigned int Data);
extern void LoRa_Recieve_Handler(void);
	
	
	
DeviceModeSetting LoraMasSla;
	
RadioState_t LoraRadioState;
uint32_t LoraChannel;
uint8_t LoraPacketSize;
int8_t LoraPacketSnrValue;
int16_t LoraPacketRssiValue;
RadioLoRaSettings_t LoRa;

static uint8_t RxTxBuffer[RX_BUFFER_SIZE];/* Reception buffer 收发缓存区*/
unsigned char tsbuff;

uint8_t Dmawbuff[BUFFER_SIZE];
uint8_t Dmarbuff[BUFFER_SIZE];
uint8_t DmaState;

extern void delay(uint32_t times);

extern int LR_Recieve_Handler(uint8_t* RxBuffer,uint8_t PacketSize);

void dma_write_start_lora(uint8_t *buffer, uint8_t size);

void dma_read_start_lora(uint8_t *buffer, uint8_t size);

void SpiWriteNss(uint8_t level);

void SX1276SetRfTxPower( int8_t power );

uint8_t SX1276GetPaSelect( uint32_t channel );

static void RxChainCalibration( void );

void SX1276SetTx( uint32_t timeout );

void SX1276WriteFifo( uint8_t *buffer, uint8_t size );

void SX1276ReadFifo( uint8_t *buffer, uint8_t size );

void SX1276SetOpMode( uint8_t opMode );

void LRmemcpy(unsigned char *p,const unsigned char *data,unsigned short len)
{
    while(len--)
		{
     *p++ =*data++;
    }
}

/*SX1276重启，所有寄存器均处于默认值*/
void SX1276SetReset(void)
{
	GPIOB->BRR |=(1<<9);//复位拉低
	delay(1);
	GPIOB->BSRR |=(1<<9);//拉高
	delay(6);
}
/*控制spi nss即cs信号 pa15 */
void SpiWriteNss(uint8_t level)
{
	if(!level)
	{
		GPIOA->BRR |=(1<<15);
	}
	else
	{
		GPIOA->BSRR |=(1<<15);
	}
}
/*SPI进行一bit数据读写 */
uint8_t SpiTransfer( uint8_t outData )
{
	
	uint8_t txData = outData;
	uint8_t rxData = 0,i=0;
	if(SPI3->SR&1)i = SPI3->DR;//清除RXNE
	while(!(SPI3->SR&2));//waiting for transmit buffer was empty
  SPI3->DR = txData;
	while(!(SPI3->SR&1));//waiting for receive buffer was not empty
	rxData = SPI3->DR;
	return rxData;
	
}

/************************************************
 SPI写1次 addr, 地址  data  数据
*************************************************/
void SX1276Write( uint8_t addr, uint8_t data )
{
    SX1276WriteBuffer( addr, &data, 1 );
}

/************************************************
 SPI读1次 addr, 地址
*************************************************/
uint8_t SX1276Read( uint8_t addr )
{
    uint8_t data;
    SX1276ReadBuffer( addr, &data, 1 );
    return data;
}

/************************************************
 SPI连续写多个地址 addr, 地址 
*buffer, 待写入缓冲区 size  数据长度
*************************************************/
void SX1276WriteBuffer( uint8_t addr, uint8_t *buffer, uint8_t size )
{
    uint8_t i;

    SpiWriteNss(0);
    SpiTransfer( addr | 0x80 );
    for( i = 0; i < size; i++ )
    {
        SpiTransfer( buffer[i] );
    }
    SpiWriteNss(1);
}
/************************************************
 SPI连续读多个地址  addr, 地址
 *buffer, 读出缓冲区  size  数据长度
*************************************************/
void SX1276ReadBuffer( uint8_t addr, uint8_t *buffer, uint8_t size )
{
    uint8_t i;

    SpiWriteNss(0);
    SpiTransfer( addr & 0x7F );
    for( i = 0; i < size; i++ )
    {
        buffer[i] = SpiTransfer( 0 );
    }
    SpiWriteNss(1);
}
/************************************************
 写入SX1276发送缓冲区  *buffer,写入缓冲区 size 数据长度
*************************************************/
void SX1276WriteFifo( uint8_t *buffer, uint8_t size )
{
    SX1276WriteBuffer( 0, buffer, size );
}
/************************************************
 从SX1276接收缓冲区读取数据 
 *buffer, 读出缓冲区  size  数据长度
*************************************************/
void SX1276ReadFifo( uint8_t *buffer, uint8_t size )
{
    SX1276ReadBuffer( 0, buffer, size );
}

/*发送完成 */
void OnTxDone( void )
{
//	LoRa.RxContinuous = false;
  SX1276SetRx(0);//连续接收
	if(LoraMasSla.LoraMS==Master)
	{
		TIM4->CR1 |=1<<0;//开计数器
		LoraMasSla.MasterTimeOut = 10000;//10秒
		LoraMasSla.MasterState = MTxdone;
	}
}
extern void USART2_TX(uint8_t *data,uint16_t len);
/*数据接收完成处理*/
void OnRxDone( uint8_t *payload, uint16_t size, int16_t _rssi, int8_t _snr )
{ 
	USART2_TX(payload,size);
	if(LoraMasSla.LoraMS==Master)
	{
		LoraMasSla.MasterState = MDateWaitHandle; 
		LoraMasSla.MasterSize = size;
		FillRAM(LoraMasSla.MasterBuffer,RX_BUFFER_SIZE,0);
		LRmemcpy(LoraMasSla.MasterBuffer,payload,size);
		SX1276SetStby( );//或者 //SX1276SetSleep( );//等待接收到的数据处理完成
	}
	else
	{
		//以后可以直接DMA存放到目的地
		LRmemcpy(LoRaBuf.Buffer,payload,size);
		LoRaBuf.Len=size;	
		LoRa_Recieve_Handler();
		if(DmaState!=RF_TX_RUNNING)
			SX1276SetRx(0);
  }

}
/*发送超时处理 */
void OnTxTimeout( void )
{
		SX1276SetRx(0);//连续接收
}
/*接收超时处理*/
void OnRxTimeout( void )
{
	  SX1276SetRx(0);//连续接收
}
/* 有接收到数据，但CRC检验错误，数据无用处理*/
void OnRxError( void )
{
		SX1276SetRx(0);//Radio.Rx(0);//连续接收
}
/*信道活动检测，检测数据的lora前导码*/
void OnCadDone(bool channelActivityDetected )
{
	 if(channelActivityDetected)
	  SX1276SetRx(2000);//CAD检测到数据开启2s接收窗
	 else
	 {
		   SX1276SetRx(0);
		}
}

/*在SPI和引脚初始化之后配置寄存器，进入射频接收状态 */
void SX1276AppInit(void)
{
//	  SpiWriteNss(1);//cs拉高
  	LoraRadioState =  RF_IDLE;
	  DmaState = RF_IDLE;
	  LoraMasSla.LoraMS = Slave;
	  LoraMasSla.MasterState = MIdle;
	  LoraMasSla.MasterTimeOut = 0;
	
	  SX1276SetReset();
    RxChainCalibration( );
    SX1276SetOpMode( RF_OPMODE_SLEEP );//初始化lora为睡眠模式，有些寄存器只在睡眠模式下可配置
		SX1276SetLoraModem( );
		SX1276Write(REG_LR_PAYLOADMAXLENGTH,0xff);

    SX1276SetChannel(RF_FREQUENCY);

		SX1276SetTxConfig( TX_OUTPUT_POWER, LORA_BANDWIDTH,
													 LORA_SPREADING_FACTOR, LORA_CODINGRATE,
													 LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
													 true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );

		SX1276SetRxConfig( LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
													 LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
													 LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
													 0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

   SX1276SetRx(0);		
	 
}	
/*设置输出功率及引脚，注：设置要对应电路引脚*/
void SX1276SetRfTxPower( int8_t power )
{
    uint8_t paConfig = 0;
    uint8_t paDac = 0;

    paConfig = SX1276Read( REG_PACONFIG );
    paDac = SX1276Read( REG_PADAC );

    paConfig = ( paConfig & RF_PACONFIG_PASELECT_MASK ) | SX1276GetPaSelect( LoraChannel );
    paConfig = ( paConfig & RF_PACONFIG_MAX_POWER_MASK ) | 0x70;

    if( ( paConfig & RF_PACONFIG_PASELECT_PABOOST ) == RF_PACONFIG_PASELECT_PABOOST )
    {
        if( power > 17 )
        {
            paDac = ( paDac & RF_PADAC_20DBM_MASK ) | RF_PADAC_20DBM_ON;
        }
        else
        {
            paDac = ( paDac & RF_PADAC_20DBM_MASK ) | RF_PADAC_20DBM_OFF;
        }
        if( ( paDac & RF_PADAC_20DBM_ON ) == RF_PADAC_20DBM_ON )
        {
            if( power < 5 )
            {
                power = 5;
            }
            if( power > 20 )
            {
                power = 20;
            }
            paConfig = ( paConfig & RF_PACONFIG_OUTPUTPOWER_MASK ) | ( uint8_t )( ( uint16_t )( power - 5 ) & 0x0F );
        }
        else
        {
            if( power < 2 )
            {
                power = 2;
            }
            if( power > 17 )
            {
                power = 17;
            }
            paConfig = ( paConfig & RF_PACONFIG_OUTPUTPOWER_MASK ) | ( uint8_t )( ( uint16_t )( power - 2 ) & 0x0F );
        }
    }
    else
    {
        if( power < -1 )
        {
            power = -1;
        }
        if( power > 14 )
        {
            power = 14;
        }
        paConfig = ( paConfig & RF_PACONFIG_OUTPUTPOWER_MASK ) | ( uint8_t )( ( uint16_t )( power + 1 ) & 0x0F );
    }
    SX1276Write( REG_PACONFIG, paConfig );/*输出引脚及输出功率设置*/
    SX1276Write( REG_PADAC, paDac );/*20dbm输出引脚设置*/
		
		tsbuff=0;
		tsbuff= SX1276Read(REG_PACONFIG);
}

/*选择sx1276芯片输出引脚：PA_BOOST或RFO */
uint8_t SX1276GetPaSelect( uint32_t channel )
{
	 return RF_PACONFIG_PASELECT_PABOOST;
//   if( channel > RF_MID_BAND_THRESH )// if( channel < RF_MID_BAND_THRESH )cf
//    {
//        return RF_PACONFIG_PASELECT_PABOOST;
//    }
//    else
//    {
//        return RF_PACONFIG_PASELECT_RFO;
//    }
}

/*设置SX1276工作频率*/
void SX1276SetChannel( uint32_t freq )
{
    LoraChannel = freq;
    freq = ( uint32_t )( ( double )freq / ( double )FREQ_STEP );
    SX1276Write( REG_FRFMSB, ( uint8_t )( ( freq >> 16 ) & 0xFF ) );
    SX1276Write( REG_FRFMID, ( uint8_t )( ( freq >> 8 ) & 0xFF ) );
    SX1276Write( REG_FRFLSB, ( uint8_t )( freq & 0xFF ) );
}
 
/************************************************
 为低频和高频带执行Rx链校准
 必须在重置后立即调用，以便所有寄存器均处于默认值
*************************************************/
static void RxChainCalibration( void )
{
    uint8_t regPaConfigInitVal;
    uint32_t initialFreq;

    // Save context
    regPaConfigInitVal = SX1276Read( REG_PACONFIG );
	  
	  //频率缺省值433mhz
    initialFreq = ( double )( ( ( uint32_t )SX1276Read( REG_FRFMSB ) << 16 ) |
                              ( ( uint32_t )SX1276Read( REG_FRFMID ) << 8 ) |
                              ( ( uint32_t )SX1276Read( REG_FRFLSB ) ) ) * ( double )FREQ_STEP;

    // Cut the PA just in case, RFO output, power = -1 dBm
    SX1276Write( REG_PACONFIG, 0x00 );

    // Launch Rx chain calibration for LF band
    SX1276Write( REG_IMAGECAL, ( SX1276Read( REG_IMAGECAL ) & RF_IMAGECAL_IMAGECAL_MASK ) | RF_IMAGECAL_IMAGECAL_START );
    while( ( SX1276Read( REG_IMAGECAL ) & RF_IMAGECAL_IMAGECAL_RUNNING ) == RF_IMAGECAL_IMAGECAL_RUNNING )
    {
    }

    // Sets a Frequency in HF band
    SX1276SetChannel( 868000000 );

    // Launch Rx chain calibration for HF band
    SX1276Write( REG_IMAGECAL, ( SX1276Read( REG_IMAGECAL ) & RF_IMAGECAL_IMAGECAL_MASK ) | RF_IMAGECAL_IMAGECAL_START );
    while( ( SX1276Read( REG_IMAGECAL ) & RF_IMAGECAL_IMAGECAL_RUNNING ) == RF_IMAGECAL_IMAGECAL_RUNNING )
    {
    }
		
    // Restore context
    SX1276Write( REG_PACONFIG, regPaConfigInitVal );
    SX1276SetChannel( initialFreq );
}

/************************************************
 接收配置
					uint8_t modem,  :LoRa或者FSK
					uint32_t bandwidth,   ：带宽
					uint32_t datarate,    :LORa扩频因子 
					                         FSK : 600..300000 bits/s
 *                                 LoRa: [6: 64, 7: 128, 8: 256, 9: 512,
 *                                         10: 1024, 11: 2048, 12: 4096  chips]
					 uint8_t coderate,      设置编码率 (只针对LoRa)
 *                                FSK : N/A ( set to 0 )
 *                                LoRa: [1: 4/5, 2: 4/6, 3: 4/7, 4: 4/8]
					 uint32_t bandwidthAfc, 设置 AFC Bandwidth (指针对FSK )
 *                                FSK : >= 2600 and <= 250000 Hz
 *                                LoRa: N/A ( set to 0 )
					 uint16_t preambleLen,  设置前导码
 *                                FSK : Number of bytes
 *                                LoRa: Length in symbols (硬件增加了4个字符)
					 uint16_t symbTimeout,  设置接收超时
 *                                FSK : timeout number of bytes
 *                                LoRa: timeout in symbols
					 bool fixLen,           是否为固定长度的包 [0: 可变的, 1: 固定的]
					 uint8_t payloadLen,    设置负载长度，当fixlen为1是需要设置
					 bool crcOn,            是否使能 CRC [0: OFF, 1: ON]
					 bool freqHopOn,        调频开关
 *                                FSK : N/A ( set to 0 )
 *                                LoRa: [0: OFF, 1: ON]
					 uint8_t hopPeriod,     每跳之间的符号数
 *                                FSK : N/A ( set to 0 )
 *                                LoRa: Number of symbols
					 bool iqInverted,       中断信号翻转(LoRa only)
 *                                FSK : N/A ( set to 0 )
 *                                LoRa: [0: not inverted, 1: inverted]
					 bool rxContinuous )    //在连续模式下接收信号 0:单次接收  1:连续接收
*************************************************/
void SX1276SetRxConfig(  uint32_t bandwidth,uint32_t datarate, uint8_t coderate,
                         uint32_t bandwidthAfc, uint16_t preambleLen,
                         uint16_t symbTimeout, bool fixLen,uint8_t payloadLen,
                         bool crcOn, bool freqHopOn, uint8_t hopPeriod,
                         bool iqInverted, bool rxContinuous )
{
//   SX1276SetLoraModem( );

		/* if( bandwidth > 2 ) */
		if( bandwidth > 3 )
		{
				// Fatal error: When using LoRa modem only bandwidths 125, 250 and 500 kHz are supported
				while( 1 );
		}
		/* bandwidth += 7; */
		if(bandwidth == 3)
		{
				bandwidth = 6;
		}
		else
		{
				bandwidth += 7;
		}

		LoRa.Bandwidth = bandwidth;
		LoRa.Datarate = datarate;
		LoRa.Coderate = coderate;
		LoRa.PreambleLen = preambleLen;
		LoRa.FixLen = fixLen;
		LoRa.PayloadLen = payloadLen;
		LoRa.CrcOn = crcOn;
		LoRa.FreqHopOn = freqHopOn;
		LoRa.HopPeriod = hopPeriod;
		LoRa.IqInverted = iqInverted;
		LoRa.RxContinuous = rxContinuous;

		if( datarate > 12 )
		{
				datarate = 12;
		}
		else if( datarate < 6 )
		{
				datarate = 6;
		}

		/* if( ( ( bandwidth == 7 ) && ( ( datarate == 11 ) || ( datarate == 12 ) ) ) || */
		/*     ( ( bandwidth == 8 ) && ( datarate == 12 ) ) ) */
		if( ( ( bandwidth == 7 || bandwidth == 6) && ( ( datarate == 11 ) || ( datarate == 12 ) ) ) ||
				( ( bandwidth == 8 ) && ( datarate == 12 ) ) )
		{
				LoRa.LowDatarateOptimize = 0x01;
		}
		else
		{
				LoRa.LowDatarateOptimize = 0x00;
		}

		SX1276Write( REG_LR_MODEMCONFIG1,
								 ( SX1276Read( REG_LR_MODEMCONFIG1 ) &
									 RFLR_MODEMCONFIG1_BW_MASK &
									 RFLR_MODEMCONFIG1_CODINGRATE_MASK &
									 RFLR_MODEMCONFIG1_IMPLICITHEADER_MASK ) |
									 ( bandwidth << 4 ) | ( coderate << 1 ) |
									 fixLen );

		SX1276Write( REG_LR_MODEMCONFIG2,
								 ( SX1276Read( REG_LR_MODEMCONFIG2 ) &
									 RFLR_MODEMCONFIG2_SF_MASK &
									 RFLR_MODEMCONFIG2_RXPAYLOADCRC_MASK &
									 RFLR_MODEMCONFIG2_SYMBTIMEOUTMSB_MASK ) |
									 ( datarate << 4 ) | ( crcOn << 2 ) |
									 ( ( symbTimeout >> 8 ) & ~RFLR_MODEMCONFIG2_SYMBTIMEOUTMSB_MASK ) );

		SX1276Write( REG_LR_MODEMCONFIG3,
								 ( SX1276Read( REG_LR_MODEMCONFIG3 ) &
									 RFLR_MODEMCONFIG3_LOWDATARATEOPTIMIZE_MASK ) |
									 ( LoRa.LowDatarateOptimize << 3 ) );

		SX1276Write( REG_LR_SYMBTIMEOUTLSB, ( uint8_t )( symbTimeout & 0xFF ) );

		SX1276Write( REG_LR_PREAMBLEMSB, ( uint8_t )( ( preambleLen >> 8 ) & 0xFF ) );
		SX1276Write( REG_LR_PREAMBLELSB, ( uint8_t )( preambleLen & 0xFF ) );

		if( fixLen == 1 )
		{
				SX1276Write( REG_LR_PAYLOADLENGTH, payloadLen );
		}

		if( LoRa.FreqHopOn == true )
		{
				SX1276Write( REG_LR_PLLHOP, ( SX1276Read( REG_LR_PLLHOP ) & RFLR_PLLHOP_FASTHOP_MASK ) | RFLR_PLLHOP_FASTHOP_ON );
				SX1276Write( REG_LR_HOPPERIOD, LoRa.HopPeriod );
		}

		if( ( bandwidth == 9 ) && ( LoraChannel > RF_MID_BAND_THRESH ) )
		{
				// ERRATA 2.1 - Sensitivity Optimization with a 500 kHz Bandwidth
				SX1276Write( REG_LR_TEST36, 0x02 );
				SX1276Write( REG_LR_TEST3A, 0x64 );
		}
		else if( bandwidth == 9 )
		{
				// ERRATA 2.1 - Sensitivity Optimization with a 500 kHz Bandwidth
				SX1276Write( REG_LR_TEST36, 0x02 );
				SX1276Write( REG_LR_TEST3A, 0x7F );
		}
		else
		{
				// ERRATA 2.1 - Sensitivity Optimization with a 500 kHz Bandwidth
				SX1276Write( REG_LR_TEST36, 0x03 );
		}

		if( datarate == 6 )
		{
				SX1276Write( REG_LR_DETECTOPTIMIZE,
										 ( SX1276Read( REG_LR_DETECTOPTIMIZE ) &
											 RFLR_DETECTIONOPTIMIZE_MASK ) |
											 RFLR_DETECTIONOPTIMIZE_SF6 );
				SX1276Write( REG_LR_DETECTIONTHRESHOLD,
										 RFLR_DETECTIONTHRESH_SF6 );
		}
		else
		{
				SX1276Write( REG_LR_DETECTOPTIMIZE,
										 ( SX1276Read( REG_LR_DETECTOPTIMIZE ) &
										 RFLR_DETECTIONOPTIMIZE_MASK ) |
										 RFLR_DETECTIONOPTIMIZE_SF7_TO_SF12 );
				SX1276Write( REG_LR_DETECTIONTHRESHOLD,
										 RFLR_DETECTIONTHRESH_SF7_TO_SF12 );
		}

}

/************************************************
 SX1276发送设置 
					int8_t power,         设置发送功率  dbm
					uint32_t bandwidth,    设置带宽
					uint32_t datarate,      LORa扩频因子 
					                        FSK : 600..300000 bits/s
 *                                LoRa: [6: 64, 7: 128, 8: 256, 9: 512,
 *                                         10: 1024, 11: 2048, 12: 4096  chips]
					uint8_t coderate,       设置编码率 (只针对LoRa)
 *                                FSK : N/A ( set to 0 )
 *                                LoRa: [1: 4/5, 2: 4/6, 3: 4/7, 4: 4/8]
					uint16_t preambleLen,   设置前导码
 *                                FSK : Number of bytes
 *                                LoRa: Length in symbols (硬件增加了4个字符)
					bool fixLen,            是否为固定长度的包 [0: 可变的, 1: 固定的]
					bool crcOn,             是否使能 CRC [0: OFF, 1: ON]
					bool freqHopOn,         调频开关
 *                                FSK : N/A ( set to 0 )
 *                                LoRa: [0: OFF, 1: ON]
					uint8_t hopPeriod,      每跳之间的符号数
 *                                FSK : N/A ( set to 0 )
 *                                LoRa: Number of symbols
					 bool iqInverted,        LoRa: Number of symbols
					 bool iqInverted,       中断信号翻转(LoRa only)
 *                                FSK : N/A ( set to 0 )
 *                                LoRa: [0: not inverted, 1: inverted]
					uint32_t timeout        发送超时时间

*************************************************/
void SX1276SetTxConfig( int8_t power,uint32_t bandwidth, uint32_t datarate, uint8_t coderate, 
												uint16_t preambleLen,bool fixLen, bool crcOn, bool freqHopOn,
                        uint8_t hopPeriod, bool iqInverted, uint32_t timeout )
{
//    SX1276SetLoraModem( );
    SX1276SetRfTxPower( power );

		LoRa.Power = power;
		/* if( bandwidth > 2 ) */
		if( bandwidth > 3 )
		{
				// Fatal error: When using LoRa modem only bandwidths 125, 250 and 500 kHz are supported
				while( 1 );
		}
		/* bandwidth += 7; */
		if(bandwidth == 3)
		{
				bandwidth = 6;
		}
		else
		{
				bandwidth += 7;
		}

			LoRa.Bandwidth = bandwidth;
			LoRa.Datarate = datarate;
			LoRa.Coderate = coderate;
			LoRa.PreambleLen = preambleLen;
			LoRa.FixLen = fixLen;
			LoRa.FreqHopOn = freqHopOn;
			LoRa.HopPeriod = hopPeriod;
			LoRa.CrcOn = crcOn;
			LoRa.IqInverted = iqInverted;
			LoRa.TxTimeout = timeout;

		if( datarate > 12 )
		{
				datarate = 12;
		}
		else if( datarate < 6 )
		{
				datarate = 6;
		}
		/* if( ( ( bandwidth == 7 ) && ( ( datarate == 11 ) || ( datarate == 12 ) ) ) || */
		/*     ( ( bandwidth == 8 ) && ( datarate == 12 ) ) ) */
		if( ( ( bandwidth == 7 || bandwidth == 6) && ( ( datarate == 11 ) || ( datarate == 12 ) ) ) ||
				( ( bandwidth == 8 ) && ( datarate == 12 ) ) )
		{
				LoRa.LowDatarateOptimize = 0x01;
		}
		else
		{
				LoRa.LowDatarateOptimize = 0x00;
		}

		if( LoRa.FreqHopOn == true )
		{
				SX1276Write( REG_LR_PLLHOP, ( SX1276Read( REG_LR_PLLHOP ) & RFLR_PLLHOP_FASTHOP_MASK ) | RFLR_PLLHOP_FASTHOP_ON );
				SX1276Write( REG_LR_HOPPERIOD, LoRa.HopPeriod );
		}

		SX1276Write( REG_LR_MODEMCONFIG1,
								 ( SX1276Read( REG_LR_MODEMCONFIG1 ) &
									 RFLR_MODEMCONFIG1_BW_MASK &
									 RFLR_MODEMCONFIG1_CODINGRATE_MASK &
									 RFLR_MODEMCONFIG1_IMPLICITHEADER_MASK ) |
									 ( bandwidth << 4 ) | ( coderate << 1 ) |
									 fixLen );

		SX1276Write( REG_LR_MODEMCONFIG2,
								 ( SX1276Read( REG_LR_MODEMCONFIG2 ) &
									 RFLR_MODEMCONFIG2_SF_MASK &
									 RFLR_MODEMCONFIG2_RXPAYLOADCRC_MASK ) |
									 ( datarate << 4 ) | ( crcOn << 2 ) );

		SX1276Write( REG_LR_MODEMCONFIG3,
								 ( SX1276Read( REG_LR_MODEMCONFIG3 ) &
									 RFLR_MODEMCONFIG3_LOWDATARATEOPTIMIZE_MASK ) |
									 ( LoRa.LowDatarateOptimize << 3 ) );

		SX1276Write( REG_LR_PREAMBLEMSB, ( preambleLen >> 8 ) & 0x00FF );
		SX1276Write( REG_LR_PREAMBLELSB, preambleLen & 0xFF );

		if( datarate == 6 )
		{/*扩频因子6：报头应为隐式，reg0x31[2:0]写0b101，reg0x37写0x0c*/
				SX1276Write( REG_LR_DETECTOPTIMIZE,
										 ( SX1276Read( REG_LR_DETECTOPTIMIZE ) &
											 RFLR_DETECTIONOPTIMIZE_MASK ) |
											 RFLR_DETECTIONOPTIMIZE_SF6 );
				SX1276Write( REG_LR_DETECTIONTHRESHOLD,
										 RFLR_DETECTIONTHRESH_SF6 );
		}
		else
		{
				SX1276Write( REG_LR_DETECTOPTIMIZE,
										 ( SX1276Read( REG_LR_DETECTOPTIMIZE ) &
										 RFLR_DETECTIONOPTIMIZE_MASK ) |
										 RFLR_DETECTIONOPTIMIZE_SF7_TO_SF12 );
				SX1276Write( REG_LR_DETECTIONTHRESHOLD,
										 RFLR_DETECTIONTHRESH_SF7_TO_SF12 );
		}
    
}

/************************************************
 数据发送  *buffer, 发送缓冲区  size 数据长度
*************************************************/
void SX1276Send( uint8_t *buffer, uint8_t size )
{

		if( LoRa.IqInverted == true )
		{
				SX1276Write( REG_LR_INVERTIQ, ( ( SX1276Read( REG_LR_INVERTIQ ) & RFLR_INVERTIQ_TX_MASK & RFLR_INVERTIQ_RX_MASK ) | RFLR_INVERTIQ_RX_OFF | RFLR_INVERTIQ_TX_ON ) );
				SX1276Write( REG_LR_INVERTIQ2, RFLR_INVERTIQ2_ON );
		}
		else
		{
				SX1276Write( REG_LR_INVERTIQ, ( ( SX1276Read( REG_LR_INVERTIQ ) & RFLR_INVERTIQ_TX_MASK & RFLR_INVERTIQ_RX_MASK ) | RFLR_INVERTIQ_RX_OFF | RFLR_INVERTIQ_TX_OFF ) );
				SX1276Write( REG_LR_INVERTIQ2, RFLR_INVERTIQ2_OFF );
		}

		LoraPacketSize = size;

		// Initializes the payload size
		SX1276Write( REG_LR_PAYLOADLENGTH, size );

		// Full buffer used for Tx
		SX1276Write( REG_LR_FIFOTXBASEADDR, 0 );
		SX1276Write( REG_LR_FIFOADDRPTR, 0 );

		// FIFO operations can not take place in Sleep mode
		if( ( SX1276Read( REG_OPMODE ) & ~RF_OPMODE_MASK ) == RF_OPMODE_SLEEP )
		{
				SX1276SetStby( );
				delay( 1 );
		}
		// Write payload buffer
//            SX1276WriteFifo( buffer, size ); //dma
		DmaState = RF_TX_RUNNING;
		dma_write_start_lora(buffer,size);

//            txTimeout = SX1276.Settings.LoRa.TxTimeout;// dma

//    SX1276SetTx( txTimeout ); // dma
}


/*设置SX1276休眠*/
void SX1276SetSleep( void )
{
//    Timer.stop(RX_TIMEOUT_TIMER);
//    Timer.stop(TX_TIMEOUT_TIMER);
    SX1276SetOpMode( RF_OPMODE_SLEEP );
    LoraRadioState = RF_IDLE;//SX1276.Settings.State = RF_IDLE;
}

/* 设置SX1276为待机模式*/
void SX1276SetStby( void )
{
//    Timer.stop(RX_TIMEOUT_TIMER);
//    Timer.stop(TX_TIMEOUT_TIMER);
    SX1276SetOpMode( RF_OPMODE_STANDBY );
    LoraRadioState = RF_IDLE;
}
/************************************************
 设置SX1276为接收模式
 uint32_t timeout 接收超时时间 0:为持续接收  
*************************************************/
void SX1276SetRx( uint32_t timeout )
{
    bool rxContinuous = false;

		if( LoRa.IqInverted == true )
		{
				SX1276Write( REG_LR_INVERTIQ, ( ( SX1276Read( REG_LR_INVERTIQ ) & RFLR_INVERTIQ_TX_MASK & RFLR_INVERTIQ_RX_MASK ) | RFLR_INVERTIQ_RX_ON | RFLR_INVERTIQ_TX_OFF ) );
				SX1276Write( REG_LR_INVERTIQ2, RFLR_INVERTIQ2_ON );
		}
		else
		{
				SX1276Write( REG_LR_INVERTIQ, ( ( SX1276Read( REG_LR_INVERTIQ ) & RFLR_INVERTIQ_TX_MASK & RFLR_INVERTIQ_RX_MASK ) | RFLR_INVERTIQ_RX_OFF | RFLR_INVERTIQ_TX_OFF ) );
				SX1276Write( REG_LR_INVERTIQ2, RFLR_INVERTIQ2_OFF );
		}

		// ERRATA 2.3 - Receiver Spurious Reception of a LoRa Signal
		if( LoRa.Bandwidth < 9 )
		{
				SX1276Write( REG_LR_DETECTOPTIMIZE, SX1276Read( REG_LR_DETECTOPTIMIZE ) & 0x7F );
				SX1276Write( REG_LR_TEST30, 0x00 );
				switch( LoRa.Bandwidth )
				{
				case 0: // 7.8 kHz
						SX1276Write( REG_LR_TEST2F, 0x48 );
						SX1276SetChannel(LoraChannel + 7810 );
						break;
				case 1: // 10.4 kHz
						SX1276Write( REG_LR_TEST2F, 0x44 );
						SX1276SetChannel(LoraChannel + 10420 );
						break;
				case 2: // 15.6 kHz
						SX1276Write( REG_LR_TEST2F, 0x44 );
						SX1276SetChannel(LoraChannel + 15620 );
						break;
				case 3: // 20.8 kHz
						SX1276Write( REG_LR_TEST2F, 0x44 );
						SX1276SetChannel(LoraChannel + 20830 );
						break;
				case 4: // 31.2 kHz
						SX1276Write( REG_LR_TEST2F, 0x44 );
						SX1276SetChannel(LoraChannel + 31250 );
						break;
				case 5: // 41.4 kHz
						SX1276Write( REG_LR_TEST2F, 0x44 );
						SX1276SetChannel(LoraChannel + 41670 );
						break;
				case 6: // 62.5 kHz
						SX1276Write( REG_LR_TEST2F, 0x40 );
						break;
				case 7: // 125 kHz
						SX1276Write( REG_LR_TEST2F, 0x40 );
						break;
				case 8: // 250 kHz
						SX1276Write( REG_LR_TEST2F, 0x40 );
						break;
				}
		}
		else
		{
				SX1276Write( REG_LR_DETECTOPTIMIZE, SX1276Read( REG_LR_DETECTOPTIMIZE ) | 0x80 );
		}

		rxContinuous = LoRa.RxContinuous;

		if( LoRa.FreqHopOn == true )
		{
				SX1276Write( REG_LR_IRQFLAGSMASK, //RFLR_IRQFLAGS_RXTIMEOUT |
																					//RFLR_IRQFLAGS_RXDONE |
																					//RFLR_IRQFLAGS_PAYLOADCRCERROR |
																					RFLR_IRQFLAGS_VALIDHEADER |
																					RFLR_IRQFLAGS_TXDONE |
																					RFLR_IRQFLAGS_CADDONE |
																					//RFLR_IRQFLAGS_FHSSCHANGEDCHANNEL |
																					RFLR_IRQFLAGS_CADDETECTED );

				// DIO0=RxDone, DIO2=FhssChangeChannel
				SX1276Write( REG_DIOMAPPING1, ( SX1276Read( REG_DIOMAPPING1 ) & RFLR_DIOMAPPING1_DIO0_MASK & RFLR_DIOMAPPING1_DIO2_MASK  ) | RFLR_DIOMAPPING1_DIO0_00 | RFLR_DIOMAPPING1_DIO2_00 );
		}
		else
		{
				SX1276Write( REG_LR_IRQFLAGSMASK, //RFLR_IRQFLAGS_RXTIMEOUT |
																					//RFLR_IRQFLAGS_RXDONE |
																				 // RFLR_IRQFLAGS_PAYLOADCRCERROR |
																					RFLR_IRQFLAGS_VALIDHEADER |
																					RFLR_IRQFLAGS_TXDONE |
																					RFLR_IRQFLAGS_CADDONE |
																					RFLR_IRQFLAGS_FHSSCHANGEDCHANNEL |
																					RFLR_IRQFLAGS_CADDETECTED );

				// DIO0=RxDone
				SX1276Write( REG_DIOMAPPING1, ( SX1276Read( REG_DIOMAPPING1 ) & RFLR_DIOMAPPING1_DIO0_MASK ) | RFLR_DIOMAPPING1_DIO0_00 );
		}
		SX1276Write( REG_LR_FIFORXBASEADDR, 0 );
		SX1276Write( REG_LR_FIFOADDRPTR, 0 );
		SX1276Write( 0x12, 0xff );//清除所有中断标志

    //memset( RxTxBuffer, 0, ( size_t )RX_BUFFER_SIZE );
		FillRAM(RxTxBuffer,RX_BUFFER_SIZE,0);

    LoraRadioState = RF_RX_RUNNING;
    if( timeout != 0 )
    {
        /* TimerSetValue( &RxTimeoutTimer, timeout ); */
        /* TimerStart( &RxTimeoutTimer ); */
    }

		if( rxContinuous == true )
		{
				SX1276SetOpMode( RFLR_OPMODE_RECEIVER );
		}
		else
		{
				SX1276SetOpMode( RFLR_OPMODE_RECEIVER_SINGLE );
		}
    
}

/************************************************
设置SX1276为发送模式 timeout发送超时时间  
*************************************************/
void SX1276SetTx( uint32_t timeout )
{
		if( LoRa.FreqHopOn == true )
		{
				SX1276Write( REG_LR_IRQFLAGSMASK, RFLR_IRQFLAGS_RXTIMEOUT |
																					RFLR_IRQFLAGS_RXDONE |
																					RFLR_IRQFLAGS_PAYLOADCRCERROR |
																					RFLR_IRQFLAGS_VALIDHEADER |
																					//RFLR_IRQFLAGS_TXDONE |
																					RFLR_IRQFLAGS_CADDONE |
																					//RFLR_IRQFLAGS_FHSSCHANGEDCHANNEL |
																					RFLR_IRQFLAGS_CADDETECTED );

				// DIO0=TxDone, DIO2=FhssChangeChannel
				SX1276Write( REG_DIOMAPPING1, ( SX1276Read( REG_DIOMAPPING1 ) & RFLR_DIOMAPPING1_DIO0_MASK & RFLR_DIOMAPPING1_DIO2_MASK ) | RFLR_DIOMAPPING1_DIO0_01 | RFLR_DIOMAPPING1_DIO2_00 );
		}
		else
		{
				SX1276Write( REG_LR_IRQFLAGSMASK, RFLR_IRQFLAGS_RXTIMEOUT |
																					RFLR_IRQFLAGS_RXDONE |
																					RFLR_IRQFLAGS_PAYLOADCRCERROR |
																					RFLR_IRQFLAGS_VALIDHEADER |
																					//RFLR_IRQFLAGS_TXDONE |
																					RFLR_IRQFLAGS_CADDONE |
																					RFLR_IRQFLAGS_FHSSCHANGEDCHANNEL |
																					RFLR_IRQFLAGS_CADDETECTED );

				// DIO0=TxDone
				SX1276Write( REG_DIOMAPPING1, ( SX1276Read( REG_DIOMAPPING1 ) & RFLR_DIOMAPPING1_DIO0_MASK ) | RFLR_DIOMAPPING1_DIO0_01 );
		}
		DmaState = RF_IDLE;	//DMA

    LoraRadioState = RF_TX_RUNNING;
    /* TimerStart( &TxTimeoutTimer ); */
    SX1276SetOpMode( RF_OPMODE_TRANSMITTER );
}

/************************************************
SX1276器件模式切换
操作REG_OPMODE的低3位以设置器件模式 
*************************************************/
void SX1276SetOpMode( uint8_t opMode )
{
   SX1276Write( REG_OPMODE, ( SX1276Read( REG_OPMODE ) & RF_OPMODE_MASK ) | opMode );
}

/************************************************
设置sx1276工作模式 LoRa/FSK, 该工程只用lora
*************************************************/
void SX1276SetLoraModem( void )
{

		SX1276SetSleep( );
		SX1276Write( REG_OPMODE, ( SX1276Read( REG_OPMODE ) & RFLR_OPMODE_LONGRANGEMODE_MASK ) | RFLR_OPMODE_LONGRANGEMODE_ON );/*设置REG_OPMODE最高位为1即lora模式*/

		SX1276Write( REG_DIOMAPPING1, 0x00 );//0x00-DIO0为RxDone
		SX1276Write( REG_DIOMAPPING2, 0x00 );

}

/*
 * DXDONE-接收到数据的处理
 * TXDONE-发送完成处理
 */
void SX1276OnDio0Event(void)
{
    volatile uint8_t irqFlags = 0;

    switch( LoraRadioState )
    {
        case RF_RX_RUNNING:
			//TimerStop( &RxTimeoutTimer );
			// RxDone interrupt
		      {
							int8_t snr = 0;
							int16_t rssi = 0;

						 if(LoraMasSla.LoraMS==Master)
						 {
		           TIM4->CR1 &=~0x1;//关计数器
							 LoraMasSla.MasterTimeOut=0;
						 }
						
							GPIOE->ODR ^=(1<<4);//用于调试，接收数据指示灯
							// Clear Irq
							SX1276Write( REG_LR_IRQFLAGS, RFLR_IRQFLAGS_RXDONE );
							irqFlags = SX1276Read( REG_LR_IRQFLAGS );
							if( ( irqFlags & RFLR_IRQFLAGS_PAYLOADCRCERROR_MASK ) == RFLR_IRQFLAGS_PAYLOADCRCERROR )//判断负载CRC是否错误，错误-丢弃该次数据
							{
								
									// Clear Irq
									SX1276Write( REG_LR_IRQFLAGS, RFLR_IRQFLAGS_PAYLOADCRCERROR );

									if( LoRa.RxContinuous == false )
									{
											LoraRadioState = RF_IDLE;
									}
									/* TimerStop( &RxTimeoutTimer ); */
									OnRxError();
									break;
							}

							LoraPacketSnrValue = SX1276Read( REG_LR_PKTSNRVALUE );//数据包信噪比估值
							if( LoraPacketSnrValue & 0x80 ) // The SNR sign bit is 1
							{
									// Invert and divide by 4
									snr = ( ( ~LoraPacketSnrValue + 1 ) & 0xFF ) >> 2;
									snr = -snr;
							}
							else
							{
									// Divide by 4
									snr = ( LoraPacketSnrValue & 0xFF ) >> 2;
							}

							rssi = SX1276Read( REG_LR_PKTRSSIVALUE );//数据包rssi即接收信号强度指示
							if( snr < 0 )
							{
									if( LoraChannel > RF_MID_BAND_THRESH )
									{
											LoraPacketRssiValue = RSSI_OFFSET_HF + rssi + ( rssi >> 4 ) +
																																		snr;
									}
									else
									{
											LoraPacketRssiValue = RSSI_OFFSET_LF + rssi + ( rssi >> 4 ) +
																																		snr;
									}
							}
							else
							{
									if( LoraChannel > RF_MID_BAND_THRESH )
									{
											LoraPacketRssiValue = RSSI_OFFSET_HF + rssi + ( rssi >> 4 );
									}
									else
									{
											LoraPacketRssiValue = RSSI_OFFSET_LF + rssi + ( rssi >> 4 );
									}
							}

							LoraPacketSize = SX1276Read( REG_LR_RXNBBYTES );//接收到的数据包长度
							SX1276Write( REG_LR_FIFOADDRPTR, SX1276Read( REG_LR_FIFORXCURRENTADDR ) );
//                    SX1276ReadFifo( RxTxBuffer, SX1276.Settings.LoRaPacketHandler.Size );//dma
							DmaState = RF_RX_RUNNING;
							dma_read_start_lora(RxTxBuffer, LoraPacketSize);
						}
            break;
        case RF_TX_RUNNING:
            /* TimerStop( &TxTimeoutTimer ); */
            // TxDone interrupt
                // Clear Irq		
                SX1276Write( REG_LR_IRQFLAGS, RFLR_IRQFLAGS_TXDONE );
                // Intentional fall through
				        LoraRadioState = RF_IDLE;
                OnTxDone( );
            
            break;
        default:
            break;
    }


}
/************************************************
 读取当前信道是否忙
  modem：当前工作模式 uint32_t freq:待检测信道 int16_t rssiThresh：指定的信号值 uint32_t maxCarrierSenseTime:最大扫描时间
返 回 值 ： 1:不忙  0:忙
*************************************************/
bool SX1276IsChannelFree(uint32_t freq, int16_t rssiThresh, uint32_t maxCarrierSenseTime )
{
    bool status = true;
//    int16_t rssi = 0;
//    uint32_t carrierSenseTime = 0;

    SX1276SetLoraModem( );

    SX1276SetChannel( freq );

    SX1276SetOpMode( RF_OPMODE_RECEIVER );

    delay( 1 );

//    carrierSenseTime = millis();

    // Perform carrier sense for maxCarrierSenseTime
//    while((millis() - carrierSenseTime) < maxCarrierSenseTime)
//    {
//        rssi = SX1276ReadRssi( modem );

//        if( rssi > rssiThresh )
//        {
//            status = false;
//            break;
//        }
//    }
    SX1276SetSleep( );
    return status;
}

/* 用来生成随机数*/
uint32_t SX1276Random( void )
{
    uint8_t i;
    uint32_t rnd = 0;

    /*
     * Radio setup for random number generation
     */
    // Set LoRa modem ON
    SX1276SetLoraModem( );

    // Disable LoRa modem interrupts
    SX1276Write( REG_LR_IRQFLAGSMASK, RFLR_IRQFLAGS_RXTIMEOUT |
                  RFLR_IRQFLAGS_RXDONE |
                  RFLR_IRQFLAGS_PAYLOADCRCERROR |
                  RFLR_IRQFLAGS_VALIDHEADER |
                  RFLR_IRQFLAGS_TXDONE |
                  RFLR_IRQFLAGS_CADDONE |
                  RFLR_IRQFLAGS_FHSSCHANGEDCHANNEL |
                  RFLR_IRQFLAGS_CADDETECTED );

    // Set radio in continuous reception
    SX1276SetOpMode( RF_OPMODE_RECEIVER );

    for( i = 0; i < 32; i++ )
    {
        delay( 1 );
        // Unfiltered RSSI value reading. Only takes the LSB value
        rnd |= ( ( uint32_t )SX1276Read( REG_LR_RSSIWIDEBAND ) & 0x01 ) << i;
    }

    SX1276SetSleep( );

    return rnd;
}
/************************************************
 获取空中时间
 uint8_t modem, LoRa模式
 uint8_t pktLen    负载长度
 时间单位ms
*************************************************/
uint32_t SX1276GetTimeOnAir( uint8_t modem, uint8_t pktLen )
{
    uint32_t airTime = 0;

			double bw = 0.0;
			// REMARK: When using LoRa modem only bandwidths 125, 250 and 500 kHz are supported
			switch( LoRa.Bandwidth )
			{
			//case 0: // 7.8 kHz
			//    bw = 7800;
			//    break;
			//case 1: // 10.4 kHz
			//    bw = 10400;
			//    break;
			//case 2: // 15.6 kHz
			//    bw = 15600;
			//    break;
			//case 3: // 20.8 kHz
			//    bw = 20800;
			//    break;
			//case 4: // 31.2 kHz
			//    bw = 31200;
			//    break;
			//case 5: // 41.4 kHz
			//    bw = 41400;
			//    break;
			//case 6: // 62.5 kHz
			//    bw = 62500;
			//    break;
			case 7: // 125 kHz
					bw = 125000;
					break;
			case 8: // 250 kHz
					bw = 250000;
					break;
			case 9: // 500 kHz
					bw = 500000;
					break;
			}

//            // Symbol rate : time for one symbol (secs)
//            double rs = bw / ( 1 << SX1276.Settings.LoRa.Datarate );
//            double ts = 1 / rs;
//            // time of preamble
//            double tPreamble = ( SX1276.Settings.LoRa.PreambleLen + 4.25 ) * ts;
//            // Symbol length of payload and time
//            double tmp = ceil( ( 8 * pktLen - 4 * SX1276.Settings.LoRa.Datarate +
//                                 28 + 16 * SX1276.Settings.LoRa.CrcOn -
//                                 ( SX1276.Settings.LoRa.FixLen ? 20 : 0 ) ) /
//                                 ( double )( 4 * ( SX1276.Settings.LoRa.Datarate -
//                                 ( ( SX1276.Settings.LoRa.LowDatarateOptimize > 0 ) ? 2 : 0 ) ) ) ) *
//                                 ( SX1276.Settings.LoRa.Coderate + 4 );
//            double nPayload = 8 + ( ( tmp > 0 ) ? tmp : 0 );
//            double tPayload = nPayload * ts;
//            // Time on air
//            double tOnAir = tPreamble + tPayload;
//            // return ms secs
//            airTime = floor( tOnAir * 1000 + 0.999 );
    
    return airTime;
}

/*启动CAD模式*/
void SX1276StartCad( void )
{
		SX1276Write( REG_LR_IRQFLAGSMASK, RFLR_IRQFLAGS_RXTIMEOUT |
																RFLR_IRQFLAGS_RXDONE |
																RFLR_IRQFLAGS_PAYLOADCRCERROR |
																RFLR_IRQFLAGS_VALIDHEADER |
																RFLR_IRQFLAGS_TXDONE |
																//RFLR_IRQFLAGS_CADDONE |
																RFLR_IRQFLAGS_FHSSCHANGEDCHANNEL // |
																//RFLR_IRQFLAGS_CADDETECTED
																);
		// DIO3=CADDone
		SX1276Write( REG_DIOMAPPING1, ( SX1276Read( REG_DIOMAPPING1 ) & RFLR_DIOMAPPING1_DIO3_MASK ) | RFLR_DIOMAPPING1_DIO3_00 );

		LoraRadioState = RF_CAD;
		SX1276SetOpMode( RFLR_OPMODE_CAD );
    
}


/* 读取当前模式下的信号强度,lora模式*/
int16_t SX1276ReadRssi( void )
{
    int16_t rssi = 0;
		if( LoraChannel > RF_MID_BAND_THRESH )
		{
				rssi = RSSI_OFFSET_HF + SX1276Read( REG_LR_RSSIVALUE );
		}
		else
		{
				rssi = RSSI_OFFSET_LF + SX1276Read( REG_LR_RSSIVALUE );
		}
    return rssi;
}

/************************************************
 设置SX1276最大负载长度
  modem 工作模式
  uint8_t max 最大数据长度
*************************************************/
void SX1276SetMaxPayloadLength(uint8_t max )
{
    SX1276SetLoraModem( );
    SX1276Write( REG_LR_PAYLOADMAXLENGTH, max );
}

/************************************************
 设置SX1276  SYNC_WORD 同步字
 enable 公有还是私有
*************************************************/
void SX1276SetPublicNetwork( bool enable )
{
   SX1276SetLoraModem( );
    LoRa.PublicNetwork = enable;
    if( enable == true )
    {
        // Change LoRa modem SyncWord
        SX1276Write( REG_LR_SYNCWORD, LORA_MAC_PUBLIC_SYNCWORD );
    }
    else
    {
        // Change LoRa modem SyncWord
        SX1276Write( REG_LR_SYNCWORD, LORA_MAC_PRIVATE_SYNCWORD );
    }
}

///超时或者IO口事件处理
void SX1276OnTimeoutEvent( void )
{
 switch( LoraRadioState )
    {
    case RF_RX_RUNNING:
				OnRxTimeout();
        break;
    case RF_TX_RUNNING:
        // Tx timeout shouldn't happen.
        // But it has been observed that when it happens it is a result of a corrupted SPI transfer
        // it depends on the platform design.
        //
        // The workaround is to put the radio in a known state. Thus, we re-initialize it.

        // BEGIN WORKAROUND
       
        // Reset the radio
        SX1276SetReset();

        // Calibrate Rx chain
        RxChainCalibration( );

        // Initialize radio default values
        SX1276SetOpMode( RF_OPMODE_SLEEP );

				SX1276SetLoraModem( );
				SX1276Write(REG_LR_PAYLOADMAXLENGTH,0x40);//报头负载最大值，如果超过该值报CRC错误，缺省值0xff

        // Restore previous network type setting.
        SX1276SetPublicNetwork( LoRa.PublicNetwork );
        // END WORKAROUND

        LoraRadioState = RF_IDLE;
				OnTxTimeout();
        break;
    default:
        break;
    }
}

uint32_t SX1276LoRaGetRFFrequency( void )
{
    uint32_t RFFrequency;
    uint8_t freq[3];
    SX1276ReadBuffer( REG_LR_FRFMSB, freq, 3 );
    RFFrequency = ( ( uint32_t )freq[0] << 16 ) | ( ( uint32_t )freq[1] << 8 ) | ( ( uint32_t )freq[2]  );
    RFFrequency = ( uint32_t )( ( double )RFFrequency * ( double )FREQ_STEP );

    return RFFrequency;
}

uint32_t SX1276LoRaGetErrorRFFrequency( void )
{
    uint32_t errorFreq;
    uint8_t freq[3];
    SX1276ReadBuffer( REG_LR_FEIMSB, freq, 3 );
    errorFreq = ( ( uint32_t )freq[0] << 16 ) | ( ( uint32_t )freq[1] << 8 ) | ( ( uint32_t )freq[2]  );
    return errorFreq;
}

/*通过dma-spi3从lora数据接收缓存区fifo读取接收到的数据*/
void dma_read_start_lora(uint8_t *buffer, uint8_t size)
{

//	memset((Dmarbuff), 0, BUFFER_SIZE);
//	memset((Dmawbuff), 0, BUFFER_SIZE);
	FillRAM(Dmarbuff,0,BUFFER_SIZE);
	FillRAM(Dmawbuff,0,BUFFER_SIZE);

 	Dmawbuff[0] = 0x0;//0x00&0x7f fifoaddr--0x00 readfifo-- &0x7f

	SPI3->CR2 |= 0x0002;//启动发送缓冲区DMA。
	SPI3->CR2 |= 0x0001;//启动接收缓冲区DMA。
	
  //spi3->rx---DMA2_Channel1，读：spi到buff
	DMA2_Channel1->CCR = 0x2082;
	DMA2_Channel1->CNDTR = size+1;
	DMA2_Channel1->CPAR = (uint32_t)(&(SPI3->DR));//外设地址，ccr的dir为0--从外设读---外设地址为源地址
	DMA2_Channel1->CMAR = (uint32_t) (buffer);//存储器地址--目标地址
	
  //spi3->tx---DMA2_Channel2 写：buff到spi
	DMA2_Channel2->CCR = 0x2090;
	DMA2_Channel2->CNDTR = size+1;//传输数量寄存器
	DMA2_Channel2->CPAR = (uint32_t)(&(SPI3->DR));//外设地址-目标地址
	DMA2_Channel2->CMAR = (uint32_t) (Dmawbuff);//存储器地址--源地址

	SpiWriteNss(0);//PA15 CS 低

	DMA2_Channel1->CCR |= 0x0001;//启动
	DMA2_Channel2->CCR |= 0x0001;	
	
}
/*通过dma-spi3将数据写入lora缓存区fifo*/
void dma_write_start_lora(uint8_t *buffer, uint8_t size)
{
	unsigned short i;		  
//	memset(Dmarbuff, 0, BUFFER_SIZE);
//	memset(Dmawbuff, 0, BUFFER_SIZE);
  FillRAM(Dmarbuff,BUFFER_SIZE,0);
	FillRAM(Dmawbuff,BUFFER_SIZE,0);
	for(i=0;i<size;i++)
	  Dmawbuff[i+1] = buffer[i];

	Dmawbuff[0] = 0x80;//0x00|0x80 fifoaddr--0x00 writefifo--0x80

	SPI3->CR2 |= 0x0002;
	SPI3->CR2 |= 0x0001;	

	DMA2_Channel1->CCR = 0x2082;
	DMA2_Channel1->CNDTR = size+1;
	DMA2_Channel1->CPAR = (uint32_t)(&(SPI3->DR));
	DMA2_Channel1->CMAR = (uint32_t) Dmarbuff;

	DMA2_Channel2->CCR = 0x2090;
	DMA2_Channel2->CNDTR = size+1;//传输数量寄存器
	DMA2_Channel2->CPAR = (uint32_t)(&(SPI3->DR));//外设地址
	DMA2_Channel2->CMAR = (uint32_t) Dmawbuff;//存储器地址		

	SpiWriteNss(0);//GPIOA->BRR = 0x0010;//PA15 CS

	DMA2_Channel1->CCR |= 0x0001;//启动
	DMA2_Channel2->CCR |= 0x0001;	
		
}

/*
 *DMA2接收完成中断即数据包spi读写完成
 *DMA-spi读取数据的第一个值为spi的读写返回位-无用，后面的值才为有效值
 *SX1276.Settings.LoRaPacketHandler.Size 该次读取到的数据包长度
 */
void DMA2_Channel1_IRQHandler_lora(void)
{
	uint8_t i;
 if(DMA2->ISR&2)
  {
		DMA2->IFCR = 0x00000001;//清除全部中断标志
		DMA2->IFCR = 0x00000002;//清除全部中断标志
		
		SpiWriteNss(1);//spi3cs拉高
		
		DMA2_Channel1->CCR &= ~0x0001;//关闭
		DMA2_Channel2->CCR &= ~0x0001;	

		SPI3->CR2 &= ~0x0002;//DISABLE
		SPI3->CR2 &= ~0x0001;		

	  if( DmaState==RF_TX_RUNNING)
		  SX1276SetTx( LoRa.TxTimeout );//lora写fifo完成
		else if(DmaState==RF_RX_RUNNING)
		{
			/*lora通过dma-spi3读取完接收到的数据*/
			DmaState = RF_IDLE;	
			for(i=0;i<LoraPacketSize;i++)
		  	RxTxBuffer[i]=RxTxBuffer[i+1];//RxTxBuffer[0] is invalid data of dmaread
			if( LoRa.RxContinuous == false )
			{
		  	LoraRadioState = RF_IDLE;
			}                  
			//   Timer.stop(RX_TIMEOUT_TIMER);																			
			OnRxDone(RxTxBuffer, LoraPacketSize, LoraPacketRssiValue, LoraPacketSnrValue);
    }
	
	}
}
//dio0-rxdone or txdone pb8
void EXTI9_5_IRQHandler_lora(void)
{
	if(EXTI_PR&EXTI_PR_PR8)
	{
    EXTI_PR |=EXTI_PR_PR8;
		SX1276OnDio0Event();
	 }
}

void L_TIM4_IRQHandler(void)
{
  TIM4->SR &=~0x1;
	LoraMasSla.MasterTimeOut--;

	if((LoraMasSla.MasterTimeOut==1)&&(LoraMasSla.MasterState == MTxdone))
	{
    LoraMasSla.MasterTimeOut=0;
		LoraMasSla.MasterState = MTimeout;
		TIM4->CR1 &=~0x1;
		       /*此添加接收超时处理*/	
  }
}

