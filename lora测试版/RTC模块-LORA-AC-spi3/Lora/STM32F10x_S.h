/*******************************************************************************
	文件：Stm32f10x_s.h
	功能：Stm32f10x系列系统寄存器定义及寄存器控制位声明
	整理：LaiHuaiHua
	创建：20100811 V1.0
	修改：
********************************************************************************/

#ifndef _stm32f10x_s_h

	#define _stm32f10x_s_h

	typedef struct
	{  	volatile unsigned int CRL;
  		volatile unsigned int CRH;
  		volatile unsigned int IDR;
  		volatile unsigned int ODR;
  		volatile unsigned int BSRR;
  		volatile unsigned int BRR;
  		volatile unsigned int LCKR;
	} GPIO_TypeDef;

	typedef struct
	{   volatile unsigned short CR1;
  		unsigned short  RESERVED0;
  		volatile unsigned short CR2;
  		unsigned short  RESERVED1;
  		volatile unsigned short SMCR;
  		unsigned short  RESERVED2;
  		volatile unsigned short DIER;
  		unsigned short  RESERVED3;
  		volatile unsigned short SR;
  		unsigned short  RESERVED4;
  		volatile unsigned short EGR;
  		unsigned short  RESERVED5;
  		volatile unsigned short CCMR1;
  		unsigned short  RESERVED6;
  		volatile unsigned short CCMR2;
  		unsigned short  RESERVED7;
  		volatile unsigned short CCER;
  		unsigned short  RESERVED8;
  		volatile unsigned short CNT;
  		unsigned short  RESERVED9;
  		volatile unsigned short PSC;
  		unsigned short  RESERVED10;
  		volatile unsigned short ARR;
  		unsigned short  RESERVED11;
  		volatile unsigned short RCR;
  		unsigned short  RESERVED12;
  		volatile unsigned short CCR1;
  		unsigned short  RESERVED13;
  		volatile unsigned short CCR2;
  		unsigned short  RESERVED14;
  		volatile unsigned short CCR3;
  		unsigned short  RESERVED15;
  		volatile unsigned short CCR4;
  		unsigned short  RESERVED16;
  		volatile unsigned short BDTR;
  		unsigned short  RESERVED17;
  		volatile unsigned short DCR;
  		unsigned short  RESERVED18;
  		volatile unsigned short DMAR;
  		unsigned short  RESERVED19;
	} TIM_TypeDef;

	typedef struct
	{  	volatile unsigned short SR;
  		unsigned short  RESERVED0;
  		volatile unsigned short DR;
  		unsigned short  RESERVED1;
  		volatile unsigned short BRR;
  		unsigned short  RESERVED2;
  		volatile unsigned short CR1;
  		unsigned short  RESERVED3;
  		volatile unsigned short CR2;
  		unsigned short  RESERVED4;
  		volatile unsigned short CR3;
  		unsigned short  RESERVED5;
  		volatile unsigned short GTPR;
  		unsigned short  RESERVED6;
	} USART_TypeDef;

	typedef struct
	{  	volatile unsigned short CR1;
  		unsigned short  RESERVED0;
  		volatile unsigned short CR2;
  		unsigned short  RESERVED1;
  		volatile unsigned short SR;
  		unsigned short  RESERVED2;
  		volatile unsigned short DR;
  		unsigned short  RESERVED3;
  		volatile unsigned short CRCPR;
  		unsigned short  RESERVED4;
  		volatile unsigned short RXCRCR;
  		unsigned short  RESERVED5;
  		volatile unsigned short TXCRCR;
  		unsigned short  RESERVED6;
  		volatile unsigned short I2SCFGR;
  		unsigned short  RESERVED7;
  		volatile unsigned short I2SPR;
  		unsigned short  RESERVED8;  
	} SPI_TypeDef;

	typedef struct
	{  	volatile unsigned int TIR;
  		volatile unsigned int TDTR;
  		volatile unsigned int TDLR;
  		volatile unsigned int TDHR;
	} CAN_TxMailBox_TypeDef;

	typedef struct
	{  	volatile unsigned int SR;
  		volatile unsigned int CR1;
  		volatile unsigned int CR2;
  		volatile unsigned int SMPR1;
  		volatile unsigned int SMPR2;
  		volatile unsigned int JOFR1;
  		volatile unsigned int JOFR2;
  		volatile unsigned int JOFR3;
  		volatile unsigned int JOFR4;
  		volatile unsigned int HTR;
  		volatile unsigned int LTR;
  		volatile unsigned int SQR1;
  		volatile unsigned int SQR2;
  		volatile unsigned int SQR3;
  		volatile unsigned int JSQR;
  		volatile unsigned int JDR1;
  		volatile unsigned int JDR2;
  		volatile unsigned int JDR3;
  		volatile unsigned int JDR4;
  		volatile unsigned int DR;
	} ADC_TypeDef;

	typedef struct
	{  	volatile unsigned int CCR;
  		volatile unsigned int CNDTR;
  		volatile unsigned int CPAR;
  		volatile unsigned int CMAR;
	} DMA_Channel_TypeDef;

	typedef struct
	{  	volatile unsigned int ISR;
  		volatile unsigned int IFCR;
	} DMA_TypeDef;

	typedef struct
	{	volatile unsigned short RDP;
		volatile unsigned short USER;
		volatile unsigned short DATA0;
		volatile unsigned short DATA1;
		volatile unsigned short WRP0;
		volatile unsigned short WRP1;
		volatile unsigned short WRP2;
		volatile unsigned short WRP3;
	}OB_TypeDef;


	/******************************************************************************/
	/*                         外设基址定义													              */
	/******************************************************************************/
	#define SysTick_BASE		  (0xe000e010)	/*SysTick base address in the alias region*/

	#define NVIC_SETENAs_BASE	  (0xe000e100)	/*NVIC SETENAs address in the alias region*/
	#define NVIC_CLRENAs_BASE	  (0xe000e180)	/*NVIC CLRENAs address in the alias region*/
	#define NVIC_SETPENDs_BASE	  (0xe000e200)	/*NVIC SETPENDs address in the alias region*/
	#define NVIC_CLRPENDs_BASE	  (0xe000e280)	/*NVIC CLRPENDs address in the alias region*/
	#define NVIC_INTPRI_BASE	  (0xe000e400)	/*NVIC INTPRI address in the alias region*/
		


	#define PERIPH_BB_BASE        (0x42000000) /*!< Peripheral base address in the alias region */
	#define SRAM_BB_BASE          (0x22000000) /*!< SRAM base address in the alias region */

	#define SRAM_BASE             (0x20000000) /*!< Peripheral base address in the bit-band region */
	#define PERIPH_BASE           (0x40000000) /*!< SRAM base address in the bit-band region */

	#define FSMC_R_BASE           (0xA0000000) /*!< FSMC registers base address */

	/*!< Peripheral memory map */
	#define APB1PERIPH_BASE       PERIPH_BASE
	#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
	#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)
	
	#define TIM2_BASE             (APB1PERIPH_BASE + 0x0000)
	#define TIM3_BASE             (APB1PERIPH_BASE + 0x0400)
	#define TIM4_BASE             (APB1PERIPH_BASE + 0x0800)
	#define TIM5_BASE             (APB1PERIPH_BASE + 0x0C00)
	#define TIM6_BASE             (APB1PERIPH_BASE + 0x1000)
	#define TIM7_BASE             (APB1PERIPH_BASE + 0x1400)
	#define RTC_BASE              (APB1PERIPH_BASE + 0x2800)
	#define WWDG_BASE             (APB1PERIPH_BASE + 0x2C00)
	#define IWDG_BASE             (APB1PERIPH_BASE + 0x3000)
	#define SPI2_BASE             (APB1PERIPH_BASE + 0x3800)
	#define SPI3_BASE             (APB1PERIPH_BASE + 0x3C00)
	#define USART2_BASE           (APB1PERIPH_BASE + 0x4400)
	#define USART3_BASE           (APB1PERIPH_BASE + 0x4800)
	#define UART4_BASE            (APB1PERIPH_BASE + 0x4C00)
	#define UART5_BASE            (APB1PERIPH_BASE + 0x5000)
	#define I2C1_BASE             (APB1PERIPH_BASE + 0x5400)
	#define I2C2_BASE             (APB1PERIPH_BASE + 0x5800)
	#define USB_BASE 	  		  (APB1PERIPH_BASE + 0x6000)
	#define CAN1_BASE             (APB1PERIPH_BASE + 0x6400)
	#define BKP_BASE              (APB1PERIPH_BASE + 0x6C00)
	#define PWR_BASE              (APB1PERIPH_BASE + 0x7000)
	#define DAC_BASE              (APB1PERIPH_BASE + 0x7400)
	
	#define AFIO_BASE             (APB2PERIPH_BASE + 0x0000)
	#define EXTI_BASE             (APB2PERIPH_BASE + 0x0400)
	#define GPIOA_BASE            (APB2PERIPH_BASE + 0x0800)
	#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
	#define GPIOC_BASE            (APB2PERIPH_BASE + 0x1000)
	#define GPIOD_BASE            (APB2PERIPH_BASE + 0x1400)
	#define GPIOE_BASE            (APB2PERIPH_BASE + 0x1800)
	#define GPIOF_BASE            (APB2PERIPH_BASE + 0x1C00)
	#define GPIOG_BASE            (APB2PERIPH_BASE + 0x2000)
	#define ADC1_BASE             (APB2PERIPH_BASE + 0x2400)
	#define ADC2_BASE             (APB2PERIPH_BASE + 0x2800)
	#define TIM1_BASE             (APB2PERIPH_BASE + 0x2C00)
	#define SPI1_BASE             (APB2PERIPH_BASE + 0x3000)
	#define TIM8_BASE             (APB2PERIPH_BASE + 0x3400)
	#define USART1_BASE           (APB2PERIPH_BASE + 0x3800)
	#define ADC3_BASE             (APB2PERIPH_BASE + 0x3C00)

	#define SDIO_BASE             (PERIPH_BASE + 0x18000)

	#define DMA1_BASE             (AHBPERIPH_BASE + 0x0000)
	#define DMA1_Channel1_BASE    (AHBPERIPH_BASE + 0x0008)
	#define DMA1_Channel2_BASE    (AHBPERIPH_BASE + 0x001C)
	#define DMA1_Channel3_BASE    (AHBPERIPH_BASE + 0x0030)
	#define DMA1_Channel4_BASE    (AHBPERIPH_BASE + 0x0044)
	#define DMA1_Channel5_BASE    (AHBPERIPH_BASE + 0x0058)
	#define DMA1_Channel6_BASE    (AHBPERIPH_BASE + 0x006C)
	#define DMA1_Channel7_BASE    (AHBPERIPH_BASE + 0x0080)
	#define DMA2_BASE             (AHBPERIPH_BASE + 0x0400)
	#define DMA2_Channel1_BASE    (AHBPERIPH_BASE + 0x0408)
	#define DMA2_Channel2_BASE    (AHBPERIPH_BASE + 0x041C)
	#define DMA2_Channel3_BASE    (AHBPERIPH_BASE + 0x0430)
	#define DMA2_Channel4_BASE    (AHBPERIPH_BASE + 0x0444)
	#define DMA2_Channel5_BASE    (AHBPERIPH_BASE + 0x0458)
	#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
	#define CRC_BASE              (AHBPERIPH_BASE + 0x3000)

	#define FLASH_BASE          (AHBPERIPH_BASE + 0x2000) /*!< Flash registers base address */
	#define OB_BASE               (0x1FFFF800)    /*!< Flash Option Bytes base address */

	#define FSMC_Bank1_R_BASE     (FSMC_R_BASE + 0x0000) /*!< FSMC Bank1 registers base address */
	#define FSMC_Bank1E_R_BASE    (FSMC_R_BASE + 0x0104) /*!< FSMC Bank1E registers base address */
	#define FSMC_Bank2_R_BASE     (FSMC_R_BASE + 0x0060) /*!< FSMC Bank2 registers base address */
	#define FSMC_Bank3_R_BASE     (FSMC_R_BASE + 0x0080) /*!< FSMC Bank3 registers base address */
	#define FSMC_Bank4_R_BASE     (FSMC_R_BASE + 0x00A0) /*!< FSMC Bank4 registers base address */

	#define DBGMCU_BASE          (0xE0042000) /*!< Debug MCU registers base address */


	/**
  * @}
  */
  
	/******************************************************************************/
	/*                         外设寄存器定义								      */
	/******************************************************************************/

	/******************************************************************************/
	/*                         中断使能寄存器定义								  */
	/******************************************************************************/
	#define NVIC_ICSR			(*(volatile unsigned int *)0xe000ed04)	 //ICSR 中断控制状态寄存器
	#define NVIC_VTOR		    (*(volatile unsigned int *)0xe000ed08)	/*Vector table Register*/
	#define NVIC_AIRCR		    (*(volatile unsigned int *)0xe000ed0c)	/*Application interrupt and reset control register*/
	#define NVIC_HSCSR			(*(volatile unsigned int *)0xe000ed24)  //HSCSR

	#define NVIC_SETENA0		(*(volatile unsigned int *)(NVIC_SETENAs_BASE+0x00)) //00-31中断使能
	#define NVIC_SETENA1		(*(volatile unsigned int *)(NVIC_SETENAs_BASE+0x04)) //32-63中断使能

	#define NVIC_CLRENA0		(*(volatile unsigned int *)(NVIC_CLRENAs_BASE+0x00)) //00-31中断使能
	#define NVIC_CLRENA1		(*(volatile unsigned int *)(NVIC_CLRENAs_BASE+0x04)) //32-63中断使能

	#define NVIC_SETPENDA0		(*(volatile unsigned int *)(NVIC_SETPENDs_BASE+0x00))//00-31中断挂起
	#define NVIC_SETPENDA1		(*(volatile unsigned int *)(NVIC_SETPENDs_BASE+0x04))//32-63中断挂起


	#define NVIC_CLRPENDA0		(*(volatile unsigned int *)(NVIC_CLRPENDs_BASE+0x00))//00-31中断挂起
	#define NVIC_CLRPENDA1		(*(volatile unsigned int *)(NVIC_CLRPENDs_BASE+0x04))//32-63中断挂起

	

	#define NVIC_PRIA0			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x00))	//00-03优先级设置
	#define NVIC_PRIA1			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x04))	//04-07优先级设置
	#define NVIC_PRIA2			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x08))	//08-11优先级设置
	#define NVIC_PRIA3			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x0c))	//12-15优先级设置
	#define NVIC_PRIA4			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x10))	//16-19优先级设置
	#define NVIC_PRIA5			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x14))	//20-23优先级设置
	#define NVIC_PRIA6			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x18))	//24-27优先级设置
	#define NVIC_PRIA7			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x1c))	//28-31优先级设置
	#define NVIC_PRIA8			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x20))	//32-35优先级设置
	#define NVIC_PRIA9			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x24))	//36-39优先级设置
	#define NVIC_PRIA10			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x28))	//40-43优先级设置
	#define NVIC_PRIA11			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x2c))	//44-47优先级设置
	#define NVIC_PRIA12			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x30))	//48-51优先级设置
	#define NVIC_PRIA13			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x34))	//52-55优先级设置
	#define NVIC_PRIA14			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x38))	//56-59优先级设置
	#define NVIC_PRIA15			(*(volatile unsigned int *)(NVIC_INTPRI_BASE+0x3c))	//60-63优先级设置


	/******************************************************************************/
	/*                         SysTick寄存器定义								  */
	/******************************************************************************/
	#define	SysTick_CTRL		(*(volatile unsigned int*)(SysTick_BASE+0x00))
	#define	SysTick_RELOAD		(*(volatile unsigned int*)(SysTick_BASE+0x04))
	#define	SysTick_CURRENT		(*(volatile unsigned int*)(SysTick_BASE+0x08))
	#define	SysTick_CALI		(*(volatile unsigned int*)(SysTick_BASE+0x0c))

	/******************************************************************************/
	/*                         RTC寄存器定义						              */
	/******************************************************************************/	
	#define RTC_CRH		        (*(volatile unsigned char *)(RTC_BASE+0x00))
	#define RTC_CRL		        (*(volatile unsigned char *)(RTC_BASE+0x04))
	#define RTC_PRLH	        (*(volatile unsigned char *)(RTC_BASE+0x08))
	#define RTC_PRLL	        (*(volatile unsigned short *)(RTC_BASE+0x0c))
	#define RTC_DIVH	        (*(volatile unsigned short *)(RTC_BASE+0x10))
	#define RTC_DIVL	        (*(volatile unsigned short *)(RTC_BASE+0x14))
	#define RTC_CNTH	        (*(volatile unsigned short *)(RTC_BASE+0x18))
	#define RTC_CNTL	        (*(volatile unsigned short *)(RTC_BASE+0x1c))
	#define RTC_ALRH	        (*(volatile unsigned short *)(RTC_BASE+0x20))
	#define RTC_ALRL	        (*(volatile unsigned short *)(RTC_BASE+0x24))
								
	/******************************************************************************/
	/*                         WWDG寄存器定义						              */
	/******************************************************************************/
	#define WWDG_CR		        (*(volatile unsigned char *)(WWDG_BASE+0x00))
	#define WWDG_CFR	        (*(volatile unsigned short *)(WWDG_BASE+0x04))
	#define WWDG_SR		        (*(volatile unsigned char *)(WWDG_BASE+0x08))

	/******************************************************************************/
	/*                         IWDG寄存器定义						              */
	/******************************************************************************/
	#define IWDG_KR		        (*(volatile unsigned short *)(IWDG_BASE+0x00))
	#define IWDG_PR		        (*(volatile unsigned char *)(IWDG_BASE+0x04))
	#define IWDG_RLR	        (*(volatile unsigned short *)(IWDG_BASE+0x08))
	#define IWDG_SR		        (*(volatile unsigned char *)(IWDG_BASE+0x0c))	

	/******************************************************************************/
	/*                         I2C1寄存器定义						              */
	/******************************************************************************/
	#define I2C1_CR1          	(*(volatile unsigned short *)(I2C1_BASE+0x00))
	#define I2C1_CR2          	(*(volatile unsigned short *)(I2C1_BASE+0x04))		
	#define I2C1_OAR1         	(*(volatile unsigned short *)(I2C1_BASE+0x08))
	#define I2C1_OAR2         	(*(volatile unsigned char *)(I2C1_BASE+0x0c))
	#define I2C1_DR           	(*(volatile unsigned char *)(I2C1_BASE+0x10))
	#define I2C1_SR1          	(*(volatile unsigned short *)(I2C1_BASE+0x14))
	#define I2C1_SR2          	(*(volatile unsigned short *)(I2C1_BASE+0x18))
	#define I2C1_CCR          	(*(volatile unsigned short *)(I2C1_BASE+0x1c))
	#define I2C1_TRISE        	(*(volatile unsigned char *)(I2C1_BASE+0x20))

	/******************************************************************************/
	/*                         I2C2寄存器定义						              */
	/******************************************************************************/
	#define I2C2_CR1          	(*(volatile unsigned short *)(I2C2_BASE+0x00))
	#define I2C2_CR2          	(*(volatile unsigned short *)(I2C2_BASE+0x04))		
	#define I2C2_OAR1         	(*(volatile unsigned short *)(I2C2_BASE+0x08))
	#define I2C2_OAR2         	(*(volatile unsigned char *)(I2C2_BASE+0x0c))
	#define I2C2_DR           	(*(volatile unsigned char *)(I2C2_BASE+0x10))
	#define I2C2_SR1          	(*(volatile unsigned short *)(I2C2_BASE+0x14))
	#define I2C2_SR2          	(*(volatile unsigned short *)(I2C2_BASE+0x18))
	#define I2C2_CCR          	(*(volatile unsigned short *)(I2C2_BASE+0x1c))
	#define I2C2_TRISE        	(*(volatile unsigned char *)(I2C2_BASE+0x20))

	/******************************************************************************/
	/*                         USB寄存器定义						              */
	/******************************************************************************/
	#define USB_EP0R          	(*(volatile unsigned short *)(USB_BASE+0x00))
	#define USB_EP1R          	(*(volatile unsigned short *)(USB_BASE+0x04))		
	#define USB_EP2R          	(*(volatile unsigned short *)(USB_BASE+0x08))
	#define USB_EP3R	        (*(volatile unsigned short *)(USB_BASE+0x0c))
	#define USB_EP4R	        (*(volatile unsigned short *)(USB_BASE+0x10))
	#define USB_EP5R          	(*(volatile unsigned short *)(USB_BASE+0x14))
	#define USB_EP6R          	(*(volatile unsigned short *)(USB_BASE+0x18))
	#define USB_EP7R          	(*(volatile unsigned short *)(USB_BASE+0x1c))

	#define USB_CR	          	(*(volatile unsigned short *)(USB_BASE+0x40))
	#define USB_ISR	          	(*(volatile unsigned short *)(USB_BASE+0x44))		
	#define USB_FNR	          	(*(volatile unsigned short *)(USB_BASE+0x48))
	#define USB_DA		        (*(volatile unsigned short *)(USB_BASE+0x4c))
	#define USB_BTA		        (*(volatile unsigned short *)(USB_BASE+0x50))


	/******************************************************************************/
	/*                         CAN1寄存器定义						              */
	/******************************************************************************/
	#define CAN1_MCR          	(*(volatile unsigned short *)(CAN1_BASE+0x00))
	#define CAN1_MSR          	(*(volatile unsigned short *)(CAN1_BASE+0x04))		
	#define CAN1_TSR          	(*(volatile unsigned int *)(CAN1_BASE+0x08))
	#define CAN1_RF0R         	(*(volatile unsigned char *)(CAN1_BASE+0x0c))
	#define CAN1_RF1F         	(*(volatile unsigned char *)(CAN1_BASE+0x10))
	#define CAN1_IER          	(*(volatile unsigned int *)(CAN1_BASE+0x14))
	#define CAN1_ESR          	(*(volatile unsigned int *)(CAN1_BASE+0x18))
	#define CAN1_BTR          	(*(volatile unsigned int *)(CAN1_BASE+0x1c))
	
	#define CAN1_TI0R         	(*(volatile unsigned int *)(CAN1_BASE+0x180))
	#define CAN1_TDT0R        	(*(volatile unsigned int *)(CAN1_BASE+0x184))
	#define CAN1_TDL0R        	(*(volatile unsigned int *)(CAN1_BASE+0x188))
	#define CAN1_TDH0R        	(*(volatile unsigned int *)(CAN1_BASE+0x18c))			
	#define CAN1_TI1R         	(*(volatile unsigned int *)(CAN1_BASE+0x190))
	#define CAN1_TD1R         	(*(volatile unsigned int *)(CAN1_BASE+0x194))
	#define CAN1_TDL1R        	(*(volatile unsigned int *)(CAN1_BASE+0x198))
	#define CAN1_TDH1R        	(*(volatile unsigned int *)(CAN1_BASE+0x19c))
	#define CAN1_TI2R         	(*(volatile unsigned int *)(CAN1_BASE+0x1a0))
	#define CAN1_TD2R         	(*(volatile unsigned int *)(CAN1_BASE+0x1a4))
	#define CAN1_TDL2R        	(*(volatile unsigned int *)(CAN1_BASE+0x1a8))
	#define CAN1_TDH2R        	(*(volatile unsigned int *)(CAN1_BASE+0x1ac))
	#define CAN1_RDI0R        	(*(volatile unsigned int *)(CAN1_BASE+0x1b0))
	#define CAN1_RDT0R        	(*(volatile unsigned int *)(CAN1_BASE+0x1b4))
	#define CAN1_RDL0R        	(*(volatile unsigned int *)(CAN1_BASE+0x1b8))
	#define CAN1_RDH0R        	(*(volatile unsigned int *)(CAN1_BASE+0x1bc))
	#define CAN1_RI1R         	(*(volatile unsigned int *)(CAN1_BASE+0x1c0))
	#define CAN1_RDT1R        	(*(volatile unsigned int *)(CAN1_BASE+0x1c4))
	#define CAN1_RDL1R        	(*(volatile unsigned int *)(CAN1_BASE+0x1c8))
	#define CAN1_RDH1R        	(*(volatile unsigned int *)(CAN1_BASE+0x1cc))	
	
	#define CAN1_FMR          	(*(volatile unsigned char *)(CAN1_BASE+0x200))
	#define CAN1_FM1R         	(*(volatile unsigned short *)(CAN1_BASE+0x204))
	
	#define CAN1_FS1R         	(*(volatile unsigned short *)(CAN1_BASE+0x20c))
	
	#define CAN1_FFA1R        	(*(volatile unsigned short *)(CAN1_BASE+0x214))	
	
	#define CAN1_F0R1         	(*(volatile unsigned int *)(CAN1_BASE+0x240))
	#define CAN1_F0R2         	(*(volatile unsigned int *)(CAN1_BASE+0x244))	
	#define CAN1_F1R1         	(*(volatile unsigned int *)(CAN1_BASE+0x248))
	#define CAN1_F1R2         	(*(volatile unsigned int *)(CAN1_BASE+0x24c))
	#define CAN1_F2R1         	(*(volatile unsigned int *)(CAN1_BASE+0x250))
	#define CAN1_F2R2         	(*(volatile unsigned int *)(CAN1_BASE+0x254))
	#define CAN1_F3R1         	(*(volatile unsigned int *)(CAN1_BASE+0x258))
	#define CAN1_F3R2         	(*(volatile unsigned int *)(CAN1_BASE+0x25c))
	#define CAN1_F4R1         	(*(volatile unsigned int *)(CAN1_BASE+0x260))
	#define CAN1_F4R2         	(*(volatile unsigned int *)(CAN1_BASE+0x264))	
	#define CAN1_F5R1         	(*(volatile unsigned int *)(CAN1_BASE+0x268))
	#define CAN1_F5R2         	(*(volatile unsigned int *)(CAN1_BASE+0x26c))
	#define CAN1_F6R1         	(*(volatile unsigned int *)(CAN1_BASE+0x270))
	#define CAN1_F6R2         	(*(volatile unsigned int *)(CAN1_BASE+0x274))
	#define CAN1_F7R1         	(*(volatile unsigned int *)(CAN1_BASE+0x278))
	#define CAN1_F7R2         	(*(volatile unsigned int *)(CAN1_BASE+0x27c))
	#define CAN1_F8R1         	(*(volatile unsigned int *)(CAN1_BASE+0x280))
	#define CAN1_F8R2         	(*(volatile unsigned int *)(CAN1_BASE+0x284))
	#define CAN1_F9R1         	(*(volatile unsigned int *)(CAN1_BASE+0x288))
	#define CAN1_F9R2         	(*(volatile unsigned int *)(CAN1_BASE+0x28c))
	#define CAN1_F10R1        	(*(volatile unsigned int *)(CAN1_BASE+0x290))
	#define CAN1_F10R2        	(*(volatile unsigned int *)(CAN1_BASE+0x294))
	#define CAN1_F11R1        	(*(volatile unsigned int *)(CAN1_BASE+0x298))
	#define CAN1_F11R2        	(*(volatile unsigned int *)(CAN1_BASE+0x29c))
	#define CAN1_F12R1        	(*(volatile unsigned int *)(CAN1_BASE+0x2a0))
	#define CAN1_F12R2        	(*(volatile unsigned int *)(CAN1_BASE+0x2a4))
	#define CAN1_F13R1        	(*(volatile unsigned int *)(CAN1_BASE+0x2a8))
	#define CAN1_F13R2        	(*(volatile unsigned int *)(CAN1_BASE+0x2ac))

	/******************************************************************************/
	/*                         BKP寄存器定义						              */
	/******************************************************************************/
	#define BKP_DR1          	(*(volatile unsigned short *)(BKP_BASE+0x04))
	#define BKP_DR2          	(*(volatile unsigned short *)(BKP_BASE+0x08))		
	#define BKP_DR3          	(*(volatile unsigned short *)(BKP_BASE+0x0c))
	#define BKP_DR4          	(*(volatile unsigned short *)(BKP_BASE+0x10))			
	#define BKP_DR5          	(*(volatile unsigned short *)(BKP_BASE+0x14))
	#define BKP_DR6          	(*(volatile unsigned short *)(BKP_BASE+0x18))	
	#define BKP_DR7          	(*(volatile unsigned short *)(BKP_BASE+0x1c))
	#define BKP_DR8          	(*(volatile unsigned short *)(BKP_BASE+0x20))
	#define BKP_DR9          	(*(volatile unsigned short *)(BKP_BASE+0x24))
	#define BKP_DR10         	(*(volatile unsigned short *)(BKP_BASE+0x28))
	#define BKP_RTCCR        	(*(volatile unsigned short *)(BKP_BASE+0x2c))
	#define BKP_CR           	(*(volatile unsigned short *)(BKP_BASE+0x30))
	#define BKP_CSR          	(*(volatile unsigned short *)(BKP_BASE+0x34))
	#define BKP_DR11         	(*(volatile unsigned short *)(BKP_BASE+0x40))
	#define BKP_DR12         	(*(volatile unsigned short *)(BKP_BASE+0x44))
	#define BKP_DR13         	(*(volatile unsigned short *)(BKP_BASE+0x48))
	#define BKP_DR14         	(*(volatile unsigned short *)(BKP_BASE+0x4c))
	#define BKP_DR15         	(*(volatile unsigned short *)(BKP_BASE+0x50))
	#define BKP_DR16         	(*(volatile unsigned short *)(BKP_BASE+0x54))
	#define BKP_DR17         	(*(volatile unsigned short *)(BKP_BASE+0x58))
	#define BKP_DR18         	(*(volatile unsigned short *)(BKP_BASE+0x5c))
	#define BKP_DR19         	(*(volatile unsigned short *)(BKP_BASE+0x60))
	#define BKP_DR20         	(*(volatile unsigned short *)(BKP_BASE+0x64))
	#define BKP_DR21         	(*(volatile unsigned short *)(BKP_BASE+0x68))
	#define BKP_DR22         	(*(volatile unsigned short *)(BKP_BASE+0x6c))
	#define BKP_DR23         	(*(volatile unsigned short *)(BKP_BASE+0x70))
	#define BKP_DR24         	(*(volatile unsigned short *)(BKP_BASE+0x74))
	#define BKP_DR25         	(*(volatile unsigned short *)(BKP_BASE+0x78))
	#define BKP_DR26         	(*(volatile unsigned short *)(BKP_BASE+0x7c))
	#define BKP_DR27         	(*(volatile unsigned short *)(BKP_BASE+0x80))
	#define BKP_DR28         	(*(volatile unsigned short *)(BKP_BASE+0x84))
	#define BKP_DR29         	(*(volatile unsigned short *)(BKP_BASE+0x88))
	#define BKP_DR30         	(*(volatile unsigned short *)(BKP_BASE+0x8c))
	#define BKP_DR31         	(*(volatile unsigned short *)(BKP_BASE+0x90))
	#define BKP_DR32         	(*(volatile unsigned short *)(BKP_BASE+0x94))
	#define BKP_DR33         	(*(volatile unsigned short *)(BKP_BASE+0x98))
	#define BKP_DR34         	(*(volatile unsigned short *)(BKP_BASE+0x9c))
	#define BKP_DR35         	(*(volatile unsigned short *)(BKP_BASE+0xa0))
	#define BKP_DR36         	(*(volatile unsigned short *)(BKP_BASE+0xa4))
	#define BKP_DR37         	(*(volatile unsigned short *)(BKP_BASE+0xa8))
	#define BKP_DR38         	(*(volatile unsigned short *)(BKP_BASE+0xac))
	#define BKP_DR39         	(*(volatile unsigned short *)(BKP_BASE+0xb0))
	#define BKP_DR40         	(*(volatile unsigned short *)(BKP_BASE+0xb4))
	#define BKP_DR41         	(*(volatile unsigned short *)(BKP_BASE+0xb8))
	#define BKP_DR42         	(*(volatile unsigned short *)(BKP_BASE+0xbc))

	/******************************************************************************/
	/*                         PWR寄存器定义						              */
	/******************************************************************************/
	#define PWR_CR           	(*(volatile unsigned short *)(PWR_BASE+0x00))
	#define PWR_CSR          	(*(volatile unsigned short *)(PWR_BASE+0x04))

	/******************************************************************************/
	/*                         DAC寄存器定义						              */
	/******************************************************************************/
	#define DAC_CR           	(*(volatile unsigned int *)(DAC_BASE+0x00))
	#define DAC_SWTRIGR      	(*(volatile unsigned char *)(DAC_BASE+0x04))
	#define DAC_DHR12R1      	(*(volatile unsigned short *)(DAC_BASE+0x08))
	#define DAC_DHR12L1      	(*(volatile unsigned short *)(DAC_BASE+0x0c))
	#define DAC_DHR8R1       	(*(volatile unsigned char *)(DAC_BASE+0x10))
	#define DAC_DHR12R2      	(*(volatile unsigned short *)(DAC_BASE+0x14))
	#define DAC_DHR12L2      	(*(volatile unsigned short *)(DAC_BASE+0x18))
	#define DAC_DHR8R2       	(*(volatile unsigned char *)(DAC_BASE+0x1c))
	#define DAC_DHR12LD      	(*(volatile unsigned int *)(DAC_BASE+0x20))
	#define DAC_DHR12RD      	(*(volatile unsigned int *)(DAC_BASE+0x24))
	#define DAC_DHR8RD       	(*(volatile unsigned short *)(DAC_BASE+0x28))
	#define DAC_D0R1         	(*(volatile unsigned short *)(DAC_BASE+0x2c))
	#define DAC_D0R2         	(*(volatile unsigned short *)(DAC_BASE+0x30))
	
	/******************************************************************************/
	/*                         AFIO寄存器定义						              */
	/******************************************************************************/
	#define AFIO_EVCR        	(*(volatile unsigned char *)(AFIO_BASE+0x00))
	#define AFIO_MAPR        	(*(volatile unsigned int *)(AFIO_BASE+0x04))
	#define AFIO_EXTICR1     	(*(volatile unsigned short *)(AFIO_BASE+0x08))
	#define AFIO_EXTICR2     	(*(volatile unsigned short *)(AFIO_BASE+0x0c))
	#define AFIO_EXTICR3     	(*(volatile unsigned short *)(AFIO_BASE+0x10))
	#define AFIO_EXTICR4     	(*(volatile unsigned short *)(AFIO_BASE+0x14))
	
	/******************************************************************************/
	/*                         EXTI寄存器定义						              */
	/******************************************************************************/
	#define EXTI_IMR         	(*(volatile unsigned int *)(EXTI_BASE+0x00))
	#define EXTI_EMR         	(*(volatile unsigned int *)(EXTI_BASE+0x04))
	#define EXTI_RTSR		    (*(volatile unsigned int *)(EXTI_BASE+0x08))
	#define EXTI_FTSR		    (*(volatile unsigned int *)(EXTI_BASE+0x0c))
	#define EXTI_SWIER	     	(*(volatile unsigned int *)(EXTI_BASE+0x10))
	#define EXTI_PR			    (*(volatile unsigned int *)(EXTI_BASE+0x14))

	/******************************************************************************/
	/*                         GPIO寄存器定义		   	     			          */
	/******************************************************************************/
	#define GPIOA				((GPIO_TypeDef *)GPIOA_BASE)
	#define GPIOB				((GPIO_TypeDef *)GPIOB_BASE)
	#define GPIOC				((GPIO_TypeDef *)GPIOC_BASE)
	#define GPIOD				((GPIO_TypeDef *)GPIOD_BASE)
	#define GPIOE				((GPIO_TypeDef *)GPIOE_BASE)
	#define GPIOF				((GPIO_TypeDef *)GPIOF_BASE)
	#define GPIOG				((GPIO_TypeDef *)GPIOG_BASE)

	/******************************************************************************/
	/*                         TIMER寄存器定义								      */
	/******************************************************************************/
	#define TIM1                ((TIM_TypeDef *) TIM1_BASE)	
	#define TIM2                ((TIM_TypeDef *) TIM2_BASE)	
	#define TIM3                ((TIM_TypeDef *) TIM3_BASE)	
	#define TIM4                ((TIM_TypeDef *) TIM4_BASE)	
	#define TIM5                ((TIM_TypeDef *) TIM5_BASE)	
	#define TIM6                ((TIM_TypeDef *) TIM6_BASE)	
	#define TIM7                ((TIM_TypeDef *) TIM7_BASE)	
	#define TIM8                ((TIM_TypeDef *) TIM8_BASE)	

	/******************************************************************************/
	/*                         USART寄存器定义						              */
	/******************************************************************************/
	#define USART1				((USART_TypeDef *) USART1_BASE)
	#define USART2				((USART_TypeDef *) USART2_BASE)
	#define USART3				((USART_TypeDef *) USART3_BASE)
	#define UART4				((USART_TypeDef *) UART4_BASE)
	#define UART5				((USART_TypeDef *) UART5_BASE)

	/******************************************************************************/
	/*                         SPI寄存器定义						              */
	/******************************************************************************/
	#define SPI1				((SPI_TypeDef *) SPI1_BASE)
	#define SPI2				((SPI_TypeDef *) SPI2_BASE)
	#define SPI3				((SPI_TypeDef *) SPI3_BASE)

	/******************************************************************************/
	/*                         ADC寄存器定义								      */
	/******************************************************************************/
	#define ADC1				((ADC_TypeDef *)ADC1_BASE)
	#define ADC2				((ADC_TypeDef *)ADC2_BASE)
	#define ADC3				((ADC_TypeDef *)ADC3_BASE)

	/******************************************************************************/
	/*                         DMA1寄存器定义							          */
	/******************************************************************************/
	#define DMA1                ((DMA_TypeDef *) DMA1_BASE)
  	#define DMA2                ((DMA_TypeDef *) DMA2_BASE)
	#define DMA1_Channel1		((DMA_Channel_TypeDef *) DMA1_Channel1_BASE)
	#define DMA1_Channel2		((DMA_Channel_TypeDef *) DMA1_Channel2_BASE)
	#define DMA1_Channel3		((DMA_Channel_TypeDef *) DMA1_Channel3_BASE)
	#define DMA1_Channel4		((DMA_Channel_TypeDef *) DMA1_Channel4_BASE)
	#define DMA1_Channel5		((DMA_Channel_TypeDef *) DMA1_Channel5_BASE)
	#define DMA1_Channel6		((DMA_Channel_TypeDef *) DMA1_Channel6_BASE)
	#define DMA1_Channel7		((DMA_Channel_TypeDef *) DMA1_Channel7_BASE)
	#define DMA2_Channel1		((DMA_Channel_TypeDef *) DMA2_Channel1_BASE)
	#define DMA2_Channel2		((DMA_Channel_TypeDef *) DMA2_Channel2_BASE)
	#define DMA2_Channel3		((DMA_Channel_TypeDef *) DMA2_Channel3_BASE)
	#define DMA2_Channel4		((DMA_Channel_TypeDef *) DMA2_Channel4_BASE)
	#define DMA2_Channel5		((DMA_Channel_TypeDef *) DMA2_Channel5_BASE)


	/******************************************************************************/
	/*                         SDIO寄存器定义								      */
	/******************************************************************************/
	#define SDIO_POWER      	(*(volatile unsigned char *)(SDIO_BASE+0x00))
	#define SDIO_CLKCR      	(*(volatile unsigned short *)(SDIO_BASE+0x04))
	#define SDIO_ARG  	    	(*(volatile unsigned int *)(SDIO_BASE+0x08))
	#define SDIO_CMD	    	(*(volatile unsigned short *)(SDIO_BASE+0x0c))
	#define SDIO_RESPCMD    	(*(volatile unsigned char *)(SDIO_BASE+0x10))
	#define SDIO_RESP1      	(*(volatile unsigned int *)(SDIO_BASE+0x14))
	#define SDIO_RESP2      	(*(volatile unsigned int *)(SDIO_BASE+0x18))
	#define SDIO_RESP3      	(*(volatile unsigned int *)(SDIO_BASE+0x1c))
	#define SDIO_RESP4      	(*(volatile unsigned int *)(SDIO_BASE+0x20))
	#define SDIO_DTIMER     	(*(volatile unsigned int *)(SDIO_BASE+0x24))
	#define SDIO_DLEN	    	(*(volatile unsigned int *)(SDIO_BASE+0x28))
	#define SDIO_DCTRL      	(*(volatile unsigned short *)(SDIO_BASE+0x2c))
	#define SDIO_DCOUNT     	(*(volatile unsigned int *)(SDIO_BASE+0x30))
	#define SDIO_STA	    	(*(volatile unsigned int *)(SDIO_BASE+0x34))
	#define SDIO_ICR	    	(*(volatile unsigned int *)(SDIO_BASE+0x38))
	#define SDIO_MASK	    	(*(volatile unsigned int *)(SDIO_BASE+0x3c))

	#define SDIO_FIFOCNT    	(*(volatile unsigned int *)(SDIO_BASE+0x48))

	#define SDIO_FIFO	    	(*(volatile unsigned int *)(SDIO_BASE+0x4c))

	/******************************************************************************/
	/*                         RCC寄存器定义							          */
	/******************************************************************************/
	#define RCC_CR		     	(*(volatile unsigned int *)(RCC_BASE+0x00))
	#define RCC_CFGR	     	(*(volatile unsigned int *)(RCC_BASE+0x04))
	#define RCC_CIR	       	 	(*(volatile unsigned int *)(RCC_BASE+0x08))
	#define RCC_APB2RSTR   	 	(*(volatile unsigned short *)(RCC_BASE+0x0c))
	#define RCC_APB1RSTR   	 	(*(volatile unsigned int *)(RCC_BASE+0x10))
	#define RCC_AHBENR     	 	(*(volatile unsigned short *)(RCC_BASE+0x14))
	#define RCC_APB2ENR    	 	(*(volatile unsigned short *)(RCC_BASE+0x18))
	#define RCC_APB1ENR    	 	(*(volatile unsigned int *)(RCC_BASE+0x1c))
	#define RCC_BDCR	     	(*(volatile unsigned int *)(RCC_BASE+0x20))
	#define RCC_CSR		     	(*(volatile unsigned int *)(RCC_BASE+0x24))

	/******************************************************************************/
	/*                         CRC寄存器定义								      */
	/******************************************************************************/
	#define CRC_DR		     	(*(volatile unsigned int *)(CRC_BASE+0x00))
	#define CRC_IDR		     	(*(volatile unsigned char *)(CRC_BASE+0x04))
	#define CRC_CR	       	 	(*(volatile unsigned char *)(CRC_BASE+0x08))

	/******************************************************************************/
	/*                         FLASH寄存器定义						              */
	/******************************************************************************/
	#define FLASH_ACR	     	(*(volatile unsigned int *)(FLASH_BASE+0x00))
	#define FLASH_KEYR     	 	(*(volatile unsigned int *)(FLASH_BASE+0x04))
	#define FLASH_OPTKEYR  	 	(*(volatile unsigned int *)(FLASH_BASE+0x08))
	#define FLASH_SR		 	(*(volatile unsigned int *)(FLASH_BASE+0x0c))
	#define FLASH_CR		 	(*(volatile unsigned int *)(FLASH_BASE+0x10))
	#define FLASH_AR	     	(*(volatile unsigned int *)(FLASH_BASE+0x14))

	#define FLASH_OBR		 	(*(volatile unsigned int *)(FLASH_BASE+0x1c))
	#define FLASH_WRPR	   	 	(*(volatile unsigned int *)(FLASH_BASE+0x20))

	/******************************************************************************/
	/*                         FSMC寄存器定义									  */
	/******************************************************************************/
	#define FSMC_BCR1	     	(*(volatile unsigned int *)(FSMC_Bank1_R_BASE+0x00))
	#define FSMC_BTR1		 	(*(volatile unsigned int *)(FSMC_Bank1_R_BASE+0x04))
	#define FSMC_BCR2		 	(*(volatile unsigned int *)(FSMC_Bank1_R_BASE+0x08))
	#define FSMC_BTR2		 	(*(volatile unsigned int *)(FSMC_Bank1_R_BASE+0x0c))
	#define FSMC_BCR3		 	(*(volatile unsigned int *)(FSMC_Bank1_R_BASE+0x10))
	#define FSMC_BTR3	     	(*(volatile unsigned int *)(FSMC_Bank1_R_BASE+0x14))
	#define FSMC_BCR4		 	(*(volatile unsigned int *)(FSMC_Bank1_R_BASE+0x18))
	#define FSMC_BTR4	     	(*(volatile unsigned int *)(FSMC_Bank1_R_BASE+0x1c))

	#define FSMC_BWTR1     	 	(*(volatile unsigned int *)(FSMC_Bank1E_R_BASE+0x00))
	#define FSMC_BWTR2	   	 	(*(volatile unsigned int *)(FSMC_Bank1E_R_BASE+0x04))
	#define FSMC_BWTR3	   	 	(*(volatile unsigned int *)(FSMC_Bank1E_R_BASE+0x08))
	#define FSMC_BWTR4	   	 	(*(volatile unsigned int *)(FSMC_Bank1E_R_BASE+0x0c))

	#define FSMC_PCR2	     	(*(volatile unsigned int *)(FSMC_Bank2_R_BASE+0x00))
	#define FSMC_SR2		 	(*(volatile unsigned int *)(FSMC_Bank2_R_BASE+0x04))
	#define FSMC_PMEM2	   	 	(*(volatile unsigned int *)(FSMC_Bank2_R_BASE+0x08))
	#define FSMC_PATT2	   	 	(*(volatile unsigned int *)(FSMC_Bank2_R_BASE+0x0c))

	#define FSMC_PCR3	     	(*(volatile unsigned int *)(FSMC_Bank3_R_BASE+0x00))
	#define FSMC_SR3		 	(*(volatile unsigned int *)(FSMC_Bank3_R_BASE+0x04))
	#define FSMC_PMEM3	   	 	(*(volatile unsigned int *)(FSMC_Bank3_R_BASE+0x08))
	#define FSMC_PATT3	   	 	(*(volatile unsigned int *)(FSMC_Bank3_R_BASE+0x0c))

	#define FSMC_PCR4	     	(*(volatile unsigned int *)(FSMC_Bank4_R_BASE+0x00))
	#define FSMC_SR4		 	(*(volatile unsigned int *)(FSMC_Bank4_R_BASE+0x04))
	#define FSMC_PMEM4	   	 	(*(volatile unsigned int *)(FSMC_Bank4_R_BASE+0x08))
	#define FSMC_PATT4	   	 	(*(volatile unsigned int *)(FSMC_Bank4_R_BASE+0x0c))
	#define FSMC_PIO4		 	(*(volatile unsigned int *)(FSMC_Bank4_R_BASE+0x10))

	/******************************************************************************/
	/*                         DBGMCU寄存器定义								      */
	/******************************************************************************/
	#define DBGMCU_IDCODE  	 	(*(volatile unsigned int *)(DBGMCU_BASE+0x00))
	#define DBGMCU_CR		 	(*(volatile unsigned int *)(DBGMCU_BASE+0x04))

	#define OB                  ((OB_TypeDef *) OB_BASE) 



	/**
  * @}
  */

	/** @addtogroup Exported_constants
  * @{
  */
  
  /** @addtogroup Peripheral_Registers_Bits_Definition
  * @{
  */
    
	/******************************************************************************/
	/*                         Peripheral Registers_Bits_Definition               */
	/******************************************************************************/

	/******************************************************************************/
	/*                                                                            */
	/*                          CRC calculation unit                              */
	/*                                                                            */
	/******************************************************************************/

	/********************  Bit definition for CRC_CR register  ********************/
	#define  CRC_RESET                        0		        /*!< RESET bit */

	/******************************************************************************/
	/*                                                                            */
	/*                             Power Control                                  */
	/*                                                                            */
	/******************************************************************************/

	/********************  Bit definition for PWR_CR register  ********************/
	#define  PWR_LPDS                         0						/*!< Low-Power Deepsleep */
	#define  PWR_PDDS                         1						/*!< Power Down Deepsleep */
	#define  PWR_CWUF                         2						/*!< Clear Wakeup Flag */
	#define  PWR_CSBF                         3			      /*!< Clear Standby Flag */
	#define  PWR_PVDE                         4			      /*!< Power Voltage Detector Enable */
	#define  PWR_PLS                          5			      /*!< PLS[2:0] bits (PVD Level Selection) */
	#define  PWR_DBP                          8			      /*!< Disable Backup Domain write protection */


	/*******************  Bit definition for PWR_CSR register  ********************/
	#define  PWR_WUF                         0				     /*!< Wakeup Flag */
	#define  PWR_SBF                         1     				/*!< Standby Flag */
	#define  PWR_PVDO                        2     				/*!< PVD Output */
	#define  PWR_EWUP                        8     				/*!< Enable WKUP pin */

	/******************************************************************************/
	/*                                                                            */
	/*                            Backup registers                                */
	/*                                                                            */
	/******************************************************************************/

	/******************  Bit definition for BKP_RTCCR register  *******************/
	#define  BKP_CAL                       ((uint16_t)0x007F)     	/*!< Calibration value */
	#define  BKP_CCO                       7				     	/*!< Calibration Clock Output */
	#define  BKP_ASOE                      8				     	/*!< Alarm or Second Output Enable */
	#define  BKP_ASOS                      9				     	/*!< Alarm or Second Output Selection */

	/********************  Bit definition for BKP_CR register  ********************/
	#define  BKP_TPE                          ((uint8_t)0x01)        	/*!< TAMPER pin enable */
	#define  BKP_TPAL                         ((uint8_t)0x02)        	/*!< TAMPER pin active level */

	/*******************  Bit definition for BKP_CSR register  ********************/
	#define  BKP_CTE                         ((uint16_t)0x0001)     	/*!< Clear Tamper event */
	#define  BKP_CTI                         ((uint16_t)0x0002)     	/*!< Clear Tamper Interrupt */
	#define  BKP_TPIE                        ((uint16_t)0x0004)     	/*!< TAMPER Pin interrupt enable */
	#define  BKP_TEF                         ((uint16_t)0x0100)     	/*!< Tamper Event Flag */
	#define  BKP_TIF                         ((uint16_t)0x0200)     	/*!< Tamper Interrupt Flag */

	/******************************************************************************/
	/*                                                                            */
	/*                         Reset and Clock Control                            */
	/*                                                                            */
	/******************************************************************************/

	/********************  Bit definition for RCC_CR register  ********************/
	#define  RCC_HSION                        	0						/*!< Internal High Speed clock enable */
	#define  RCC_HSIRDY                       	1						/*!< Internal High Speed clock ready flag */
	#define  RCC_HSITRIM                      	3						/*!< Internal High Speed clock trimming */
	#define  RCC_HSICAL                       	8						/*!< Internal High Speed clock Calibration */
	#define  RCC_HSEON                        	16					/*!< External High Speed clock enable */
	#define  RCC_HSERDY                       	17					/*!< External High Speed clock ready flag */
	#define  RCC_HSEBYP                       	18					/*!< External High Speed clock Bypass */
	#define  RCC_CSSON                        	19					/*!< Clock Security System enable */
	#define  RCC_PLLON                        	24					/*!< PLL enable */
	#define  RCC_PLLRDY                       	25					/*!< PLL clock ready flag */

	/*******************  Bit definition for RCC_CFGR register  *******************/
	#define  RCC_SW                      	  	0						/*!< SW[1:0] bits (System clock Switch) */
	#define  RCC_SWS                     	  	2						/*!< SWS[1:0] bits (System Clock Switch Status) */
	#define  RCC_HPRE                    	  	4						/*!< HPRE[3:0] bits (AHB prescaler) */
	#define  RCC_PPRE1                   	  	8						/*!< PRE1[2:0] bits (APB1 prescaler) */
	#define  RCC_PPRE2                   	  	11					/*!< PRE2[2:0] bits (APB2 prescaler) */
	#define  RCC_ADCPRE                  	  	14					/*!< ADCPRE[1:0] bits (ADC prescaler) */
	#define  RCC_PLLSRC                  	  	16					/*!< PLL entry clock source */
	#define  RCC_PLLXTPRE                	  	17					/*!< HSE divider for PLL entry */
	#define  RCC_PLLMUL                 	  	18					/*!< PLLMUL[3:0] bits (PLL multiplication factor) */
	#define  RCC_USBPRE                  	  	22					/*!< USB prescaler */
	#define  RCC_MCO                     	  	24					/*!< MCO[2:0] bits (Microcontroller Clock Output) */

	/*!<******************  Bit definition for RCC_CIR register  ********************/
	#define  RCC_LSIRDYF                  	 	0						/*!< LSI Ready Interrupt flag */
	#define  RCC_LSERDYF                  		1						/*!< LSE Ready Interrupt flag */
	#define  RCC_HSIRDYF                  		2						/*!< HSI Ready Interrupt flag */
	#define  RCC_HSERDYF                  		3						/*!< HSE Ready Interrupt flag */
	#define  RCC_PLLRDYF                  		4						/*!< PLL Ready Interrupt flag */
	#define  RCC_CSSF                     		7						/*!< Clock Security System Interrupt flag */
	#define  RCC_LSIRDYIE                 		8						/*!< LSI Ready Interrupt Enable */
	#define  RCC_LSERDYIE                 		9						/*!< LSE Ready Interrupt Enable */
	#define  RCC_HSIRDYIE                 		10					/*!< HSI Ready Interrupt Enable */
	#define  RCC_HSERDYIE                 		11					/*!< HSE Ready Interrupt Enable */
	#define  RCC_PLLRDYIE                 		12					/*!< PLL Ready Interrupt Enable */
	#define  RCC_LSIRDYC                  		16					/*!< LSI Ready Interrupt Clear */
	#define  RCC_LSERDYC                  		17					/*!< LSE Ready Interrupt Clear */
	#define  RCC_HSIRDYC                  		18					/*!< HSI Ready Interrupt Clear */
	#define  RCC_HSERDYC                  		19					/*!< HSE Ready Interrupt Clear */
	#define  RCC_PLLRDYC                  		20					/*!< PLL Ready Interrupt Clear */
	#define  RCC_CSSC                     		23					/*!< Clock Security System Interrupt Clear */

	/*****************  Bit definition for RCC_APB2RSTR register  *****************/
	#define  RCC_AFIORST             			0						/*!< Alternate Function I/O reset */
	#define  RCC_IOPARST             			2						/*!< I/O port A reset */
	#define  RCC_IOPBRST             			3						/*!< IO port B reset */
	#define  RCC_IOPCRST             			4						/*!< IO port C reset */
	#define  RCC_IOPDRST             			5						/*!< IO port D reset */
	#define  RCC_IOPERST             			6						/*!< IO port E reset */
	#define  RCC_IOPFRST             			7						/*!< IO port F reset */
	#define  RCC_IOPGRST             			8						/*!< IO port G reset */
	#define  RCC_ADC1RST             			9						/*!< ADC 1 interface reset */
	#define  RCC_ADC2RST             			10					/*!< ADC 2 interface reset */
	#define  RCC_TIM1RST             			11					/*!< TIM1 Timer reset */
	#define  RCC_SPI1RST             			12					/*!< SPI 1 reset */
	#define  RCC_TIM8RST             			13					/*!< TIM8 Timer reset */
	#define  RCC_USART1RST           			14					/*!< USART1 reset */
	#define  RCC_ADC3RST             			15					/*!< ADC3 interface reset */

	/*****************  Bit definition for RCC_APB1RSTR register  *****************/
	#define  RCC_TIM2RST             			0						/*!< Timer 2 reset */
	#define  RCC_TIM3RST             			1						/*!< Timer 3 reset */
	#define  RCC_TIM4RST             			2						/*!< Timer 4 reset */
	#define  RCC_TIM5RST             			3						/*!< Timer 5 reset */
	#define  RCC_TIM6RST             			4						/*!< Timer 6 reset */
	#define  RCC_TIM7RST             			5						/*!< Timer 7 reset */
	#define  RCC_WWDGRST             			11					/*!< Window Watchdog reset */
	#define  RCC_SPI2RST             			14					/*!< SPI 2 reset */
	#define  RCC_SPI3RST             			15					/*!< SPI 3 reset */
	#define  RCC_USART2RST           			17					/*!< USART 2 reset */
	#define  RCC_USART3RST           			18					/*!< RUSART 3 reset */
	#define  RCC_UART4RST            			19					/*!< USART 4 reset */
	#define  RCC_UART5RST            			20					/*!< USART 5 reset */
	#define  RCC_I2C1RST             			21					/*!< I2C 1 reset */
	#define  RCC_I2C2RST             			22					/*!< I2C 2 reset */
	#define  RCC_USBRST              			23					/*!< USB reset */
	#define  RCC_CANRST              			25					/*!< CAN reset */
	#define  RCC_BKPRST              			27					/*!< Backup interface reset */
	#define  RCC_PWRRST              			28					/*!< Power interface reset */
	#define  RCC_DACRST              			29					/*!< DAC interface reset */

	/******************  Bit definition for RCC_AHBENR register  ******************/
	#define  RCC_DMA1EN                			0						/*!< DMA1 clock enable */
	#define  RCC_DMA2EN                			1						/*!< DMA2 clock enable */
	#define  RCC_SRAMEN                			2						/*!< SRAM interface clock enable */
	#define  RCC_FLITFEN               			4						/*!< FLITF clock enable */
	#define  RCC_CRCEN                 			6						/*!< CRC clock enable */
	#define  RCC_FSMCEN                			8						/*!< FSMC clock enable */
	#define  RCC_SDIOEN                			10					/*!< SDIO clock enable */
												 
	/******************  Bit definition for RCC_APB2ENR register  *****************/
	#define  RCC_AFIOEN               			0						/*!< Alternate Function I/O clock enable */
	#define  RCC_IOPAEN               			2						/*!< I/O port A clock enable */
	#define  RCC_IOPBEN               			3						/*!< I/O port B clock enable */
	#define  RCC_IOPCEN               			4						/*!< I/O port C clock enable */
	#define  RCC_IOPDEN               			5						/*!< I/O port D clock enable */
	#define  RCC_IOPEEN               			6						/*!< I/O port E clock enable */
	#define  RCC_IOPFEN               			7						/*!< I/O port F clock enable */
	#define  RCC_IOPGEN               			8						/*!< I/O port G clock enable */
	#define  RCC_ADC1EN               			9						/*!< ADC 1 interface clock enable */
	#define  RCC_ADC2EN               			10					/*!< ADC 2 interface clock enable */
	#define  RCC_TIM1EN               			11					/*!< TIM1 Timer clock enable */
	#define  RCC_SPI1EN               			12					/*!< SPI 1 clock enable */
	#define  RCC_TIM8EN               			13					/*!< TIM8 Timer clock enable */
	#define  RCC_USART1EN             			14					/*!< USART1 clock enable */
	#define  RCC_ADC3EN               			15					/*!< DMA1 clock enable */

	/*****************  Bit definition for RCC_APB1ENR register  ******************/
	#define  RCC_TIM2EN               			0						/*!< Timer 2 clock enabled*/
	#define  RCC_TIM3EN               			1						/*!< Timer 3 clock enable */
	#define  RCC_TIM4EN               			2						/*!< Timer 4 clock enable */
	#define  RCC_TIM5EN               			3						/*!< Timer 5 clock enable */
	#define  RCC_TIM6EN               			4						/*!< Timer 6 clock enable */
	#define  RCC_TIM7EN               			5						/*!< Timer 7 clock enable */
	#define  RCC_WWDGEN               			11					/*!< Window Watchdog clock enable */
	#define  RCC_SPI2EN               			14					/*!< SPI 2 clock enable */
	#define  RCC_SPI3EN               			15					/*!< SPI 3 clock enable */
	#define  RCC_USART2EN             			17					/*!< USART 2 clock enable */
	#define  RCC_USART3EN             			18					/*!< USART 3 clock enable */
	#define  RCC_UART4EN              			19					/*!< USART 4 clock enable */
	#define  RCC_UART5EN              			20					/*!< USART 5 clock enable */
	#define  RCC_I2C1EN               			21					/*!< I2C 1 clock enable */
	#define  RCC_I2C2EN               			22					/*!< I2C 2 clock enable */
	#define  RCC_USBEN                			23					/*!< USB clock enable */
	#define  RCC_CANEN                			25					/*!< CAN clock enable */
	#define  RCC_BKPEN                			27					/*!< Backup interface clock enable */
	#define  RCC_PWREN                			28					/*!< Power interface clock enable */
	#define  RCC_DACEN                			29					/*!< DAC interface clock enable */

	/*******************  Bit definition for RCC_BDCR register  *******************/
	#define  RCC_LSEON                   		0						/*!< External Low Speed oscillator enable */
	#define  RCC_LSERDY                  		1						/*!< External Low Speed oscillator Ready */
	#define  RCC_LSEBYP                  		2						/*!< External Low Speed oscillator Bypass */
	#define  RCC_RTCSEL                  		8						/*!< RTCSEL[1:0] bits (RTC clock source selection) */
	#define  RCC_RTCEN                   		15					/*!< RTC clock enable */
	#define  RCC_BDRST                   		16					/*!< Backup domain software reset  */
												
	/*******************  Bit definition for RCC_CSR register  ********************/  
	#define  RCC_LSION                    		0						/*!< Internal Low Speed oscillator enable */
	#define  RCC_LSIRDY                   		1						/*!< Internal Low Speed oscillator Ready */
	#define  RCC_RMVF                     		24					/*!< Remove reset flag */
	#define  RCC_PINRSTF                  		26					/*!< PIN reset flag */
	#define  RCC_PORRSTF                  		27					/*!< POR/PDR reset flag */
	#define  RCC_SFTRSTF                  		28					/*!< Software Reset flag */
	#define  RCC_IWDGRSTF                 		29					/*!< Independent Watchdog reset flag */
	#define  RCC_WWDGRSTF                 		30					/*!< Window watchdog reset flag */
	#define  RCC_LPWRRSTF                 		31					/*!< Low-Power reset flag */

	/******************************************************************************/
	/*                                                                            */
	/*                General Purpose and Alternate Function IO                   */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for GPIO_CRL register  *******************/
	#define  GPIO_MODE0                      	0						/*!< MODE0[1:0] bits (Port x mode bits, pin 0) */
	#define  GPIO_MODE1                      	4						/*!< MODE1[1:0] bits (Port x mode bits, pin 1) */
	#define  GPIO_MODE2                      	8						/*!< MODE2[1:0] bits (Port x mode bits, pin 2) */
	#define  GPIO_MODE3                      	12					/*!< MODE3[1:0] bits (Port x mode bits, pin 3) */
	#define  GPIO_MODE4                      	16					/*!< MODE4[1:0] bits (Port x mode bits, pin 4) */
	#define  GPIO_MODE5                      	20					/*!< MODE5[1:0] bits (Port x mode bits, pin 5) */
	#define  GPIO_MODE6                      	24					/*!< MODE6[1:0] bits (Port x mode bits, pin 6) */
	#define  GPIO_MODE7                      	28					/*!< MODE7[1:0] bits (Port x mode bits, pin 7) */
	#define  GPIO_CNF0                       	2						/*!< CNF0[1:0] bits (Port x configuration bits, pin 0) */
	#define  GPIO_CNF1                       	6						/*!< CNF1[1:0] bits (Port x configuration bits, pin 1) */
	#define  GPIO_CNF2                       	10					/*!< CNF2[1:0] bits (Port x configuration bits, pin 2) */
	#define  GPIO_CNF3                       	14					/*!< CNF3[1:0] bits (Port x configuration bits, pin 3) */
	#define  GPIO_CNF4                       	18					/*!< CNF4[1:0] bits (Port x configuration bits, pin 4) */
	#define  GPIO_CNF5                       	22					/*!< CNF5[1:0] bits (Port x configuration bits, pin 5) */
	#define  GPIO_CNF6                       	26					/*!< CNF6[1:0] bits (Port x configuration bits, pin 6) */
	#define  GPIO_CNF7                       	30					/*!< CNF7[1:0] bits (Port x configuration bits, pin 7) */

	/*******************  Bit definition for GPIO_CRH register  *******************/
	#define  GPIO_MODE8                      	0						/*!< MODE8[1:0] bits (Port x mode bits, pin 8) */
	#define  GPIO_MODE9                      	4						/*!< MODE9[1:0] bits (Port x mode bits, pin 9) */
	#define  GPIO_MODE10                     	8						/*!< MODE10[1:0] bits (Port x mode bits, pin 10) */
	#define  GPIO_MODE11                     	12					/*!< MODE11[1:0] bits (Port x mode bits, pin 11) */
	#define  GPIO_MODE12                     	16					/*!< MODE12[1:0] bits (Port x mode bits, pin 12) */
	#define  GPIO_MODE13                     	20					/*!< MODE13[1:0] bits (Port x mode bits, pin 13) */
	#define  GPIO_MODE14                     	24					/*!< MODE14[1:0] bits (Port x mode bits, pin 14) */
	#define  GPIO_MODE15                     	28					/*!< MODE15[1:0] bits (Port x mode bits, pin 15) */
	#define  GPIO_CNF8                       	2						/*!< CNF8[1:0] bits (Port x configuration bits, pin 8) */
	#define  GPIO_CNF9                       	6						/*!< CNF9[1:0] bits (Port x configuration bits, pin 9) */
	#define  GPIO_CNF10                      	10					/*!< CNF10[1:0] bits (Port x configuration bits, pin 10) */
	#define  GPIO_CNF11                      	14					/*!< CNF11[1:0] bits (Port x configuration bits, pin 11) */
	#define  GPIO_CNF12                      	18					/*!< CNF12[1:0] bits (Port x configuration bits, pin 12) */
	#define  GPIO_CNF13                      	22					/*!< CNF13[1:0] bits (Port x configuration bits, pin 13) */
	#define  GPIO_CNF14                      	26					/*!< CNF14[1:0] bits (Port x configuration bits, pin 14) */
	#define  GPIO_CNF15                      	30					/*!< CNF15[1:0] bits (Port x configuration bits, pin 15) */

	/*!<******************  Bit definition for GPIO_IDR register  *******************/
	#define GPIO_IDR0                        	0						/*!< Port input data, bit 0 */
	#define GPIO_IDR1                        	1						/*!< Port input data, bit 1 */
	#define GPIO_IDR2                        	2						/*!< Port input data, bit 2 */
	#define GPIO_IDR3                        	3						/*!< Port input data, bit 3 */
	#define GPIO_IDR4                        	4						/*!< Port input data, bit 4 */
	#define GPIO_IDR5                        	5						/*!< Port input data, bit 5 */
	#define GPIO_IDR6                        	6						/*!< Port input data, bit 6 */
	#define GPIO_IDR7                        	7						/*!< Port input data, bit 7 */
	#define GPIO_IDR8                        	8						/*!< Port input data, bit 8 */
	#define GPIO_IDR9                        	9						/*!< Port input data, bit 9 */
	#define GPIO_IDR10                       	10					/*!< Port input data, bit 10 */
	#define GPIO_IDR11                       	11					/*!< Port input data, bit 11 */
	#define GPIO_IDR12                       	12					/*!< Port input data, bit 12 */
	#define GPIO_IDR13                       	13					/*!< Port input data, bit 13 */
	#define GPIO_IDR14                       	14					/*!< Port input data, bit 14 */
	#define GPIO_IDR15                       	15					/*!< Port input data, bit 15 */

	/*******************  Bit definition for GPIO_ODR register  *******************/
	#define GPIO_ODR0                        	0						/*!< Port output data, bit 0 */
	#define GPIO_ODR1                        	1						/*!< Port output data, bit 1 */
	#define GPIO_ODR2                        	2						/*!< Port output data, bit 2 */
	#define GPIO_ODR3                        	3						/*!< Port output data, bit 3 */
	#define GPIO_ODR4                        	4						/*!< Port output data, bit 4 */
	#define GPIO_ODR5                        	5						/*!< Port output data, bit 5 */
	#define GPIO_ODR6                        	6						/*!< Port output data, bit 6 */
	#define GPIO_ODR7                        	7						/*!< Port output data, bit 7 */
	#define GPIO_ODR8                        	8						/*!< Port output data, bit 8 */
	#define GPIO_ODR9                        	9						/*!< Port output data, bit 9 */
	#define GPIO_ODR10                       	10					/*!< Port output data, bit 10 */
	#define GPIO_ODR11                       	11					/*!< Port output data, bit 11 */
	#define GPIO_ODR12                       	12					/*!< Port output data, bit 12 */
	#define GPIO_ODR13                       	13					/*!< Port output data, bit 13 */
	#define GPIO_ODR14                       	14					/*!< Port output data, bit 14 */
	#define GPIO_ODR15                       	15					/*!< Port output data, bit 15 */

	/******************  Bit definition for GPIO_BSRR register  *******************/
	#define GPIO_BS0                        	0						/*!< Port x Set bit 0 */
	#define GPIO_BS1                        	1						/*!< Port x Set bit 1 */
	#define GPIO_BS2                        	2						/*!< Port x Set bit 2 */
	#define GPIO_BS3                        	3						/*!< Port x Set bit 3 */
	#define GPIO_BS4                        	4						/*!< Port x Set bit 4 */
	#define GPIO_BS5                        	5						/*!< Port x Set bit 5 */
	#define GPIO_BS6                        	6						/*!< Port x Set bit 6 */
	#define GPIO_BS7                        	7						/*!< Port x Set bit 7 */
	#define GPIO_BS8                        	8						/*!< Port x Set bit 8 */
	#define GPIO_BS9                        	9						/*!< Port x Set bit 9 */
	#define GPIO_BS10                       	10					/*!< Port x Set bit 10 */
	#define GPIO_BS11                       	11					/*!< Port x Set bit 11 */
	#define GPIO_BS12                       	12					/*!< Port x Set bit 12 */
	#define GPIO_BS13                       	13					/*!< Port x Set bit 13 */
	#define GPIO_BS14                       	14					/*!< Port x Set bit 14 */
	#define GPIO_BS15                       	15					/*!< Port x Set bit 15 */
	#define GPIO_BR0                        	16					/*!< Port x Reset bit 0 */
	#define GPIO_BR1                        	17					/*!< Port x Reset bit 1 */
	#define GPIO_BR2                        	18					/*!< Port x Reset bit 2 */
	#define GPIO_BR3                        	19					/*!< Port x Reset bit 3 */
	#define GPIO_BR4                        	20					/*!< Port x Reset bit 4 */
	#define GPIO_BR5                        	21					/*!< Port x Reset bit 5 */
	#define GPIO_BR6                        	22					/*!< Port x Reset bit 6 */
	#define GPIO_BR7                        	23					/*!< Port x Reset bit 7 */
	#define GPIO_BR8                        	24					/*!< Port x Reset bit 8 */
	#define GPIO_BR9                        	25					/*!< Port x Reset bit 9 */
	#define GPIO_BR10                       	26					/*!< Port x Reset bit 10 */
	#define GPIO_BR11                       	27					/*!< Port x Reset bit 11 */
	#define GPIO_BR12                       	28					/*!< Port x Reset bit 12 */
	#define GPIO_BR13                       	29					/*!< Port x Reset bit 13 */
	#define GPIO_BR14                       	30					/*!< Port x Reset bit 14 */
	#define GPIO_BR15                       	31					/*!< Port x Reset bit 15 */

	/*******************  Bit definition for GPIO_BRR register  *******************/
	#define GPIO_BRR0                         	0						/*!< Port x Reset bit 0 */
	#define GPIO_BRR1                         	1						/*!< Port x Reset bit 1 */
	#define GPIO_BRR2                         	2						/*!< Port x Reset bit 2 */
	#define GPIO_BRR3                         	3						/*!< Port x Reset bit 3 */
	#define GPIO_BRR4                         	4						/*!< Port x Reset bit 4 */
	#define GPIO_BRR5                         	5						/*!< Port x Reset bit 5 */
	#define GPIO_BRR6                         	6						/*!< Port x Reset bit 6 */
	#define GPIO_BRR7                         	7						/*!< Port x Reset bit 7 */
	#define GPIO_BRR8                         	8						/*!< Port x Reset bit 8 */
	#define GPIO_BRR9                         	9						/*!< Port x Reset bit 9 */
	#define GPIO_BRR10                        	10					/*!< Port x Reset bit 10 */
	#define GPIO_BRR11                        	11					/*!< Port x Reset bit 11 */
	#define GPIO_BRR12                        	12					/*!< Port x Reset bit 12 */
	#define GPIO_BRR13                        	13					/*!< Port x Reset bit 13 */
	#define GPIO_BRR14                        	14					/*!< Port x Reset bit 14 */
	#define GPIO_BRR15                        	15					/*!< Port x Reset bit 15 */

	/******************  Bit definition for GPIO_LCKR register  *******************/
	#define GPIO_LCK0                       	0						/*!< Port x Lock bit 0 */
	#define GPIO_LCK1                       	1						/*!< Port x Lock bit 1 */
	#define GPIO_LCK2                       	2						/*!< Port x Lock bit 2 */
	#define GPIO_LCK3                       	3						/*!< Port x Lock bit 3 */
	#define GPIO_LCK4                       	4						/*!< Port x Lock bit 4 */
	#define GPIO_LCK5                       	5						/*!< Port x Lock bit 5 */
	#define GPIO_LCK6                       	6						/*!< Port x Lock bit 6 */
	#define GPIO_LCK7                       	7						/*!< Port x Lock bit 7 */
	#define GPIO_LCK8                       	8						/*!< Port x Lock bit 8 */
	#define GPIO_LCK9                       	9						/*!< Port x Lock bit 9 */
	#define GPIO_LCK10                      	10					/*!< Port x Lock bit 10 */
	#define GPIO_LCK11                      	11					/*!< Port x Lock bit 11 */
	#define GPIO_LCK12                      	12					/*!< Port x Lock bit 12 */
	#define GPIO_LCK13                      	13					/*!< Port x Lock bit 13 */
	#define GPIO_LCK14                      	14					/*!< Port x Lock bit 14 */
	#define GPIO_LCK15                      	15					/*!< Port x Lock bit 15 */
	#define GPIO_LCKK                       	16					/*!< Lock key */

	/*----------------------------------------------------------------------------*/

	/******************  Bit definition for AFIO_EVCR register  *******************/
	#define AFIO_EVCR_PIN                        ((uint8_t)0x0F)               	/*!< PIN[3:0] bits (Pin selection) */
	#define AFIO_EVCR_PIN_0                      ((uint8_t)0x01)               	/*!< Bit 0 */
	#define AFIO_EVCR_PIN_1                      ((uint8_t)0x02)               	/*!< Bit 1 */
	#define AFIO_EVCR_PIN_2                      ((uint8_t)0x04)               	/*!< Bit 2 */
	#define AFIO_EVCR_PIN_3                      ((uint8_t)0x08)               	/*!< Bit 3 */

	/*!< PIN configuration */
	#define AFIO_EVCR_PIN_PX0                    ((uint8_t)0x00)               	/*!< Pin 0 selected */
	#define AFIO_EVCR_PIN_PX1                    ((uint8_t)0x01)               	/*!< Pin 1 selected */
	#define AFIO_EVCR_PIN_PX2                    ((uint8_t)0x02)               	/*!< Pin 2 selected */
	#define AFIO_EVCR_PIN_PX3                    ((uint8_t)0x03)               	/*!< Pin 3 selected */
	#define AFIO_EVCR_PIN_PX4                    ((uint8_t)0x04)               	/*!< Pin 4 selected */
	#define AFIO_EVCR_PIN_PX5                    ((uint8_t)0x05)               	/*!< Pin 5 selected */
	#define AFIO_EVCR_PIN_PX6                    ((uint8_t)0x06)               	/*!< Pin 6 selected */
	#define AFIO_EVCR_PIN_PX7                    ((uint8_t)0x07)               	/*!< Pin 7 selected */
	#define AFIO_EVCR_PIN_PX8                    ((uint8_t)0x08)               	/*!< Pin 8 selected */
	#define AFIO_EVCR_PIN_PX9                    ((uint8_t)0x09)               	/*!< Pin 9 selected */
	#define AFIO_EVCR_PIN_PX10                   ((uint8_t)0x0A)               	/*!< Pin 10 selected */
	#define AFIO_EVCR_PIN_PX11                   ((uint8_t)0x0B)               	/*!< Pin 11 selected */
	#define AFIO_EVCR_PIN_PX12                   ((uint8_t)0x0C)               	/*!< Pin 12 selected */
	#define AFIO_EVCR_PIN_PX13                   ((uint8_t)0x0D)               	/*!< Pin 13 selected */
	#define AFIO_EVCR_PIN_PX14                   ((uint8_t)0x0E)               	/*!< Pin 14 selected */
	#define AFIO_EVCR_PIN_PX15                   ((uint8_t)0x0F)               	/*!< Pin 15 selected */

	#define AFIO_EVCR_PORT                       ((uint8_t)0x70)               	/*!< PORT[2:0] bits (Port selection) */
	#define AFIO_EVCR_PORT_0                     ((uint8_t)0x10)               	/*!< Bit 0 */
	#define AFIO_EVCR_PORT_1                     ((uint8_t)0x20)               	/*!< Bit 1 */
	#define AFIO_EVCR_PORT_2                     ((uint8_t)0x40)               	/*!< Bit 2 */

	/*!< PORT configuration */
	#define AFIO_EVCR_PORT_PA                    ((uint8_t)0x00)               	/*!< Port A selected */
	#define AFIO_EVCR_PORT_PB                    ((uint8_t)0x10)               	/*!< Port B selected */
	#define AFIO_EVCR_PORT_PC                    ((uint8_t)0x20)               	/*!< Port C selected */
	#define AFIO_EVCR_PORT_PD                    ((uint8_t)0x30)               	/*!< Port D selected */
	#define AFIO_EVCR_PORT_PE                    ((uint8_t)0x40)               	/*!< Port E selected */

	#define AFIO_EVCR_EVOE                       ((uint8_t)0x80)               	/*!< Event Output Enable */

	/******************  Bit definition for AFIO_MAPR register  *******************/
	#define AFIO_MAPR_SPI1 _REMAP                (0x00000001)        	/*!< SPI1 remapping */
	#define AFIO_MAPR_I2C1_REMAP                 (0x00000002)        	/*!< I2C1 remapping */
	#define AFIO_MAPR_USART1_REMAP               (0x00000004)        	/*!< USART1 remapping */
	#define AFIO_MAPR_USART2_REMAP               (0x00000008)        	/*!< USART2 remapping */

	#define AFIO_MAPR_USART3_REMAP               (0x00000030)        	/*!< USART3_REMAP[1:0] bits (USART3 remapping) */
	#define AFIO_MAPR_USART3_REMAP_0             (0x00000010)        	/*!< Bit 0 */
	#define AFIO_MAPR_USART3_REMAP_1             (0x00000020)        	/*!< Bit 1 */

	/* USART3_REMAP configuration */
	#define AFIO_MAPR_USART3_REMAP_NOREMAP       (0x00000000)        	/*!< No remap (TX/PB10, RX/PB11, CK/PB12, CTS/PB13, RTS/PB14) */
	#define AFIO_MAPR_USART3_REMAP_PARTIALREMAP  (0x00000010)        	/*!< Partial remap (TX/PC10, RX/PC11, CK/PC12, CTS/PB13, RTS/PB14) */
	#define AFIO_MAPR_USART3_REMAP_FULLREMAP     (0x00000030)        	/*!< Full remap (TX/PD8, RX/PD9, CK/PD10, CTS/PD11, RTS/PD12) */

	#define AFIO_MAPR_TIM1_REMAP                 (0x000000C0)        	/*!< TIM1_REMAP[1:0] bits (TIM1 remapping) */
	#define AFIO_MAPR_TIM1_REMAP_0               (0x00000040)        	/*!< Bit 0 */
	#define AFIO_MAPR_TIM1_REMAP_1               (0x00000080)        	/*!< Bit 1 */

	/*!< TIM1_REMAP configuration */
	#define AFIO_MAPR_TIM1_REMAP_NOREMAP         (0x00000000)        	/*!< No remap (ETR/PA12, CH1/PA8, CH2/PA9, CH3/PA10, CH4/PA11, BKIN/PB12, CH1N/PB13, CH2N/PB14, CH3N/PB15) */
	#define AFIO_MAPR_TIM1_REMAP_PARTIALREMAP    (0x00000040)        	/*!< Partial remap (ETR/PA12, CH1/PA8, CH2/PA9, CH3/PA10, CH4/PA11, BKIN/PA6, CH1N/PA7, CH2N/PB0, CH3N/PB1) */
	#define AFIO_MAPR_TIM1_REMAP_FULLREMAP       (0x000000C0)        	/*!< Full remap (ETR/PE7, CH1/PE9, CH2/PE11, CH3/PE13, CH4/PE14, BKIN/PE15, CH1N/PE8, CH2N/PE10, CH3N/PE12) */

	#define AFIO_MAPR_TIM2_REMAP                 (0x00000300)        	/*!< TIM2_REMAP[1:0] bits (TIM2 remapping) */
	#define AFIO_MAPR_TIM2_REMAP_0               (0x00000100)        	/*!< Bit 0 */
	#define AFIO_MAPR_TIM2_REMAP_1               (0x00000200)        	/*!< Bit 1 */

	/*!< TIM2_REMAP configuration */
	#define AFIO_MAPR_TIM2_REMAP_NOREMAP         (0x00000000)        	/*!< No remap (CH1/ETR/PA0, CH2/PA1, CH3/PA2, CH4/PA3) */
	#define AFIO_MAPR_TIM2_REMAP_PARTIALREMAP1   (0x00000100)        	/*!< Partial remap (CH1/ETR/PA15, CH2/PB3, CH3/PA2, CH4/PA3) */
	#define AFIO_MAPR_TIM2_REMAP_PARTIALREMAP2   (0x00000200)        	/*!< Partial remap (CH1/ETR/PA0, CH2/PA1, CH3/PB10, CH4/PB11) */
	#define AFIO_MAPR_TIM2_REMAP_FULLREMAP       (0x00000300)        	/*!< Full remap (CH1/ETR/PA15, CH2/PB3, CH3/PB10, CH4/PB11) */

	#define AFIO_MAPR_TIM3_REMAP                 (0x00000C00)        	/*!< TIM3_REMAP[1:0] bits (TIM3 remapping) */
	#define AFIO_MAPR_TIM3_REMAP_0               (0x00000400)        	/*!< Bit 0 */
	#define AFIO_MAPR_TIM3_REMAP_1               (0x00000800)        	/*!< Bit 1 */

	/*!< TIM3_REMAP configuration */
	#define AFIO_MAPR_TIM3_REMAP_NOREMAP         (0x00000000)        	/*!< No remap (CH1/PA6, CH2/PA7, CH3/PB0, CH4/PB1) */
	#define AFIO_MAPR_TIM3_REMAP_PARTIALREMAP    (0x00000800)        	/*!< Partial remap (CH1/PB4, CH2/PB5, CH3/PB0, CH4/PB1) */
	#define AFIO_MAPR_TIM3_REMAP_FULLREMAP       (0x00000C00)        	/*!< Full remap (CH1/PC6, CH2/PC7, CH3/PC8, CH4/PC9) */

	#define AFIO_MAPR_TIM4_REMAP                 (0x00001000)        	/*!< Port D0/Port D1 mapping on OSC_IN/OSC_OUT */

	#define AFIO_MAPR_CAN_REMAP                  (0x00006000)        	/*!< CAN_REMAP[1:0] bits (CAN Alternate function remapping) */
	#define AFIO_MAPR_CAN_REMAP_0                (0x00002000)        	/*!< Bit 0 */
	#define AFIO_MAPR_CAN_REMAP_1                (0x00004000)        	/*!< Bit 1 */

	/*!< CAN_REMAP configuration */
	#define AFIO_MAPR_CAN_REMAP_REMAP1           (0x00000000)        	/*!< CANRX mapped to PA11, CANTX mapped to PA12 */
	#define AFIO_MAPR_CAN_REMAP_REMAP2           (0x00004000)        	/*!< CANRX mapped to PB8, CANTX mapped to PB9 */
	#define AFIO_MAPR_CAN_REMAP_REMAP3           (0x00006000)        	/*!< CANRX mapped to PD0, CANTX mapped to PD1 */

	#define AFIO_MAPR_PD01_REMAP                 (0x00008000)        	/*!< Port D0/Port D1 mapping on OSC_IN/OSC_OUT */
	#define AFIO_MAPR_TIM5CH4_IREMAP             (0x00010000)        	/*!< TIM5 Channel4 Internal Remap */
	#define AFIO_MAPR_ADC1_ETRGINJ_REMAP         (0x00020000)        	/*!< ADC 1 External Trigger Injected Conversion remapping */
	#define AFIO_MAPR_ADC1_ETRGREG_REMAP         (0x00040000)        	/*!< ADC 1 External Trigger Regular Conversion remapping */
	#define AFIO_MAPR_ADC2_ETRGINJ_REMAP         (0x00080000)        	/*!< ADC 2 External Trigger Injected Conversion remapping */
	#define AFIO_MAPR_ADC2_ETRGREG_REMAP         (0x00100000)        	/*!< ADC 2 External Trigger Regular Conversion remapping */

	#define AFIO_MAPR_SWJ_CFG                    (0x07000000)        	/*!< SWJ_CFG[2:0] bits (Serial Wire JTAG configuration) */
	#define AFIO_MAPR_SWJ_CFG_0                  (0x01000000)        	/*!< Bit 0 */
	#define AFIO_MAPR_SWJ_CFG_1                  (0x02000000)        	/*!< Bit 1 */
	#define AFIO_MAPR_SWJ_CFG_2                  (0x04000000)        	/*!< Bit 2 */

	/*!< SWJ_CFG configuration */
	#define AFIO_MAPR_SWJ_CFG_RESET              (0x00000000)        	/*!< Full SWJ (JTAG-DP + SW-DP) : Reset State */
	#define AFIO_MAPR_SWJ_CFG_NOJNTRST           (0x01000000)        	/*!< Full SWJ (JTAG-DP + SW-DP) but without JNTRST */
	#define AFIO_MAPR_SWJ_CFG_JTAGDISABLE        (0x02000000)        	/*!< JTAG-DP Disabled and SW-DP Enabled */
	#define AFIO_MAPR_SWJ_CFG_DISABLE            (0x04000000)        	/*!< JTAG-DP Disabled and SW-DP Disabled */

	
/*****************  Bit definition for AFIO_EXTICR1 register  *****************/
	#define EXTI0                   0							            	/*!< EXTI 0 configuration */
	#define EXTI1                   4							            	/*!< EXTI 1 configuration */
	#define EXTI2                   8							            	/*!< EXTI 2 configuration */
	#define EXTI3                   12							           	/*!< EXTI 3 configuration */
	#define EXTI4                   0							            	/*!< EXTI 4 configuration */
	#define EXTI5                   4							            	/*!< EXTI 5 configuration */
	#define EXTI6                   8							            	/*!< EXTI 6 configuration */
	#define EXTI7                   12							           	/*!< EXTI 7 configuration */
	#define EXTI8                   0							            	/*!< EXTI 8 configuration */
	#define EXTI9                   4							            	/*!< EXTI 9 configuration */
	#define EXTI10                  8							            	/*!< EXTI 10 configuration */
	#define EXTI11                  12							           	/*!< EXTI 11 configuration */
	#define EXTI12                  0							            	/*!< EXTI 12 configuration */
	#define EXTI13                  4							            	/*!< EXTI 13 configuration */
	#define EXTI14                  8							            	/*!< EXTI 14 configuration */
	#define EXTI15                  12							           	/*!< EXTI 15 configuration */


	/******************************************************************************/
	/*                                                                            */
	/*                               SystemTick                                   */
	/*                                                                            */
	/******************************************************************************/

	/*****************  Bit definition for SysTick_CTRL register  *****************/
	#define  SysTick_ENABLE                 	0	        	/*!< Counter enable */
	#define  SysTick_TICKINT               		1	        	/*!< Counting down to 0 pends the SysTick handler */
	#define  SysTick_CLKSOURCE             		2 	       	/*!< Clock source */
	#define  SysTick_COUNTFLAG             		16        	/*!< Count Flag */

	/*****************  Bit definition for SysTick_CALIB register  ****************/
	#define  SysTick_CALIB_TENMS                 (0x00FFFFFF)        	/*!< Reload value to use for 10ms timing */
	#define  SysTick_SKEW                 		30        	/*!< Calibration value is not exactly 10 ms */
	#define  SysTick_NOREF                		31        	/*!< The reference clock is not provided */

	/******************************************************************************/
	/*                                                                            */
	/*                  Nested Vectored Interrupt Controller                      */
	/*                                                                            */
	/******************************************************************************/

	/******************  Bit definition for NVIC_ISER register  *******************/
	#define  NVIC_EnableWWDG                  	0        		/*Enable WWDG Interrupt */
	#define  NVIC_EnablePVD                  	1       		/*Enable PVD Interrupt */
	#define  NVIC_EnableTAMPER                  2	        	/* */
	#define  NVIC_EnableRTC                  	3        		/**/
	#define  NVIC_EnableFLASH                  	4       		/*!< bit 4 */
	#define  NVIC_EnableRCC                  	5        		/*!< bit 5 */
	#define  NVIC_EnableEXTI0                  	6	       		/*!< bit 6 */
	#define  NVIC_EnableEXTI1                  	7	       		/*!< bit 6 */
	#define  NVIC_EnableEXTI2                  	8	       		/*!< bit 6 */
	#define  NVIC_EnableEXTI3                  	9	       		/*!< bit 6 */
	#define  NVIC_EnableEXTI4                  	10       		/*!< bit 6 */
	#define  NVIC_EnableDMA11                  	11        	/*!< bit 7 */
	#define  NVIC_EnableDMA12                  	12        	/*!< bit 7 */
	#define  NVIC_EnableDMA13                  	13        	/*!< bit 7 */
	#define  NVIC_EnableDMA14                  	14        	/*!< bit 7 */
	#define  NVIC_EnableDMA15                  	15        	/*!< bit 7 */
	#define  NVIC_EnableDMA16                  	16        	/*!< bit 7 */
	#define  NVIC_EnableDMA17                  	17        	/*!< bit 7 */
	#define  NVIC_EnableADC                  	18        	/*!< bit 8 */
	#define  NVIC_EnableUSBTX                  	19        	/*!< bit 9 */
	#define  NVIC_EnableUSBRX                 	20        	/*!< bit 10 */
	#define  NVIC_EnableCANRX1                 	21        	/*!< bit 11 */
	#define  NVIC_EnableCANSCE                 	22        	/*!< bit 12 */
	#define  NVIC_EnableEXTI5_9                 23        	/*!< bit 13 */
	#define  NVIC_EnableTIM1_BRK                24        	/*!< bit 14 */
	#define  NVIC_EnableTIM1_UP                 25        	/*!< bit 15 */
	#define  NVIC_EnableTIM1_TRG_COM            26        	/*!< bit 16 */
	#define  NVIC_EnableTIM1_CC                 27        	/*!< bit 17 */
	#define  NVIC_EnableTIM2                 	28        	/*!< bit 18 */
	#define  NVIC_EnableTIM3                 	29        	/*!< bit 18 */
	#define  NVIC_EnableTIM4                 	30        	/*!< bit 18 */
	#define  NVIC_EnableI2C1_EV                 31        	/*!< bit 19 */
	#define  NVIC_EnableI2C1_ER                 0	        	/*!< bit 19 */
	#define  NVIC_EnableI2C2_EV                 1	        	/*!< bit 19 */
	#define  NVIC_EnableI2C2_ER                 2 	       	/*!< bit 19 */
	#define  NVIC_EnableSPI1                  	3        		/**/
	#define  NVIC_EnableSPI2                  	4       		/*!< bit 4 */
	#define  NVIC_EnableUSART1                 	5        		/*!< bit 5 */
	#define  NVIC_EnableUSART2                 	6	       		/*!< bit 6 */
	#define  NVIC_EnableUSART3                 	7	       		/*!< bit 6 */
	#define  NVIC_EnableEXTI10_15              	8	       		/*!< bit 6 */
	#define  NVIC_EnableRTCAlarm               	9	       		/*!< bit 6 */
	#define  NVIC_EnableUSB_Weak               	10       		/*!< bit 6 */
	#define  NVIC_EnableTIM8_BRK               	11        	/*!< bit 7 */
	#define  NVIC_EnableTIM8_UP                	12        	/*!< bit 7 */
	#define  NVIC_EnableTIM8_TRG_COM           	13        	/*!< bit 7 */
	#define  NVIC_EnableTIM8_CC                	14        	/*!< bit 7 */
	#define  NVIC_EnableADC3                  	15        	/*!< bit 7 */
	#define  NVIC_EnableFSMC                  	16        	/*!< bit 7 */
	#define  NVIC_EnableSDIO                  	17        	/*!< bit 7 */
	#define  NVIC_EnableTIM5                  	18        	/*!< bit 8 */
	#define	 NVIC_EnableSPI3					19        	/*!< bit 8 */
	#define  NVIC_EnableUART4                  	20        	/*!< bit 9 */									   
	#define  NVIC_EnableUART5                 	21        	/*!< bit 10 */
	#define  NVIC_EnableTIM6                 	22        	/*!< bit 11 */
	#define  NVIC_EnableTIM7                 	23        	/*!< bit 12 */
	#define  NVIC_EnableDMA21	                24        	/*!< bit 13 */
	#define  NVIC_EnableDMA22	                25        	/*!< bit 14 */
	#define  NVIC_EnableDMA23  		            26        	/*!< bit 15 */
	#define  NVIC_EnableDMA24_5            		27        	/*!< bit 16 */


	/******************  Bit definition for NVIC_ICER register  *******************/
	#define  NVIC_ICER_CLRENA                   (0xFFFFFFFF)        	/*!< Interrupt clear-enable bits */
	#define  NVIC_ICER_CLRENA_0                  (0x00000001)        	/*!< bit 0 */
	#define  NVIC_ICER_CLRENA_1                  (0x00000002)        	/*!< bit 1 */
	#define  NVIC_ICER_CLRENA_2                  (0x00000004)        	/*!< bit 2 */
	#define  NVIC_ICER_CLRENA_3                  (0x00000008)        	/*!< bit 3 */
	#define  NVIC_ICER_CLRENA_4                  (0x00000010)        	/*!< bit 4 */
	#define  NVIC_ICER_CLRENA_5                  (0x00000020)        	/*!< bit 5 */
	#define  NVIC_ICER_CLRENA_6                  (0x00000040)        	/*!< bit 6 */
	#define  NVIC_ICER_CLRENA_7                  (0x00000080)        	/*!< bit 7 */
	#define  NVIC_ICER_CLRENA_8                  (0x00000100)        	/*!< bit 8 */
	#define  NVIC_ICER_CLRENA_9                  (0x00000200)        	/*!< bit 9 */
	#define  NVIC_ICER_CLRENA_10                 (0x00000400)        	/*!< bit 10 */
	#define  NVIC_ICER_CLRENA_11                 (0x00000800)        	/*!< bit 11 */
	#define  NVIC_ICER_CLRENA_12                 (0x00001000)        	/*!< bit 12 */
	#define  NVIC_ICER_CLRENA_13                 (0x00002000)        	/*!< bit 13 */
	#define  NVIC_ICER_CLRENA_14                 (0x00004000)        	/*!< bit 14 */
	#define  NVIC_ICER_CLRENA_15                 (0x00008000)        	/*!< bit 15 */
	#define  NVIC_ICER_CLRENA_16                 (0x00010000)        	/*!< bit 16 */
	#define  NVIC_ICER_CLRENA_17                 (0x00020000)        	/*!< bit 17 */
	#define  NVIC_ICER_CLRENA_18                 (0x00040000)        	/*!< bit 18 */
	#define  NVIC_ICER_CLRENA_19                 (0x00080000)        	/*!< bit 19 */
	#define  NVIC_ICER_CLRENA_20                 (0x00100000)        	/*!< bit 20 */
	#define  NVIC_ICER_CLRENA_21                 (0x00200000)        	/*!< bit 21 */
	#define  NVIC_ICER_CLRENA_22                 (0x00400000)        	/*!< bit 22 */
	#define  NVIC_ICER_CLRENA_23                 (0x00800000)        	/*!< bit 23 */
	#define  NVIC_ICER_CLRENA_24                 (0x01000000)        	/*!< bit 24 */
	#define  NVIC_ICER_CLRENA_25                 (0x02000000)        	/*!< bit 25 */
	#define  NVIC_ICER_CLRENA_26                 (0x04000000)        	/*!< bit 26 */
	#define  NVIC_ICER_CLRENA_27                 (0x08000000)        	/*!< bit 27 */
	#define  NVIC_ICER_CLRENA_28                 (0x10000000)        	/*!< bit 28 */
	#define  NVIC_ICER_CLRENA_29                 (0x20000000)        	/*!< bit 29 */
	#define  NVIC_ICER_CLRENA_30                 (0x40000000)        	/*!< bit 30 */
	#define  NVIC_ICER_CLRENA_31                 (0x80000000)        	/*!< bit 31 */

	/******************  Bit definition for NVIC_ISPR register  *******************/
	#define  NVIC_ISPR_SETPEND                   (0xFFFFFFFF)        	/*!< Interrupt set-pending bits */
	#define  NVIC_ISPR_SETPEND_0                 (0x00000001)        	/*!< bit 0 */
	#define  NVIC_ISPR_SETPEND_1                 (0x00000002)        	/*!< bit 1 */
	#define  NVIC_ISPR_SETPEND_2                 (0x00000004)        	/*!< bit 2 */
	#define  NVIC_ISPR_SETPEND_3                 (0x00000008)        	/*!< bit 3 */
	#define  NVIC_ISPR_SETPEND_4                 (0x00000010)        	/*!< bit 4 */
	#define  NVIC_ISPR_SETPEND_5                 (0x00000020)        	/*!< bit 5 */
	#define  NVIC_ISPR_SETPEND_6                 (0x00000040)        	/*!< bit 6 */
	#define  NVIC_ISPR_SETPEND_7                 (0x00000080)        	/*!< bit 7 */
	#define  NVIC_ISPR_SETPEND_8                 (0x00000100)        	/*!< bit 8 */
	#define  NVIC_ISPR_SETPEND_9                 (0x00000200)        	/*!< bit 9 */
	#define  NVIC_ISPR_SETPEND_10                (0x00000400)        	/*!< bit 10 */
	#define  NVIC_ISPR_SETPEND_11                (0x00000800)        	/*!< bit 11 */
	#define  NVIC_ISPR_SETPEND_12                (0x00001000)        	/*!< bit 12 */
	#define  NVIC_ISPR_SETPEND_13                (0x00002000)        	/*!< bit 13 */
	#define  NVIC_ISPR_SETPEND_14                (0x00004000)        	/*!< bit 14 */
	#define  NVIC_ISPR_SETPEND_15                (0x00008000)        	/*!< bit 15 */
	#define  NVIC_ISPR_SETPEND_16                (0x00010000)        	/*!< bit 16 */
	#define  NVIC_ISPR_SETPEND_17                (0x00020000)        	/*!< bit 17 */
	#define  NVIC_ISPR_SETPEND_18                (0x00040000)        	/*!< bit 18 */
	#define  NVIC_ISPR_SETPEND_19                (0x00080000)        	/*!< bit 19 */
	#define  NVIC_ISPR_SETPEND_20                (0x00100000)        	/*!< bit 20 */
	#define  NVIC_ISPR_SETPEND_21                (0x00200000)        	/*!< bit 21 */
	#define  NVIC_ISPR_SETPEND_22                (0x00400000)        	/*!< bit 22 */
	#define  NVIC_ISPR_SETPEND_23                (0x00800000)        	/*!< bit 23 */
	#define  NVIC_ISPR_SETPEND_24                (0x01000000)        	/*!< bit 24 */
	#define  NVIC_ISPR_SETPEND_25                (0x02000000)        	/*!< bit 25 */
	#define  NVIC_ISPR_SETPEND_26                (0x04000000)        	/*!< bit 26 */
	#define  NVIC_ISPR_SETPEND_27                (0x08000000)        	/*!< bit 27 */
	#define  NVIC_ISPR_SETPEND_28                (0x10000000)        	/*!< bit 28 */
	#define  NVIC_ISPR_SETPEND_29                (0x20000000)        	/*!< bit 29 */
	#define  NVIC_ISPR_SETPEND_30                (0x40000000)        	/*!< bit 30 */
	#define  NVIC_ISPR_SETPEND_31                (0x80000000)        	/*!< bit 31 */

	/******************  Bit definition for NVIC_ICPR register  *******************/
	#define  NVIC_ICPR_CLRPEND                   (0xFFFFFFFF)        	/*!< Interrupt clear-pending bits */
	#define  NVIC_ICPR_CLRPEND_0                 (0x00000001)        	/*!< bit 0 */
	#define  NVIC_ICPR_CLRPEND_1                 (0x00000002)        	/*!< bit 1 */
	#define  NVIC_ICPR_CLRPEND_2                 (0x00000004)        	/*!< bit 2 */
	#define  NVIC_ICPR_CLRPEND_3                 (0x00000008)        	/*!< bit 3 */
	#define  NVIC_ICPR_CLRPEND_4                 (0x00000010)        	/*!< bit 4 */
	#define  NVIC_ICPR_CLRPEND_5                 (0x00000020)        	/*!< bit 5 */
	#define  NVIC_ICPR_CLRPEND_6                 (0x00000040)        	/*!< bit 6 */
	#define  NVIC_ICPR_CLRPEND_7                 (0x00000080)        	/*!< bit 7 */
	#define  NVIC_ICPR_CLRPEND_8                 (0x00000100)        	/*!< bit 8 */
	#define  NVIC_ICPR_CLRPEND_9                 (0x00000200)        	/*!< bit 9 */
	#define  NVIC_ICPR_CLRPEND_10                (0x00000400)        	/*!< bit 10 */
	#define  NVIC_ICPR_CLRPEND_11                (0x00000800)        	/*!< bit 11 */
	#define  NVIC_ICPR_CLRPEND_12                (0x00001000)        	/*!< bit 12 */
	#define  NVIC_ICPR_CLRPEND_13                (0x00002000)        	/*!< bit 13 */
	#define  NVIC_ICPR_CLRPEND_14                (0x00004000)        	/*!< bit 14 */
	#define  NVIC_ICPR_CLRPEND_15                (0x00008000)        	/*!< bit 15 */
	#define  NVIC_ICPR_CLRPEND_16                (0x00010000)        	/*!< bit 16 */
	#define  NVIC_ICPR_CLRPEND_17                (0x00020000)        	/*!< bit 17 */
	#define  NVIC_ICPR_CLRPEND_18                (0x00040000)        	/*!< bit 18 */
	#define  NVIC_ICPR_CLRPEND_19                (0x00080000)        	/*!< bit 19 */
	#define  NVIC_ICPR_CLRPEND_20                (0x00100000)        	/*!< bit 20 */
	#define  NVIC_ICPR_CLRPEND_21                (0x00200000)        	/*!< bit 21 */
	#define  NVIC_ICPR_CLRPEND_22                (0x00400000)        	/*!< bit 22 */
	#define  NVIC_ICPR_CLRPEND_23                (0x00800000)        	/*!< bit 23 */
	#define  NVIC_ICPR_CLRPEND_24                (0x01000000)        	/*!< bit 24 */
	#define  NVIC_ICPR_CLRPEND_25                (0x02000000)        	/*!< bit 25 */
	#define  NVIC_ICPR_CLRPEND_26                (0x04000000)        	/*!< bit 26 */
	#define  NVIC_ICPR_CLRPEND_27                (0x08000000)        	/*!< bit 27 */
	#define  NVIC_ICPR_CLRPEND_28                (0x10000000)        	/*!< bit 28 */
	#define  NVIC_ICPR_CLRPEND_29                (0x20000000)        	/*!< bit 29 */
	#define  NVIC_ICPR_CLRPEND_30                (0x40000000)        	/*!< bit 30 */
	#define  NVIC_ICPR_CLRPEND_31                (0x80000000)        	/*!< bit 31 */

	/******************  Bit definition for NVIC_IABR register  *******************/
	#define  NVIC_IABR_ACTIVE                    (0xFFFFFFFF)        	/*!< Interrupt active flags */
	#define  NVIC_IABR_ACTIVE_0                  (0x00000001)        	/*!< bit 0 */
	#define  NVIC_IABR_ACTIVE_1                  (0x00000002)        	/*!< bit 1 */
	#define  NVIC_IABR_ACTIVE_2                  (0x00000004)        	/*!< bit 2 */
	#define  NVIC_IABR_ACTIVE_3                  (0x00000008)        	/*!< bit 3 */
	#define  NVIC_IABR_ACTIVE_4                  (0x00000010)        	/*!< bit 4 */
	#define  NVIC_IABR_ACTIVE_5                  (0x00000020)        	/*!< bit 5 */
	#define  NVIC_IABR_ACTIVE_6                  (0x00000040)        	/*!< bit 6 */
	#define  NVIC_IABR_ACTIVE_7                  (0x00000080)        	/*!< bit 7 */
	#define  NVIC_IABR_ACTIVE_8                  (0x00000100)        	/*!< bit 8 */
	#define  NVIC_IABR_ACTIVE_9                  (0x00000200)        	/*!< bit 9 */
	#define  NVIC_IABR_ACTIVE_10                 (0x00000400)        	/*!< bit 10 */
	#define  NVIC_IABR_ACTIVE_11                 (0x00000800)        	/*!< bit 11 */
	#define  NVIC_IABR_ACTIVE_12                 (0x00001000)        	/*!< bit 12 */
	#define  NVIC_IABR_ACTIVE_13                 (0x00002000)        	/*!< bit 13 */
	#define  NVIC_IABR_ACTIVE_14                 (0x00004000)        	/*!< bit 14 */
	#define  NVIC_IABR_ACTIVE_15                 (0x00008000)        	/*!< bit 15 */
	#define  NVIC_IABR_ACTIVE_16                 (0x00010000)        	/*!< bit 16 */
	#define  NVIC_IABR_ACTIVE_17                 (0x00020000)        	/*!< bit 17 */
	#define  NVIC_IABR_ACTIVE_18                 (0x00040000)        	/*!< bit 18 */
	#define  NVIC_IABR_ACTIVE_19                 (0x00080000)        	/*!< bit 19 */
	#define  NVIC_IABR_ACTIVE_20                 (0x00100000)        	/*!< bit 20 */
	#define  NVIC_IABR_ACTIVE_21                 (0x00200000)        	/*!< bit 21 */
	#define  NVIC_IABR_ACTIVE_22                 (0x00400000)        	/*!< bit 22 */
	#define  NVIC_IABR_ACTIVE_23                 (0x00800000)        	/*!< bit 23 */
	#define  NVIC_IABR_ACTIVE_24                 (0x01000000)        	/*!< bit 24 */
	#define  NVIC_IABR_ACTIVE_25                 (0x02000000)        	/*!< bit 25 */
	#define  NVIC_IABR_ACTIVE_26                 (0x04000000)        	/*!< bit 26 */
	#define  NVIC_IABR_ACTIVE_27                 (0x08000000)        	/*!< bit 27 */
	#define  NVIC_IABR_ACTIVE_28                 (0x10000000)        	/*!< bit 28 */
	#define  NVIC_IABR_ACTIVE_29                 (0x20000000)        	/*!< bit 29 */
	#define  NVIC_IABR_ACTIVE_30                 (0x40000000)        	/*!< bit 30 */
	#define  NVIC_IABR_ACTIVE_31                 (0x80000000)        	/*!< bit 31 */

	/******************  Bit definition for NVIC_PRI0 register  *******************/
	#define  NVIC_PRI00                     	0	       		/*!< Priority of interrupt 0 */
	#define  NVIC_PRI01                     	8     	   	/*!< Priority of interrupt 1 */
	#define  NVIC_PRI02                     	16	       	/*!< Priority of interrupt 2 */
	#define  NVIC_PRI03                     	24        	/*!< Priority of interrupt 3 */
	#define  NVIC_PRI04                   		0			   	/*!< Priority of interrupt 4 */
	#define  NVIC_PRI05                   		8    			/*!< Priority of interrupt 5 */
	#define  NVIC_PRI06                   		16   			/*!< Priority of interrupt 6 */
	#define  NVIC_PRI07                   		24   			/*!< Priority of interrupt 7 */
	#define  NVIC_PRI08                   		0	   			/*!< Priority of interrupt 8 */
	#define  NVIC_PRI09                   		8    			/*!< Priority of interrupt 9 */
	#define  NVIC_PRI10                   		16  			/*!< Priority of interrupt 10 */
	#define  NVIC_PRI11                   		24  			/*!< Priority of interrupt 11 */
	#define  NVIC_PRI12                     	0				/*!< Priority of interrupt 12 */
	#define  NVIC_PRI13                     	8 			/*!< Priority of interrupt 13 */
	#define  NVIC_PRI14                     	16			/*!< Priority of interrupt 14 */
	#define  NVIC_PRI15                     	24			/*!< Priority of interrupt 15 */
	#define  NVIC_PRI16                     	0				/*!< Priority of interrupt 16 */
	#define  NVIC_PRI17                     	8 			/*!< Priority of interrupt 17 */
	#define  NVIC_PRI18                     	16			/*!< Priority of interrupt 18 */
	#define  NVIC_PRI19                     	24			/*!< Priority of interrupt 19 */
	#define  NVIC_PRI20                     	0				/*!< Priority of interrupt 20 */
	#define  NVIC_PRI21                     	8 			/*!< Priority of interrupt 21 */
	#define  NVIC_PRI22                     	16			/*!< Priority of interrupt 22 */
	#define  NVIC_PRI23                     	24			/*!< Priority of interrupt 23 */
	#define  NVIC_PRI24                     	0				/*!< Priority of interrupt 24 */
	#define  NVIC_PRI25                     	8 			/*!< Priority of interrupt 25 */
	#define  NVIC_PRI26                     	16			/*!< Priority of interrupt 26 */
	#define  NVIC_PRI27                     	24			/*!< Priority of interrupt 27 */
	#define  NVIC_PRI28                     	0				/*!< Priority of interrupt 28 */
	#define  NVIC_PRI29                     	8 			/*!< Priority of interrupt 29 */
	#define  NVIC_PRI30                     	16			/*!< Priority of interrupt 30 */
	#define  NVIC_PRI31                     	24			/*!< Priority of interrupt 31 */
	#define  NVIC_PRI32                     	0				/*!< Priority of interrupt 32 */
	#define  NVIC_PRI33                     	8 			/*!< Priority of interrupt 33 */
	#define  NVIC_PRI34                     	16			/*!< Priority of interrupt 34 */
	#define  NVIC_PRI35                     	24			/*!< Priority of interrupt 35 */
	#define  NVIC_PRI36                     	0				/*!< Priority of interrupt 36 */
	#define  NVIC_PRI37                     	8 			/*!< Priority of interrupt 37 */
	#define  NVIC_PRI38                     	16			/*!< Priority of interrupt 38 */
	#define  NVIC_PRI39                     	24			/*!< Priority of interrupt 39 */
	#define  NVIC_PRI40                     	0				/*!< Priority of interrupt 40 */
	#define  NVIC_PRI41                     	8 			/*!< Priority of interrupt 41 */
	#define  NVIC_PRI42                     	16			/*!< Priority of interrupt 42 */
	#define  NVIC_PRI43                     	24			/*!< Priority of interrupt 43 */
	#define  NVIC_PRI44                     	0				/*!< Priority of interrupt 44 */
	#define  NVIC_PRI45                     	8 			/*!< Priority of interrupt 45 */
	#define  NVIC_PRI46                     	16			/*!< Priority of interrupt 46 */
	#define  NVIC_PRI47                     	24			/*!< Priority of interrupt 47 */
	#define  NVIC_PRI48                     	0				/*!< Priority of interrupt 48 */
	#define  NVIC_PRI49                     	8 			/*!< Priority of interrupt 49 */
	#define  NVIC_PRI50                     	16			/*!< Priority of interrupt 50 */
	#define  NVIC_PRI51                     	24			/*!< Priority of interrupt 51 */
	#define  NVIC_PRI52                     	0				/*!< Priority of interrupt 52 */
	#define  NVIC_PRI53                     	8				/*!< Priority of interrupt 53 */
	#define  NVIC_PRI54                     	16			/*!< Priority of interrupt 54 */
	#define  NVIC_PRI55                     	24			/*!< Priority of interrupt 55 */
	#define  NVIC_PRI56                     	0				/*!< Priority of interrupt 56 */
	#define  NVIC_PRI57                     	8				/*!< Priority of interrupt 57 */
	#define  NVIC_PRI58                     	16			/*!< Priority of interrupt 58 */
	#define  NVIC_PRI59                     	24			/*!< Priority of interrupt 59 */
	

	/******************  Bit definition for SCB_CPUID register  *******************/
	#define  SCB_CPUID_REVISION                  ((uint32_t)0x0000000F)        /*!< Implementation defined revision number */
	#define  SCB_CPUID_PARTNO                    ((uint32_t)0x0000FFF0)        /*!< Number of processor within family */
	#define  SCB_CPUID_Constant                  ((uint32_t)0x000F0000)        /*!< Reads as 0x0F */
	#define  SCB_CPUID_VARIANT                   ((uint32_t)0x00F00000)        /*!< Implementation defined variant number */
	#define  SCB_CPUID_IMPLEMENTER               ((uint32_t)0xFF000000)        /*!< Implementer code. ARM is 0x41 */

	/*******************  Bit definition for SCB_ICSR register  *******************/
	#define  SCB_ICSR_VECTACTIVE                 (0x000001FF)        	/*!< Active ISR number field */
	#define  SCB_ICSR_RETTOBASE                  (0x00000800)        	/*!< All active exceptions minus the IPSR_current_exception yields the empty set */
	#define  SCB_ICSR_VECTPENDING                (0x003FF000)        	/*!< Pending ISR number field */
	#define  SCB_ICSR_ISRPENDING                 (0x00400000)        	/*!< Interrupt pending flag */
	#define  SCB_ICSR_ISRPREEMPT                 (0x00800000)        	/*!< It indicates that a pending interrupt becomes active in the next running cycle */
	#define  SCB_ICSR_PENDSTCLR                  (0x02000000)        	/*!< Clear pending SysTick bit */
	#define  SCB_ICSR_PENDSTSET                  (0x04000000)        	/*!< Set pending SysTick bit */
	#define  SCB_ICSR_PENDSVCLR                  (0x08000000)        	/*!< Clear pending pendSV bit */
	#define  SCB_ICSR_PENDSVSET                  (0x10000000)        	/*!< Set pending pendSV bit */
	#define  SCB_ICSR_NMIPENDSET                 (0x80000000)        	/*!< Set pending NMI bit */

	/*******************  Bit definition for SCB_VTOR register  *******************/
	#define  SCB_VTOR_TBLOFF                     (0x1FFFFF80)        	/*!< Vector table base offset field */
	#define  SCB_VTOR_TBLBASE                    (0x20000000)        	/*!< Table base in code(0) or RAM(1) */

	/*!<*****************  Bit definition for SCB_AIRCR register  *******************/
	#define  SCB_AIRCR_VECTRESET                 (0x00000001)        	/*!< System Reset bit */
	#define  SCB_AIRCR_VECTCLRACTIVE             (0x00000002)        	/*!< Clear active vector bit */
	#define  SCB_AIRCR_SYSRESETREQ               (0x00000004)        	/*!< Requests chip control logic to generate a reset */

	#define  SCB_AIRCR_PRIGROUP                  (0x00000700)        	/*!< PRIGROUP[2:0] bits (Priority group) */
	#define  SCB_AIRCR_PRIGROUP_0                (0x00000100)        	/*!< Bit 0 */
	#define  SCB_AIRCR_PRIGROUP_1                (0x00000200)        	/*!< Bit 1 */
	#define  SCB_AIRCR_PRIGROUP_2                (0x00000400)        	/*!< Bit 2  */

	/* prority group configuration */
	#define  SCB_AIRCR_PRIGROUP0                 (0x00000000)        	/*!< Priority group=0 (7 bits of pre-emption priority, 1 bit of subpriority) */
	#define  SCB_AIRCR_PRIGROUP1                 (0x00000100)        	/*!< Priority group=1 (6 bits of pre-emption priority, 2 bits of subpriority) */
	#define  SCB_AIRCR_PRIGROUP2                 (0x00000200)        	/*!< Priority group=2 (5 bits of pre-emption priority, 3 bits of subpriority) */
	#define  SCB_AIRCR_PRIGROUP3                 (0x00000300)        	/*!< Priority group=3 (4 bits of pre-emption priority, 4 bits of subpriority) */
	#define  SCB_AIRCR_PRIGROUP4                 (0x00000400)        	/*!< Priority group=4 (3 bits of pre-emption priority, 5 bits of subpriority) */
	#define  SCB_AIRCR_PRIGROUP5                 (0x00000500)        	/*!< Priority group=5 (2 bits of pre-emption priority, 6 bits of subpriority) */
	#define  SCB_AIRCR_PRIGROUP6                 (0x00000600)        	/*!< Priority group=6 (1 bit of pre-emption priority, 7 bits of subpriority) */
	#define  SCB_AIRCR_PRIGROUP7                 (0x00000700)        	/*!< Priority group=7 (no pre-emption priority, 8 bits of subpriority) */

	#define  SCB_AIRCR_ENDIANESS                 (0x00008000)        	/*!< Data endianness bit */
	#define  SCB_AIRCR_VECTKEY                   (0xFFFF0000)        	/*!< Register key (VECTKEY) - Reads as 0xFA05 (VECTKEYSTAT) */

	/*******************  Bit definition for SCB_SCR register  ********************/
	#define  SCB_SCR_SLEEPONEXIT                 ((uint8_t)0x02)               	/*!< Sleep on exit bit */
	#define  SCB_SCR_SLEEPDEEP                   ((uint8_t)0x04)               	/*!< Sleep deep bit */
	#define  SCB_SCR_SEVONPEND                   ((uint8_t)0x10)               	/*!< Wake up from WFE */

	/********************  Bit definition for SCB_CCR register  *******************/
	#define  SCB_CCR_NONBASETHRDENA              ((uint16_t)0x0001)            	/*!< Thread mode can be entered from any level in Handler mode by controlled return value */
	#define  SCB_CCR_USERSETMPEND                ((uint16_t)0x0002)            	/*!< Enables user code to write the Software Trigger Interrupt register to trigger (pend) a Main exception */
	#define  SCB_CCR_UNALIGN_TRP                 ((uint16_t)0x0008)            	/*!< Trap for unaligned access */
	#define  SCB_CCR_DIV_0_TRP                   ((uint16_t)0x0010)            	/*!< Trap on Divide by 0 */
	#define  SCB_CCR_BFHFNMIGN                   ((uint16_t)0x0100)            	/*!< Handlers running at priority -1 and -2 */
	#define  SCB_CCR_STKALIGN                    ((uint16_t)0x0200)            	/*!< On exception entry, the SP used prior to the exception is adjusted to be 8-byte aligned */

	/*******************  Bit definition for SCB_SHPR register ********************/
	#define  SCB_SHPR_PRI_N                      (0x000000FF)        	/*!< Priority of system handler 4,8, and 12. Mem Manage, reserved and Debug Monitor */
	#define  SCB_SHPR_PRI_N1                     (0x0000FF00)        	/*!< Priority of system handler 5,9, and 13. Bus Fault, reserved and reserved */
	#define  SCB_SHPR_PRI_N2                     (0x00FF0000)        	/*!< Priority of system handler 6,10, and 14. Usage Fault, reserved and PendSV */
	#define  SCB_SHPR_PRI_N3                     (0xFF000000)        	/*!< Priority of system handler 7,11, and 15. Reserved, SVCall and SysTick */

	/******************  Bit definition for SCB_SHCSR register  *******************/
	#define  SCB_SHCSR_MEMFAULTACT               (0x00000001)        	/*!< MemManage is active */
	#define  SCB_SHCSR_BUSFAULTACT               (0x00000002)        	/*!< BusFault is active */
	#define  SCB_SHCSR_USGFAULTACT               (0x00000008)        	/*!< UsageFault is active */
	#define  SCB_SHCSR_SVCALLACT                 (0x00000080)        	/*!< SVCall is active */
	#define  SCB_SHCSR_MONITORACT                (0x00000100)        	/*!< Monitor is active */
	#define  SCB_SHCSR_PENDSVACT                 (0x00000400)        	/*!< PendSV is active */
	#define  SCB_SHCSR_SYSTICKACT                (0x00000800)        	/*!< SysTick is active */
	#define  SCB_SHCSR_USGFAULTPENDED            (0x00001000)        	/*!< Usage Fault is pended */
	#define  SCB_SHCSR_MEMFAULTPENDED            (0x00002000)        	/*!< MemManage is pended */
	#define  SCB_SHCSR_BUSFAULTPENDED            (0x00004000)        	/*!< Bus Fault is pended */
	#define  SCB_SHCSR_SVCALLPENDED              (0x00008000)        	/*!< SVCall is pended */
	#define  SCB_SHCSR_MEMFAULTENA               (0x00010000)        	/*!< MemManage enable */
	#define  SCB_SHCSR_BUSFAULTENA               (0x00020000)        	/*!< Bus Fault enable */
	#define  SCB_SHCSR_USGFAULTENA               (0x00040000)        	/*!< UsageFault enable */

	/*******************  Bit definition for SCB_CFSR register  *******************/
	/*!< MFSR */
	#define  SCB_CFSR_IACCVIOL                   (0x00000001)        	/*!< Instruction access violation */
	#define  SCB_CFSR_DACCVIOL                   (0x00000002)        	/*!< Data access violation */
	#define  SCB_CFSR_MUNSTKERR                  (0x00000008)        	/*!< Unstacking error */
	#define  SCB_CFSR_MSTKERR                    (0x00000010)        	/*!< Stacking error */
	#define  SCB_CFSR_MMARVALID                  (0x00000080)        	/*!< Memory Manage Address Register address valid flag */
	/*!< BFSR */
	#define  SCB_CFSR_IBUSERR                    (0x00000100)        	/*!< Instruction bus error flag */
	#define  SCB_CFSR_PRECISERR                  (0x00000200)        	/*!< Precise data bus error */
	#define  SCB_CFSR_IMPRECISERR                (0x00000400)        	/*!< Imprecise data bus error */
	#define  SCB_CFSR_UNSTKERR                   (0x00000800)        	/*!< Unstacking error */
	#define  SCB_CFSR_STKERR                     (0x00001000)        	/*!< Stacking error */
	#define  SCB_CFSR_BFARVALID                  (0x00008000)        	/*!< Bus Fault Address Register address valid flag */
	/*!< UFSR */
	#define  SCB_CFSR_UNDEFINSTR                 (0x00010000)        	/*!< The processor attempt to excecute an undefined instruction */
	#define  SCB_CFSR_INVSTATE                   (0x00020000)        	/*!< Invalid combination of EPSR and instruction */
	#define  SCB_CFSR_INVPC                      (0x00040000)        	/*!< Attempt to load EXC_RETURN into pc illegally */
	#define  SCB_CFSR_NOCP                       (0x00080000)        	/*!< Attempt to use a coprocessor instruction */
	#define  SCB_CFSR_UNALIGNED                  (0x01000000)        	/*!< Fault occurs when there is an attempt to make an unaligned memory access */
	#define  SCB_CFSR_DIVBYZERO                  (0x02000000)        	/*!< Fault occurs when SDIV or DIV instruction is used with a divisor of 0 */

	/*******************  Bit definition for SCB_HFSR register  *******************/
	#define  SCB_HFSR_VECTTBL                    (0x00000002)        	/*!< Fault occures because of vector table read on exception processing */
	#define  SCB_HFSR_FORCED                     (0x40000000)        	/*!< Hard Fault activated when a configurable Fault was received and cannot activate */
	#define  SCB_HFSR_DEBUGEVT                   (0x80000000)        	/*!< Fault related to debug */

	/*******************  Bit definition for SCB_DFSR register  *******************/
	#define  SCB_DFSR_HALTED                     ((uint8_t)0x01)               	/*!< Halt request flag */
	#define  SCB_DFSR_BKPT                       ((uint8_t)0x02)               	/*!< BKPT flag */
	#define  SCB_DFSR_DWTTRAP                    ((uint8_t)0x04)               	/*!< Data Watchpoint and Trace (DWT) flag */
	#define  SCB_DFSR_VCATCH                     ((uint8_t)0x08)               	/*!< Vector catch flag */
	#define  SCB_DFSR_EXTERNAL                   ((uint8_t)0x10)               	/*!< External debug request flag */

	/*******************  Bit definition for SCB_MMFAR register  ******************/
	#define  SCB_MMFAR_ADDRESS                   (0xFFFFFFFF)        	/*!< Mem Manage fault address field */

	/*******************  Bit definition for SCB_BFAR register  *******************/
	#define  SCB_BFAR_ADDRESS                    (0xFFFFFFFF)        	/*!< Bus fault address field */

	/*******************  Bit definition for SCB_afsr register  *******************/
	#define  SCB_AFSR_IMPDEF                     (0xFFFFFFFF)        	/*!< Implementation defined */

	/******************************************************************************/
	/*                                                                            */
	/*                    External Interrupt/Event Controller                     */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for EXTI_IMR register  *******************/
	#define  EXTI_IMR_MR0                        (0x00000001)        	/*!< Interrupt Mask on line 0 */
	#define  EXTI_IMR_MR1                        (0x00000002)        	/*!< Interrupt Mask on line 1 */
	#define  EXTI_IMR_MR2                        (0x00000004)        	/*!< Interrupt Mask on line 2 */
	#define  EXTI_IMR_MR3                        (0x00000008)        	/*!< Interrupt Mask on line 3 */
	#define  EXTI_IMR_MR4                        (0x00000010)        	/*!< Interrupt Mask on line 4 */
	#define  EXTI_IMR_MR5                        (0x00000020)        	/*!< Interrupt Mask on line 5 */
	#define  EXTI_IMR_MR6                        (0x00000040)        	/*!< Interrupt Mask on line 6 */
	#define  EXTI_IMR_MR7                        (0x00000080)        	/*!< Interrupt Mask on line 7 */
	#define  EXTI_IMR_MR8                        (0x00000100)        	/*!< Interrupt Mask on line 8 */
	#define  EXTI_IMR_MR9                        (0x00000200)        	/*!< Interrupt Mask on line 9 */
	#define  EXTI_IMR_MR10                       (0x00000400)        	/*!< Interrupt Mask on line 10 */
	#define  EXTI_IMR_MR11                       (0x00000800)        	/*!< Interrupt Mask on line 11 */
	#define  EXTI_IMR_MR12                       (0x00001000)        	/*!< Interrupt Mask on line 12 */
	#define  EXTI_IMR_MR13                       (0x00002000)        	/*!< Interrupt Mask on line 13 */
	#define  EXTI_IMR_MR14                       (0x00004000)        	/*!< Interrupt Mask on line 14 */
	#define  EXTI_IMR_MR15                       (0x00008000)        	/*!< Interrupt Mask on line 15 */
	#define  EXTI_IMR_MR16                       (0x00010000)        	/*!< Interrupt Mask on line 16 */
	#define  EXTI_IMR_MR17                       (0x00020000)        	/*!< Interrupt Mask on line 17 */
	#define  EXTI_IMR_MR18                       (0x00040000)        	/*!< Interrupt Mask on line 18 */

	/*******************  Bit definition for EXTI_EMR register  *******************/
	#define  EXTI_EMR_MR0                        (0x00000001)        	/*!< Event Mask on line 0 */
	#define  EXTI_EMR_MR1                        (0x00000002)        	/*!< Event Mask on line 1 */
	#define  EXTI_EMR_MR2                        (0x00000004)        	/*!< Event Mask on line 2 */
	#define  EXTI_EMR_MR3                        (0x00000008)        	/*!< Event Mask on line 3 */
	#define  EXTI_EMR_MR4                        (0x00000010)        	/*!< Event Mask on line 4 */
	#define  EXTI_EMR_MR5                        (0x00000020)        	/*!< Event Mask on line 5 */
	#define  EXTI_EMR_MR6                        (0x00000040)        	/*!< Event Mask on line 6 */
	#define  EXTI_EMR_MR7                        (0x00000080)        	/*!< Event Mask on line 7 */
	#define  EXTI_EMR_MR8                        (0x00000100)        	/*!< Event Mask on line 8 */
	#define  EXTI_EMR_MR9                        (0x00000200)        	/*!< Event Mask on line 9 */
	#define  EXTI_EMR_MR10                       (0x00000400)        	/*!< Event Mask on line 10 */
	#define  EXTI_EMR_MR11                       (0x00000800)        	/*!< Event Mask on line 11 */
	#define  EXTI_EMR_MR12                       (0x00001000)        	/*!< Event Mask on line 12 */
	#define  EXTI_EMR_MR13                       (0x00002000)        	/*!< Event Mask on line 13 */
	#define  EXTI_EMR_MR14                       (0x00004000)        	/*!< Event Mask on line 14 */
	#define  EXTI_EMR_MR15                       (0x00008000)        	/*!< Event Mask on line 15 */
	#define  EXTI_EMR_MR16                       (0x00010000)        	/*!< Event Mask on line 16 */
	#define  EXTI_EMR_MR17                       (0x00020000)        	/*!< Event Mask on line 17 */
	#define  EXTI_EMR_MR18                       (0x00040000)        	/*!< Event Mask on line 18 */

	/******************  Bit definition for EXTI_RTSR register  *******************/
	#define  EXTI_RTSR_TR0                       (0x00000001)        	/*!< Rising trigger event configuration bit of line 0 */
	#define  EXTI_RTSR_TR1                       (0x00000002)        	/*!< Rising trigger event configuration bit of line 1 */
	#define  EXTI_RTSR_TR2                       (0x00000004)        	/*!< Rising trigger event configuration bit of line 2 */
	#define  EXTI_RTSR_TR3                       (0x00000008)        	/*!< Rising trigger event configuration bit of line 3 */
	#define  EXTI_RTSR_TR4                       (0x00000010)        	/*!< Rising trigger event configuration bit of line 4 */
	#define  EXTI_RTSR_TR5                       (0x00000020)        	/*!< Rising trigger event configuration bit of line 5 */
	#define  EXTI_RTSR_TR6                       (0x00000040)        	/*!< Rising trigger event configuration bit of line 6 */
	#define  EXTI_RTSR_TR7                       (0x00000080)        	/*!< Rising trigger event configuration bit of line 7 */
	#define  EXTI_RTSR_TR8                       (0x00000100)        	/*!< Rising trigger event configuration bit of line 8 */
	#define  EXTI_RTSR_TR9                       (0x00000200)        	/*!< Rising trigger event configuration bit of line 9 */
	#define  EXTI_RTSR_TR10                      (0x00000400)        	/*!< Rising trigger event configuration bit of line 10 */
	#define  EXTI_RTSR_TR11                      (0x00000800)        	/*!< Rising trigger event configuration bit of line 11 */
	#define  EXTI_RTSR_TR12                      (0x00001000)        	/*!< Rising trigger event configuration bit of line 12 */
	#define  EXTI_RTSR_TR13                      (0x00002000)        	/*!< Rising trigger event configuration bit of line 13 */
	#define  EXTI_RTSR_TR14                      (0x00004000)        	/*!< Rising trigger event configuration bit of line 14 */
	#define  EXTI_RTSR_TR15                      (0x00008000)        	/*!< Rising trigger event configuration bit of line 15 */
	#define  EXTI_RTSR_TR16                      (0x00010000)        	/*!< Rising trigger event configuration bit of line 16 */
	#define  EXTI_RTSR_TR17                      (0x00020000)        	/*!< Rising trigger event configuration bit of line 17 */
	#define  EXTI_RTSR_TR18                      (0x00040000)        	/*!< Rising trigger event configuration bit of line 18 */

	/******************  Bit definition for EXTI_FTSR register  *******************/
	#define  EXTI_FTSR_TR0                       (0x00000001)        	/*!< Falling trigger event configuration bit of line 0 */
	#define  EXTI_FTSR_TR1                       (0x00000002)        	/*!< Falling trigger event configuration bit of line 1 */
	#define  EXTI_FTSR_TR2                       (0x00000004)        	/*!< Falling trigger event configuration bit of line 2 */
	#define  EXTI_FTSR_TR3                       (0x00000008)        	/*!< Falling trigger event configuration bit of line 3 */
	#define  EXTI_FTSR_TR4                       (0x00000010)        	/*!< Falling trigger event configuration bit of line 4 */
	#define  EXTI_FTSR_TR5                       (0x00000020)        	/*!< Falling trigger event configuration bit of line 5 */
	#define  EXTI_FTSR_TR6                       (0x00000040)        	/*!< Falling trigger event configuration bit of line 6 */
	#define  EXTI_FTSR_TR7                       (0x00000080)        	/*!< Falling trigger event configuration bit of line 7 */
	#define  EXTI_FTSR_TR8                       (0x00000100)        	/*!< Falling trigger event configuration bit of line 8 */
	#define  EXTI_FTSR_TR9                       (0x00000200)        	/*!< Falling trigger event configuration bit of line 9 */
	#define  EXTI_FTSR_TR10                      (0x00000400)        	/*!< Falling trigger event configuration bit of line 10 */
	#define  EXTI_FTSR_TR11                      (0x00000800)        	/*!< Falling trigger event configuration bit of line 11 */
	#define  EXTI_FTSR_TR12                      (0x00001000)        	/*!< Falling trigger event configuration bit of line 12 */
	#define  EXTI_FTSR_TR13                      (0x00002000)        	/*!< Falling trigger event configuration bit of line 13 */
	#define  EXTI_FTSR_TR14                      (0x00004000)        	/*!< Falling trigger event configuration bit of line 14 */
	#define  EXTI_FTSR_TR15                      (0x00008000)        	/*!< Falling trigger event configuration bit of line 15 */
	#define  EXTI_FTSR_TR16                      (0x00010000)        	/*!< Falling trigger event configuration bit of line 16 */
	#define  EXTI_FTSR_TR17                      (0x00020000)        	/*!< Falling trigger event configuration bit of line 17 */
	#define  EXTI_FTSR_TR18                      (0x00040000)        	/*!< Falling trigger event configuration bit of line 18 */

	/******************  Bit definition for EXTI_SWIER register  ******************/
	#define  EXTI_SWIER_SWIER0                   (0x00000001)        	/*!< Software Interrupt on line 0 */
	#define  EXTI_SWIER_SWIER1                   (0x00000002)        	/*!< Software Interrupt on line 1 */
	#define  EXTI_SWIER_SWIER2                   (0x00000004)        	/*!< Software Interrupt on line 2 */
	#define  EXTI_SWIER_SWIER3                   (0x00000008)        	/*!< Software Interrupt on line 3 */
	#define  EXTI_SWIER_SWIER4                   (0x00000010)        	/*!< Software Interrupt on line 4 */
	#define  EXTI_SWIER_SWIER5                   (0x00000020)        	/*!< Software Interrupt on line 5 */
	#define  EXTI_SWIER_SWIER6                   (0x00000040)        	/*!< Software Interrupt on line 6 */
	#define  EXTI_SWIER_SWIER7                   (0x00000080)        	/*!< Software Interrupt on line 7 */
	#define  EXTI_SWIER_SWIER8                   (0x00000100)        	/*!< Software Interrupt on line 8 */
	#define  EXTI_SWIER_SWIER9                   (0x00000200)        	/*!< Software Interrupt on line 9 */
	#define  EXTI_SWIER_SWIER10                  (0x00000400)        	/*!< Software Interrupt on line 10 */
	#define  EXTI_SWIER_SWIER11                  (0x00000800)        	/*!< Software Interrupt on line 11 */
	#define  EXTI_SWIER_SWIER12                  (0x00001000)        	/*!< Software Interrupt on line 12 */
	#define  EXTI_SWIER_SWIER13                  (0x00002000)        	/*!< Software Interrupt on line 13 */
	#define  EXTI_SWIER_SWIER14                  (0x00004000)        	/*!< Software Interrupt on line 14 */
	#define  EXTI_SWIER_SWIER15                  (0x00008000)        	/*!< Software Interrupt on line 15 */
	#define  EXTI_SWIER_SWIER16                  (0x00010000)        	/*!< Software Interrupt on line 16 */
	#define  EXTI_SWIER_SWIER17                  (0x00020000)        	/*!< Software Interrupt on line 17 */
	#define  EXTI_SWIER_SWIER18                  (0x00040000)        	/*!< Software Interrupt on line 18 */

	/*******************  Bit definition for EXTI_PR register  ********************/
	#define  EXTI_PR_PR0                         (0x00000001)        	/*!< Pending bit 0 */
	#define  EXTI_PR_PR1                         (0x00000002)        	/*!< Pending bit 1 */
	#define  EXTI_PR_PR2                         (0x00000004)        	/*!< Pending bit 2 */
	#define  EXTI_PR_PR3                         (0x00000008)        	/*!< Pending bit 3 */
	#define  EXTI_PR_PR4                         (0x00000010)        	/*!< Pending bit 4 */
	#define  EXTI_PR_PR5                         (0x00000020)        	/*!< Pending bit 5 */
	#define  EXTI_PR_PR6                         (0x00000040)        	/*!< Pending bit 6 */
	#define  EXTI_PR_PR7                         (0x00000080)        	/*!< Pending bit 7 */
	#define  EXTI_PR_PR8                         (0x00000100)        	/*!< Pending bit 8 */
	#define  EXTI_PR_PR9                         (0x00000200)        	/*!< Pending bit 9 */
	#define  EXTI_PR_PR10                        (0x00000400)        	/*!< Pending bit 10 */
	#define  EXTI_PR_PR11                        (0x00000800)        	/*!< Pending bit 11 */
	#define  EXTI_PR_PR12                        (0x00001000)        	/*!< Pending bit 12 */
	#define  EXTI_PR_PR13                        (0x00002000)        	/*!< Pending bit 13 */
	#define  EXTI_PR_PR14                        (0x00004000)        	/*!< Pending bit 14 */
	#define  EXTI_PR_PR15                        (0x00008000)        	/*!< Pending bit 15 */
	#define  EXTI_PR_PR16                        (0x00010000)        	/*!< Pending bit 16 */
	#define  EXTI_PR_PR17                        (0x00020000)        	/*!< Pending bit 17 */
	#define  EXTI_PR_PR18                        (0x00040000)        	/*!< Trigger request occurred on the external interrupt line 18 */

	/******************************************************************************/
	/*                                                                            */
	/*                             DMA Controller                                 */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for DMA_ISR register  ********************/
	#define  DMA_GIF1                        0        	/*!< Channel 1 Global interrupt flag */
	#define  DMA_TCIF1                       1        	/*!< Channel 1 Transfer Complete flag */
	#define  DMA_HTIF1                       2        	/*!< Channel 1 Half Transfer flag */
	#define  DMA_TEIF1                       3        	/*!< Channel 1 Transfer Error flag */
	#define  DMA_GIF2                        4        	/*!< Channel 2 Global interrupt flag */
	#define  DMA_TCIF2                       5        	/*!< Channel 2 Transfer Complete flag */
	#define  DMA_HTIF2                       6        	/*!< Channel 2 Half Transfer flag */
	#define  DMA_TEIF2                       7        	/*!< Channel 2 Transfer Error flag */
	#define  DMA_GIF3                        8        	/*!< Channel 3 Global interrupt flag */
	#define  DMA_TCIF3                       9        	/*!< Channel 3 Transfer Complete flag */
	#define  DMA_HTIF3                       10        	/*!< Channel 3 Half Transfer flag */
	#define  DMA_TEIF3                       11        	/*!< Channel 3 Transfer Error flag */
	#define  DMA_GIF4                        12        	/*!< Channel 4 Global interrupt flag */
	#define  DMA_TCIF4                       13        	/*!< Channel 4 Transfer Complete flag */
	#define  DMA_HTIF4                       14        	/*!< Channel 4 Half Transfer flag */
	#define  DMA_TEIF4                       15        	/*!< Channel 4 Transfer Error flag */
	#define  DMA_GIF5                        16        	/*!< Channel 5 Global interrupt flag */
	#define  DMA_TCIF5                       17        	/*!< Channel 5 Transfer Complete flag */
	#define  DMA_HTIF5                       18        	/*!< Channel 5 Half Transfer flag */
	#define  DMA_TEIF5                       19        	/*!< Channel 5 Transfer Error flag */
	#define  DMA_GIF6                        20        	/*!< Channel 6 Global interrupt flag */
	#define  DMA_TCIF6                       21        	/*!< Channel 6 Transfer Complete flag */
	#define  DMA_HTIF6                       22        	/*!< Channel 6 Half Transfer flag */
	#define  DMA_TEIF6                       23        	/*!< Channel 6 Transfer Error flag */
	#define  DMA_GIF7                        24        	/*!< Channel 7 Global interrupt flag */
	#define  DMA_TCIF7                       25        	/*!< Channel 7 Transfer Complete flag */
	#define  DMA_HTIF7                       26        	/*!< Channel 7 Half Transfer flag */
	#define  DMA_TEIF7                       27        	/*!< Channel 7 Transfer Error flag */

	/*******************  Bit definition for DMA_IFCR register  *******************/
	#define  DMA_CGIF1                      0         	/*!< Channel 1 Global interrupt clearr */
	#define  DMA_CTCIF1                     1         	/*!< Channel 1 Transfer Complete clear */
	#define  DMA_CHTIF1                     2         	/*!< Channel 1 Half Transfer clear */
	#define  DMA_CTEIF1                     3         	/*!< Channel 1 Transfer Error clear */
	#define  DMA_CGIF2                      4         	/*!< Channel 2 Global interrupt clear */
	#define  DMA_CTCIF2                     5         	/*!< Channel 2 Transfer Complete clear */
	#define  DMA_CHTIF2                     6         	/*!< Channel 2 Half Transfer clear */
	#define  DMA_CTEIF2                     7         	/*!< Channel 2 Transfer Error clear */
	#define  DMA_CGIF3                      8         	/*!< Channel 3 Global interrupt clear */
	#define  DMA_CTCIF3                     9         	/*!< Channel 3 Transfer Complete clear */
	#define  DMA_CHTIF3                     10        	/*!< Channel 3 Half Transfer clear */
	#define  DMA_CTEIF3                     11        	/*!< Channel 3 Transfer Error clear */
	#define  DMA_CGIF4                      12        	/*!< Channel 4 Global interrupt clear */
	#define  DMA_CTCIF4                     13        	/*!< Channel 4 Transfer Complete clear */
	#define  DMA_CHTIF4                     14        	/*!< Channel 4 Half Transfer clear */
	#define  DMA_CTEIF4                     15        	/*!< Channel 4 Transfer Error clear */
	#define  DMA_CGIF5                      16        	/*!< Channel 5 Global interrupt clear */
	#define  DMA_CTCIF5                     17        	/*!< Channel 5 Transfer Complete clear */
	#define  DMA_CHTIF5                     18        	/*!< Channel 5 Half Transfer clear */
	#define  DMA_CTEIF5                     19        	/*!< Channel 5 Transfer Error clear */
	#define  DMA_CGIF6                      20        	/*!< Channel 6 Global interrupt clear */
	#define  DMA_CTCIF6                     21        	/*!< Channel 6 Transfer Complete clear */
	#define  DMA_CHTIF6                     22        	/*!< Channel 6 Half Transfer clear */
	#define  DMA_CTEIF6                     23        	/*!< Channel 6 Transfer Error clear */
	#define  DMA_CGIF7                      24        	/*!< Channel 7 Global interrupt clear */
	#define  DMA_CTCIF7                     25        	/*!< Channel 7 Transfer Complete clear */
	#define  DMA_CHTIF7                     26        	/*!< Channel 7 Half Transfer clear */
	#define  DMA_CTEIF7                     27        	/*!< Channel 7 Transfer Error clear */

	/*******************  Bit definition for DMA_CCR register  *******************/
	#define  DMA_EN                         0            	/*!< Channel enable*/
	#define  DMA_TCIE                       1            	/*!< Transfer complete interrupt enable */
	#define  DMA_HTIE                       2            	/*!< Half Transfer interrupt enable */
	#define  DMA_TEIE                       3            	/*!< Transfer error interrupt enable */
	#define  DMA_DIR                        4            	/*!< Data transfer direction */
	#define  DMA_CIRC                       5            	/*!< Circular mode */
	#define  DMA_PINC                       6            	/*!< Peripheral increment mode */
	#define  DMA_MINC                       7            	/*!< Memory increment mode */
	#define  DMA_PSIZE                      8            	/*!< PSIZE[1:0] bits (Peripheral size) */
	#define  DMA_MSIZE                      10            	/*!< MSIZE[1:0] bits (Memory size) */
	#define  DMA_PL                         12            	/*!< PL[1:0] bits(Channel Priority level) */
	#define  DMA_MEM2MEM                    14            	/*!< Memory to memory mode */


	/******************  Bit definition for DMA_CNDTR1 register  ******************/
	#define  DMA_CNDTR1_NDT                      ((uint16_t)0xFFFF)            	/*!<Number of data to Transfer */

	/******************  Bit definition for DMA_CNDTR2 register  ******************/
	#define  DMA_CNDTR2_NDT                      ((uint16_t)0xFFFF)            	/*!<Number of data to Transfer */

	/******************  Bit definition for DMA_CNDTR3 register  ******************/
	#define  DMA_CNDTR3_NDT                      ((uint16_t)0xFFFF)            	/*!<Number of data to Transfer */

	/******************  Bit definition for DMA_CNDTR4 register  ******************/
	#define  DMA_CNDTR4_NDT                      ((uint16_t)0xFFFF)            	/*!<Number of data to Transfer */

	/******************  Bit definition for DMA_CNDTR5 register  ******************/
	#define  DMA_CNDTR5_NDT                      ((uint16_t)0xFFFF)            	/*!<Number of data to Transfer */

	/******************  Bit definition for DMA_CNDTR6 register  ******************/
	#define  DMA_CNDTR6_NDT                      ((uint16_t)0xFFFF)            	/*!<Number of data to Transfer */

	/******************  Bit definition for DMA_CNDTR7 register  ******************/
	#define  DMA_CNDTR7_NDT                      ((uint16_t)0xFFFF)            	/*!<Number of data to Transfer */

	/******************  Bit definition for DMA_CPAR1 register  *******************/
	#define  DMA_CPAR1_PA                        (0xFFFFFFFF)        	/*!<Peripheral Address */

	/******************  Bit definition for DMA_CPAR2 register  *******************/
	#define  DMA_CPAR2_PA                        (0xFFFFFFFF)        	/*!<Peripheral Address */

	/******************  Bit definition for DMA_CPAR3 register  *******************/
	#define  DMA_CPAR3_PA                        (0xFFFFFFFF)        	/*!<Peripheral Address */


	/******************  Bit definition for DMA_CPAR4 register  *******************/
	#define  DMA_CPAR4_PA                        (0xFFFFFFFF)        	/*!<Peripheral Address */

	/******************  Bit definition for DMA_CPAR5 register  *******************/
	#define  DMA_CPAR5_PA                        (0xFFFFFFFF)        	/*!<Peripheral Address */

	/******************  Bit definition for DMA_CPAR6 register  *******************/
	#define  DMA_CPAR6_PA                        (0xFFFFFFFF)        	/*!<Peripheral Address */


	/******************  Bit definition for DMA_CPAR7 register  *******************/
	#define  DMA_CPAR7_PA                        (0xFFFFFFFF)        	/*!<Peripheral Address */

	/******************  Bit definition for DMA_CMAR1 register  *******************/
	#define  DMA_CMAR1_MA                        (0xFFFFFFFF)        	/*!<Memory Address */

	/******************  Bit definition for DMA_CMAR2 register  *******************/
	#define  DMA_CMAR2_MA                        (0xFFFFFFFF)        	/*!<Memory Address */

	/******************  Bit definition for DMA_CMAR3 register  *******************/
	#define  DMA_CMAR3_MA                        (0xFFFFFFFF)        	/*!<Memory Address */


	/******************  Bit definition for DMA_CMAR4 register  *******************/
	#define  DMA_CMAR4_MA                        (0xFFFFFFFF)        	/*!<Memory Address */

	/******************  Bit definition for DMA_CMAR5 register  *******************/
	#define  DMA_CMAR5_MA                        (0xFFFFFFFF)        	/*!<Memory Address */

	/******************  Bit definition for DMA_CMAR6 register  *******************/
	#define  DMA_CMAR6_MA                        (0xFFFFFFFF)        	/*!<Memory Address */

	/******************  Bit definition for DMA_CMAR7 register  *******************/
	#define  DMA_CMAR7_MA                        (0xFFFFFFFF)        	/*!<Memory Address */

	/******************************************************************************/
	/*                                                                            */
	/*                        Analog to Digital Converter                         */
	/*                                                                            */
	/******************************************************************************/

	/********************  Bit definition for ADC_SR register  ********************/
	#define  ADC_AWD                          0               	/*!<Analog watchdog flag */
	#define  ADC_EOC                          1               	/*!<End of conversion */
	#define  ADC_JEOC                         2               	/*!<Injected channel end of conversion */
	#define  ADC_JSTRT                        3               	/*!<Injected channel Start flag */
	#define  ADC_STRT                         4               	/*!<Regular channel Start flag */

	/*******************  Bit definition for ADC_CR1 register  ********************/
	#define  ADC_AWDCH                       0        			/*!<AWDCH[4:0] bits (Analog watchdog channel select bits) */
	#define  ADC_EOCIE                       5        			/*!<Interrupt enable for EOC */
	#define  ADC_AWDIE                       6        			/*!<AAnalog Watchdog interrupt enable */
	#define  ADC_JEOCIE                      7        			/*!<Interrupt enable for injected channels */
	#define  ADC_SCAN                        8        			/*!<Scan mode */
	#define  ADC_AWDSGL                      9        			/*!<Enable the watchdog on a single channel in scan mode */
	#define  ADC_JAUTO                       10        			/*!<Automatic injected group conversion */
	#define  ADC_DISCEN                      11        			/*!<Discontinuous mode on regular channels */
	#define  ADC_JDISCEN                     12        			/*!<Discontinuous mode on injected channels */
	#define  ADC_DISCNUM                     13        			/*!<DISCNUM[2:0] bits (Discontinuous mode channel count) */
	#define  ADC_DUALMOD                     16        			/*!<DUALMOD[3:0] bits (Dual mode selection) */
	#define  ADC_JAWDEN                      22        			/*!<Analog watchdog enable on injected channels */
	#define  ADC_AWDEN                       23        			/*!<Analog watchdog enable on regular channels */

  
	/*******************  Bit definition for ADC_CR2 register  ********************/
	#define  ADC_ADON                        0        			/*!<A/D Converter ON / OFF */
	#define  ADC_CONT                        1        			/*!<Continuous Conversion */
	#define  ADC_CAL                         2        			/*!<A/D Calibration */
	#define  ADC_RSTCAL                      3        			/*!<Reset Calibration */
	#define  ADC_DMA                         8        			/*!<Direct Memory access mode */
	#define  ADC_ALIGN                       11        			/*!<Data Alignment */
	#define  ADC_JEXTSEL                     12        			/*!<JEXTSEL[2:0] bits (External event select for injected group) */
	#define  ADC_JEXTTRIG                    15        			/*!<External Trigger Conversion mode for injected channels */
	#define  ADC_EXTSEL                      17        			/*!<EXTSEL[2:0] bits (External Event Select for regular group) */
	#define  ADC_EXTTRIG                     20        			/*!<External Trigger Conversion mode for regular channels */
	#define  ADC_JSWSTART                    21        			/*!<Start Conversion of injected channels */
	#define  ADC_SWSTART                     22        			/*!<Start Conversion of regular channels */
	#define  ADC_TSVREFE                     23        			/*!<Temperature Sensor and VREFINT Enable */

	/******************  Bit definition for ADC_SMPR1 register  *******************/
	#define  ADC_SMP10                     0        			/*!<SMP10[2:0] bits (Channel 10 Sample time selection) */
	#define  ADC_SMP11                     3        			/*!<SMP11[2:0] bits (Channel 11 Sample time selection) */
	#define  ADC_SMP12                     6        			/*!<SMP12[2:0] bits (Channel 12 Sample time selection) */
	#define  ADC_SMP13                     9        			/*!<SMP13[2:0] bits (Channel 13 Sample time selection) */
	#define  ADC_SMP14                     12        			/*!<SMP14[2:0] bits (Channel 14 Sample time selection) */
	#define  ADC_SMP15                     15        			/*!<SMP15[2:0] bits (Channel 15 Sample time selection) */
	#define  ADC_SMP16                     18        			/*!<SMP16[2:0] bits (Channel 16 Sample time selection) */
	#define  ADC_SMP17                     21        			/*!<SMP17[2:0] bits (Channel 17 Sample time selection) */

	/******************  Bit definition for ADC_SMPR2 register  *******************/
	#define  ADC_SMP0                      0        			/*!<SMP0[2:0] bits (Channel 0 Sample time selection) */
	#define  ADC_SMP1                      3        			/*!<SMP1[2:0] bits (Channel 1 Sample time selection) */
	#define  ADC_SMP2                      6        			/*!<SMP2[2:0] bits (Channel 2 Sample time selection) */
	#define  ADC_SMP3                      9        			/*!<SMP3[2:0] bits (Channel 3 Sample time selection) */
	#define  ADC_SMP4                      12        			/*!<SMP4[2:0] bits (Channel 4 Sample time selection) */
	#define  ADC_SMP5                      15        			/*!<SMP5[2:0] bits (Channel 5 Sample time selection) */
	#define  ADC_SMP6                      18        			/*!<SMP6[2:0] bits (Channel 6 Sample time selection) */
	#define  ADC_SMP7                      21        			/*!<SMP7[2:0] bits (Channel 7 Sample time selection) */
	#define  ADC_SMP8                      24        			/*!<SMP8[2:0] bits (Channel 8 Sample time selection) */
	#define  ADC_SMP9                      27        			/*!<SMP9[2:0] bits (Channel 9 Sample time selection) */


	/*******************  Bit definition for ADC_SQR1 register  *******************/
	#define  ADC_SQ13                       0        			/*!<SQ13[4:0] bits (13th conversion in regular sequence) */
	#define  ADC_SQ14                       5        			/*!<SQ14[4:0] bits (14th conversion in regular sequence) */
	#define  ADC_SQ15                       10        			/*!<SQ15[4:0] bits (15th conversion in regular sequence) */
	#define  ADC_SQ16                       15        			/*!<SQ16[4:0] bits (16th conversion in regular sequence) */
	#define  ADC_LEN                        20        			/*!<L[3:0] bits (Regular channel sequence length) */

	/*******************  Bit definition for ADC_SQR2 register  *******************/
	#define  ADC_SQ7                        0        			/*!<SQ7[4:0] bits (7th conversion in regular sequence) */
	#define  ADC_SQ8                        5        			/*!<SQ8[4:0] bits (8th conversion in regular sequence) */
	#define  ADC_SQ9                        10        			/*!<SQ9[4:0] bits (9th conversion in regular sequence) */
	#define  ADC_SQ10                       15        			/*!<SQ10[4:0] bits (10th conversion in regular sequence) */
	#define  ADC_SQ11                       20        			/*!<SQ11[4:0] bits (11th conversion in regular sequence) */
	#define  ADC_SQ12                       25        			/*!<SQ12[4:0] bits (12th conversion in regular sequence) */

	/*******************  Bit definition for ADC_SQR3 register  *******************/
	#define  ADC_SQ1                        0        			/*!<SQ1[4:0] bits (1st conversion in regular sequence) */
	#define  ADC_SQ2                        5        			/*!<SQ2[4:0] bits (2nd conversion in regular sequence) */
	#define  ADC_SQ3                        10        			/*!<SQ3[4:0] bits (3rd conversion in regular sequence) */
	#define  ADC_SQ4                        15        			/*!<SQ4[4:0] bits (4th conversion in regular sequence) */
	#define  ADC_SQ5                        20        			/*!<SQ5[4:0] bits (5th conversion in regular sequence) */
	#define  ADC_SQ6                        25        			/*!<SQ6[4:0] bits (6th conversion in regular sequence) */

	/*******************  Bit definition for ADC_JSQR register  *******************/
	#define  ADC_JSQ1                       0        			/*!<JSQ1[4:0] bits (1st conversion in injected sequence) */  
	#define  ADC_JSQ2                       5        			/*!<JSQ2[4:0] bits (2nd conversion in injected sequence) */
	#define  ADC_JSQ3                       10        			/*!<JSQ3[4:0] bits (3rd conversion in injected sequence) */
	#define  ADC_JSQ4                       15        			/*!<JSQ4[4:0] bits (4th conversion in injected sequence) */
	#define  ADC_JLEN                       20        			/*!<JL[1:0] bits (Injected Sequence length) */



	/******************************************************************************/
	/*                                                                            */
	/*                      Digital to Analog Converter                           */
	/*                                                                            */
	/******************************************************************************/

	/********************  Bit definition for DAC_CR register  ********************/
	#define  DAC_CR_EN1                          (0x00000001)        	/*!<DAC channel1 enable */
	#define  DAC_CR_BOFF1                        (0x00000002)        	/*!<DAC channel1 output buffer disable */
	#define  DAC_CR_TEN1                         (0x00000004)        	/*!<DAC channel1 Trigger enable */

	#define  DAC_CR_TSEL1                        (0x00000038)        	/*!<TSEL1[2:0] (DAC channel1 Trigger selection) */
	#define  DAC_CR_TSEL1_0                      (0x00000008)        	/*!<Bit 0 */
	#define  DAC_CR_TSEL1_1                      (0x00000010)        	/*!<Bit 1 */
	#define  DAC_CR_TSEL1_2                      (0x00000020)        	/*!<Bit 2 */

	#define  DAC_CR_WAVE1                        (0x000000C0)        	/*!<WAVE1[1:0] (DAC channel1 noise/triangle wave generation enable) */
	#define  DAC_CR_WAVE1_0                      (0x00000040)        	/*!<Bit 0 */
	#define  DAC_CR_WAVE1_1                      (0x00000080)        	/*!<Bit 1 */

	#define  DAC_CR_MAMP1                        (0x00000F00)        	/*!<MAMP1[3:0] (DAC channel1 Mask/Amplitude selector) */
	#define  DAC_CR_MAMP1_0                      (0x00000100)        	/*!<Bit 0 */
	#define  DAC_CR_MAMP1_1                      (0x00000200)        	/*!<Bit 1 */
	#define  DAC_CR_MAMP1_2                      (0x00000400)        	/*!<Bit 2 */
	#define  DAC_CR_MAMP1_3                      (0x00000800)        	/*!<Bit 3 */

	#define  DAC_CR_DMAEN1                       (0x00001000)        	/*!<DAC channel1 DMA enable */
	#define  DAC_CR_EN2                          (0x00010000)        	/*!<DAC channel2 enable */
	#define  DAC_CR_BOFF2                        (0x00020000)        	/*!<DAC channel2 output buffer disable */
	#define  DAC_CR_TEN2                         (0x00040000)        	/*!<DAC channel2 Trigger enable */

	#define  DAC_CR_TSEL2                        (0x00380000)        	/*!<TSEL2[2:0] (DAC channel2 Trigger selection) */
	#define  DAC_CR_TSEL2_0                      (0x00080000)        	/*!<Bit 0 */
	#define  DAC_CR_TSEL2_1                      (0x00100000)        	/*!<Bit 1 */
	#define  DAC_CR_TSEL2_2                      (0x00200000)        	/*!<Bit 2 */

	#define  DAC_CR_WAVE2                        (0x00C00000)        	/*!<WAVE2[1:0] (DAC channel2 noise/triangle wave generation enable) */
	#define  DAC_CR_WAVE2_0                      (0x00400000)        	/*!<Bit 0 */
	#define  DAC_CR_WAVE2_1                      (0x00800000)        	/*!<Bit 1 */

	#define  DAC_CR_MAMP2                        (0x0F000000)        	/*!<MAMP2[3:0] (DAC channel2 Mask/Amplitude selector) */
	#define  DAC_CR_MAMP2_0                      (0x01000000)        	/*!<Bit 0 */
	#define  DAC_CR_MAMP2_1                      (0x02000000)        	/*!<Bit 1 */
	#define  DAC_CR_MAMP2_2                      (0x04000000)        	/*!<Bit 2 */
	#define  DAC_CR_MAMP2_3                      (0x08000000)        	/*!<Bit 3 */

	#define  DAC_CR_DMAEN2                       (0x10000000)        	/*!<DAC channel2 DMA enabled */

	/*****************  Bit definition for DAC_SWTRIGR register  ******************/
	#define  DAC_SWTRIGR_SWTRIG1                 ((uint8_t)0x01)               	/*!<DAC channel1 software trigger */
	#define  DAC_SWTRIGR_SWTRIG2                 ((uint8_t)0x02)               	/*!<DAC channel2 software trigger */

	/*****************  Bit definition for DAC_DHR12R1 register  ******************/
	#define  DAC_DHR12R1_DACC1DHR                ((uint16_t)0x0FFF)            	/*!<DAC channel1 12-bit Right aligned data */

	/*****************  Bit definition for DAC_DHR12L1 register  ******************/
	#define  DAC_DHR12L1_DACC1DHR                ((uint16_t)0xFFF0)            	/*!<DAC channel1 12-bit Left aligned data */

	/******************  Bit definition for DAC_DHR8R1 register  ******************/
	#define  DAC_DHR8R1_DACC1DHR                 ((uint8_t)0xFF)               	/*!<DAC channel1 8-bit Right aligned data */

	/*****************  Bit definition for DAC_DHR12R2 register  ******************/
	#define  DAC_DHR12R2_DACC2DHR                ((uint16_t)0x0FFF)            	/*!<DAC channel2 12-bit Right aligned data */

	/*****************  Bit definition for DAC_DHR12L2 register  ******************/
	#define  DAC_DHR12L2_DACC2DHR                ((uint16_t)0xFFF0)            	/*!<DAC channel2 12-bit Left aligned data */

	/******************  Bit definition for DAC_DHR8R2 register  ******************/
	#define  DAC_DHR8R2_DACC2DHR                 ((uint8_t)0xFF)               	/*!<DAC channel2 8-bit Right aligned data */

	/*****************  Bit definition for DAC_DHR12RD register  ******************/
	#define  DAC_DHR12RD_DACC1DHR                (0x00000FFF)        	/*!<DAC channel1 12-bit Right aligned data */
	#define  DAC_DHR12RD_DACC2DHR                (0x0FFF0000)        	/*!<DAC channel2 12-bit Right aligned data */

	/*****************  Bit definition for DAC_DHR12LD register  ******************/
	#define  DAC_DHR12LD_DACC1DHR                (0x0000FFF0)        	/*!<DAC channel1 12-bit Left aligned data */
	#define  DAC_DHR12LD_DACC2DHR                (0xFFF00000)        	/*!<DAC channel2 12-bit Left aligned data */

	/******************  Bit definition for DAC_DHR8RD register  ******************/
	#define  DAC_DHR8RD_DACC1DHR                 ((uint16_t)0x00FF)            	/*!<DAC channel1 8-bit Right aligned data */
	#define  DAC_DHR8RD_DACC2DHR                 ((uint16_t)0xFF00)            	/*!<DAC channel2 8-bit Right aligned data */

	/*******************  Bit definition for DAC_DOR1 register  *******************/
	#define  DAC_DOR1_DACC1DOR                   ((uint16_t)0x0FFF)            	/*!<DAC channel1 data output */

	/*******************  Bit definition for DAC_DOR2 register  *******************/
	#define  DAC_DOR2_DACC2DOR                   ((uint16_t)0x0FFF)            	/*!<DAC channel2 data output */

	/******************************************************************************/
	/*                                                                            */
	/*                                    TIM                                     */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for TIM_CR1 register  ********************/
	#define  TIM_CEN                            0           	/*!<Counter enable */
	#define  TIM_UDIS                           1            	/*!<Update disable */
	#define  TIM_URS                            2            	/*!<Update request source */
	#define  TIM_OPM                            3            	/*!<One pulse mode */
	#define  TIM_DIR                            4            	/*!<Direction */
	#define  TIM_CMS                            5            	/*!<CMS[1:0] bits (Center-aligned mode selection) */
	#define  TIM_ARPE                           7            	/*!<Auto-reload preload enable */
	#define  TIM_CKD                            8            	/*!<CKD[1:0] bits (clock division) */

	/*******************  Bit definition for TIM_CR2 register  ********************/
	#define  TIM_CCPC                           0            	/*!<Capture/Compare Preloaded Control */
	#define  TIM_CCUS                           2            	/*!<Capture/Compare Control Update Selection */
	#define  TIM_CCDS                           3            	/*!<Capture/Compare DMA Selection */
	#define  TIM_MMS                            4            	/*!<MMS[2:0] bits (Master Mode Selection) */
	#define  TIM_TI1S                           7            	/*!<TI1 Selection */
	#define  TIM_OIS1                           8            	/*!<Output Idle state 1 (OC1 output) */
	#define  TIM_OIS1N                          9            	/*!<Output Idle state 1 (OC1N output) */
	#define  TIM_OIS2                           10           	/*!<Output Idle state 2 (OC2 output) */
	#define  TIM_OIS2N                          11           	/*!<Output Idle state 2 (OC2N output) */
	#define  TIM_OIS3                           12           	/*!<Output Idle state 3 (OC3 output) */
	#define  TIM_OIS3N                          13           	/*!<Output Idle state 3 (OC3N output) */
	#define  TIM_OIS4                           14           	/*!<Output Idle state 4 (OC4 output) */

	/*******************  Bit definition for TIM_SMCR register  *******************/
	#define  TIM_SMS                            0            	/*!<SMS[2:0] bits (Slave mode selection) */
	#define  TIM_TS                             4            	/*!<TS[2:0] bits (Trigger selection) */
	#define  TIM_MSM                            7            	/*!<Master/slave mode */
	#define  TIM_ETF                            8            	/*!<ETF[3:0] bits (External trigger filter) */
	#define  TIM_ETPS                           12           	/*!<ETPS[1:0] bits (External trigger prescaler) */
	#define  TIM_ECE                            14           	/*!<External clock enable */
	#define  TIM_ETP                            15           	/*!<External trigger polarity */

	/*******************  Bit definition for TIM_DIER register  *******************/
	#define  TIM_UIE                            0            	/*!<Update interrupt enable */
	#define  TIM_CC1IE                          1            	/*!<Capture/Compare 1 interrupt enable */
	#define  TIM_CC2IE                          2            	/*!<Capture/Compare 2 interrupt enable */
	#define  TIM_CC3IE                          3            	/*!<Capture/Compare 3 interrupt enable */
	#define  TIM_CC4IE                          4            	/*!<Capture/Compare 4 interrupt enable */
	#define  TIM_COMIE                          5            	/*!<COM interrupt enable */
	#define  TIM_TIE                            6            	/*!<Trigger interrupt enable */
	#define  TIM_BIE                            7            	/*!<Break interrupt enable */
	#define  TIM_UDE                            8            	/*!<Update DMA request enable */
	#define  TIM_CC1DE                          9            	/*!<Capture/Compare 1 DMA request enable */
	#define  TIM_CC2DE                          10           	/*!<Capture/Compare 2 DMA request enable */
	#define  TIM_CC3DE                          11           	/*!<Capture/Compare 3 DMA request enable */
	#define  TIM_CC4DE                          12           	/*!<Capture/Compare 4 DMA request enable */
	#define  TIM_COMDE                          13           	/*!<COM DMA request enable */
	#define  TIM_TDE                            14           	/*!<Trigger DMA request enable */

	/********************  Bit definition for TIM_SR register  ********************/
	#define  TIM_UIF                            0            	/*!<Update interrupt Flag */
	#define  TIM_CC1IF                          1            	/*!<Capture/Compare 1 interrupt Flag */
	#define  TIM_CC2IF                          2            	/*!<Capture/Compare 2 interrupt Flag */
	#define  TIM_CC3IF                          3            	/*!<Capture/Compare 3 interrupt Flag */
	#define  TIM_CC4IF                          4            	/*!<Capture/Compare 4 interrupt Flag */
	#define  TIM_COMIF                          5            	/*!<COM interrupt Flag */
	#define  TIM_TIF                            6            	/*!<Trigger interrupt Flag */
	#define  TIM_BIF                            7            	/*!<Break interrupt Flag */
	#define  TIM_CC1OF                          9            	/*!<Capture/Compare 1 Overcapture Flag */
	#define  TIM_CC2OF                          10           	/*!<Capture/Compare 2 Overcapture Flag */
	#define  TIM_CC3OF                          11           	/*!<Capture/Compare 3 Overcapture Flag */
	#define  TIM_CC4OF                          12           	/*!<Capture/Compare 4 Overcapture Flag */

	/*******************  Bit definition for TIM_EGR register  ********************/
	#define  TIM_UG                             0            	/*!<Update Generation */
	#define  TIM_CC1G                           1           	/*!<Capture/Compare 1 Generation */
	#define  TIM_CC2G                           2            	/*!<Capture/Compare 2 Generation */
	#define  TIM_CC3G                           3           	/*!<Capture/Compare 3 Generation */
	#define  TIM_CC4G                           4            	/*!<Capture/Compare 4 Generation */
	#define  TIM_COMG                           5            	/*!<Capture/Compare Control Update Generation */
	#define  TIM_TG                             6            	/*!<Trigger Generation */
	#define  TIM_BG                             7            	/*!<Break Generation */

	/******************  Bit definition for TIM_CCMR1 register  *******************/
	#define  TIM_CC1S                           0            	/*!<CC1S[1:0] bits (Capture/Compare 1 Selection) */
	#define  TIM_OC1FE                          2            	/*!<Output Compare 1 Fast enable */
	#define  TIM_OC1PE                          3            	/*!<Output Compare 1 Preload enable */
	#define  TIM_OC1M                           4            	/*!<OC1M[2:0] bits (Output Compare 1 Mode) */
	#define  TIM_OC1CE                          7            	/*!<Output Compare 1Clear Enable */
	#define  TIM_CC2S                           8            	/*!<CC2S[1:0] bits (Capture/Compare 2 Selection) */
	#define  TIM_OC2FE                          10           	/*!<Output Compare 2 Fast enable */
	#define  TIM_OC2PE                          11           	/*!<Output Compare 2 Preload enable */
	#define  TIM_OC2M                           12           	/*!<OC2M[2:0] bits (Output Compare 2 Mode) */
	#define  TIM_OC2CE                          15           	/*!<Output Compare 2 Clear Enable */

	/*----------------------------------------------------------------------------*/
	#define  TIM_IC1PSC                         2            	/*!<IC1PSC[1:0] bits (Input Capture 1 Prescaler) */
	#define  TIM_IC1F                           4            	/*!<IC1F[3:0] bits (Input Capture 1 Filter) */
	#define  TIM_IC2PSC                         10           	/*!<IC2PSC[1:0] bits (Input Capture 2 Prescaler) */
	#define  TIM_IC2F                           12           	/*!<IC2F[3:0] bits (Input Capture 2 Filter) */

	/******************  Bit definition for TIM_CCMR2 register  *******************/
	#define  TIM_CC3S                           0            	/*!<CC3S[1:0] bits (Capture/Compare 3 Selection) */
	#define  TIM_OC3FE                          2            	/*!<Output Compare 3 Fast enable */
	#define  TIM_OC3PE                          3            	/*!<Output Compare 3 Preload enable */
	#define  TIM_OC3M                           4            	/*!<OC3M[2:0] bits (Output Compare 3 Mode) */
	#define  TIM_OC3CE                          7            	/*!<Output Compare 3 Clear Enable */
	#define  TIM_CC4S                           8            	/*!<CC4S[1:0] bits (Capture/Compare 4 Selection) */
	#define  TIM_OC4FE                          10           	/*!<Output Compare 4 Fast enable */
	#define  TIM_OC4PE                          11           	/*!<Output Compare 4 Preload enable */
	#define  TIM_OC4M                           12           	/*!<OC4M[2:0] bits (Output Compare 4 Mode) */
	#define  TIM_OC4CE                          15           	/*!<Output Compare 4 Clear Enable */

	/*----------------------------------------------------------------------------*/
	#define  TIM_IC3PSC                         2            	/*!<IC3PSC[1:0] bits (Input Capture 3 Prescaler) */
	#define  TIM_IC3F                           4            	/*!<IC3F[3:0] bits (Input Capture 3 Filter) */
	#define  TIM_IC4PSC                         10           	/*!<IC4PSC[1:0] bits (Input Capture 4 Prescaler) */
	#define  TIM_IC4F                           12           	/*!<IC4F[3:0] bits (Input Capture 4 Filter) */

	/*******************  Bit definition for TIM_CCER register  *******************/
	#define  TIM_CC1E                           0            	/*!<Capture/Compare 1 output enable */
	#define  TIM_CC1P                           1            	/*!<Capture/Compare 1 output Polarity */
	#define  TIM_CC1NE                          2            	/*!<Capture/Compare 1 Complementary output enable */
	#define  TIM_CC1NP                          3            	/*!<Capture/Compare 1 Complementary output Polarity */
	#define  TIM_CC2E                           4            	/*!<Capture/Compare 2 output enable */
	#define  TIM_CC2P                           5            	/*!<Capture/Compare 2 output Polarity */
	#define  TIM_CC2NE                          6            	/*!<Capture/Compare 2 Complementary output enable */
	#define  TIM_CC2NP                          7            	/*!<Capture/Compare 2 Complementary output Polarity */
	#define  TIM_CC3E                           8            	/*!<Capture/Compare 3 output enable */
	#define  TIM_CC3P                           9            	/*!<Capture/Compare 3 output Polarity */
	#define  TIM_CC3NE                          10           	/*!<Capture/Compare 3 Complementary output enable */
	#define  TIM_CC3NP                          11           	/*!<Capture/Compare 3 Complementary output Polarity */
	#define  TIM_CC4E                           12           	/*!<Capture/Compare 4 output enable */
	#define  TIM_CC4P                           13           	/*!<Capture/Compare 4 output Polarity */

	/*******************  Bit definition for TIM_BDTR register  *******************/
	#define  TIM_DTG                            0            	/*!<DTG[0:7] bits (Dead-Time Generator set-up) */
	#define  TIM_LOCK                           8            	/*!<LOCK[1:0] bits (Lock Configuration) */
	#define  TIM_OSSI                           10           	/*!<Off-State Selection for Idle mode */
	#define  TIM_OSSR                           11           	/*!<Off-State Selection for Run mode */
	#define  TIM_BKE                            12           	/*!<Break enable */
	#define  TIM_BKP                            13           	/*!<Break Polarity */
	#define  TIM_AOE                            14           	/*!<Automatic Output enable */
	#define  TIM_MOE                            15           	/*!<Main Output enable */

	/*******************  Bit definition for TIM_DCR register  ********************/
	#define  TIM_DBA                            0            	/*!<DBA[4:0] bits (DMA Base Address) */
	#define  TIM_DBL                            8            	/*!<DBL[4:0] bits (DMA Burst Length) */

	/******************************************************************************/
	/*                                                                            */
	/*                             Real-Time Clock                                */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for RTC_CRH register  ********************/
	#define  RTC_SECIE                       	0               	/*!<Second Interrupt Enable */
	#define  RTC_ALRIE                       	1               	/*!<Alarm Interrupt Enable */
	#define  RTC_OWIE                        	2               	/*!<OverfloW Interrupt Enable */

	/*******************  Bit definition for RTC register  ********************/
	#define  RTC_SECF                        	0               	/*!<Second Flag */
	#define  RTC_ALRF                        	1               	/*!<Alarm Flag */
	#define  RTC_OWF                         	2               	/*!<OverfloW Flag */
	#define  RTC_RSF                         	3               	/*!<Registers Synchronized Flag */
	#define  RTC_CNF                         	4               	/*!<Configuration Flag */
	#define  RTC_RTOFF                       	5               	/*!<RTC operation OFF */


	/******************************************************************************/
	/*                                                                            */
	/*                           Independent WATCHDOG                             */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for IWDG_KR register  ********************/
	#define  IWDG_KR_KEY                         ((uint16_t)0xFFFF)            	/*!<Key value (write only, read 0000h) */

	/*******************  Bit definition for IWDG_PR register  ********************/
	#define  IWDG_PR_PR                          ((uint8_t)0x07)               	/*!<PR[2:0] (Prescaler divider) */
	#define  IWDG_PR_PR_0                        ((uint8_t)0x01)               	/*!<Bit 0 */
	#define  IWDG_PR_PR_1                        ((uint8_t)0x02)               	/*!<Bit 1 */
	#define  IWDG_PR_PR_2                        ((uint8_t)0x04)               	/*!<Bit 2 */

	/*******************  Bit definition for IWDG_RLR register  *******************/
	#define  IWDG_RLR_RL                         ((uint16_t)0x0FFF)            	/*!<Watchdog counter reload value */

	/*******************  Bit definition for IWDG_SR register  ********************/
	#define  IWDG_SR_PVU                         ((uint8_t)0x01)               	/*!<Watchdog prescaler value update */
	#define  IWDG_SR_RVU                         ((uint8_t)0x02)               	/*!<Watchdog counter reload value update */

	/******************************************************************************/
	/*                                                                            */
	/*                            Window WATCHDOG                                 */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for WWDG_CR register  ********************/
	#define  WWDG_CR_T                           ((uint8_t)0x7F)               	/*!<T[6:0] bits (7-Bit counter (MSB to LSB)) */
	#define  WWDG_CR_T0                          ((uint8_t)0x01)               	/*!<Bit 0 */
	#define  WWDG_CR_T1                          ((uint8_t)0x02)               	/*!<Bit 1 */
	#define  WWDG_CR_T2                          ((uint8_t)0x04)               	/*!<Bit 2 */
	#define  WWDG_CR_T3                          ((uint8_t)0x08)               	/*!<Bit 3 */
	#define  WWDG_CR_T4                          ((uint8_t)0x10)               	/*!<Bit 4 */
	#define  WWDG_CR_T5                          ((uint8_t)0x20)               	/*!<Bit 5 */
	#define  WWDG_CR_T6                          ((uint8_t)0x40)               	/*!<Bit 6 */

	#define  WWDG_CR_WDGA                        ((uint8_t)0x80)               	/*!<Activation bit */

	/*******************  Bit definition for WWDG_CFR register  *******************/
	#define  WWDG_CFR_W                          ((uint16_t)0x007F)            	/*!<W[6:0] bits (7-bit window value) */
	#define  WWDG_CFR_W0                         ((uint16_t)0x0001)            	/*!<Bit 0 */
	#define  WWDG_CFR_W1                         ((uint16_t)0x0002)            	/*!<Bit 1 */
	#define  WWDG_CFR_W2                         ((uint16_t)0x0004)            	/*!<Bit 2 */
	#define  WWDG_CFR_W3                         ((uint16_t)0x0008)            	/*!<Bit 3 */
	#define  WWDG_CFR_W4                         ((uint16_t)0x0010)            	/*!<Bit 4 */
	#define  WWDG_CFR_W5                         ((uint16_t)0x0020)            	/*!<Bit 5 */
	#define  WWDG_CFR_W6                         ((uint16_t)0x0040)            	/*!<Bit 6 */

	#define  WWDG_CFR_WDGTB                      ((uint16_t)0x0180)            	/*!<WDGTB[1:0] bits (Timer Base) */
	#define  WWDG_CFR_WDGTB0                     ((uint16_t)0x0080)            	/*!<Bit 0 */
	#define  WWDG_CFR_WDGTB1                     ((uint16_t)0x0100)            	/*!<Bit 1 */

	#define  WWDG_CFR_EWI                        ((uint16_t)0x0200)            	/*!<Early Wakeup Interrupt */

	/*******************  Bit definition for WWDG_SR register  ********************/
	#define  WWDG_SR_EWIF                        ((uint8_t)0x01)               	/*!<Early Wakeup Interrupt Flag */

	/******************************************************************************/
	/*                                                                            */
	/*                       Flexible Static Memory Controller                    */
	/*                                                                            */
	/******************************************************************************/
	/******************  Bit definition for FSMC_BCR1 register  *******************/
	#define  FSMC_MBKEN                     0        		/*!<Memory bank enable bit */
	#define  FSMC_MUXEN                     1        		/*!<Address/data multiplexing enable bit */
	#define  FSMC_MTYP                      2        		/*!<MTYP[1:0] bits (Memory type) */
	#define  FSMC_MWID                      4       	 	/*!<MWID[1:0] bits (Memory data bus width) */
	#define  FSMC_FACCEN                    6     	   	/*!<Flash access enable */
	#define  FSMC_BURSTEN                   8   	     	/*!<Burst enable bit */
	#define  FSMC_WAITPOL                   9 	       	/*!<Wait signal polarity bit */
	#define  FSMC_WRAPMOD                   10        	/*!<Wrapped burst mode support */
	#define  FSMC_WAITCFG                   11        	/*!<Wait timing configuration */
	#define  FSMC_WREN                      12        	/*!<Write enable bit */
	#define  FSMC_WAITEN                    13        	/*!<Wait enable bit */
	#define  FSMC_EXTMOD                    14        	/*!<Extended mode enable */
	#define  FSMC_CBURSTRW                  19        	/*!<Write burst enable */

	

	/******************  Bit definition for FSMC_BTR1 and FSMC_BWTR1 register  ******************/
	#define  FSMC_ADDSET                    0	        	/*!<ADDSET[3:0] bits (Address setup phase duration) */
	#define  FSMC_ADDHLD                    4	        	/*!<ADDHLD[3:0] bits (Address-hold phase duration) */
	#define  FSMC_DATAST                    8	        	/*!<DATAST [3:0] bits (Data-phase duration) */
	#define  FSMC_BUSTURN                   16        	/*!<BUSTURN[3:0] bits (Bus turnaround phase duration) */
	#define  FSMC_CLKDIV                    20        	/*!<CLKDIV[3:0] bits (Clock divide ratio) */
	#define  FSMC_DATLAT                    24        	/*!<DATLA[3:0] bits (Data latency) */
	#define  FSMC_ACCMOD                    28        	/*!<ACCMOD[1:0] bits (Access mode) */



	/******************  Bit definition for FSMC_PCR2 register  *******************/
	#define  FSMC_PCR2_PWAITEN                   (0x00000002)        	/*!<Wait feature enable bit */
	#define  FSMC_PCR2_PBKEN                     (0x00000004)        	/*!<PC Card/NAND Flash memory bank enable bit */
	#define  FSMC_PCR2_PTYP                      (0x00000008)        	/*!<Memory type */

	#define  FSMC_PCR2_PWID                      (0x00000030)        	/*!<PWID[1:0] bits (NAND Flash databus width) */
	#define  FSMC_PCR2_PWID_0                    (0x00000010)        	/*!<Bit 0 */
	#define  FSMC_PCR2_PWID_1                    (0x00000020)        	/*!<Bit 1 */

	#define  FSMC_PCR2_ECCEN                     (0x00000040)        	/*!<ECC computation logic enable bit */

	#define  FSMC_PCR2_TCLR                      (0x00001E00)        	/*!<TCLR[3:0] bits (CLE to RE delay) */
	#define  FSMC_PCR2_TCLR_0                    (0x00000200)        	/*!<Bit 0 */
	#define  FSMC_PCR2_TCLR_1                    (0x00000400)        	/*!<Bit 1 */
	#define  FSMC_PCR2_TCLR_2                    (0x00000800)        	/*!<Bit 2 */
	#define  FSMC_PCR2_TCLR_3                    (0x00001000)        	/*!<Bit 3 */

	#define  FSMC_PCR2_TAR                       (0x0001E000)        	/*!<TAR[3:0] bits (ALE to RE delay) */
	#define  FSMC_PCR2_TAR_0                     (0x00002000)        	/*!<Bit 0 */
	#define  FSMC_PCR2_TAR_1                     (0x00004000)        	/*!<Bit 1 */
	#define  FSMC_PCR2_TAR_2                     (0x00008000)        	/*!<Bit 2 */
	#define  FSMC_PCR2_TAR_3                     (0x00010000)        	/*!<Bit 3 */

	#define  FSMC_PCR2_ECCPS                     (0x000E0000)        	/*!<ECCPS[1:0] bits (ECC page size) */
	#define  FSMC_PCR2_ECCPS_0                   (0x00020000)        	/*!<Bit 0 */
	#define  FSMC_PCR2_ECCPS_1                   (0x00040000)        	/*!<Bit 1 */
	#define  FSMC_PCR2_ECCPS_2                   (0x00080000)        	/*!<Bit 2 */

	/******************  Bit definition for FSMC_PCR3 register  *******************/
	#define  FSMC_PCR3_PWAITEN                   (0x00000002)        	/*!<Wait feature enable bit */
	#define  FSMC_PCR3_PBKEN                     (0x00000004)        	/*!<PC Card/NAND Flash memory bank enable bit */
	#define  FSMC_PCR3_PTYP                      (0x00000008)        	/*!<Memory type */

	#define  FSMC_PCR3_PWID                      (0x00000030)        	/*!<PWID[1:0] bits (NAND Flash databus width) */
	#define  FSMC_PCR3_PWID_0                    (0x00000010)        	/*!<Bit 0 */
	#define  FSMC_PCR3_PWID_1                    (0x00000020)        	/*!<Bit 1 */

	#define  FSMC_PCR3_ECCEN                     (0x00000040)        	/*!<ECC computation logic enable bit */

	#define  FSMC_PCR3_TCLR                      (0x00001E00)        	/*!<TCLR[3:0] bits (CLE to RE delay) */
	#define  FSMC_PCR3_TCLR_0                    (0x00000200)        	/*!<Bit 0 */
	#define  FSMC_PCR3_TCLR_1                    (0x00000400)        	/*!<Bit 1 */
	#define  FSMC_PCR3_TCLR_2                    (0x00000800)        	/*!<Bit 2 */
	#define  FSMC_PCR3_TCLR_3                    (0x00001000)        	/*!<Bit 3 */

	#define  FSMC_PCR3_TAR                       (0x0001E000)        	/*!<TAR[3:0] bits (ALE to RE delay) */
	#define  FSMC_PCR3_TAR_0                     (0x00002000)        	/*!<Bit 0 */
	#define  FSMC_PCR3_TAR_1                     (0x00004000)        	/*!<Bit 1 */
	#define  FSMC_PCR3_TAR_2                     (0x00008000)        	/*!<Bit 2 */
	#define  FSMC_PCR3_TAR_3                     (0x00010000)        	/*!<Bit 3 */

	#define  FSMC_PCR3_ECCPS                     (0x000E0000)        	/*!<ECCPS[2:0] bits (ECC page size) */
	#define  FSMC_PCR3_ECCPS_0                   (0x00020000)        	/*!<Bit 0 */
	#define  FSMC_PCR3_ECCPS_1                   (0x00040000)        	/*!<Bit 1 */
	#define  FSMC_PCR3_ECCPS_2                   (0x00080000)        	/*!<Bit 2 */

	/******************  Bit definition for FSMC_PCR4 register  *******************/
	#define  FSMC_PCR4_PWAITEN                   (0x00000002)        	/*!<Wait feature enable bit */
	#define  FSMC_PCR4_PBKEN                     (0x00000004)        	/*!<PC Card/NAND Flash memory bank enable bit */
	#define  FSMC_PCR4_PTYP                      (0x00000008)        	/*!<Memory type */

	#define  FSMC_PCR4_PWID                      (0x00000030)        	/*!<PWID[1:0] bits (NAND Flash databus width) */
	#define  FSMC_PCR4_PWID_0                    (0x00000010)        	/*!<Bit 0 */
	#define  FSMC_PCR4_PWID_1                    (0x00000020)        	/*!<Bit 1 */

	#define  FSMC_PCR4_ECCEN                     (0x00000040)        	/*!<ECC computation logic enable bit */

	#define  FSMC_PCR4_TCLR                      (0x00001E00)        	/*!<TCLR[3:0] bits (CLE to RE delay) */
	#define  FSMC_PCR4_TCLR_0                    (0x00000200)        	/*!<Bit 0 */
	#define  FSMC_PCR4_TCLR_1                    (0x00000400)        	/*!<Bit 1 */
	#define  FSMC_PCR4_TCLR_2                    (0x00000800)        	/*!<Bit 2 */
	#define  FSMC_PCR4_TCLR_3                    (0x00001000)        	/*!<Bit 3 */

	#define  FSMC_PCR4_TAR                       (0x0001E000)        	/*!<TAR[3:0] bits (ALE to RE delay) */
	#define  FSMC_PCR4_TAR_0                     (0x00002000)        	/*!<Bit 0 */
	#define  FSMC_PCR4_TAR_1                     (0x00004000)        	/*!<Bit 1 */
	#define  FSMC_PCR4_TAR_2                     (0x00008000)        	/*!<Bit 2 */
	#define  FSMC_PCR4_TAR_3                     (0x00010000)        	/*!<Bit 3 */

	#define  FSMC_PCR4_ECCPS                     (0x000E0000)        	/*!<ECCPS[2:0] bits (ECC page size) */
	#define  FSMC_PCR4_ECCPS_0                   (0x00020000)        	/*!<Bit 0 */
	#define  FSMC_PCR4_ECCPS_1                   (0x00040000)        	/*!<Bit 1 */
	#define  FSMC_PCR4_ECCPS_2                   (0x00080000)        	/*!<Bit 2 */

	/*******************  Bit definition for FSMC_SR2 register  *******************/
	#define  FSMC_SR2_IRS                        ((uint8_t)0x01)               	/*!<Interrupt Rising Edge status */
	#define  FSMC_SR2_ILS                        ((uint8_t)0x02)               	/*!<Interrupt Level status */
	#define  FSMC_SR2_IFS                        ((uint8_t)0x04)               	/*!<Interrupt Falling Edge status */
	#define  FSMC_SR2_IREN                       ((uint8_t)0x08)               	/*!<Interrupt Rising Edge detection Enable bit */
	#define  FSMC_SR2_ILEN                       ((uint8_t)0x10)               	/*!<Interrupt Level detection Enable bit */
	#define  FSMC_SR2_IFEN                       ((uint8_t)0x20)               	/*!<Interrupt Falling Edge detection Enable bit */
	#define  FSMC_SR2_FEMPT                      ((uint8_t)0x40)               	/*!<FIFO empty */

	/*******************  Bit definition for FSMC_SR3 register  *******************/
	#define  FSMC_SR3_IRS                        ((uint8_t)0x01)               	/*!<Interrupt Rising Edge status */
	#define  FSMC_SR3_ILS                        ((uint8_t)0x02)               	/*!<Interrupt Level status */
	#define  FSMC_SR3_IFS                        ((uint8_t)0x04)               	/*!<Interrupt Falling Edge status */
	#define  FSMC_SR3_IREN                       ((uint8_t)0x08)               	/*!<Interrupt Rising Edge detection Enable bit */
	#define  FSMC_SR3_ILEN                       ((uint8_t)0x10)               	/*!<Interrupt Level detection Enable bit */
	#define  FSMC_SR3_IFEN                       ((uint8_t)0x20)               	/*!<Interrupt Falling Edge detection Enable bit */
	#define  FSMC_SR3_FEMPT                      ((uint8_t)0x40)               	/*!<FIFO empty */

	/*******************  Bit definition for FSMC_SR4 register  *******************/
	#define  FSMC_SR4_IRS                        ((uint8_t)0x01)               	/*!<Interrupt Rising Edge status */
	#define  FSMC_SR4_ILS                        ((uint8_t)0x02)               	/*!<Interrupt Level status */
	#define  FSMC_SR4_IFS                        ((uint8_t)0x04)               	/*!<Interrupt Falling Edge status */
	#define  FSMC_SR4_IREN                       ((uint8_t)0x08)               	/*!<Interrupt Rising Edge detection Enable bit */
	#define  FSMC_SR4_ILEN                       ((uint8_t)0x10)               	/*!<Interrupt Level detection Enable bit */
	#define  FSMC_SR4_IFEN                       ((uint8_t)0x20)               	/*!<Interrupt Falling Edge detection Enable bit */
	#define  FSMC_SR4_FEMPT                      ((uint8_t)0x40)               	/*!<FIFO empty */

	/******************  Bit definition for FSMC_PMEM2 register  ******************/
	#define  FSMC_PMEM2_MEMSET2                  (0x000000FF)        	/*!<MEMSET2[7:0] bits (Common memory 2 setup time) */
	#define  FSMC_PMEM2_MEMSET2_0                (0x00000001)        	/*!<Bit 0 */
	#define  FSMC_PMEM2_MEMSET2_1                (0x00000002)        	/*!<Bit 1 */
	#define  FSMC_PMEM2_MEMSET2_2                (0x00000004)        	/*!<Bit 2 */
	#define  FSMC_PMEM2_MEMSET2_3                (0x00000008)        	/*!<Bit 3 */
	#define  FSMC_PMEM2_MEMSET2_4                (0x00000010)        	/*!<Bit 4 */
	#define  FSMC_PMEM2_MEMSET2_5                (0x00000020)        	/*!<Bit 5 */
	#define  FSMC_PMEM2_MEMSET2_6                (0x00000040)        	/*!<Bit 6 */
	#define  FSMC_PMEM2_MEMSET2_7                (0x00000080)        	/*!<Bit 7 */

	#define  FSMC_PMEM2_MEMWAIT2                 (0x0000FF00)        	/*!<MEMWAIT2[7:0] bits (Common memory 2 wait time) */
	#define  FSMC_PMEM2_MEMWAIT2_0               (0x00000100)        	/*!<Bit 0 */
	#define  FSMC_PMEM2_MEMWAIT2_1               (0x00000200)        	/*!<Bit 1 */
	#define  FSMC_PMEM2_MEMWAIT2_2               (0x00000400)        	/*!<Bit 2 */
	#define  FSMC_PMEM2_MEMWAIT2_3               (0x00000800)        	/*!<Bit 3 */
	#define  FSMC_PMEM2_MEMWAIT2_4               (0x00001000)        	/*!<Bit 4 */
	#define  FSMC_PMEM2_MEMWAIT2_5               (0x00002000)        	/*!<Bit 5 */
	#define  FSMC_PMEM2_MEMWAIT2_6               (0x00004000)        	/*!<Bit 6 */
	#define  FSMC_PMEM2_MEMWAIT2_7               (0x00008000)        	/*!<Bit 7 */

	#define  FSMC_PMEM2_MEMHOLD2                 (0x00FF0000)        	/*!<MEMHOLD2[7:0] bits (Common memory 2 hold time) */
	#define  FSMC_PMEM2_MEMHOLD2_0               (0x00010000)        	/*!<Bit 0 */
	#define  FSMC_PMEM2_MEMHOLD2_1               (0x00020000)        	/*!<Bit 1 */
	#define  FSMC_PMEM2_MEMHOLD2_2               (0x00040000)        	/*!<Bit 2 */
	#define  FSMC_PMEM2_MEMHOLD2_3               (0x00080000)        	/*!<Bit 3 */
	#define  FSMC_PMEM2_MEMHOLD2_4               (0x00100000)        	/*!<Bit 4 */
	#define  FSMC_PMEM2_MEMHOLD2_5               (0x00200000)        	/*!<Bit 5 */
	#define  FSMC_PMEM2_MEMHOLD2_6               (0x00400000)        	/*!<Bit 6 */
	#define  FSMC_PMEM2_MEMHOLD2_7               (0x00800000)        	/*!<Bit 7 */

	#define  FSMC_PMEM2_MEMHIZ2                  (0xFF000000)        	/*!<MEMHIZ2[7:0] bits (Common memory 2 databus HiZ time) */
	#define  FSMC_PMEM2_MEMHIZ2_0                (0x01000000)        	/*!<Bit 0 */
	#define  FSMC_PMEM2_MEMHIZ2_1                (0x02000000)        	/*!<Bit 1 */
	#define  FSMC_PMEM2_MEMHIZ2_2                (0x04000000)        	/*!<Bit 2 */
	#define  FSMC_PMEM2_MEMHIZ2_3                (0x08000000)        	/*!<Bit 3 */
	#define  FSMC_PMEM2_MEMHIZ2_4                (0x10000000)        	/*!<Bit 4 */
	#define  FSMC_PMEM2_MEMHIZ2_5                (0x20000000)        	/*!<Bit 5 */
	#define  FSMC_PMEM2_MEMHIZ2_6                (0x40000000)        	/*!<Bit 6 */
	#define  FSMC_PMEM2_MEMHIZ2_7                (0x80000000)        	/*!<Bit 7 */

	/******************  Bit definition for FSMC_PMEM3 register  ******************/
	#define  FSMC_PMEM3_MEMSET3                  (0x000000FF)        	/*!<MEMSET3[7:0] bits (Common memory 3 setup time) */
	#define  FSMC_PMEM3_MEMSET3_0                (0x00000001)        	/*!<Bit 0 */
	#define  FSMC_PMEM3_MEMSET3_1                (0x00000002)        	/*!<Bit 1 */
	#define  FSMC_PMEM3_MEMSET3_2                (0x00000004)        	/*!<Bit 2 */
	#define  FSMC_PMEM3_MEMSET3_3                (0x00000008)        	/*!<Bit 3 */
	#define  FSMC_PMEM3_MEMSET3_4                (0x00000010)        	/*!<Bit 4 */
	#define  FSMC_PMEM3_MEMSET3_5                (0x00000020)        	/*!<Bit 5 */
	#define  FSMC_PMEM3_MEMSET3_6                (0x00000040)        	/*!<Bit 6 */
	#define  FSMC_PMEM3_MEMSET3_7                (0x00000080)        	/*!<Bit 7 */

	#define  FSMC_PMEM3_MEMWAIT3                 (0x0000FF00)        	/*!<MEMWAIT3[7:0] bits (Common memory 3 wait time) */
	#define  FSMC_PMEM3_MEMWAIT3_0               (0x00000100)        	/*!<Bit 0 */
	#define  FSMC_PMEM3_MEMWAIT3_1               (0x00000200)        	/*!<Bit 1 */
	#define  FSMC_PMEM3_MEMWAIT3_2               (0x00000400)        	/*!<Bit 2 */
	#define  FSMC_PMEM3_MEMWAIT3_3               (0x00000800)        	/*!<Bit 3 */
	#define  FSMC_PMEM3_MEMWAIT3_4               (0x00001000)        	/*!<Bit 4 */
	#define  FSMC_PMEM3_MEMWAIT3_5               (0x00002000)        	/*!<Bit 5 */
	#define  FSMC_PMEM3_MEMWAIT3_6               (0x00004000)        	/*!<Bit 6 */
	#define  FSMC_PMEM3_MEMWAIT3_7               (0x00008000)        	/*!<Bit 7 */

	#define  FSMC_PMEM3_MEMHOLD3                 (0x00FF0000)        	/*!<MEMHOLD3[7:0] bits (Common memory 3 hold time) */
	#define  FSMC_PMEM3_MEMHOLD3_0               (0x00010000)        	/*!<Bit 0 */
	#define  FSMC_PMEM3_MEMHOLD3_1               (0x00020000)        	/*!<Bit 1 */
	#define  FSMC_PMEM3_MEMHOLD3_2               (0x00040000)        	/*!<Bit 2 */
	#define  FSMC_PMEM3_MEMHOLD3_3               (0x00080000)        	/*!<Bit 3 */
	#define  FSMC_PMEM3_MEMHOLD3_4               (0x00100000)        	/*!<Bit 4 */
	#define  FSMC_PMEM3_MEMHOLD3_5               (0x00200000)        	/*!<Bit 5 */
	#define  FSMC_PMEM3_MEMHOLD3_6               (0x00400000)        	/*!<Bit 6 */
	#define  FSMC_PMEM3_MEMHOLD3_7               (0x00800000)        	/*!<Bit 7 */

	#define  FSMC_PMEM3_MEMHIZ3                  (0xFF000000)        	/*!<MEMHIZ3[7:0] bits (Common memory 3 databus HiZ time) */
	#define  FSMC_PMEM3_MEMHIZ3_0                (0x01000000)        	/*!<Bit 0 */
	#define  FSMC_PMEM3_MEMHIZ3_1                (0x02000000)        	/*!<Bit 1 */
	#define  FSMC_PMEM3_MEMHIZ3_2                (0x04000000)        	/*!<Bit 2 */
	#define  FSMC_PMEM3_MEMHIZ3_3                (0x08000000)        	/*!<Bit 3 */
	#define  FSMC_PMEM3_MEMHIZ3_4                (0x10000000)        	/*!<Bit 4 */
	#define  FSMC_PMEM3_MEMHIZ3_5                (0x20000000)        	/*!<Bit 5 */
	#define  FSMC_PMEM3_MEMHIZ3_6                (0x40000000)        	/*!<Bit 6 */
	#define  FSMC_PMEM3_MEMHIZ3_7                (0x80000000)        	/*!<Bit 7 */

	/******************  Bit definition for FSMC_PMEM4 register  ******************/
	#define  FSMC_PMEM4_MEMSET4                  (0x000000FF)        	/*!<MEMSET4[7:0] bits (Common memory 4 setup time) */
	#define  FSMC_PMEM4_MEMSET4_0                (0x00000001)        	/*!<Bit 0 */
	#define  FSMC_PMEM4_MEMSET4_1                (0x00000002)        	/*!<Bit 1 */
	#define  FSMC_PMEM4_MEMSET4_2                (0x00000004)        	/*!<Bit 2 */
	#define  FSMC_PMEM4_MEMSET4_3                (0x00000008)        	/*!<Bit 3 */
	#define  FSMC_PMEM4_MEMSET4_4                (0x00000010)        	/*!<Bit 4 */
	#define  FSMC_PMEM4_MEMSET4_5                (0x00000020)        	/*!<Bit 5 */
	#define  FSMC_PMEM4_MEMSET4_6                (0x00000040)        	/*!<Bit 6 */
	#define  FSMC_PMEM4_MEMSET4_7                (0x00000080)        	/*!<Bit 7 */

	#define  FSMC_PMEM4_MEMWAIT4                 (0x0000FF00)        	/*!<MEMWAIT4[7:0] bits (Common memory 4 wait time) */
	#define  FSMC_PMEM4_MEMWAIT4_0               (0x00000100)        	/*!<Bit 0 */
	#define  FSMC_PMEM4_MEMWAIT4_1               (0x00000200)        	/*!<Bit 1 */
	#define  FSMC_PMEM4_MEMWAIT4_2               (0x00000400)        	/*!<Bit 2 */
	#define  FSMC_PMEM4_MEMWAIT4_3               (0x00000800)        	/*!<Bit 3 */
	#define  FSMC_PMEM4_MEMWAIT4_4               (0x00001000)        	/*!<Bit 4 */
	#define  FSMC_PMEM4_MEMWAIT4_5               (0x00002000)        	/*!<Bit 5 */
	#define  FSMC_PMEM4_MEMWAIT4_6               (0x00004000)        	/*!<Bit 6 */
	#define  FSMC_PMEM4_MEMWAIT4_7               (0x00008000)        	/*!<Bit 7 */

	#define  FSMC_PMEM4_MEMHOLD4                 (0x00FF0000)        	/*!<MEMHOLD4[7:0] bits (Common memory 4 hold time) */
	#define  FSMC_PMEM4_MEMHOLD4_0               (0x00010000)        	/*!<Bit 0 */
	#define  FSMC_PMEM4_MEMHOLD4_1               (0x00020000)        	/*!<Bit 1 */
	#define  FSMC_PMEM4_MEMHOLD4_2               (0x00040000)        	/*!<Bit 2 */
	#define  FSMC_PMEM4_MEMHOLD4_3               (0x00080000)        	/*!<Bit 3 */
	#define  FSMC_PMEM4_MEMHOLD4_4               (0x00100000)        	/*!<Bit 4 */
	#define  FSMC_PMEM4_MEMHOLD4_5               (0x00200000)        	/*!<Bit 5 */
	#define  FSMC_PMEM4_MEMHOLD4_6               (0x00400000)        	/*!<Bit 6 */
	#define  FSMC_PMEM4_MEMHOLD4_7               (0x00800000)        	/*!<Bit 7 */

	#define  FSMC_PMEM4_MEMHIZ4                  (0xFF000000)        	/*!<MEMHIZ4[7:0] bits (Common memory 4 databus HiZ time) */
	#define  FSMC_PMEM4_MEMHIZ4_0                (0x01000000)        	/*!<Bit 0 */
	#define  FSMC_PMEM4_MEMHIZ4_1                (0x02000000)        	/*!<Bit 1 */
	#define  FSMC_PMEM4_MEMHIZ4_2                (0x04000000)        	/*!<Bit 2 */
	#define  FSMC_PMEM4_MEMHIZ4_3                (0x08000000)        	/*!<Bit 3 */
	#define  FSMC_PMEM4_MEMHIZ4_4                (0x10000000)        	/*!<Bit 4 */
	#define  FSMC_PMEM4_MEMHIZ4_5                (0x20000000)        	/*!<Bit 5 */
	#define  FSMC_PMEM4_MEMHIZ4_6                (0x40000000)        	/*!<Bit 6 */
	#define  FSMC_PMEM4_MEMHIZ4_7                (0x80000000)        	/*!<Bit 7 */

	/******************  Bit definition for FSMC_PATT2 register  ******************/
	#define  FSMC_PATT2_ATTSET2                  (0x000000FF)        	/*!<ATTSET2[7:0] bits (Attribute memory 2 setup time) */
	#define  FSMC_PATT2_ATTSET2_0                (0x00000001)        	/*!<Bit 0 */
	#define  FSMC_PATT2_ATTSET2_1                (0x00000002)        	/*!<Bit 1 */
	#define  FSMC_PATT2_ATTSET2_2                (0x00000004)        	/*!<Bit 2 */
	#define  FSMC_PATT2_ATTSET2_3                (0x00000008)        	/*!<Bit 3 */
	#define  FSMC_PATT2_ATTSET2_4                (0x00000010)        	/*!<Bit 4 */
	#define  FSMC_PATT2_ATTSET2_5                (0x00000020)        	/*!<Bit 5 */
	#define  FSMC_PATT2_ATTSET2_6                (0x00000040)        	/*!<Bit 6 */
	#define  FSMC_PATT2_ATTSET2_7                (0x00000080)        	/*!<Bit 7 */

	#define  FSMC_PATT2_ATTWAIT2                 (0x0000FF00)        	/*!<ATTWAIT2[7:0] bits (Attribute memory 2 wait time) */
	#define  FSMC_PATT2_ATTWAIT2_0               (0x00000100)        	/*!<Bit 0 */
	#define  FSMC_PATT2_ATTWAIT2_1               (0x00000200)        	/*!<Bit 1 */
	#define  FSMC_PATT2_ATTWAIT2_2               (0x00000400)        	/*!<Bit 2 */
	#define  FSMC_PATT2_ATTWAIT2_3               (0x00000800)        	/*!<Bit 3 */
	#define  FSMC_PATT2_ATTWAIT2_4               (0x00001000)        	/*!<Bit 4 */
	#define  FSMC_PATT2_ATTWAIT2_5               (0x00002000)        	/*!<Bit 5 */
	#define  FSMC_PATT2_ATTWAIT2_6               (0x00004000)        	/*!<Bit 6 */
	#define  FSMC_PATT2_ATTWAIT2_7               (0x00008000)        	/*!<Bit 7 */

	#define  FSMC_PATT2_ATTHOLD2                 (0x00FF0000)        	/*!<ATTHOLD2[7:0] bits (Attribute memory 2 hold time) */
	#define  FSMC_PATT2_ATTHOLD2_0               (0x00010000)        	/*!<Bit 0 */
	#define  FSMC_PATT2_ATTHOLD2_1               (0x00020000)        	/*!<Bit 1 */
	#define  FSMC_PATT2_ATTHOLD2_2               (0x00040000)        	/*!<Bit 2 */
	#define  FSMC_PATT2_ATTHOLD2_3               (0x00080000)        	/*!<Bit 3 */
	#define  FSMC_PATT2_ATTHOLD2_4               (0x00100000)        	/*!<Bit 4 */
	#define  FSMC_PATT2_ATTHOLD2_5               (0x00200000)        	/*!<Bit 5 */
	#define  FSMC_PATT2_ATTHOLD2_6               (0x00400000)        	/*!<Bit 6 */
	#define  FSMC_PATT2_ATTHOLD2_7               (0x00800000)        	/*!<Bit 7 */

	#define  FSMC_PATT2_ATTHIZ2                  (0xFF000000)        	/*!<ATTHIZ2[7:0] bits (Attribute memory 2 databus HiZ time) */
	#define  FSMC_PATT2_ATTHIZ2_0                (0x01000000)        	/*!<Bit 0 */
	#define  FSMC_PATT2_ATTHIZ2_1                (0x02000000)        	/*!<Bit 1 */
	#define  FSMC_PATT2_ATTHIZ2_2                (0x04000000)        	/*!<Bit 2 */
	#define  FSMC_PATT2_ATTHIZ2_3                (0x08000000)        	/*!<Bit 3 */
	#define  FSMC_PATT2_ATTHIZ2_4                (0x10000000)        	/*!<Bit 4 */
	#define  FSMC_PATT2_ATTHIZ2_5                (0x20000000)        	/*!<Bit 5 */
	#define  FSMC_PATT2_ATTHIZ2_6                (0x40000000)        	/*!<Bit 6 */
	#define  FSMC_PATT2_ATTHIZ2_7                (0x80000000)        	/*!<Bit 7 */

	/******************  Bit definition for FSMC_PATT3 register  ******************/
	#define  FSMC_PATT3_ATTSET3                  (0x000000FF)        	/*!<ATTSET3[7:0] bits (Attribute memory 3 setup time) */
	#define  FSMC_PATT3_ATTSET3_0                (0x00000001)        	/*!<Bit 0 */
	#define  FSMC_PATT3_ATTSET3_1                (0x00000002)        	/*!<Bit 1 */
	#define  FSMC_PATT3_ATTSET3_2                (0x00000004)        	/*!<Bit 2 */
	#define  FSMC_PATT3_ATTSET3_3                (0x00000008)        	/*!<Bit 3 */
	#define  FSMC_PATT3_ATTSET3_4                (0x00000010)        	/*!<Bit 4 */
	#define  FSMC_PATT3_ATTSET3_5                (0x00000020)        	/*!<Bit 5 */
	#define  FSMC_PATT3_ATTSET3_6                (0x00000040)        	/*!<Bit 6 */
	#define  FSMC_PATT3_ATTSET3_7                (0x00000080)        	/*!<Bit 7 */

	#define  FSMC_PATT3_ATTWAIT3                 (0x0000FF00)        	/*!<ATTWAIT3[7:0] bits (Attribute memory 3 wait time) */
	#define  FSMC_PATT3_ATTWAIT3_0               (0x00000100)        	/*!<Bit 0 */
	#define  FSMC_PATT3_ATTWAIT3_1               (0x00000200)        	/*!<Bit 1 */
	#define  FSMC_PATT3_ATTWAIT3_2               (0x00000400)        	/*!<Bit 2 */
	#define  FSMC_PATT3_ATTWAIT3_3               (0x00000800)        	/*!<Bit 3 */
	#define  FSMC_PATT3_ATTWAIT3_4               (0x00001000)        	/*!<Bit 4 */
	#define  FSMC_PATT3_ATTWAIT3_5               (0x00002000)        	/*!<Bit 5 */
	#define  FSMC_PATT3_ATTWAIT3_6               (0x00004000)        	/*!<Bit 6 */
	#define  FSMC_PATT3_ATTWAIT3_7               (0x00008000)        	/*!<Bit 7 */

	#define  FSMC_PATT3_ATTHOLD3                 (0x00FF0000)        	/*!<ATTHOLD3[7:0] bits (Attribute memory 3 hold time) */
	#define  FSMC_PATT3_ATTHOLD3_0               (0x00010000)        	/*!<Bit 0 */
	#define  FSMC_PATT3_ATTHOLD3_1               (0x00020000)        	/*!<Bit 1 */
	#define  FSMC_PATT3_ATTHOLD3_2               (0x00040000)        	/*!<Bit 2 */
	#define  FSMC_PATT3_ATTHOLD3_3               (0x00080000)        	/*!<Bit 3 */
	#define  FSMC_PATT3_ATTHOLD3_4               (0x00100000)        	/*!<Bit 4 */
	#define  FSMC_PATT3_ATTHOLD3_5               (0x00200000)        	/*!<Bit 5 */
	#define  FSMC_PATT3_ATTHOLD3_6               (0x00400000)        	/*!<Bit 6 */
	#define  FSMC_PATT3_ATTHOLD3_7               (0x00800000)        	/*!<Bit 7 */

	#define  FSMC_PATT3_ATTHIZ3                  (0xFF000000)        	/*!<ATTHIZ3[7:0] bits (Attribute memory 3 databus HiZ time) */
	#define  FSMC_PATT3_ATTHIZ3_0                (0x01000000)        	/*!<Bit 0 */
	#define  FSMC_PATT3_ATTHIZ3_1                (0x02000000)        	/*!<Bit 1 */
	#define  FSMC_PATT3_ATTHIZ3_2                (0x04000000)        	/*!<Bit 2 */
	#define  FSMC_PATT3_ATTHIZ3_3                (0x08000000)        	/*!<Bit 3 */
	#define  FSMC_PATT3_ATTHIZ3_4                (0x10000000)        	/*!<Bit 4 */
	#define  FSMC_PATT3_ATTHIZ3_5                (0x20000000)        	/*!<Bit 5 */
	#define  FSMC_PATT3_ATTHIZ3_6                (0x40000000)        	/*!<Bit 6 */
	#define  FSMC_PATT3_ATTHIZ3_7                (0x80000000)        	/*!<Bit 7 */

	/******************  Bit definition for FSMC_PATT4 register  ******************/
	#define  FSMC_PATT4_ATTSET4                  (0x000000FF)        	/*!<ATTSET4[7:0] bits (Attribute memory 4 setup time) */
	#define  FSMC_PATT4_ATTSET4_0                (0x00000001)        	/*!<Bit 0 */
	#define  FSMC_PATT4_ATTSET4_1                (0x00000002)        	/*!<Bit 1 */
	#define  FSMC_PATT4_ATTSET4_2                (0x00000004)        	/*!<Bit 2 */
	#define  FSMC_PATT4_ATTSET4_3                (0x00000008)        	/*!<Bit 3 */
	#define  FSMC_PATT4_ATTSET4_4                (0x00000010)        	/*!<Bit 4 */
	#define  FSMC_PATT4_ATTSET4_5                (0x00000020)        	/*!<Bit 5 */
	#define  FSMC_PATT4_ATTSET4_6                (0x00000040)        	/*!<Bit 6 */
	#define  FSMC_PATT4_ATTSET4_7                (0x00000080)        	/*!<Bit 7 */

	#define  FSMC_PATT4_ATTWAIT4                 (0x0000FF00)        	/*!<ATTWAIT4[7:0] bits (Attribute memory 4 wait time) */
	#define  FSMC_PATT4_ATTWAIT4_0               (0x00000100)        	/*!<Bit 0 */
	#define  FSMC_PATT4_ATTWAIT4_1               (0x00000200)        	/*!<Bit 1 */
	#define  FSMC_PATT4_ATTWAIT4_2               (0x00000400)        	/*!<Bit 2 */
	#define  FSMC_PATT4_ATTWAIT4_3               (0x00000800)        	/*!<Bit 3 */
	#define  FSMC_PATT4_ATTWAIT4_4               (0x00001000)        	/*!<Bit 4 */
	#define  FSMC_PATT4_ATTWAIT4_5               (0x00002000)        	/*!<Bit 5 */
	#define  FSMC_PATT4_ATTWAIT4_6               (0x00004000)        	/*!<Bit 6 */
	#define  FSMC_PATT4_ATTWAIT4_7               (0x00008000)        	/*!<Bit 7 */

	#define  FSMC_PATT4_ATTHOLD4                 (0x00FF0000)        	/*!<ATTHOLD4[7:0] bits (Attribute memory 4 hold time) */
	#define  FSMC_PATT4_ATTHOLD4_0               (0x00010000)        	/*!<Bit 0 */
	#define  FSMC_PATT4_ATTHOLD4_1               (0x00020000)        	/*!<Bit 1 */
	#define  FSMC_PATT4_ATTHOLD4_2               (0x00040000)        	/*!<Bit 2 */
	#define  FSMC_PATT4_ATTHOLD4_3               (0x00080000)        	/*!<Bit 3 */
	#define  FSMC_PATT4_ATTHOLD4_4               (0x00100000)        	/*!<Bit 4 */
	#define  FSMC_PATT4_ATTHOLD4_5               (0x00200000)        	/*!<Bit 5 */
	#define  FSMC_PATT4_ATTHOLD4_6               (0x00400000)        	/*!<Bit 6 */
	#define  FSMC_PATT4_ATTHOLD4_7               (0x00800000)        	/*!<Bit 7 */

	#define  FSMC_PATT4_ATTHIZ4                  (0xFF000000)        	/*!<ATTHIZ4[7:0] bits (Attribute memory 4 databus HiZ time) */
	#define  FSMC_PATT4_ATTHIZ4_0                (0x01000000)        	/*!<Bit 0 */
	#define  FSMC_PATT4_ATTHIZ4_1                (0x02000000)        	/*!<Bit 1 */
	#define  FSMC_PATT4_ATTHIZ4_2                (0x04000000)        	/*!<Bit 2 */
	#define  FSMC_PATT4_ATTHIZ4_3                (0x08000000)        	/*!<Bit 3 */
	#define  FSMC_PATT4_ATTHIZ4_4                (0x10000000)        	/*!<Bit 4 */
	#define  FSMC_PATT4_ATTHIZ4_5                (0x20000000)        	/*!<Bit 5 */
	#define  FSMC_PATT4_ATTHIZ4_6                (0x40000000)        	/*!<Bit 6 */
	#define  FSMC_PATT4_ATTHIZ4_7                (0x80000000)        	/*!<Bit 7 */

	/******************  Bit definition for FSMC_PIO4 register  *******************/
	#define  FSMC_PIO4_IOSET4                    (0x000000FF)        	/*!<IOSET4[7:0] bits (I/O 4 setup time) */
	#define  FSMC_PIO4_IOSET4_0                  (0x00000001)        	/*!<Bit 0 */
	#define  FSMC_PIO4_IOSET4_1                  (0x00000002)        	/*!<Bit 1 */
	#define  FSMC_PIO4_IOSET4_2                  (0x00000004)        	/*!<Bit 2 */
	#define  FSMC_PIO4_IOSET4_3                  (0x00000008)        	/*!<Bit 3 */
	#define  FSMC_PIO4_IOSET4_4                  (0x00000010)        	/*!<Bit 4 */
	#define  FSMC_PIO4_IOSET4_5                  (0x00000020)        	/*!<Bit 5 */
	#define  FSMC_PIO4_IOSET4_6                  (0x00000040)        	/*!<Bit 6 */
	#define  FSMC_PIO4_IOSET4_7                  (0x00000080)        	/*!<Bit 7 */

	#define  FSMC_PIO4_IOWAIT4                   (0x0000FF00)        	/*!<IOWAIT4[7:0] bits (I/O 4 wait time) */
	#define  FSMC_PIO4_IOWAIT4_0                 (0x00000100)        	/*!<Bit 0 */
	#define  FSMC_PIO4_IOWAIT4_1                 (0x00000200)        	/*!<Bit 1 */
	#define  FSMC_PIO4_IOWAIT4_2                 (0x00000400)        	/*!<Bit 2 */
	#define  FSMC_PIO4_IOWAIT4_3                 (0x00000800)        	/*!<Bit 3 */
	#define  FSMC_PIO4_IOWAIT4_4                 (0x00001000)        	/*!<Bit 4 */
	#define  FSMC_PIO4_IOWAIT4_5                 (0x00002000)        	/*!<Bit 5 */
	#define  FSMC_PIO4_IOWAIT4_6                 (0x00004000)        	/*!<Bit 6 */
	#define  FSMC_PIO4_IOWAIT4_7                 (0x00008000)        	/*!<Bit 7 */

	#define  FSMC_PIO4_IOHOLD4                   (0x00FF0000)        	/*!<IOHOLD4[7:0] bits (I/O 4 hold time) */
	#define  FSMC_PIO4_IOHOLD4_0                 (0x00010000)        	/*!<Bit 0 */
	#define  FSMC_PIO4_IOHOLD4_1                 (0x00020000)        	/*!<Bit 1 */
	#define  FSMC_PIO4_IOHOLD4_2                 (0x00040000)        	/*!<Bit 2 */
	#define  FSMC_PIO4_IOHOLD4_3                 (0x00080000)        	/*!<Bit 3 */
	#define  FSMC_PIO4_IOHOLD4_4                 (0x00100000)        	/*!<Bit 4 */
	#define  FSMC_PIO4_IOHOLD4_5                 (0x00200000)        	/*!<Bit 5 */
	#define  FSMC_PIO4_IOHOLD4_6                 (0x00400000)        	/*!<Bit 6 */
	#define  FSMC_PIO4_IOHOLD4_7                 (0x00800000)        	/*!<Bit 7 */

	#define  FSMC_PIO4_IOHIZ4                    (0xFF000000)        	/*!<IOHIZ4[7:0] bits (I/O 4 databus HiZ time) */
	#define  FSMC_PIO4_IOHIZ4_0                  (0x01000000)        	/*!<Bit 0 */
	#define  FSMC_PIO4_IOHIZ4_1                  (0x02000000)        	/*!<Bit 1 */
	#define  FSMC_PIO4_IOHIZ4_2                  (0x04000000)        	/*!<Bit 2 */
	#define  FSMC_PIO4_IOHIZ4_3                  (0x08000000)        	/*!<Bit 3 */
	#define  FSMC_PIO4_IOHIZ4_4                  (0x10000000)        	/*!<Bit 4 */
	#define  FSMC_PIO4_IOHIZ4_5                  (0x20000000)        	/*!<Bit 5 */
	#define  FSMC_PIO4_IOHIZ4_6                  (0x40000000)        	/*!<Bit 6 */
	#define  FSMC_PIO4_IOHIZ4_7                  (0x80000000)        	/*!<Bit 7 */


	/******************  Bit definition for FSMC_ECCR2 register  ******************/
	#define  FSMC_ECCR2_ECC2                     (0xFFFFFFFF)        	/*!<ECC result */

	/******************  Bit definition for FSMC_ECCR3 register  ******************/
	#define  FSMC_ECCR3_ECC3                     (0xFFFFFFFF)        	/*!<ECC result */

	/******************************************************************************/
	/*                                                                            */
	/*                          SD host Interface                                 */
	/*                                                                            */
	/******************************************************************************/

	/******************  Bit definition for SDIO_POWER register  ******************/
	#define  SDIO_POWER_PWRCTRL                  ((uint8_t)0x03)               	/*!<PWRCTRL[1:0] bits (Power supply control bits) */
	#define  SDIO_POWER_PWRCTRL_0                ((uint8_t)0x01)               	/*!<Bit 0 */
	#define  SDIO_POWER_PWRCTRL_1                ((uint8_t)0x02)               	/*!<Bit 1 */

	/******************  Bit definition for SDIO_CLKCR register  ******************/
	#define  SDIO_CLKCR_CLKDIV                   ((uint16_t)0x00FF)            	/*!<Clock divide factor */
	#define  SDIO_CLKCR_CLKEN                    ((uint16_t)0x0100)            	/*!<Clock enable bit */
	#define  SDIO_CLKCR_PWRSAV                   ((uint16_t)0x0200)            	/*!<Power saving configuration bit */
	#define  SDIO_CLKCR_BYPASS                   ((uint16_t)0x0400)            	/*!<Clock divider bypass enable bit */

	#define  SDIO_CLKCR_WIDBUS                   ((uint16_t)0x1800)            	/*!<WIDBUS[1:0] bits (Wide bus mode enable bit) */
	#define  SDIO_CLKCR_WIDBUS_0                 ((uint16_t)0x0800)            	/*!<Bit 0 */
	#define  SDIO_CLKCR_WIDBUS_1                 ((uint16_t)0x1000)            	/*!<Bit 1 */

	#define  SDIO_CLKCR_NEGEDGE                  ((uint16_t)0x2000)            	/*!<SDIO_CK dephasing selection bit */
	#define  SDIO_CLKCR_HWFC_EN                  ((uint16_t)0x4000)            	/*!<HW Flow Control enable */

	/*******************  Bit definition for SDIO_ARG register  *******************/
	#define  SDIO_ARG_CMDARG                     (0xFFFFFFFF)            	/*!<Command argument */

	/*******************  Bit definition for SDIO_CMD register  *******************/
	#define  SDIO_CMD_CMDINDEX                   ((uint16_t)0x003F)            	/*!<Command Index */

	#define  SDIO_CMD_WAITRESP                   ((uint16_t)0x00C0)            	/*!<WAITRESP[1:0] bits (Wait for response bits) */
	#define  SDIO_CMD_WAITRESP_0                 ((uint16_t)0x0040)            	/*!< Bit 0 */
	#define  SDIO_CMD_WAITRESP_1                 ((uint16_t)0x0080)            	/*!< Bit 1 */

	#define  SDIO_CMD_WAITINT                    ((uint16_t)0x0100)            	/*!<CPSM Waits for Interrupt Request */
	#define  SDIO_CMD_WAITPEND                   ((uint16_t)0x0200)            	/*!<CPSM Waits for ends of data transfer (CmdPend internal signal) */
	#define  SDIO_CMD_CPSMEN                     ((uint16_t)0x0400)            	/*!<Command path state machine (CPSM) Enable bit */
	#define  SDIO_CMD_SDIOSUSPEND                ((uint16_t)0x0800)            	/*!<SD I/O suspend command */
	#define  SDIO_CMD_ENCMDCOMPL                 ((uint16_t)0x1000)            	/*!<Enable CMD completion */
	#define  SDIO_CMD_NIEN                       ((uint16_t)0x2000)            	/*!<Not Interrupt Enable */
	#define  SDIO_CMD_CEATACMD                   ((uint16_t)0x4000)            	/*!<CE-ATA command */

	/*****************  Bit definition for SDIO_RESPCMD register  *****************/
	#define  SDIO_RESPCMD_RESPCMD                ((uint8_t)0x3F)               	/*!<Response command index */

	/******************  Bit definition for SDIO_RESP0 register  ******************/
	#define  SDIO_RESP0_CARDSTATUS0              (0xFFFFFFFF)        	/*!<Card Status */

	/******************  Bit definition for SDIO_RESP1 register  ******************/
	#define  SDIO_RESP1_CARDSTATUS1              (0xFFFFFFFF)        	/*!<Card Status */

	/******************  Bit definition for SDIO_RESP2 register  ******************/
	#define  SDIO_RESP2_CARDSTATUS2              (0xFFFFFFFF)        	/*!<Card Status */

	/******************  Bit definition for SDIO_RESP3 register  ******************/
	#define  SDIO_RESP3_CARDSTATUS3              (0xFFFFFFFF)        	/*!<Card Status */

	/******************  Bit definition for SDIO_RESP4 register  ******************/
	#define  SDIO_RESP4_CARDSTATUS4              (0xFFFFFFFF)        	/*!<Card Status */

	/******************  Bit definition for SDIO_DTIMER register  *****************/
	#define  SDIO_DTIMER_DATATIME                (0xFFFFFFFF)        	/*!<Data timeout period. */

	/******************  Bit definition for SDIO_DLEN register  *******************/
	#define  SDIO_DLEN_DATALENGTH                (0x01FFFFFF)        	/*!<Data length value */

	/******************  Bit definition for SDIO_DCTRL register  ******************/
	#define  SDIO_DCTRL_DTEN                     ((uint16_t)0x0001)            	/*!<Data transfer enabled bit */
	#define  SDIO_DCTRL_DTDIR                    ((uint16_t)0x0002)            	/*!<Data transfer direction selection */
	#define  SDIO_DCTRL_DTMODE                   ((uint16_t)0x0004)            	/*!<Data transfer mode selection */
	#define  SDIO_DCTRL_DMAEN                    ((uint16_t)0x0008)            	/*!<DMA enabled bit */

	#define  SDIO_DCTRL_DBLOCKSIZE               ((uint16_t)0x00F0)            	/*!<DBLOCKSIZE[3:0] bits (Data block size) */
	#define  SDIO_DCTRL_DBLOCKSIZE_0             ((uint16_t)0x0010)            	/*!<Bit 0 */
	#define  SDIO_DCTRL_DBLOCKSIZE_1             ((uint16_t)0x0020)            	/*!<Bit 1 */
	#define  SDIO_DCTRL_DBLOCKSIZE_2             ((uint16_t)0x0040)            	/*!<Bit 2 */
	#define  SDIO_DCTRL_DBLOCKSIZE_3             ((uint16_t)0x0080)            	/*!<Bit 3 */

	#define  SDIO_DCTRL_RWSTART                  ((uint16_t)0x0100)            	/*!<Read wait start */
	#define  SDIO_DCTRL_RWSTOP                   ((uint16_t)0x0200)            	/*!<Read wait stop */
	#define  SDIO_DCTRL_RWMOD                    ((uint16_t)0x0400)            	/*!<Read wait mode */
	#define  SDIO_DCTRL_SDIOEN                   ((uint16_t)0x0800)            	/*!<SD I/O enable functions */

	/******************  Bit definition for SDIO_DCOUNT register  *****************/
	#define  SDIO_DCOUNT_DATACOUNT               (0x01FFFFFF)        	/*!<Data count value */

	/******************  Bit definition for SDIO_STA register  ********************/
	#define  SDIO_STA_CCRCFAIL                   (0x00000001)        	/*!<Command response received (CRC check failed) */
	#define  SDIO_STA_DCRCFAIL                   (0x00000002)        	/*!<Data block sent/received (CRC check failed) */
	#define  SDIO_STA_CTIMEOUT                   (0x00000004)        	/*!<Command response timeout */
	#define  SDIO_STA_DTIMEOUT                   (0x00000008)        	/*!<Data timeout */
	#define  SDIO_STA_TXUNDERR                   (0x00000010)        	/*!<Transmit FIFO underrun error */
	#define  SDIO_STA_RXOVERR                    (0x00000020)        	/*!<Received FIFO overrun error */
	#define  SDIO_STA_CMDREND                    (0x00000040)        	/*!<Command response received (CRC check passed) */
	#define  SDIO_STA_CMDSENT                    (0x00000080)        	/*!<Command sent (no response required) */
	#define  SDIO_STA_DATAEND                    (0x00000100)        	/*!<Data end (data counter, SDIDCOUNT, is zero) */
	#define  SDIO_STA_STBITERR                   (0x00000200)        	/*!<Start bit not detected on all data signals in wide bus mode */
	#define  SDIO_STA_DBCKEND                    (0x00000400)        	/*!<Data block sent/received (CRC check passed) */
	#define  SDIO_STA_CMDACT                     (0x00000800)        	/*!<Command transfer in progress */
	#define  SDIO_STA_TXACT                      (0x00001000)        	/*!<Data transmit in progress */
	#define  SDIO_STA_RXACT                      (0x00002000)        	/*!<Data receive in progress */
	#define  SDIO_STA_TXFIFOHE                   (0x00004000)        	/*!<Transmit FIFO Half Empty: at least 8 words can be written into the FIFO */
	#define  SDIO_STA_RXFIFOHF                   (0x00008000)        	/*!<Receive FIFO Half Full: there are at least 8 words in the FIFO */
	#define  SDIO_STA_TXFIFOF                    (0x00010000)        	/*!<Transmit FIFO full */
	#define  SDIO_STA_RXFIFOF                    (0x00020000)        	/*!<Receive FIFO full */
	#define  SDIO_STA_TXFIFOE                    (0x00040000)        	/*!<Transmit FIFO empty */
	#define  SDIO_STA_RXFIFOE                    (0x00080000)        	/*!<Receive FIFO empty */
	#define  SDIO_STA_TXDAVL                     (0x00100000)        	/*!<Data available in transmit FIFO */
	#define  SDIO_STA_RXDAVL                     (0x00200000)        	/*!<Data available in receive FIFO */
	#define  SDIO_STA_SDIOIT                     (0x00400000)        	/*!<SDIO interrupt received */
	#define  SDIO_STA_CEATAEND                   (0x00800000)        	/*!<CE-ATA command completion signal received for CMD61 */

	/*******************  Bit definition for SDIO_ICR register  *******************/
	#define  SDIO_ICR_CCRCFAILC                  (0x00000001)        	/*!<CCRCFAIL flag clear bit */
	#define  SDIO_ICR_DCRCFAILC                  (0x00000002)        	/*!<DCRCFAIL flag clear bit */
	#define  SDIO_ICR_CTIMEOUTC                  (0x00000004)        	/*!<CTIMEOUT flag clear bit */
	#define  SDIO_ICR_DTIMEOUTC                  (0x00000008)        	/*!<DTIMEOUT flag clear bit */
	#define  SDIO_ICR_TXUNDERRC                  (0x00000010)        	/*!<TXUNDERR flag clear bit */
	#define  SDIO_ICR_RXOVERRC                   (0x00000020)        	/*!<RXOVERR flag clear bit */
	#define  SDIO_ICR_CMDRENDC                   (0x00000040)        	/*!<CMDREND flag clear bit */
	#define  SDIO_ICR_CMDSENTC                   (0x00000080)        	/*!<CMDSENT flag clear bit */
	#define  SDIO_ICR_DATAENDC                   (0x00000100)        	/*!<DATAEND flag clear bit */
	#define  SDIO_ICR_STBITERRC                  (0x00000200)        	/*!<STBITERR flag clear bit */
	#define  SDIO_ICR_DBCKENDC                   (0x00000400)        	/*!<DBCKEND flag clear bit */
	#define  SDIO_ICR_SDIOITC                    (0x00400000)        	/*!<SDIOIT flag clear bit */
	#define  SDIO_ICR_CEATAENDC                  (0x00800000)        	/*!<CEATAEND flag clear bit */

	/******************  Bit definition for SDIO_MASK register  *******************/
	#define  SDIO_MASK_CCRCFAILIE                (0x00000001)        	/*!<Command CRC Fail Interrupt Enable */
	#define  SDIO_MASK_DCRCFAILIE                (0x00000002)        	/*!<Data CRC Fail Interrupt Enable */
	#define  SDIO_MASK_CTIMEOUTIE                (0x00000004)        	/*!<Command TimeOut Interrupt Enable */
	#define  SDIO_MASK_DTIMEOUTIE                (0x00000008)        	/*!<Data TimeOut Interrupt Enable */
	#define  SDIO_MASK_TXUNDERRIE                (0x00000010)        	/*!<Tx FIFO UnderRun Error Interrupt Enable */
	#define  SDIO_MASK_RXOVERRIE                 (0x00000020)        	/*!<Rx FIFO OverRun Error Interrupt Enable */
	#define  SDIO_MASK_CMDRENDIE                 (0x00000040)        	/*!<Command Response Received Interrupt Enable */
	#define  SDIO_MASK_CMDSENTIE                 (0x00000080)        	/*!<Command Sent Interrupt Enable */
	#define  SDIO_MASK_DATAENDIE                 (0x00000100)        	/*!<Data End Interrupt Enable */
	#define  SDIO_MASK_STBITERRIE                (0x00000200)        	/*!<Start Bit Error Interrupt Enable */
	#define  SDIO_MASK_DBCKENDIE                 (0x00000400)        	/*!<Data Block End Interrupt Enable */
	#define  SDIO_MASK_CMDACTIE                  (0x00000800)        	/*!<CCommand Acting Interrupt Enable */
	#define  SDIO_MASK_TXACTIE                   (0x00001000)        	/*!<Data Transmit Acting Interrupt Enable */
	#define  SDIO_MASK_RXACTIE                   (0x00002000)        	/*!<Data receive acting interrupt enabled */
	#define  SDIO_MASK_TXFIFOHEIE                (0x00004000)        	/*!<Tx FIFO Half Empty interrupt Enable */
	#define  SDIO_MASK_RXFIFOHFIE                (0x00008000)        	/*!<Rx FIFO Half Full interrupt Enable */
	#define  SDIO_MASK_TXFIFOFIE                 (0x00010000)        	/*!<Tx FIFO Full interrupt Enable */
	#define  SDIO_MASK_RXFIFOFIE                 (0x00020000)        	/*!<Rx FIFO Full interrupt Enable */
	#define  SDIO_MASK_TXFIFOEIE                 (0x00040000)        	/*!<Tx FIFO Empty interrupt Enable */
	#define  SDIO_MASK_RXFIFOEIE                 (0x00080000)        	/*!<Rx FIFO Empty interrupt Enable */
	#define  SDIO_MASK_TXDAVLIE                  (0x00100000)        	/*!<Data available in Tx FIFO interrupt Enable */
	#define  SDIO_MASK_RXDAVLIE                  (0x00200000)        	/*!<Data available in Rx FIFO interrupt Enable */
	#define  SDIO_MASK_SDIOITIE                  (0x00400000)        	/*!<SDIO Mode Interrupt Received interrupt Enable */
	#define  SDIO_MASK_CEATAENDIE                (0x00800000)        	/*!<CE-ATA command completion signal received Interrupt Enable */

	/*****************  Bit definition for SDIO_FIFOCNT register  *****************/
	#define  SDIO_FIFOCNT_FIFOCOUNT              (0x00FFFFFF)        	/*!<Remaining number of words to be written to or read from the FIFO */

	/******************  Bit definition for SDIO_FIFO register  *******************/
	#define  SDIO_FIFO_FIFODATA                  (0xFFFFFFFF)        	/*!<Receive and transmit FIFO data */

	/******************************************************************************/
	/*                                                                            */
	/*                                   USB                                      */
	/*                                                                            */
	/******************************************************************************/

	/*!<Endpoint-specific registers */
	/*******************  Bit definition for USB_EP0R register  *******************/
	#define  USB_EP0R_EA                         ((uint16_t)0x000F)            	/*!<Endpoint Address */

	#define  USB_EP0R_STAT_TX                    ((uint16_t)0x0030)            	/*!<STAT_TX[1:0] bits (Status bits, for transmission transfers) */
	#define  USB_EP0R_STAT_TX_0                  ((uint16_t)0x0010)            	/*!<Bit 0 */
	#define  USB_EP0R_STAT_TX_1                  ((uint16_t)0x0020)            	/*!<Bit 1 */

	#define  USB_EP0R_DTOG_TX                    ((uint16_t)0x0040)            	/*!<Data Toggle, for transmission transfers */
	#define  USB_EP0R_CTR_TX                     ((uint16_t)0x0080)            	/*!<Correct Transfer for transmission */
	#define  USB_EP0R_EP_KIND                    ((uint16_t)0x0100)            	/*!<Endpoint Kind */

	#define  USB_EP0R_EP_TYPE                    ((uint16_t)0x0600)            	/*!<EP_TYPE[1:0] bits (Endpoint type) */
	#define  USB_EP0R_EP_TYPE_0                  ((uint16_t)0x0200)            	/*!<Bit 0 */
	#define  USB_EP0R_EP_TYPE_1                  ((uint16_t)0x0400)            	/*!<Bit 1 */

	#define  USB_EP0R_SETUP                      ((uint16_t)0x0800)            	/*!<Setup transaction completed */

	#define  USB_EP0R_STAT_RX                    ((uint16_t)0x3000)            	/*!<STAT_RX[1:0] bits (Status bits, for reception transfers) */
	#define  USB_EP0R_STAT_RX_0                  ((uint16_t)0x1000)            	/*!<Bit 0 */
	#define  USB_EP0R_STAT_RX_1                  ((uint16_t)0x2000)            	/*!<Bit 1 */

	#define  USB_EP0R_DTOG_RX                    ((uint16_t)0x4000)            	/*!<Data Toggle, for reception transfers */
	#define  USB_EP0R_CTR_RX                     ((uint16_t)0x8000)            	/*!<Correct Transfer for reception */

	/*******************  Bit definition for USB_EP1R register  *******************/
	#define  USB_EP1R_EA                         ((uint16_t)0x000F)            	/*!<Endpoint Address */

	#define  USB_EP1R_STAT_TX                    ((uint16_t)0x0030)            	/*!<STAT_TX[1:0] bits (Status bits, for transmission transfers) */
	#define  USB_EP1R_STAT_TX_0                  ((uint16_t)0x0010)            	/*!<Bit 0 */
	#define  USB_EP1R_STAT_TX_1                  ((uint16_t)0x0020)            	/*!<Bit 1 */

	#define  USB_EP1R_DTOG_TX                    ((uint16_t)0x0040)            	/*!<Data Toggle, for transmission transfers */
	#define  USB_EP1R_CTR_TX                     ((uint16_t)0x0080)            	/*!<Correct Transfer for transmission */
	#define  USB_EP1R_EP_KIND                    ((uint16_t)0x0100)            	/*!<Endpoint Kind */

	#define  USB_EP1R_EP_TYPE                    ((uint16_t)0x0600)            	/*!<EP_TYPE[1:0] bits (Endpoint type) */
	#define  USB_EP1R_EP_TYPE_0                  ((uint16_t)0x0200)            	/*!<Bit 0 */
	#define  USB_EP1R_EP_TYPE_1                  ((uint16_t)0x0400)            	/*!<Bit 1 */

	#define  USB_EP1R_SETUP                      ((uint16_t)0x0800)            	/*!<Setup transaction completed */

	#define  USB_EP1R_STAT_RX                    ((uint16_t)0x3000)            	/*!<STAT_RX[1:0] bits (Status bits, for reception transfers) */
	#define  USB_EP1R_STAT_RX_0                  ((uint16_t)0x1000)            	/*!<Bit 0 */
	#define  USB_EP1R_STAT_RX_1                  ((uint16_t)0x2000)            	/*!<Bit 1 */

	#define  USB_EP1R_DTOG_RX                    ((uint16_t)0x4000)            	/*!<Data Toggle, for reception transfers */
	#define  USB_EP1R_CTR_RX                     ((uint16_t)0x8000)            	/*!<Correct Transfer for reception */

	/*******************  Bit definition for USB_EP2R register  *******************/
	#define  USB_EP2R_EA                         ((uint16_t)0x000F)            	/*!<Endpoint Address */

	#define  USB_EP2R_STAT_TX                    ((uint16_t)0x0030)            	/*!<STAT_TX[1:0] bits (Status bits, for transmission transfers) */
	#define  USB_EP2R_STAT_TX_0                  ((uint16_t)0x0010)            	/*!<Bit 0 */
	#define  USB_EP2R_STAT_TX_1                  ((uint16_t)0x0020)            	/*!<Bit 1 */

	#define  USB_EP2R_DTOG_TX                    ((uint16_t)0x0040)            	/*!<Data Toggle, for transmission transfers */
	#define  USB_EP2R_CTR_TX                     ((uint16_t)0x0080)            	/*!<Correct Transfer for transmission */
	#define  USB_EP2R_EP_KIND                    ((uint16_t)0x0100)            	/*!<Endpoint Kind */

	#define  USB_EP2R_EP_TYPE                    ((uint16_t)0x0600)            	/*!<EP_TYPE[1:0] bits (Endpoint type) */
	#define  USB_EP2R_EP_TYPE_0                  ((uint16_t)0x0200)            	/*!<Bit 0 */
	#define  USB_EP2R_EP_TYPE_1                  ((uint16_t)0x0400)            	/*!<Bit 1 */

	#define  USB_EP2R_SETUP                      ((uint16_t)0x0800)            	/*!<Setup transaction completed */

	#define  USB_EP2R_STAT_RX                    ((uint16_t)0x3000)            	/*!<STAT_RX[1:0] bits (Status bits, for reception transfers) */
	#define  USB_EP2R_STAT_RX_0                  ((uint16_t)0x1000)            	/*!<Bit 0 */
	#define  USB_EP2R_STAT_RX_1                  ((uint16_t)0x2000)            	/*!<Bit 1 */

	#define  USB_EP2R_DTOG_RX                    ((uint16_t)0x4000)            	/*!<Data Toggle, for reception transfers */
	#define  USB_EP2R_CTR_RX                     ((uint16_t)0x8000)            	/*!<Correct Transfer for reception */

	/*******************  Bit definition for USB_EP3R register  *******************/
	#define  USB_EP3R_EA                         ((uint16_t)0x000F)            	/*!<Endpoint Address */

	#define  USB_EP3R_STAT_TX                    ((uint16_t)0x0030)            	/*!<STAT_TX[1:0] bits (Status bits, for transmission transfers) */
	#define  USB_EP3R_STAT_TX_0                  ((uint16_t)0x0010)            	/*!<Bit 0 */
	#define  USB_EP3R_STAT_TX_1                  ((uint16_t)0x0020)            	/*!<Bit 1 */

	#define  USB_EP3R_DTOG_TX                    ((uint16_t)0x0040)            	/*!<Data Toggle, for transmission transfers */
	#define  USB_EP3R_CTR_TX                     ((uint16_t)0x0080)            	/*!<Correct Transfer for transmission */
	#define  USB_EP3R_EP_KIND                    ((uint16_t)0x0100)            	/*!<Endpoint Kind */

	#define  USB_EP3R_EP_TYPE                    ((uint16_t)0x0600)            	/*!<EP_TYPE[1:0] bits (Endpoint type) */
	#define  USB_EP3R_EP_TYPE_0                  ((uint16_t)0x0200)            	/*!<Bit 0 */
	#define  USB_EP3R_EP_TYPE_1                  ((uint16_t)0x0400)            	/*!<Bit 1 */

	#define  USB_EP3R_SETUP                      ((uint16_t)0x0800)            	/*!<Setup transaction completed */

	#define  USB_EP3R_STAT_RX                    ((uint16_t)0x3000)            	/*!<STAT_RX[1:0] bits (Status bits, for reception transfers) */
	#define  USB_EP3R_STAT_RX_0                  ((uint16_t)0x1000)            	/*!<Bit 0 */
	#define  USB_EP3R_STAT_RX_1                  ((uint16_t)0x2000)            	/*!<Bit 1 */

	#define  USB_EP3R_DTOG_RX                    ((uint16_t)0x4000)            	/*!<Data Toggle, for reception transfers */
	#define  USB_EP3R_CTR_RX                     ((uint16_t)0x8000)            	/*!<Correct Transfer for reception */

	/*******************  Bit definition for USB_EP4R register  *******************/
	#define  USB_EP4R_EA                         ((uint16_t)0x000F)            	/*!<Endpoint Address */

	#define  USB_EP4R_STAT_TX                    ((uint16_t)0x0030)            	/*!<STAT_TX[1:0] bits (Status bits, for transmission transfers) */
	#define  USB_EP4R_STAT_TX_0                  ((uint16_t)0x0010)            	/*!<Bit 0 */
	#define  USB_EP4R_STAT_TX_1                  ((uint16_t)0x0020)            	/*!<Bit 1 */

	#define  USB_EP4R_DTOG_TX                    ((uint16_t)0x0040)            	/*!<Data Toggle, for transmission transfers */
	#define  USB_EP4R_CTR_TX                     ((uint16_t)0x0080)            	/*!<Correct Transfer for transmission */
	#define  USB_EP4R_EP_KIND                    ((uint16_t)0x0100)            	/*!<Endpoint Kind */

	#define  USB_EP4R_EP_TYPE                    ((uint16_t)0x0600)            	/*!<EP_TYPE[1:0] bits (Endpoint type) */
	#define  USB_EP4R_EP_TYPE_0                  ((uint16_t)0x0200)            	/*!<Bit 0 */
	#define  USB_EP4R_EP_TYPE_1                  ((uint16_t)0x0400)            	/*!<Bit 1 */

	#define  USB_EP4R_SETUP                      ((uint16_t)0x0800)            	/*!<Setup transaction completed */

	#define  USB_EP4R_STAT_RX                    ((uint16_t)0x3000)            	/*!<STAT_RX[1:0] bits (Status bits, for reception transfers) */
	#define  USB_EP4R_STAT_RX_0                  ((uint16_t)0x1000)            	/*!<Bit 0 */
	#define  USB_EP4R_STAT_RX_1                  ((uint16_t)0x2000)            	/*!<Bit 1 */

	#define  USB_EP4R_DTOG_RX                    ((uint16_t)0x4000)            	/*!<Data Toggle, for reception transfers */
	#define  USB_EP4R_CTR_RX                     ((uint16_t)0x8000)            	/*!<Correct Transfer for reception */

	/*******************  Bit definition for USB_EP5R register  *******************/
	#define  USB_EP5R_EA                         ((uint16_t)0x000F)            	/*!<Endpoint Address */

	#define  USB_EP5R_STAT_TX                    ((uint16_t)0x0030)            	/*!<STAT_TX[1:0] bits (Status bits, for transmission transfers) */
	#define  USB_EP5R_STAT_TX_0                  ((uint16_t)0x0010)            	/*!<Bit 0 */
	#define  USB_EP5R_STAT_TX_1                  ((uint16_t)0x0020)            	/*!<Bit 1 */

	#define  USB_EP5R_DTOG_TX                    ((uint16_t)0x0040)            	/*!<Data Toggle, for transmission transfers */
	#define  USB_EP5R_CTR_TX                     ((uint16_t)0x0080)            	/*!<Correct Transfer for transmission */
	#define  USB_EP5R_EP_KIND                    ((uint16_t)0x0100)            	/*!<Endpoint Kind */

	#define  USB_EP5R_EP_TYPE                    ((uint16_t)0x0600)            	/*!<EP_TYPE[1:0] bits (Endpoint type) */
	#define  USB_EP5R_EP_TYPE_0                  ((uint16_t)0x0200)            	/*!<Bit 0 */
	#define  USB_EP5R_EP_TYPE_1                  ((uint16_t)0x0400)            	/*!<Bit 1 */

	#define  USB_EP5R_SETUP                      ((uint16_t)0x0800)            	/*!<Setup transaction completed */

	#define  USB_EP5R_STAT_RX                    ((uint16_t)0x3000)            	/*!<STAT_RX[1:0] bits (Status bits, for reception transfers) */
	#define  USB_EP5R_STAT_RX_0                  ((uint16_t)0x1000)            	/*!<Bit 0 */
	#define  USB_EP5R_STAT_RX_1                  ((uint16_t)0x2000)            	/*!<Bit 1 */

	#define  USB_EP5R_DTOG_RX                    ((uint16_t)0x4000)            	/*!<Data Toggle, for reception transfers */
	#define  USB_EP5R_CTR_RX                     ((uint16_t)0x8000)            	/*!<Correct Transfer for reception */

	/*******************  Bit definition for USB_EP6R register  *******************/
	#define  USB_EP6R_EA                         ((uint16_t)0x000F)            	/*!<Endpoint Address */

	#define  USB_EP6R_STAT_TX                    ((uint16_t)0x0030)            	/*!<STAT_TX[1:0] bits (Status bits, for transmission transfers) */
	#define  USB_EP6R_STAT_TX_0                  ((uint16_t)0x0010)            	/*!<Bit 0 */
	#define  USB_EP6R_STAT_TX_1                  ((uint16_t)0x0020)            	/*!<Bit 1 */

	#define  USB_EP6R_DTOG_TX                    ((uint16_t)0x0040)            	/*!<Data Toggle, for transmission transfers */
	#define  USB_EP6R_CTR_TX                     ((uint16_t)0x0080)            	/*!<Correct Transfer for transmission */
	#define  USB_EP6R_EP_KIND                    ((uint16_t)0x0100)            	/*!<Endpoint Kind */

	#define  USB_EP6R_EP_TYPE                    ((uint16_t)0x0600)            	/*!<EP_TYPE[1:0] bits (Endpoint type) */
	#define  USB_EP6R_EP_TYPE_0                  ((uint16_t)0x0200)            	/*!<Bit 0 */
	#define  USB_EP6R_EP_TYPE_1                  ((uint16_t)0x0400)            	/*!<Bit 1 */

	#define  USB_EP6R_SETUP                      ((uint16_t)0x0800)            	/*!<Setup transaction completed */

	#define  USB_EP6R_STAT_RX                    ((uint16_t)0x3000)            	/*!<STAT_RX[1:0] bits (Status bits, for reception transfers) */
	#define  USB_EP6R_STAT_RX_0                  ((uint16_t)0x1000)            	/*!<Bit 0 */
	#define  USB_EP6R_STAT_RX_1                  ((uint16_t)0x2000)            	/*!<Bit 1 */

	#define  USB_EP6R_DTOG_RX                    ((uint16_t)0x4000)            	/*!<Data Toggle, for reception transfers */
	#define  USB_EP6R_CTR_RX                     ((uint16_t)0x8000)            	/*!<Correct Transfer for reception */

	/*******************  Bit definition for USB_EP7R register  *******************/
	#define  USB_EP7R_EA                         ((uint16_t)0x000F)            	/*!<Endpoint Address */

	#define  USB_EP7R_STAT_TX                    ((uint16_t)0x0030)            	/*!<STAT_TX[1:0] bits (Status bits, for transmission transfers) */
	#define  USB_EP7R_STAT_TX_0                  ((uint16_t)0x0010)            	/*!<Bit 0 */
	#define  USB_EP7R_STAT_TX_1                  ((uint16_t)0x0020)            	/*!<Bit 1 */

	#define  USB_EP7R_DTOG_TX                    ((uint16_t)0x0040)            	/*!<Data Toggle, for transmission transfers */
	#define  USB_EP7R_CTR_TX                     ((uint16_t)0x0080)            	/*!<Correct Transfer for transmission */
	#define  USB_EP7R_EP_KIND                    ((uint16_t)0x0100)            	/*!<Endpoint Kind */

	#define  USB_EP7R_EP_TYPE                    ((uint16_t)0x0600)            	/*!<EP_TYPE[1:0] bits (Endpoint type) */
	#define  USB_EP7R_EP_TYPE_0                  ((uint16_t)0x0200)            	/*!<Bit 0 */
	#define  USB_EP7R_EP_TYPE_1                  ((uint16_t)0x0400)            	/*!<Bit 1 */

	#define  USB_EP7R_SETUP                      ((uint16_t)0x0800)            	/*!<Setup transaction completed */

	#define  USB_EP7R_STAT_RX                    ((uint16_t)0x3000)            	/*!<STAT_RX[1:0] bits (Status bits, for reception transfers) */
	#define  USB_EP7R_STAT_RX_0                  ((uint16_t)0x1000)            	/*!<Bit 0 */
	#define  USB_EP7R_STAT_RX_1                  ((uint16_t)0x2000)            	/*!<Bit 1 */

	#define  USB_EP7R_DTOG_RX                    ((uint16_t)0x4000)            	/*!<Data Toggle, for reception transfers */
	#define  USB_EP7R_CTR_RX                     ((uint16_t)0x8000)            	/*!<Correct Transfer for reception */

	/*!<Common registers */
	/*******************  Bit definition for USB_CNTR register  *******************/
	#define  USB_CNTR_FRES                       ((uint16_t)0x0001)            	/*!<Force USB Reset */
	#define  USB_CNTR_PDWN                       ((uint16_t)0x0002)            	/*!<Power down */
	#define  USB_CNTR_LP_MODE                    ((uint16_t)0x0004)            	/*!<Low-power mode */
	#define  USB_CNTR_FSUSP                      ((uint16_t)0x0008)            	/*!<Force suspend */
	#define  USB_CNTR_RESUME                     ((uint16_t)0x0010)            	/*!<Resume request */
	#define  USB_CNTR_ESOFM                      ((uint16_t)0x0100)            	/*!<Expected Start Of Frame Interrupt Mask */
	#define  USB_CNTR_SOFM                       ((uint16_t)0x0200)            	/*!<Start Of Frame Interrupt Mask */
	#define  USB_CNTR_RESETM                     ((uint16_t)0x0400)            	/*!<RESET Interrupt Mask */
	#define  USB_CNTR_SUSPM                      ((uint16_t)0x0800)            	/*!<Suspend mode Interrupt Mask */
	#define  USB_CNTR_WKUPM                      ((uint16_t)0x1000)            	/*!<Wakeup Interrupt Mask */
	#define  USB_CNTR_ERRM                       ((uint16_t)0x2000)            	/*!<Error Interrupt Mask */
	#define  USB_CNTR_PMAOVRM                    ((uint16_t)0x4000)            	/*!<Packet Memory Area Over / Underrun Interrupt Mask */
	#define  USB_CNTR_CTRM                       ((uint16_t)0x8000)            	/*!<Correct Transfer Interrupt Mask */

	/*******************  Bit definition for USB_ISTR register  *******************/
	#define  USB_ISTR_EP_ID                      ((uint16_t)0x000F)            	/*!<Endpoint Identifier */
	#define  USB_ISTR_DIR                        ((uint16_t)0x0010)            	/*!<Direction of transaction */
	#define  USB_ISTR_ESOF                       ((uint16_t)0x0100)            	/*!<Expected Start Of Frame */
	#define  USB_ISTR_SOF                        ((uint16_t)0x0200)            	/*!<Start Of Frame */
	#define  USB_ISTR_RESET                      ((uint16_t)0x0400)            	/*!<USB RESET request */
	#define  USB_ISTR_SUSP                       ((uint16_t)0x0800)            	/*!<Suspend mode request */
	#define  USB_ISTR_WKUP                       ((uint16_t)0x1000)            	/*!<Wake up */
	#define  USB_ISTR_ERR                        ((uint16_t)0x2000)            	/*!<Error */
	#define  USB_ISTR_PMAOVR                     ((uint16_t)0x4000)            	/*!<Packet Memory Area Over / Underrun */
	#define  USB_ISTR_CTR                        ((uint16_t)0x8000)            	/*!<Correct Transfer */

	/*******************  Bit definition for USB_FNR register  ********************/
	#define  USB_FNR_FN                          ((uint16_t)0x07FF)            	/*!<Frame Number */
	#define  USB_FNR_LSOF                        ((uint16_t)0x1800)            	/*!<Lost SOF */
	#define  USB_FNR_LCK                         ((uint16_t)0x2000)            	/*!<Locked */
	#define  USB_FNR_RXDM                        ((uint16_t)0x4000)            	/*!<Receive Data - Line Status */
	#define  USB_FNR_RXDP                        ((uint16_t)0x8000)            	/*!<Receive Data + Line Status */

	/******************  Bit definition for USB_DADDR register  *******************/
	#define  USB_DADDR_ADD                       ((uint8_t)0x7F)               	/*!<ADD[6:0] bits (Device Address) */
	#define  USB_DADDR_ADD0                      ((uint8_t)0x01)               	/*!<Bit 0 */
	#define  USB_DADDR_ADD1                      ((uint8_t)0x02)               	/*!<Bit 1 */
	#define  USB_DADDR_ADD2                      ((uint8_t)0x04)               	/*!<Bit 2 */
	#define  USB_DADDR_ADD3                      ((uint8_t)0x08)               	/*!<Bit 3 */
	#define  USB_DADDR_ADD4                      ((uint8_t)0x10)               	/*!<Bit 4 */
	#define  USB_DADDR_ADD5                      ((uint8_t)0x20)               	/*!<Bit 5 */
	#define  USB_DADDR_ADD6                      ((uint8_t)0x40)               	/*!<Bit 6 */

	#define  USB_DADDR_EF                        ((uint8_t)0x80)               	/*!<Enable Function */

	/******************  Bit definition for USB_BTABLE register  ******************/    
	#define  USB_BTABLE_BTABLE                   ((uint16_t)0xFFF8)            	/*!<Buffer Table */

	/*!<Buffer descriptor table */
	/*****************  Bit definition for USB_ADDR0_TX register  *****************/
	#define  USB_ADDR0_TX_ADDR0_TX               ((uint16_t)0xFFFE)            	/*!<Transmission Buffer Address 0 */

	/*****************  Bit definition for USB_ADDR1_TX register  *****************/
	#define  USB_ADDR1_TX_ADDR1_TX               ((uint16_t)0xFFFE)            	/*!<Transmission Buffer Address 1 */

	/*****************  Bit definition for USB_ADDR2_TX register  *****************/
	#define  USB_ADDR2_TX_ADDR2_TX               ((uint16_t)0xFFFE)            	/*!<Transmission Buffer Address 2 */

	/*****************  Bit definition for USB_ADDR3_TX register  *****************/
	#define  USB_ADDR3_TX_ADDR3_TX               ((uint16_t)0xFFFE)            	/*!<Transmission Buffer Address 3 */

	/*****************  Bit definition for USB_ADDR4_TX register  *****************/
	#define  USB_ADDR4_TX_ADDR4_TX               ((uint16_t)0xFFFE)            	/*!<Transmission Buffer Address 4 */

	/*****************  Bit definition for USB_ADDR5_TX register  *****************/
	#define  USB_ADDR5_TX_ADDR5_TX               ((uint16_t)0xFFFE)            	/*!<Transmission Buffer Address 5 */

	/*****************  Bit definition for USB_ADDR6_TX register  *****************/
	#define  USB_ADDR6_TX_ADDR6_TX               ((uint16_t)0xFFFE)            	/*!<Transmission Buffer Address 6 */

	/*****************  Bit definition for USB_ADDR7_TX register  *****************/
	#define  USB_ADDR7_TX_ADDR7_TX               ((uint16_t)0xFFFE)            	/*!<Transmission Buffer Address 7 */

	/*----------------------------------------------------------------------------*/

	/*****************  Bit definition for USB_COUNT0_TX register  ****************/
	#define  USB_COUNT0_TX_COUNT0_TX             ((uint16_t)0x03FF)            	/*!<Transmission Byte Count 0 */

	/*****************  Bit definition for USB_COUNT1_TX register  ****************/
	#define  USB_COUNT1_TX_COUNT1_TX             ((uint16_t)0x03FF)            	/*!<Transmission Byte Count 1 */

	/*****************  Bit definition for USB_COUNT2_TX register  ****************/
	#define  USB_COUNT2_TX_COUNT2_TX             ((uint16_t)0x03FF)            	/*!<Transmission Byte Count 2 */

	/*****************  Bit definition for USB_COUNT3_TX register  ****************/
	#define  USB_COUNT3_TX_COUNT3_TX             ((uint16_t)0x03FF)            	/*!<Transmission Byte Count 3 */

	/*****************  Bit definition for USB_COUNT4_TX register  ****************/
	#define  USB_COUNT4_TX_COUNT4_TX             ((uint16_t)0x03FF)            	/*!<Transmission Byte Count 4 */

	/*****************  Bit definition for USB_COUNT5_TX register  ****************/
	#define  USB_COUNT5_TX_COUNT5_TX             ((uint16_t)0x03FF)            	/*!<Transmission Byte Count 5 */

	/*****************  Bit definition for USB_COUNT6_TX register  ****************/
	#define  USB_COUNT6_TX_COUNT6_TX             ((uint16_t)0x03FF)            	/*!<Transmission Byte Count 6 */

	/*****************  Bit definition for USB_COUNT7_TX register  ****************/
	#define  USB_COUNT7_TX_COUNT7_TX             ((uint16_t)0x03FF)            	/*!<Transmission Byte Count 7 */

	/*----------------------------------------------------------------------------*/

	/****************  Bit definition for USB_COUNT0_TX_0 register  ***************/
	#define  USB_COUNT0_TX_0_COUNT0_TX_0         (0x000003FF)        	/*!<Transmission Byte Count 0 (low) */

	/****************  Bit definition for USB_COUNT0_TX_1 register  ***************/
	#define  USB_COUNT0_TX_1_COUNT0_TX_1         (0x03FF0000)        	/*!<Transmission Byte Count 0 (high) */

	/****************  Bit definition for USB_COUNT1_TX_0 register  ***************/
	#define  USB_COUNT1_TX_0_COUNT1_TX_0          (0x000003FF)        	/*!<Transmission Byte Count 1 (low) */

	/****************  Bit definition for USB_COUNT1_TX_1 register  ***************/
	#define  USB_COUNT1_TX_1_COUNT1_TX_1          (0x03FF0000)        	/*!<Transmission Byte Count 1 (high) */

	/****************  Bit definition for USB_COUNT2_TX_0 register  ***************/
	#define  USB_COUNT2_TX_0_COUNT2_TX_0         (0x000003FF)        	/*!<Transmission Byte Count 2 (low) */

	/****************  Bit definition for USB_COUNT2_TX_1 register  ***************/
	#define  USB_COUNT2_TX_1_COUNT2_TX_1         (0x03FF0000)        	/*!<Transmission Byte Count 2 (high) */

	/****************  Bit definition for USB_COUNT3_TX_0 register  ***************/
	#define  USB_COUNT3_TX_0_COUNT3_TX_0         ((uint16_t)0x000003FF)        	/*!<Transmission Byte Count 3 (low) */

	/****************  Bit definition for USB_COUNT3_TX_1 register  ***************/
	#define  USB_COUNT3_TX_1_COUNT3_TX_1         ((uint16_t)0x03FF0000)        	/*!<Transmission Byte Count 3 (high) */

	/****************  Bit definition for USB_COUNT4_TX_0 register  ***************/
	#define  USB_COUNT4_TX_0_COUNT4_TX_0         (0x000003FF)        	/*!<Transmission Byte Count 4 (low) */

	/****************  Bit definition for USB_COUNT4_TX_1 register  ***************/
	#define  USB_COUNT4_TX_1_COUNT4_TX_1         (0x03FF0000)        	/*!<Transmission Byte Count 4 (high) */

	/****************  Bit definition for USB_COUNT5_TX_0 register  ***************/
	#define  USB_COUNT5_TX_0_COUNT5_TX_0         (0x000003FF)        	/*!<Transmission Byte Count 5 (low) */

	/****************  Bit definition for USB_COUNT5_TX_1 register  ***************/
	#define  USB_COUNT5_TX_1_COUNT5_TX_1         (0x03FF0000)        	/*!<Transmission Byte Count 5 (high) */

	/****************  Bit definition for USB_COUNT6_TX_0 register  ***************/
	#define  USB_COUNT6_TX_0_COUNT6_TX_0         (0x000003FF)        	/*!<Transmission Byte Count 6 (low) */

	/****************  Bit definition for USB_COUNT6_TX_1 register  ***************/
	#define  USB_COUNT6_TX_1_COUNT6_TX_1         (0x03FF0000)        	/*!<Transmission Byte Count 6 (high) */

	/****************  Bit definition for USB_COUNT7_TX_0 register  ***************/
	#define  USB_COUNT7_TX_0_COUNT7_TX_0         (0x000003FF)        	/*!<Transmission Byte Count 7 (low) */

	/****************  Bit definition for USB_COUNT7_TX_1 register  ***************/
	#define  USB_COUNT7_TX_1_COUNT7_TX_1         (0x03FF0000)        	/*!<Transmission Byte Count 7 (high) */

	/*----------------------------------------------------------------------------*/

	/*****************  Bit definition for USB_ADDR0_RX register  *****************/
	#define  USB_ADDR0_RX_ADDR0_RX               ((uint16_t)0xFFFE)            	/*!<Reception Buffer Address 0 */

	/*****************  Bit definition for USB_ADDR1_RX register  *****************/
	#define  USB_ADDR1_RX_ADDR1_RX               ((uint16_t)0xFFFE)            	/*!<Reception Buffer Address 1 */

	/*****************  Bit definition for USB_ADDR2_RX register  *****************/
	#define  USB_ADDR2_RX_ADDR2_RX               ((uint16_t)0xFFFE)            	/*!<Reception Buffer Address 2 */

	/*****************  Bit definition for USB_ADDR3_RX register  *****************/
	#define  USB_ADDR3_RX_ADDR3_RX               ((uint16_t)0xFFFE)            	/*!<Reception Buffer Address 3 */

	/*****************  Bit definition for USB_ADDR4_RX register  *****************/
	#define  USB_ADDR4_RX_ADDR4_RX               ((uint16_t)0xFFFE)            	/*!<Reception Buffer Address 4 */

	/*****************  Bit definition for USB_ADDR5_RX register  *****************/
	#define  USB_ADDR5_RX_ADDR5_RX               ((uint16_t)0xFFFE)            	/*!<Reception Buffer Address 5 */

	/*****************  Bit definition for USB_ADDR6_RX register  *****************/
	#define  USB_ADDR6_RX_ADDR6_RX               ((uint16_t)0xFFFE)            	/*!<Reception Buffer Address 6 */

	/*****************  Bit definition for USB_ADDR7_RX register  *****************/
	#define  USB_ADDR7_RX_ADDR7_RX               ((uint16_t)0xFFFE)            	/*!<Reception Buffer Address 7 */

	/*----------------------------------------------------------------------------*/

	/*****************  Bit definition for USB_COUNT0_RX register  ****************/
	#define  USB_COUNT0_RX_COUNT0_RX             ((uint16_t)0x03FF)            	/*!<Reception Byte Count */

	#define  USB_COUNT0_RX_NUM_BLOCK             ((uint16_t)0x7C00)            	/*!<NUM_BLOCK[4:0] bits (Number of blocks) */
	#define  USB_COUNT0_RX_NUM_BLOCK_0           ((uint16_t)0x0400)            	/*!<Bit 0 */
	#define  USB_COUNT0_RX_NUM_BLOCK_1           ((uint16_t)0x0800)            	/*!<Bit 1 */
	#define  USB_COUNT0_RX_NUM_BLOCK_2           ((uint16_t)0x1000)            	/*!<Bit 2 */
	#define  USB_COUNT0_RX_NUM_BLOCK_3           ((uint16_t)0x2000)            	/*!<Bit 3 */
	#define  USB_COUNT0_RX_NUM_BLOCK_4           ((uint16_t)0x4000)            	/*!<Bit 4 */

	#define  USB_COUNT0_RX_BLSIZE                ((uint16_t)0x8000)            	/*!<BLock SIZE */

	/*****************  Bit definition for USB_COUNT1_RX register  ****************/
	#define  USB_COUNT1_RX_COUNT1_RX             ((uint16_t)0x03FF)            	/*!<Reception Byte Count */

	#define  USB_COUNT1_RX_NUM_BLOCK             ((uint16_t)0x7C00)            	/*!<NUM_BLOCK[4:0] bits (Number of blocks) */
	#define  USB_COUNT1_RX_NUM_BLOCK_0           ((uint16_t)0x0400)            	/*!<Bit 0 */
	#define  USB_COUNT1_RX_NUM_BLOCK_1           ((uint16_t)0x0800)            	/*!<Bit 1 */
	#define  USB_COUNT1_RX_NUM_BLOCK_2           ((uint16_t)0x1000)            	/*!<Bit 2 */
	#define  USB_COUNT1_RX_NUM_BLOCK_3           ((uint16_t)0x2000)            	/*!<Bit 3 */
	#define  USB_COUNT1_RX_NUM_BLOCK_4           ((uint16_t)0x4000)            	/*!<Bit 4 */

	#define  USB_COUNT1_RX_BLSIZE                ((uint16_t)0x8000)            	/*!<BLock SIZE */

	/*****************  Bit definition for USB_COUNT2_RX register  ****************/
	#define  USB_COUNT2_RX_COUNT2_RX             ((uint16_t)0x03FF)            	/*!<Reception Byte Count */

	#define  USB_COUNT2_RX_NUM_BLOCK             ((uint16_t)0x7C00)            	/*!<NUM_BLOCK[4:0] bits (Number of blocks) */
	#define  USB_COUNT2_RX_NUM_BLOCK_0           ((uint16_t)0x0400)            	/*!<Bit 0 */
	#define  USB_COUNT2_RX_NUM_BLOCK_1           ((uint16_t)0x0800)            	/*!<Bit 1 */
	#define  USB_COUNT2_RX_NUM_BLOCK_2           ((uint16_t)0x1000)            	/*!<Bit 2 */
	#define  USB_COUNT2_RX_NUM_BLOCK_3           ((uint16_t)0x2000)            	/*!<Bit 3 */
	#define  USB_COUNT2_RX_NUM_BLOCK_4           ((uint16_t)0x4000)            	/*!<Bit 4 */

	#define  USB_COUNT2_RX_BLSIZE                ((uint16_t)0x8000)            	/*!<BLock SIZE */

	/*****************  Bit definition for USB_COUNT3_RX register  ****************/
	#define  USB_COUNT3_RX_COUNT3_RX             ((uint16_t)0x03FF)            	/*!<Reception Byte Count */

	#define  USB_COUNT3_RX_NUM_BLOCK             ((uint16_t)0x7C00)            	/*!<NUM_BLOCK[4:0] bits (Number of blocks) */
	#define  USB_COUNT3_RX_NUM_BLOCK_0           ((uint16_t)0x0400)            	/*!<Bit 0 */
	#define  USB_COUNT3_RX_NUM_BLOCK_1           ((uint16_t)0x0800)            	/*!<Bit 1 */
	#define  USB_COUNT3_RX_NUM_BLOCK_2           ((uint16_t)0x1000)            	/*!<Bit 2 */
	#define  USB_COUNT3_RX_NUM_BLOCK_3           ((uint16_t)0x2000)            	/*!<Bit 3 */
	#define  USB_COUNT3_RX_NUM_BLOCK_4           ((uint16_t)0x4000)            	/*!<Bit 4 */

	#define  USB_COUNT3_RX_BLSIZE                ((uint16_t)0x8000)            	/*!<BLock SIZE */

	/*****************  Bit definition for USB_COUNT4_RX register  ****************/
	#define  USB_COUNT4_RX_COUNT4_RX             ((uint16_t)0x03FF)            	/*!<Reception Byte Count */

	#define  USB_COUNT4_RX_NUM_BLOCK             ((uint16_t)0x7C00)            	/*!<NUM_BLOCK[4:0] bits (Number of blocks) */
	#define  USB_COUNT4_RX_NUM_BLOCK_0           ((uint16_t)0x0400)            	/*!<Bit 0 */
	#define  USB_COUNT4_RX_NUM_BLOCK_1           ((uint16_t)0x0800)            	/*!<Bit 1 */
	#define  USB_COUNT4_RX_NUM_BLOCK_2           ((uint16_t)0x1000)            	/*!<Bit 2 */
	#define  USB_COUNT4_RX_NUM_BLOCK_3           ((uint16_t)0x2000)            	/*!<Bit 3 */
	#define  USB_COUNT4_RX_NUM_BLOCK_4           ((uint16_t)0x4000)            	/*!<Bit 4 */

	#define  USB_COUNT4_RX_BLSIZE                ((uint16_t)0x8000)            	/*!<BLock SIZE */

	/*****************  Bit definition for USB_COUNT5_RX register  ****************/
	#define  USB_COUNT5_RX_COUNT5_RX             ((uint16_t)0x03FF)            	/*!<Reception Byte Count */

	#define  USB_COUNT5_RX_NUM_BLOCK             ((uint16_t)0x7C00)            	/*!<NUM_BLOCK[4:0] bits (Number of blocks) */
	#define  USB_COUNT5_RX_NUM_BLOCK_0           ((uint16_t)0x0400)            	/*!<Bit 0 */
	#define  USB_COUNT5_RX_NUM_BLOCK_1           ((uint16_t)0x0800)            	/*!<Bit 1 */
	#define  USB_COUNT5_RX_NUM_BLOCK_2           ((uint16_t)0x1000)            	/*!<Bit 2 */
	#define  USB_COUNT5_RX_NUM_BLOCK_3           ((uint16_t)0x2000)            	/*!<Bit 3 */
	#define  USB_COUNT5_RX_NUM_BLOCK_4           ((uint16_t)0x4000)            	/*!<Bit 4 */

	#define  USB_COUNT5_RX_BLSIZE                ((uint16_t)0x8000)            	/*!<BLock SIZE */

	/*****************  Bit definition for USB_COUNT6_RX register  ****************/
	#define  USB_COUNT6_RX_COUNT6_RX             ((uint16_t)0x03FF)            	/*!<Reception Byte Count */

	#define  USB_COUNT6_RX_NUM_BLOCK             ((uint16_t)0x7C00)            	/*!<NUM_BLOCK[4:0] bits (Number of blocks) */
	#define  USB_COUNT6_RX_NUM_BLOCK_0           ((uint16_t)0x0400)            	/*!<Bit 0 */
	#define  USB_COUNT6_RX_NUM_BLOCK_1           ((uint16_t)0x0800)            	/*!<Bit 1 */
	#define  USB_COUNT6_RX_NUM_BLOCK_2           ((uint16_t)0x1000)            	/*!<Bit 2 */
	#define  USB_COUNT6_RX_NUM_BLOCK_3           ((uint16_t)0x2000)            	/*!<Bit 3 */
	#define  USB_COUNT6_RX_NUM_BLOCK_4           ((uint16_t)0x4000)            	/*!<Bit 4 */

	#define  USB_COUNT6_RX_BLSIZE                ((uint16_t)0x8000)            	/*!<BLock SIZE */

	/*****************  Bit definition for USB_COUNT7_RX register  ****************/
	#define  USB_COUNT7_RX_COUNT7_RX             ((uint16_t)0x03FF)            	/*!<Reception Byte Count */

	#define  USB_COUNT7_RX_NUM_BLOCK             ((uint16_t)0x7C00)            	/*!<NUM_BLOCK[4:0] bits (Number of blocks) */
	#define  USB_COUNT7_RX_NUM_BLOCK_0           ((uint16_t)0x0400)            	/*!<Bit 0 */
	#define  USB_COUNT7_RX_NUM_BLOCK_1           ((uint16_t)0x0800)            	/*!<Bit 1 */
	#define  USB_COUNT7_RX_NUM_BLOCK_2           ((uint16_t)0x1000)            	/*!<Bit 2 */
	#define  USB_COUNT7_RX_NUM_BLOCK_3           ((uint16_t)0x2000)            	/*!<Bit 3 */
	#define  USB_COUNT7_RX_NUM_BLOCK_4           ((uint16_t)0x4000)            	/*!<Bit 4 */

	#define  USB_COUNT7_RX_BLSIZE                ((uint16_t)0x8000)            	/*!<BLock SIZE */

	/*----------------------------------------------------------------------------*/

	/****************  Bit definition for USB_COUNT0_RX_0 register  ***************/
	#define  USB_COUNT0_RX_0_COUNT0_RX_0         (0x000003FF)        	/*!<Reception Byte Count (low) */

	#define  USB_COUNT0_RX_0_NUM_BLOCK_0         (0x00007C00)        	/*!<NUM_BLOCK_0[4:0] bits (Number of blocks) (low) */
	#define  USB_COUNT0_RX_0_NUM_BLOCK_0_0       (0x00000400)        	/*!<Bit 0 */
	#define  USB_COUNT0_RX_0_NUM_BLOCK_0_1       (0x00000800)        	/*!<Bit 1 */
	#define  USB_COUNT0_RX_0_NUM_BLOCK_0_2       (0x00001000)        	/*!<Bit 2 */
	#define  USB_COUNT0_RX_0_NUM_BLOCK_0_3       (0x00002000)        	/*!<Bit 3 */
	#define  USB_COUNT0_RX_0_NUM_BLOCK_0_4       (0x00004000)        	/*!<Bit 4 */

	#define  USB_COUNT0_RX_0_BLSIZE_0            (0x00008000)        	/*!<BLock SIZE (low) */

	/****************  Bit definition for USB_COUNT0_RX_1 register  ***************/
	#define  USB_COUNT0_RX_1_COUNT0_RX_1         (0x03FF0000)        	/*!<Reception Byte Count (high) */

	#define  USB_COUNT0_RX_1_NUM_BLOCK_1         (0x7C000000)        	/*!<NUM_BLOCK_1[4:0] bits (Number of blocks) (high) */
	#define  USB_COUNT0_RX_1_NUM_BLOCK_1_0       (0x04000000)        	/*!<Bit 1 */
	#define  USB_COUNT0_RX_1_NUM_BLOCK_1_1       (0x08000000)        	/*!<Bit 1 */
	#define  USB_COUNT0_RX_1_NUM_BLOCK_1_2       (0x10000000)        	/*!<Bit 2 */
	#define  USB_COUNT0_RX_1_NUM_BLOCK_1_3       (0x20000000)        	/*!<Bit 3 */
	#define  USB_COUNT0_RX_1_NUM_BLOCK_1_4       (0x40000000)        	/*!<Bit 4 */

	#define  USB_COUNT0_RX_1_BLSIZE_1            (0x80000000)        	/*!<BLock SIZE (high) */

	/****************  Bit definition for USB_COUNT1_RX_0 register  ***************/
	#define  USB_COUNT1_RX_0_COUNT1_RX_0         (0x000003FF)        	/*!<Reception Byte Count (low) */

	#define  USB_COUNT1_RX_0_NUM_BLOCK_0         (0x00007C00)        	/*!<NUM_BLOCK_0[4:0] bits (Number of blocks) (low) */
	#define  USB_COUNT1_RX_0_NUM_BLOCK_0_0       (0x00000400)        	/*!<Bit 0 */
	#define  USB_COUNT1_RX_0_NUM_BLOCK_0_1       (0x00000800)        	/*!<Bit 1 */
	#define  USB_COUNT1_RX_0_NUM_BLOCK_0_2       (0x00001000)        	/*!<Bit 2 */
	#define  USB_COUNT1_RX_0_NUM_BLOCK_0_3       (0x00002000)        	/*!<Bit 3 */
	#define  USB_COUNT1_RX_0_NUM_BLOCK_0_4       (0x00004000)        	/*!<Bit 4 */

	#define  USB_COUNT1_RX_0_BLSIZE_0            (0x00008000)        	/*!<BLock SIZE (low) */

	/****************  Bit definition for USB_COUNT1_RX_1 register  ***************/
	#define  USB_COUNT1_RX_1_COUNT1_RX_1         (0x03FF0000)        	/*!<Reception Byte Count (high) */

	#define  USB_COUNT1_RX_1_NUM_BLOCK_1         (0x7C000000)        	/*!<NUM_BLOCK_1[4:0] bits (Number of blocks) (high) */
	#define  USB_COUNT1_RX_1_NUM_BLOCK_1_0       (0x04000000)        	/*!<Bit 0 */
	#define  USB_COUNT1_RX_1_NUM_BLOCK_1_1       (0x08000000)        	/*!<Bit 1 */
	#define  USB_COUNT1_RX_1_NUM_BLOCK_1_2       (0x10000000)        	/*!<Bit 2 */
	#define  USB_COUNT1_RX_1_NUM_BLOCK_1_3       (0x20000000)        	/*!<Bit 3 */
	#define  USB_COUNT1_RX_1_NUM_BLOCK_1_4       (0x40000000)        	/*!<Bit 4 */

	#define  USB_COUNT1_RX_1_BLSIZE_1            (0x80000000)        	/*!<BLock SIZE (high) */

	/****************  Bit definition for USB_COUNT2_RX_0 register  ***************/
	#define  USB_COUNT2_RX_0_COUNT2_RX_0         (0x000003FF)        	/*!<Reception Byte Count (low) */

	#define  USB_COUNT2_RX_0_NUM_BLOCK_0         (0x00007C00)        	/*!<NUM_BLOCK_0[4:0] bits (Number of blocks) (low) */
	#define  USB_COUNT2_RX_0_NUM_BLOCK_0_0       (0x00000400)        	/*!<Bit 0 */
	#define  USB_COUNT2_RX_0_NUM_BLOCK_0_1       (0x00000800)        	/*!<Bit 1 */
	#define  USB_COUNT2_RX_0_NUM_BLOCK_0_2       (0x00001000)        	/*!<Bit 2 */
	#define  USB_COUNT2_RX_0_NUM_BLOCK_0_3       (0x00002000)        	/*!<Bit 3 */
	#define  USB_COUNT2_RX_0_NUM_BLOCK_0_4       (0x00004000)        	/*!<Bit 4 */

	#define  USB_COUNT2_RX_0_BLSIZE_0            (0x00008000)        	/*!<BLock SIZE (low) */

	/****************  Bit definition for USB_COUNT2_RX_1 register  ***************/
	#define  USB_COUNT2_RX_1_COUNT2_RX_1         (0x03FF0000)        	/*!<Reception Byte Count (high) */

	#define  USB_COUNT2_RX_1_NUM_BLOCK_1         (0x7C000000)        	/*!<NUM_BLOCK_1[4:0] bits (Number of blocks) (high) */
	#define  USB_COUNT2_RX_1_NUM_BLOCK_1_0       (0x04000000)        	/*!<Bit 0 */
	#define  USB_COUNT2_RX_1_NUM_BLOCK_1_1       (0x08000000)        	/*!<Bit 1 */
	#define  USB_COUNT2_RX_1_NUM_BLOCK_1_2       (0x10000000)        	/*!<Bit 2 */
	#define  USB_COUNT2_RX_1_NUM_BLOCK_1_3       (0x20000000)        	/*!<Bit 3 */
	#define  USB_COUNT2_RX_1_NUM_BLOCK_1_4       (0x40000000)        	/*!<Bit 4 */

	#define  USB_COUNT2_RX_1_BLSIZE_1            (0x80000000)        	/*!<BLock SIZE (high) */

	/****************  Bit definition for USB_COUNT3_RX_0 register  ***************/
	#define  USB_COUNT3_RX_0_COUNT3_RX_0         (0x000003FF)        	/*!<Reception Byte Count (low) */

	#define  USB_COUNT3_RX_0_NUM_BLOCK_0         (0x00007C00)        	/*!<NUM_BLOCK_0[4:0] bits (Number of blocks) (low) */
	#define  USB_COUNT3_RX_0_NUM_BLOCK_0_0       (0x00000400)        	/*!<Bit 0 */
	#define  USB_COUNT3_RX_0_NUM_BLOCK_0_1       (0x00000800)        	/*!<Bit 1 */
	#define  USB_COUNT3_RX_0_NUM_BLOCK_0_2       (0x00001000)        	/*!<Bit 2 */
	#define  USB_COUNT3_RX_0_NUM_BLOCK_0_3       (0x00002000)        	/*!<Bit 3 */
	#define  USB_COUNT3_RX_0_NUM_BLOCK_0_4       (0x00004000)        	/*!<Bit 4 */

	#define  USB_COUNT3_RX_0_BLSIZE_0            (0x00008000)        	/*!<BLock SIZE (low) */

	/****************  Bit definition for USB_COUNT3_RX_1 register  ***************/
	#define  USB_COUNT3_RX_1_COUNT3_RX_1         (0x03FF0000)        	/*!<Reception Byte Count (high) */

	#define  USB_COUNT3_RX_1_NUM_BLOCK_1         (0x7C000000)        	/*!<NUM_BLOCK_1[4:0] bits (Number of blocks) (high) */
	#define  USB_COUNT3_RX_1_NUM_BLOCK_1_0       (0x04000000)        	/*!<Bit 0 */
	#define  USB_COUNT3_RX_1_NUM_BLOCK_1_1       (0x08000000)        	/*!<Bit 1 */
	#define  USB_COUNT3_RX_1_NUM_BLOCK_1_2       (0x10000000)        	/*!<Bit 2 */
	#define  USB_COUNT3_RX_1_NUM_BLOCK_1_3       (0x20000000)        	/*!<Bit 3 */
	#define  USB_COUNT3_RX_1_NUM_BLOCK_1_4       (0x40000000)        	/*!<Bit 4 */

	#define  USB_COUNT3_RX_1_BLSIZE_1            (0x80000000)        	/*!<BLock SIZE (high) */

	/****************  Bit definition for USB_COUNT4_RX_0 register  ***************/
	#define  USB_COUNT4_RX_0_COUNT4_RX_0         (0x000003FF)        	/*!<Reception Byte Count (low) */

	#define  USB_COUNT4_RX_0_NUM_BLOCK_0         (0x00007C00)        	/*!<NUM_BLOCK_0[4:0] bits (Number of blocks) (low) */
	#define  USB_COUNT4_RX_0_NUM_BLOCK_0_0      (0x00000400)        	/*!<Bit 0 */
	#define  USB_COUNT4_RX_0_NUM_BLOCK_0_1      (0x00000800)        	/*!<Bit 1 */
	#define  USB_COUNT4_RX_0_NUM_BLOCK_0_2      (0x00001000)        	/*!<Bit 2 */
	#define  USB_COUNT4_RX_0_NUM_BLOCK_0_3      (0x00002000)        	/*!<Bit 3 */
	#define  USB_COUNT4_RX_0_NUM_BLOCK_0_4      (0x00004000)        	/*!<Bit 4 */

	#define  USB_COUNT4_RX_0_BLSIZE_0            (0x00008000)        	/*!<BLock SIZE (low) */

	/****************  Bit definition for USB_COUNT4_RX_1 register  ***************/
	#define  USB_COUNT4_RX_1_COUNT4_RX_1         (0x03FF0000)        	/*!<Reception Byte Count (high) */

	#define  USB_COUNT4_RX_1_NUM_BLOCK_1         (0x7C000000)        	/*!<NUM_BLOCK_1[4:0] bits (Number of blocks) (high) */
	#define  USB_COUNT4_RX_1_NUM_BLOCK_1_0       (0x04000000)        	/*!<Bit 0 */
	#define  USB_COUNT4_RX_1_NUM_BLOCK_1_1       (0x08000000)        	/*!<Bit 1 */
	#define  USB_COUNT4_RX_1_NUM_BLOCK_1_2       (0x10000000)        	/*!<Bit 2 */
	#define  USB_COUNT4_RX_1_NUM_BLOCK_1_3       (0x20000000)        	/*!<Bit 3 */
	#define  USB_COUNT4_RX_1_NUM_BLOCK_1_4       (0x40000000)        	/*!<Bit 4 */

	#define  USB_COUNT4_RX_1_BLSIZE_1            (0x80000000)        	/*!<BLock SIZE (high) */

	/****************  Bit definition for USB_COUNT5_RX_0 register  ***************/
	#define  USB_COUNT5_RX_0_COUNT5_RX_0         (0x000003FF)        	/*!<Reception Byte Count (low) */

	#define  USB_COUNT5_RX_0_NUM_BLOCK_0         (0x00007C00)        	/*!<NUM_BLOCK_0[4:0] bits (Number of blocks) (low) */
	#define  USB_COUNT5_RX_0_NUM_BLOCK_0_0       (0x00000400)        	/*!<Bit 0 */
	#define  USB_COUNT5_RX_0_NUM_BLOCK_0_1       (0x00000800)        	/*!<Bit 1 */
	#define  USB_COUNT5_RX_0_NUM_BLOCK_0_2       (0x00001000)        	/*!<Bit 2 */
	#define  USB_COUNT5_RX_0_NUM_BLOCK_0_3       (0x00002000)        	/*!<Bit 3 */
	#define  USB_COUNT5_RX_0_NUM_BLOCK_0_4       (0x00004000)        	/*!<Bit 4 */

	#define  USB_COUNT5_RX_0_BLSIZE_0            (0x00008000)        	/*!<BLock SIZE (low) */

	/****************  Bit definition for USB_COUNT5_RX_1 register  ***************/
	#define  USB_COUNT5_RX_1_COUNT5_RX_1         (0x03FF0000)        	/*!<Reception Byte Count (high) */

	#define  USB_COUNT5_RX_1_NUM_BLOCK_1         (0x7C000000)        	/*!<NUM_BLOCK_1[4:0] bits (Number of blocks) (high) */
	#define  USB_COUNT5_RX_1_NUM_BLOCK_1_0       (0x04000000)        	/*!<Bit 0 */
	#define  USB_COUNT5_RX_1_NUM_BLOCK_1_1       (0x08000000)        	/*!<Bit 1 */
	#define  USB_COUNT5_RX_1_NUM_BLOCK_1_2       (0x10000000)        	/*!<Bit 2 */
	#define  USB_COUNT5_RX_1_NUM_BLOCK_1_3       (0x20000000)        	/*!<Bit 3 */
	#define  USB_COUNT5_RX_1_NUM_BLOCK_1_4       (0x40000000)        	/*!<Bit 4 */

	#define  USB_COUNT5_RX_1_BLSIZE_1            (0x80000000)        	/*!<BLock SIZE (high) */

	/***************  Bit definition for USB_COUNT6_RX_0  register  ***************/
	#define  USB_COUNT6_RX_0_COUNT6_RX_0         (0x000003FF)        	/*!<Reception Byte Count (low) */

	#define  USB_COUNT6_RX_0_NUM_BLOCK_0         (0x00007C00)        	/*!<NUM_BLOCK_0[4:0] bits (Number of blocks) (low) */
	#define  USB_COUNT6_RX_0_NUM_BLOCK_0_0       (0x00000400)        	/*!<Bit 0 */
	#define  USB_COUNT6_RX_0_NUM_BLOCK_0_1       (0x00000800)        	/*!<Bit 1 */
	#define  USB_COUNT6_RX_0_NUM_BLOCK_0_2       (0x00001000)        	/*!<Bit 2 */
	#define  USB_COUNT6_RX_0_NUM_BLOCK_0_3       (0x00002000)        	/*!<Bit 3 */
	#define  USB_COUNT6_RX_0_NUM_BLOCK_0_4       (0x00004000)        	/*!<Bit 4 */

	#define  USB_COUNT6_RX_0_BLSIZE_0            (0x00008000)        	/*!<BLock SIZE (low) */

	/****************  Bit definition for USB_COUNT6_RX_1 register  ***************/
	#define  USB_COUNT6_RX_1_COUNT6_RX_1         (0x03FF0000)        	/*!<Reception Byte Count (high) */

	#define  USB_COUNT6_RX_1_NUM_BLOCK_1         (0x7C000000)        	/*!<NUM_BLOCK_1[4:0] bits (Number of blocks) (high) */
	#define  USB_COUNT6_RX_1_NUM_BLOCK_1_0       (0x04000000)        	/*!<Bit 0 */
	#define  USB_COUNT6_RX_1_NUM_BLOCK_1_1       (0x08000000)        	/*!<Bit 1 */
	#define  USB_COUNT6_RX_1_NUM_BLOCK_1_2       (0x10000000)        	/*!<Bit 2 */
	#define  USB_COUNT6_RX_1_NUM_BLOCK_1_3       (0x20000000)        	/*!<Bit 3 */
	#define  USB_COUNT6_RX_1_NUM_BLOCK_1_4       (0x40000000)        	/*!<Bit 4 */

	#define  USB_COUNT6_RX_1_BLSIZE_1            (0x80000000)        	/*!<BLock SIZE (high) */

	/***************  Bit definition for USB_COUNT7_RX_0 register  ****************/
	#define  USB_COUNT7_RX_0_COUNT7_RX_0         (0x000003FF)        	/*!<Reception Byte Count (low) */

	#define  USB_COUNT7_RX_0_NUM_BLOCK_0         (0x00007C00)        	/*!<NUM_BLOCK_0[4:0] bits (Number of blocks) (low) */
	#define  USB_COUNT7_RX_0_NUM_BLOCK_0_0       (0x00000400)        	/*!<Bit 0 */
	#define  USB_COUNT7_RX_0_NUM_BLOCK_0_1       (0x00000800)        	/*!<Bit 1 */
	#define  USB_COUNT7_RX_0_NUM_BLOCK_0_2       (0x00001000)        	/*!<Bit 2 */
	#define  USB_COUNT7_RX_0_NUM_BLOCK_0_3       (0x00002000)        	/*!<Bit 3 */
	#define  USB_COUNT7_RX_0_NUM_BLOCK_0_4       (0x00004000)        	/*!<Bit 4 */

	#define  USB_COUNT7_RX_0_BLSIZE_0            (0x00008000)        	/*!<BLock SIZE (low) */

	/***************  Bit definition for USB_COUNT7_RX_1 register  ****************/
	#define  USB_COUNT7_RX_1_COUNT7_RX_1         (0x03FF0000)        	/*!<Reception Byte Count (high) */

	#define  USB_COUNT7_RX_1_NUM_BLOCK_1         (0x7C000000)        	/*!<NUM_BLOCK_1[4:0] bits (Number of blocks) (high) */
	#define  USB_COUNT7_RX_1_NUM_BLOCK_1_0       (0x04000000)        	/*!<Bit 0 */
	#define  USB_COUNT7_RX_1_NUM_BLOCK_1_1       (0x08000000)        	/*!<Bit 1 */
	#define  USB_COUNT7_RX_1_NUM_BLOCK_1_2       (0x10000000)        	/*!<Bit 2 */
	#define  USB_COUNT7_RX_1_NUM_BLOCK_1_3       (0x20000000)        	/*!<Bit 3 */
	#define  USB_COUNT7_RX_1_NUM_BLOCK_1_4       (0x40000000)        	/*!<Bit 4 */

	#define  USB_COUNT7_RX_1_BLSIZE_1            (0x80000000)        	/*!<BLock SIZE (high) */

	/******************************************************************************/
	/*                                                                            */
	/*                         Controller Area Network                            */
	/*                                                                            */
	/******************************************************************************/

	/*!<CAN control and status registers */
	/*******************  Bit definition for CAN_MCR register  ********************/
	#define  CAN_MCR_INRQ                        ((uint16_t)0x0001)            	/*!<Initialization Request */
	#define  CAN_MCR_SLEEP                       ((uint16_t)0x0002)            	/*!<Sleep Mode Request */
	#define  CAN_MCR_TXFP                        ((uint16_t)0x0004)            	/*!<Transmit FIFO Priority */
	#define  CAN_MCR_RFLM                        ((uint16_t)0x0008)            	/*!<Receive FIFO Locked Mode */
	#define  CAN_MCR_NART                        ((uint16_t)0x0010)            	/*!<No Automatic Retransmission */
	#define  CAN_MCR_AWUM                        ((uint16_t)0x0020)            	/*!<Automatic Wakeup Mode */
	#define  CAN_MCR_ABOM                        ((uint16_t)0x0040)            	/*!<Automatic Bus-Off Management */
	#define  CAN_MCR_TTCM                        ((uint16_t)0x0080)            	/*!<Time Triggered Communication Mode */
	#define  CAN_MCR_RESET                       ((uint16_t)0x8000)            	/*!<bxCAN software master reset */

	/*******************  Bit definition for CAN_MSR register  ********************/
	#define  CAN_MSR_INAK                        ((uint16_t)0x0001)            	/*!<Initialization Acknowledge */
	#define  CAN_MSR_SLAK                        ((uint16_t)0x0002)            	/*!<Sleep Acknowledge */
	#define  CAN_MSR_ERRI                        ((uint16_t)0x0004)            	/*!<Error Interrupt */
	#define  CAN_MSR_WKUI                        ((uint16_t)0x0008)            	/*!<Wakeup Interrupt */
	#define  CAN_MSR_SLAKI                       ((uint16_t)0x0010)            	/*!<Sleep Acknowledge Interrupt */
	#define  CAN_MSR_TXM                         ((uint16_t)0x0100)            	/*!<Transmit Mode */
	#define  CAN_MSR_RXM                         ((uint16_t)0x0200)            	/*!<Receive Mode */
	#define  CAN_MSR_SAMP                        ((uint16_t)0x0400)            	/*!<Last Sample Point */
	#define  CAN_MSR_RX                          ((uint16_t)0x0800)            	/*!<CAN Rx Signal */

	/*******************  Bit definition for CAN_TSR register  ********************/
	#define  CAN_TSR_RQCP0                       (0x00000001)        	/*!<Request Completed Mailbox0 */
	#define  CAN_TSR_TXOK0                       (0x00000002)        	/*!<Transmission OK of Mailbox0 */
	#define  CAN_TSR_ALST0                       (0x00000004)        	/*!<Arbitration Lost for Mailbox0 */
	#define  CAN_TSR_TERR0                       (0x00000008)        	/*!<Transmission Error of Mailbox0 */
	#define  CAN_TSR_ABRQ0                       (0x00000080)        	/*!<Abort Request for Mailbox0 */
	#define  CAN_TSR_RQCP1                       (0x00000100)        	/*!<Request Completed Mailbox1 */
	#define  CAN_TSR_TXOK1                       (0x00000200)        	/*!<Transmission OK of Mailbox1 */
	#define  CAN_TSR_ALST1                       (0x00000400)        	/*!<Arbitration Lost for Mailbox1 */
	#define  CAN_TSR_TERR1                       (0x00000800)        	/*!<Transmission Error of Mailbox1 */
	#define  CAN_TSR_ABRQ1                       (0x00008000)        	/*!<Abort Request for Mailbox 1 */
	#define  CAN_TSR_RQCP2                       (0x00010000)        	/*!<Request Completed Mailbox2 */
	#define  CAN_TSR_TXOK2                       (0x00020000)        	/*!<Transmission OK of Mailbox 2 */
	#define  CAN_TSR_ALST2                       (0x00040000)        	/*!<Arbitration Lost for mailbox 2 */
	#define  CAN_TSR_TERR2                       (0x00080000)        	/*!<Transmission Error of Mailbox 2 */
	#define  CAN_TSR_ABRQ2                       (0x00800000)        	/*!<Abort Request for Mailbox 2 */
	#define  CAN_TSR_CODE                        (0x03000000)        	/*!<Mailbox Code */

	#define  CAN_TSR_TME                         (0x1C000000)        	/*!<TME[2:0] bits */
	#define  CAN_TSR_TME0                        (0x04000000)        	/*!<Transmit Mailbox 0 Empty */
	#define  CAN_TSR_TME1                        (0x08000000)        	/*!<Transmit Mailbox 1 Empty */
	#define  CAN_TSR_TME2                        (0x10000000)        	/*!<Transmit Mailbox 2 Empty */

	#define  CAN_TSR_LOW                         (0xE0000000)        	/*!<LOW[2:0] bits */
	#define  CAN_TSR_LOW0                        (0x20000000)        	/*!<Lowest Priority Flag for Mailbox 0 */
	#define  CAN_TSR_LOW1                        (0x40000000)        	/*!<Lowest Priority Flag for Mailbox 1 */
	#define  CAN_TSR_LOW2                        (0x80000000)        	/*!<Lowest Priority Flag for Mailbox 2 */

	/*******************  Bit definition for CAN_RF0R register  *******************/
	#define  CAN_RF0R_FMP0                       ((uint8_t)0x03)               	/*!<FIFO 0 Message Pending */
	#define  CAN_RF0R_FULL0                      ((uint8_t)0x08)               	/*!<FIFO 0 Full */
	#define  CAN_RF0R_FOVR0                      ((uint8_t)0x10)               	/*!<FIFO 0 Overrun */
	#define  CAN_RF0R_RFOM0                      ((uint8_t)0x20)               	/*!<Release FIFO 0 Output Mailbox */

	/*******************  Bit definition for CAN_RF1R register  *******************/
	#define  CAN_RF1R_FMP1                       ((uint8_t)0x03)               	/*!<FIFO 1 Message Pending */
	#define  CAN_RF1R_FULL1                      ((uint8_t)0x08)               	/*!<FIFO 1 Full */
	#define  CAN_RF1R_FOVR1                      ((uint8_t)0x10)               	/*!<FIFO 1 Overrun */
	#define  CAN_RF1R_RFOM1                      ((uint8_t)0x20)               	/*!<Release FIFO 1 Output Mailbox */

	/********************  Bit definition for CAN_IER register  *******************/
	#define  CAN_IER_TMEIE                       (0x00000001)        	/*!<Transmit Mailbox Empty Interrupt Enable */
	#define  CAN_IER_FMPIE0                      (0x00000002)        	/*!<FIFO Message Pending Interrupt Enable */
	#define  CAN_IER_FFIE0                       (0x00000004)        	/*!<FIFO Full Interrupt Enable */
	#define  CAN_IER_FOVIE0                      (0x00000008)        	/*!<FIFO Overrun Interrupt Enable */
	#define  CAN_IER_FMPIE1                      (0x00000010)        	/*!<FIFO Message Pending Interrupt Enable */
	#define  CAN_IER_FFIE1                       (0x00000020)        	/*!<FIFO Full Interrupt Enable */
	#define  CAN_IER_FOVIE1                      (0x00000040)        	/*!<FIFO Overrun Interrupt Enable */
	#define  CAN_IER_EWGIE                       (0x00000100)        	/*!<Error Warning Interrupt Enable */
	#define  CAN_IER_EPVIE                       (0x00000200)        	/*!<Error Passive Interrupt Enable */
	#define  CAN_IER_BOFIE                       (0x00000400)        	/*!<Bus-Off Interrupt Enable */
	#define  CAN_IER_LECIE                       (0x00000800)        	/*!<Last Error Code Interrupt Enable */
	#define  CAN_IER_ERRIE                       (0x00008000)        	/*!<Error Interrupt Enable */
	#define  CAN_IER_WKUIE                       (0x00010000)        	/*!<Wakeup Interrupt Enable */
	#define  CAN_IER_SLKIE                       (0x00020000)        	/*!<Sleep Interrupt Enable */

	/********************  Bit definition for CAN_ESR register  *******************/
	#define  CAN_ESR_EWGF                        (0x00000001)        	/*!<Error Warning Flag */
	#define  CAN_ESR_EPVF                        (0x00000002)        	/*!<Error Passive Flag */
	#define  CAN_ESR_BOFF                        (0x00000004)        	/*!<Bus-Off Flag */

	#define  CAN_ESR_LEC                         (0x00000070)        	/*!<LEC[2:0] bits (Last Error Code) */
	#define  CAN_ESR_LEC_0                       (0x00000010)        	/*!<Bit 0 */
	#define  CAN_ESR_LEC_1                       (0x00000020)        	/*!<Bit 1 */
	#define  CAN_ESR_LEC_2                       (0x00000040)        	/*!<Bit 2 */

	#define  CAN_ESR_TEC                         (0x00FF0000)        	/*!<Least significant byte of the 9-bit Transmit Error Counter */
	#define  CAN_ESR_REC                         (0xFF000000)        	/*!<Receive Error Counter */

	/*******************  Bit definition for CAN_BTR register  ********************/
	#define  CAN_BTR_BRP                         (0x000003FF)        	/*!<Baud Rate Prescaler */
	#define  CAN_BTR_TS1                         (0x000F0000)        	/*!<Time Segment 1 */
	#define  CAN_BTR_TS2                         (0x00700000)        	/*!<Time Segment 2 */
	#define  CAN_BTR_SJW                         (0x03000000)        	/*!<Resynchronization Jump Width */
	#define  CAN_BTR_LBKM                        (0x40000000)        	/*!<Loop Back Mode (Debug) */
	#define  CAN_BTR_SILM                        (0x80000000)        	/*!<Silent Mode */

	/*!<Mailbox registers */
	/******************  Bit definition for CAN_TI0R register  ********************/
	#define  CAN_TI0R_TXRQ                       (0x00000001)        	/*!<Transmit Mailbox Request */
	#define  CAN_TI0R_RTR                        (0x00000002)        	/*!<Remote Transmission Request */
	#define  CAN_TI0R_IDE                        (0x00000004)        	/*!<Identifier Extension */
	#define  CAN_TI0R_EXID                       (0x001FFFF8)        	/*!<Extended Identifier */
	#define  CAN_TI0R_STID                       (0xFFE00000)        	/*!<Standard Identifier or Extended Identifier */

	/******************  Bit definition for CAN_TDT0R register  *******************/
	#define  CAN_TDT0R_DLC                       (0x0000000F)        	/*!<Data Length Code */
	#define  CAN_TDT0R_TGT                       (0x00000100)        	/*!<Transmit Global Time */
	#define  CAN_TDT0R_TIME                      (0xFFFF0000)        	/*!<Message Time Stamp */

	/******************  Bit definition for CAN_TDL0R register  *******************/
	#define  CAN_TDL0R_DATA0                     (0x000000FF)        	/*!<Data byte 0 */
	#define  CAN_TDL0R_DATA1                     (0x0000FF00)        	/*!<Data byte 1 */
	#define  CAN_TDL0R_DATA2                     (0x00FF0000)        	/*!<Data byte 2 */
	#define  CAN_TDL0R_DATA3                     (0xFF000000)        	/*!<Data byte 3 */

	/******************  Bit definition for CAN_TDH0R register  *******************/
	#define  CAN_TDH0R_DATA4                     (0x000000FF)        	/*!<Data byte 4 */
	#define  CAN_TDH0R_DATA5                     (0x0000FF00)        	/*!<Data byte 5 */
	#define  CAN_TDH0R_DATA6                     (0x00FF0000)        	/*!<Data byte 6 */
	#define  CAN_TDH0R_DATA7                     (0xFF000000)        	/*!<Data byte 7 */

	/*******************  Bit definition for CAN_TI1R register  *******************/
	#define  CAN_TI1R_TXRQ                       (0x00000001)        	/*!<Transmit Mailbox Request */
	#define  CAN_TI1R_RTR                        (0x00000002)        	/*!<Remote Transmission Request */
	#define  CAN_TI1R_IDE                        (0x00000004)        	/*!<Identifier Extension */
	#define  CAN_TI1R_EXID                       (0x001FFFF8)        	/*!<Extended Identifier */
	#define  CAN_TI1R_STID                       (0xFFE00000)        	/*!<Standard Identifier or Extended Identifier */

	/*******************  Bit definition for CAN_TDT1R register  ******************/
	#define  CAN_TDT1R_DLC                       (0x0000000F)        	/*!<Data Length Code */
	#define  CAN_TDT1R_TGT                       (0x00000100)        	/*!<Transmit Global Time */
	#define  CAN_TDT1R_TIME                      (0xFFFF0000)        	/*!<Message Time Stamp */

	/*******************  Bit definition for CAN_TDL1R register  ******************/
	#define  CAN_TDL1R_DATA0                     (0x000000FF)        	/*!<Data byte 0 */
	#define  CAN_TDL1R_DATA1                     (0x0000FF00)        	/*!<Data byte 1 */
	#define  CAN_TDL1R_DATA2                     (0x00FF0000)        	/*!<Data byte 2 */
	#define  CAN_TDL1R_DATA3                     (0xFF000000)        	/*!<Data byte 3 */

	/*******************  Bit definition for CAN_TDH1R register  ******************/
	#define  CAN_TDH1R_DATA4                     (0x000000FF)        	/*!<Data byte 4 */
	#define  CAN_TDH1R_DATA5                     (0x0000FF00)        	/*!<Data byte 5 */
	#define  CAN_TDH1R_DATA6                     (0x00FF0000)        	/*!<Data byte 6 */
	#define  CAN_TDH1R_DATA7                     (0xFF000000)        	/*!<Data byte 7 */

	/*******************  Bit definition for CAN_TI2R register  *******************/
	#define  CAN_TI2R_TXRQ                       (0x00000001)        	/*!<Transmit Mailbox Request */
	#define  CAN_TI2R_RTR                        (0x00000002)        	/*!<Remote Transmission Request */
	#define  CAN_TI2R_IDE                        (0x00000004)        	/*!<Identifier Extension */
	#define  CAN_TI2R_EXID                       (0x001FFFF8)        	/*!<Extended identifier */
	#define  CAN_TI2R_STID                       (0xFFE00000)        	/*!<Standard Identifier or Extended Identifier */

	/*******************  Bit definition for CAN_TDT2R register  ******************/  
	#define  CAN_TDT2R_DLC                       (0x0000000F)        	/*!<Data Length Code */
	#define  CAN_TDT2R_TGT                       (0x00000100)        	/*!<Transmit Global Time */
	#define  CAN_TDT2R_TIME                      (0xFFFF0000)        	/*!<Message Time Stamp */

	/*******************  Bit definition for CAN_TDL2R register  ******************/
	#define  CAN_TDL2R_DATA0                     (0x000000FF)        	/*!<Data byte 0 */
	#define  CAN_TDL2R_DATA1                     (0x0000FF00)        	/*!<Data byte 1 */
	#define  CAN_TDL2R_DATA2                     (0x00FF0000)        	/*!<Data byte 2 */
	#define  CAN_TDL2R_DATA3                     (0xFF000000)        	/*!<Data byte 3 */

	/*******************  Bit definition for CAN_TDH2R register  ******************/
	#define  CAN_TDH2R_DATA4                     (0x000000FF)        	/*!<Data byte 4 */
	#define  CAN_TDH2R_DATA5                     (0x0000FF00)        	/*!<Data byte 5 */
	#define  CAN_TDH2R_DATA6                     (0x00FF0000)        	/*!<Data byte 6 */
	#define  CAN_TDH2R_DATA7                     (0xFF000000)        	/*!<Data byte 7 */

	/*******************  Bit definition for CAN_RI0R register  *******************/
	#define  CAN_RI0R_RTR                        (0x00000002)        	/*!<Remote Transmission Request */
	#define  CAN_RI0R_IDE                        (0x00000004)        	/*!<Identifier Extension */
	#define  CAN_RI0R_EXID                       (0x001FFFF8)        	/*!<Extended Identifier */
	#define  CAN_RI0R_STID                       (0xFFE00000)        	/*!<Standard Identifier or Extended Identifier */

	/*******************  Bit definition for CAN_RDT0R register  ******************/
	#define  CAN_RDT0R_DLC                       (0x0000000F)        	/*!<Data Length Code */
	#define  CAN_RDT0R_FMI                       (0x0000FF00)        	/*!<Filter Match Index */
	#define  CAN_RDT0R_TIME                      (0xFFFF0000)        	/*!<Message Time Stamp */

	/*******************  Bit definition for CAN_RDL0R register  ******************/
	#define  CAN_RDL0R_DATA0                     (0x000000FF)        	/*!<Data byte 0 */
	#define  CAN_RDL0R_DATA1                     (0x0000FF00)        	/*!<Data byte 1 */
	#define  CAN_RDL0R_DATA2                     (0x00FF0000)        	/*!<Data byte 2 */
	#define  CAN_RDL0R_DATA3                     (0xFF000000)        	/*!<Data byte 3 */

	/*******************  Bit definition for CAN_RDH0R register  ******************/
	#define  CAN_RDH0R_DATA4                     (0x000000FF)        	/*!<Data byte 4 */
	#define  CAN_RDH0R_DATA5                     (0x0000FF00)        	/*!<Data byte 5 */
	#define  CAN_RDH0R_DATA6                     (0x00FF0000)        	/*!<Data byte 6 */
	#define  CAN_RDH0R_DATA7                     (0xFF000000)        	/*!<Data byte 7 */

	/*******************  Bit definition for CAN_RI1R register  *******************/
	#define  CAN_RI1R_RTR                        (0x00000002)        	/*!<Remote Transmission Request */
	#define  CAN_RI1R_IDE                        (0x00000004)        	/*!<Identifier Extension */
	#define  CAN_RI1R_EXID                       (0x001FFFF8)        	/*!<Extended identifier */
	#define  CAN_RI1R_STID                       (0xFFE00000)        	/*!<Standard Identifier or Extended Identifier */

	/*******************  Bit definition for CAN_RDT1R register  ******************/
	#define  CAN_RDT1R_DLC                       (0x0000000F)        	/*!<Data Length Code */
	#define  CAN_RDT1R_FMI                       (0x0000FF00)        	/*!<Filter Match Index */
	#define  CAN_RDT1R_TIME                      (0xFFFF0000)        	/*!<Message Time Stamp */

	/*******************  Bit definition for CAN_RDL1R register  ******************/
	#define  CAN_RDL1R_DATA0                     (0x000000FF)        	/*!<Data byte 0 */
	#define  CAN_RDL1R_DATA1                     (0x0000FF00)        	/*!<Data byte 1 */
	#define  CAN_RDL1R_DATA2                     (0x00FF0000)        	/*!<Data byte 2 */
	#define  CAN_RDL1R_DATA3                     (0xFF000000)        	/*!<Data byte 3 */

	/*******************  Bit definition for CAN_RDH1R register  ******************/
	#define  CAN_RDH1R_DATA4                     (0x000000FF)        	/*!<Data byte 4 */
	#define  CAN_RDH1R_DATA5                     (0x0000FF00)        	/*!<Data byte 5 */
	#define  CAN_RDH1R_DATA6                     (0x00FF0000)        	/*!<Data byte 6 */
	#define  CAN_RDH1R_DATA7                     (0xFF000000)        	/*!<Data byte 7 */

	/*!<CAN filter registers */
	/*******************  Bit definition for CAN_FMR register  ********************/
	#define  CAN_FMR_FINIT                       ((uint8_t)0x01)               	/*!<Filter Init Mode */

	/*******************  Bit definition for CAN_FM1R register  *******************/
	#define  CAN_FM1R_FBM                        ((uint16_t)0x3FFF)            	/*!<Filter Mode */
	#define  CAN_FM1R_FBM0                       ((uint16_t)0x0001)            	/*!<Filter Init Mode bit 0 */
	#define  CAN_FM1R_FBM1                       ((uint16_t)0x0002)            	/*!<Filter Init Mode bit 1 */
	#define  CAN_FM1R_FBM2                       ((uint16_t)0x0004)            	/*!<Filter Init Mode bit 2 */
	#define  CAN_FM1R_FBM3                       ((uint16_t)0x0008)            	/*!<Filter Init Mode bit 3 */
	#define  CAN_FM1R_FBM4                       ((uint16_t)0x0010)            	/*!<Filter Init Mode bit 4 */
	#define  CAN_FM1R_FBM5                       ((uint16_t)0x0020)            	/*!<Filter Init Mode bit 5 */
	#define  CAN_FM1R_FBM6                       ((uint16_t)0x0040)            	/*!<Filter Init Mode bit 6 */
	#define  CAN_FM1R_FBM7                       ((uint16_t)0x0080)            	/*!<Filter Init Mode bit 7 */
	#define  CAN_FM1R_FBM8                       ((uint16_t)0x0100)            	/*!<Filter Init Mode bit 8 */
	#define  CAN_FM1R_FBM9                       ((uint16_t)0x0200)            	/*!<Filter Init Mode bit 9 */
	#define  CAN_FM1R_FBM10                      ((uint16_t)0x0400)            	/*!<Filter Init Mode bit 10 */
	#define  CAN_FM1R_FBM11                      ((uint16_t)0x0800)            	/*!<Filter Init Mode bit 11 */
	#define  CAN_FM1R_FBM12                      ((uint16_t)0x1000)            	/*!<Filter Init Mode bit 12 */
	#define  CAN_FM1R_FBM13                      ((uint16_t)0x2000)            	/*!<Filter Init Mode bit 13 */

	/*******************  Bit definition for CAN_FS1R register  *******************/
	#define  CAN_FS1R_FSC                        ((uint16_t)0x3FFF)            	/*!<Filter Scale Configuration */
	#define  CAN_FS1R_FSC0                       ((uint16_t)0x0001)            	/*!<Filter Scale Configuration bit 0 */
	#define  CAN_FS1R_FSC1                       ((uint16_t)0x0002)            	/*!<Filter Scale Configuration bit 1 */
	#define  CAN_FS1R_FSC2                       ((uint16_t)0x0004)            	/*!<Filter Scale Configuration bit 2 */
	#define  CAN_FS1R_FSC3                       ((uint16_t)0x0008)            	/*!<Filter Scale Configuration bit 3 */
	#define  CAN_FS1R_FSC4                       ((uint16_t)0x0010)            	/*!<Filter Scale Configuration bit 4 */
	#define  CAN_FS1R_FSC5                       ((uint16_t)0x0020)            	/*!<Filter Scale Configuration bit 5 */
	#define  CAN_FS1R_FSC6                       ((uint16_t)0x0040)            	/*!<Filter Scale Configuration bit 6 */
	#define  CAN_FS1R_FSC7                       ((uint16_t)0x0080)            	/*!<Filter Scale Configuration bit 7 */
	#define  CAN_FS1R_FSC8                       ((uint16_t)0x0100)            	/*!<Filter Scale Configuration bit 8 */
	#define  CAN_FS1R_FSC9                       ((uint16_t)0x0200)            	/*!<Filter Scale Configuration bit 9 */
	#define  CAN_FS1R_FSC10                      ((uint16_t)0x0400)            	/*!<Filter Scale Configuration bit 10 */
	#define  CAN_FS1R_FSC11                      ((uint16_t)0x0800)            	/*!<Filter Scale Configuration bit 11 */
	#define  CAN_FS1R_FSC12                      ((uint16_t)0x1000)            	/*!<Filter Scale Configuration bit 12 */
	#define  CAN_FS1R_FSC13                      ((uint16_t)0x2000)            	/*!<Filter Scale Configuration bit 13 */

	/******************  Bit definition for CAN_FFA1R register  *******************/
	#define  CAN_FFA1R_FFA                       ((uint16_t)0x3FFF)            	/*!<Filter FIFO Assignment */
	#define  CAN_FFA1R_FFA0                      ((uint16_t)0x0001)            	/*!<Filter FIFO Assignment for Filter 0 */
	#define  CAN_FFA1R_FFA1                      ((uint16_t)0x0002)            	/*!<Filter FIFO Assignment for Filter 1 */
	#define  CAN_FFA1R_FFA2                      ((uint16_t)0x0004)            	/*!<Filter FIFO Assignment for Filter 2 */
	#define  CAN_FFA1R_FFA3                      ((uint16_t)0x0008)            	/*!<Filter FIFO Assignment for Filter 3 */
	#define  CAN_FFA1R_FFA4                      ((uint16_t)0x0010)            	/*!<Filter FIFO Assignment for Filter 4 */
	#define  CAN_FFA1R_FFA5                      ((uint16_t)0x0020)            	/*!<Filter FIFO Assignment for Filter 5 */
	#define  CAN_FFA1R_FFA6                      ((uint16_t)0x0040)            	/*!<Filter FIFO Assignment for Filter 6 */
	#define  CAN_FFA1R_FFA7                      ((uint16_t)0x0080)            	/*!<Filter FIFO Assignment for Filter 7 */
	#define  CAN_FFA1R_FFA8                      ((uint16_t)0x0100)            	/*!<Filter FIFO Assignment for Filter 8 */
	#define  CAN_FFA1R_FFA9                      ((uint16_t)0x0200)            	/*!<Filter FIFO Assignment for Filter 9 */
	#define  CAN_FFA1R_FFA10                     ((uint16_t)0x0400)            	/*!<Filter FIFO Assignment for Filter 10 */
	#define  CAN_FFA1R_FFA11                     ((uint16_t)0x0800)            	/*!<Filter FIFO Assignment for Filter 11 */
	#define  CAN_FFA1R_FFA12                     ((uint16_t)0x1000)            	/*!<Filter FIFO Assignment for Filter 12 */
	#define  CAN_FFA1R_FFA13                     ((uint16_t)0x2000)            	/*!<Filter FIFO Assignment for Filter 13 */

	/*******************  Bit definition for CAN_FA1R register  *******************/
	#define  CAN_FA1R_FACT                       ((uint16_t)0x3FFF)            	/*!<Filter Active */
	#define  CAN_FA1R_FACT0                      ((uint16_t)0x0001)            	/*!<Filter 0 Active */
	#define  CAN_FA1R_FACT1                      ((uint16_t)0x0002)            	/*!<Filter 1 Active */
	#define  CAN_FA1R_FACT2                      ((uint16_t)0x0004)            	/*!<Filter 2 Active */
	#define  CAN_FA1R_FACT3                      ((uint16_t)0x0008)            	/*!<Filter 3 Active */
	#define  CAN_FA1R_FACT4                      ((uint16_t)0x0010)            	/*!<Filter 4 Active */
	#define  CAN_FA1R_FACT5                      ((uint16_t)0x0020)            	/*!<Filter 5 Active */
	#define  CAN_FA1R_FACT6                      ((uint16_t)0x0040)            	/*!<Filter 6 Active */
	#define  CAN_FA1R_FACT7                      ((uint16_t)0x0080)            	/*!<Filter 7 Active */
	#define  CAN_FA1R_FACT8                      ((uint16_t)0x0100)            	/*!<Filter 8 Active */
	#define  CAN_FA1R_FACT9                      ((uint16_t)0x0200)            	/*!<Filter 9 Active */
	#define  CAN_FA1R_FACT10                     ((uint16_t)0x0400)            	/*!<Filter 10 Active */
	#define  CAN_FA1R_FACT11                     ((uint16_t)0x0800)            	/*!<Filter 11 Active */
	#define  CAN_FA1R_FACT12                     ((uint16_t)0x1000)            	/*!<Filter 12 Active */
	#define  CAN_FA1R_FACT13                     ((uint16_t)0x2000)            	/*!<Filter 13 Active */

	/*******************  Bit definition for CAN_F0R1 register  *******************/
	#define  CAN_F0R1_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F0R1_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F0R1_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F0R1_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F0R1_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F0R1_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F0R1_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F0R1_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F0R1_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F0R1_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F0R1_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F0R1_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F0R1_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F0R1_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F0R1_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F0R1_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F0R1_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F0R1_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F0R1_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F0R1_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F0R1_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F0R1_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F0R1_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F0R1_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F0R1_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F0R1_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F0R1_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F0R1_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F0R1_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F0R1_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F0R1_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F0R1_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F1R1 register  *******************/
	#define  CAN_F1R1_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F1R1_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F1R1_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F1R1_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F1R1_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F1R1_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F1R1_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F1R1_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F1R1_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F1R1_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F1R1_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F1R1_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F1R1_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F1R1_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F1R1_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F1R1_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F1R1_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F1R1_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F1R1_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F1R1_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F1R1_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F1R1_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F1R1_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F1R1_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F1R1_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F1R1_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F1R1_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F1R1_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F1R1_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F1R1_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F1R1_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F1R1_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F2R1 register  *******************/
	#define  CAN_F2R1_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F2R1_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F2R1_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F2R1_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F2R1_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F2R1_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F2R1_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F2R1_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F2R1_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F2R1_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F2R1_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F2R1_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F2R1_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F2R1_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F2R1_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F2R1_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F2R1_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F2R1_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F2R1_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F2R1_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F2R1_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F2R1_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F2R1_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F2R1_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F2R1_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F2R1_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F2R1_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F2R1_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F2R1_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F2R1_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F2R1_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F2R1_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F3R1 register  *******************/
	#define  CAN_F3R1_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F3R1_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F3R1_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F3R1_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F3R1_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F3R1_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F3R1_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F3R1_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F3R1_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F3R1_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F3R1_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F3R1_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F3R1_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F3R1_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F3R1_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F3R1_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F3R1_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F3R1_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F3R1_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F3R1_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F3R1_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F3R1_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F3R1_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F3R1_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F3R1_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F3R1_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F3R1_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F3R1_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F3R1_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F3R1_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F3R1_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F3R1_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F4R1 register  *******************/
	#define  CAN_F4R1_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F4R1_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F4R1_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F4R1_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F4R1_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F4R1_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F4R1_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F4R1_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F4R1_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F4R1_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F4R1_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F4R1_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F4R1_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F4R1_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F4R1_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F4R1_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F4R1_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F4R1_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F4R1_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F4R1_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F4R1_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F4R1_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F4R1_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F4R1_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F4R1_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F4R1_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F4R1_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F4R1_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F4R1_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F4R1_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F4R1_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F4R1_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F5R1 register  *******************/
	#define  CAN_F5R1_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F5R1_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F5R1_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F5R1_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F5R1_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F5R1_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F5R1_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F5R1_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F5R1_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F5R1_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F5R1_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F5R1_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F5R1_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F5R1_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F5R1_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F5R1_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F5R1_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F5R1_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F5R1_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F5R1_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F5R1_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F5R1_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F5R1_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F5R1_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F5R1_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F5R1_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F5R1_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F5R1_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F5R1_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F5R1_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F5R1_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F5R1_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F6R1 register  *******************/
	#define  CAN_F6R1_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F6R1_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F6R1_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F6R1_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F6R1_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F6R1_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F6R1_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F6R1_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F6R1_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F6R1_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F6R1_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F6R1_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F6R1_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F6R1_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F6R1_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F6R1_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F6R1_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F6R1_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F6R1_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F6R1_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F6R1_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F6R1_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F6R1_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F6R1_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F6R1_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F6R1_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F6R1_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F6R1_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F6R1_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F6R1_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F6R1_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F6R1_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F7R1 register  *******************/
	#define  CAN_F7R1_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F7R1_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F7R1_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F7R1_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F7R1_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F7R1_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F7R1_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F7R1_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F7R1_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F7R1_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F7R1_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F7R1_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F7R1_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F7R1_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F7R1_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F7R1_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F7R1_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F7R1_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F7R1_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F7R1_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F7R1_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F7R1_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F7R1_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F7R1_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F7R1_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F7R1_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F7R1_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F7R1_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F7R1_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F7R1_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F7R1_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F7R1_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F8R1 register  *******************/
	#define  CAN_F8R1_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F8R1_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F8R1_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F8R1_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F8R1_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F8R1_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F8R1_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F8R1_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F8R1_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F8R1_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F8R1_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F8R1_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F8R1_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F8R1_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F8R1_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F8R1_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F8R1_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F8R1_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F8R1_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F8R1_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F8R1_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F8R1_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F8R1_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F8R1_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F8R1_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F8R1_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F8R1_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F8R1_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F8R1_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F8R1_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F8R1_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F8R1_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F9R1 register  *******************/
	#define  CAN_F9R1_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F9R1_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F9R1_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F9R1_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F9R1_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F9R1_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F9R1_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F9R1_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F9R1_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F9R1_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F9R1_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F9R1_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F9R1_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F9R1_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F9R1_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F9R1_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F9R1_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F9R1_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F9R1_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F9R1_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F9R1_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F9R1_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F9R1_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F9R1_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F9R1_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F9R1_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F9R1_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F9R1_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F9R1_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F9R1_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F9R1_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F9R1_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F10R1 register  ******************/
	#define  CAN_F10R1_FB0                       (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F10R1_FB1                       (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F10R1_FB2                       (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F10R1_FB3                       (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F10R1_FB4                       (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F10R1_FB5                       (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F10R1_FB6                       (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F10R1_FB7                       (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F10R1_FB8                       (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F10R1_FB9                       (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F10R1_FB10                      (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F10R1_FB11                      (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F10R1_FB12                      (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F10R1_FB13                      (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F10R1_FB14                      (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F10R1_FB15                      (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F10R1_FB16                      (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F10R1_FB17                      (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F10R1_FB18                      (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F10R1_FB19                      (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F10R1_FB20                      (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F10R1_FB21                      (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F10R1_FB22                      (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F10R1_FB23                      (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F10R1_FB24                      (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F10R1_FB25                      (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F10R1_FB26                      (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F10R1_FB27                      (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F10R1_FB28                      (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F10R1_FB29                      (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F10R1_FB30                      (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F10R1_FB31                      (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F11R1 register  ******************/
	#define  CAN_F11R1_FB0                       (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F11R1_FB1                       (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F11R1_FB2                       (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F11R1_FB3                       (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F11R1_FB4                       (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F11R1_FB5                       (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F11R1_FB6                       (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F11R1_FB7                       (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F11R1_FB8                       (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F11R1_FB9                       (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F11R1_FB10                      (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F11R1_FB11                      (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F11R1_FB12                      (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F11R1_FB13                      (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F11R1_FB14                      (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F11R1_FB15                      (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F11R1_FB16                      (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F11R1_FB17                      (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F11R1_FB18                      (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F11R1_FB19                      (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F11R1_FB20                      (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F11R1_FB21                      (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F11R1_FB22                      (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F11R1_FB23                      (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F11R1_FB24                      (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F11R1_FB25                      (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F11R1_FB26                      (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F11R1_FB27                      (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F11R1_FB28                      (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F11R1_FB29                      (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F11R1_FB30                      (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F11R1_FB31                      (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F12R1 register  ******************/
	#define  CAN_F12R1_FB0                       (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F12R1_FB1                       (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F12R1_FB2                       (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F12R1_FB3                       (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F12R1_FB4                       (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F12R1_FB5                       (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F12R1_FB6                       (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F12R1_FB7                       (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F12R1_FB8                       (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F12R1_FB9                       (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F12R1_FB10                      (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F12R1_FB11                      (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F12R1_FB12                      (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F12R1_FB13                      (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F12R1_FB14                      (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F12R1_FB15                      (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F12R1_FB16                      (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F12R1_FB17                      (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F12R1_FB18                      (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F12R1_FB19                      (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F12R1_FB20                      (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F12R1_FB21                      (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F12R1_FB22                      (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F12R1_FB23                      (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F12R1_FB24                      (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F12R1_FB25                      (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F12R1_FB26                      (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F12R1_FB27                      (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F12R1_FB28                      (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F12R1_FB29                      (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F12R1_FB30                      (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F12R1_FB31                      (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F13R1 register  ******************/
	#define  CAN_F13R1_FB0                       (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F13R1_FB1                       (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F13R1_FB2                       (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F13R1_FB3                       (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F13R1_FB4                       (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F13R1_FB5                       (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F13R1_FB6                       (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F13R1_FB7                       (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F13R1_FB8                       (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F13R1_FB9                       (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F13R1_FB10                      (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F13R1_FB11                      (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F13R1_FB12                      (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F13R1_FB13                      (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F13R1_FB14                      (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F13R1_FB15                      (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F13R1_FB16                      (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F13R1_FB17                      (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F13R1_FB18                      (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F13R1_FB19                      (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F13R1_FB20                      (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F13R1_FB21                      (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F13R1_FB22                      (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F13R1_FB23                      (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F13R1_FB24                      (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F13R1_FB25                      (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F13R1_FB26                      (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F13R1_FB27                      (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F13R1_FB28                      (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F13R1_FB29                      (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F13R1_FB30                      (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F13R1_FB31                      (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F0R2 register  *******************/
	#define  CAN_F0R2_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F0R2_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F0R2_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F0R2_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F0R2_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F0R2_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F0R2_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F0R2_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F0R2_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F0R2_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F0R2_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F0R2_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F0R2_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F0R2_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F0R2_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F0R2_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F0R2_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F0R2_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F0R2_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F0R2_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F0R2_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F0R2_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F0R2_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F0R2_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F0R2_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F0R2_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F0R2_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F0R2_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F0R2_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F0R2_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F0R2_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F0R2_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F1R2 register  *******************/
	#define  CAN_F1R2_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F1R2_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F1R2_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F1R2_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F1R2_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F1R2_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F1R2_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F1R2_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F1R2_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F1R2_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F1R2_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F1R2_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F1R2_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F1R2_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F1R2_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F1R2_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F1R2_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F1R2_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F1R2_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F1R2_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F1R2_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F1R2_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F1R2_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F1R2_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F1R2_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F1R2_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F1R2_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F1R2_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F1R2_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F1R2_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F1R2_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F1R2_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F2R2 register  *******************/
	#define  CAN_F2R2_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F2R2_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F2R2_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F2R2_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F2R2_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F2R2_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F2R2_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F2R2_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F2R2_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F2R2_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F2R2_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F2R2_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F2R2_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F2R2_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F2R2_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F2R2_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F2R2_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F2R2_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F2R2_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F2R2_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F2R2_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F2R2_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F2R2_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F2R2_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F2R2_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F2R2_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F2R2_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F2R2_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F2R2_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F2R2_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F2R2_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F2R2_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F3R2 register  *******************/
	#define  CAN_F3R2_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F3R2_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F3R2_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F3R2_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F3R2_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F3R2_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F3R2_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F3R2_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F3R2_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F3R2_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F3R2_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F3R2_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F3R2_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F3R2_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F3R2_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F3R2_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F3R2_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F3R2_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F3R2_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F3R2_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F3R2_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F3R2_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F3R2_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F3R2_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F3R2_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F3R2_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F3R2_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F3R2_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F3R2_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F3R2_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F3R2_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F3R2_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F4R2 register  *******************/
	#define  CAN_F4R2_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F4R2_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F4R2_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F4R2_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F4R2_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F4R2_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F4R2_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F4R2_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F4R2_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F4R2_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F4R2_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F4R2_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F4R2_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F4R2_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F4R2_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F4R2_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F4R2_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F4R2_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F4R2_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F4R2_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F4R2_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F4R2_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F4R2_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F4R2_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F4R2_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F4R2_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F4R2_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F4R2_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F4R2_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F4R2_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F4R2_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F4R2_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F5R2 register  *******************/
	#define  CAN_F5R2_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F5R2_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F5R2_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F5R2_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F5R2_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F5R2_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F5R2_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F5R2_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F5R2_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F5R2_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F5R2_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F5R2_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F5R2_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F5R2_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F5R2_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F5R2_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F5R2_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F5R2_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F5R2_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F5R2_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F5R2_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F5R2_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F5R2_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F5R2_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F5R2_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F5R2_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F5R2_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F5R2_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F5R2_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F5R2_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F5R2_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F5R2_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F6R2 register  *******************/
	#define  CAN_F6R2_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F6R2_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F6R2_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F6R2_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F6R2_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F6R2_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F6R2_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F6R2_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F6R2_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F6R2_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F6R2_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F6R2_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F6R2_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F6R2_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F6R2_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F6R2_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F6R2_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F6R2_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F6R2_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F6R2_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F6R2_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F6R2_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F6R2_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F6R2_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F6R2_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F6R2_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F6R2_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F6R2_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F6R2_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F6R2_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F6R2_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F6R2_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F7R2 register  *******************/
	#define  CAN_F7R2_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F7R2_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F7R2_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F7R2_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F7R2_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F7R2_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F7R2_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F7R2_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F7R2_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F7R2_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F7R2_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F7R2_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F7R2_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F7R2_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F7R2_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F7R2_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F7R2_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F7R2_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F7R2_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F7R2_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F7R2_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F7R2_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F7R2_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F7R2_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F7R2_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F7R2_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F7R2_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F7R2_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F7R2_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F7R2_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F7R2_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F7R2_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F8R2 register  *******************/
	#define  CAN_F8R2_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F8R2_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F8R2_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F8R2_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F8R2_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F8R2_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F8R2_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F8R2_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F8R2_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F8R2_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F8R2_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F8R2_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F8R2_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F8R2_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F8R2_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F8R2_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F8R2_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F8R2_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F8R2_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F8R2_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F8R2_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F8R2_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F8R2_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F8R2_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F8R2_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F8R2_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F8R2_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F8R2_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F8R2_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F8R2_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F8R2_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F8R2_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F9R2 register  *******************/
	#define  CAN_F9R2_FB0                        (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F9R2_FB1                        (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F9R2_FB2                        (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F9R2_FB3                        (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F9R2_FB4                        (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F9R2_FB5                        (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F9R2_FB6                        (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F9R2_FB7                        (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F9R2_FB8                        (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F9R2_FB9                        (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F9R2_FB10                       (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F9R2_FB11                       (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F9R2_FB12                       (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F9R2_FB13                       (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F9R2_FB14                       (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F9R2_FB15                       (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F9R2_FB16                       (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F9R2_FB17                       (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F9R2_FB18                       (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F9R2_FB19                       (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F9R2_FB20                       (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F9R2_FB21                       (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F9R2_FB22                       (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F9R2_FB23                       (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F9R2_FB24                       (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F9R2_FB25                       (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F9R2_FB26                       (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F9R2_FB27                       (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F9R2_FB28                       (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F9R2_FB29                       (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F9R2_FB30                       (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F9R2_FB31                       (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F10R2 register  ******************/
	#define  CAN_F10R2_FB0                       (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F10R2_FB1                       (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F10R2_FB2                       (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F10R2_FB3                       (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F10R2_FB4                       (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F10R2_FB5                       (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F10R2_FB6                       (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F10R2_FB7                       (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F10R2_FB8                       (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F10R2_FB9                       (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F10R2_FB10                      (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F10R2_FB11                      (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F10R2_FB12                      (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F10R2_FB13                      (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F10R2_FB14                      (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F10R2_FB15                      (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F10R2_FB16                      (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F10R2_FB17                      (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F10R2_FB18                      (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F10R2_FB19                      (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F10R2_FB20                      (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F10R2_FB21                      (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F10R2_FB22                      (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F10R2_FB23                      (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F10R2_FB24                      (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F10R2_FB25                      (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F10R2_FB26                      (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F10R2_FB27                      (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F10R2_FB28                      (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F10R2_FB29                      (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F10R2_FB30                      (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F10R2_FB31                      (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F11R2 register  ******************/
	#define  CAN_F11R2_FB0                       (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F11R2_FB1                       (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F11R2_FB2                       (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F11R2_FB3                       (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F11R2_FB4                       (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F11R2_FB5                       (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F11R2_FB6                       (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F11R2_FB7                       (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F11R2_FB8                       (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F11R2_FB9                       (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F11R2_FB10                      (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F11R2_FB11                      (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F11R2_FB12                      (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F11R2_FB13                      (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F11R2_FB14                      (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F11R2_FB15                      (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F11R2_FB16                      (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F11R2_FB17                      (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F11R2_FB18                      (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F11R2_FB19                      (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F11R2_FB20                      (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F11R2_FB21                      (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F11R2_FB22                      (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F11R2_FB23                      (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F11R2_FB24                      (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F11R2_FB25                      (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F11R2_FB26                      (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F11R2_FB27                      (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F11R2_FB28                      (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F11R2_FB29                      (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F11R2_FB30                      (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F11R2_FB31                      (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F12R2 register  ******************/
	#define  CAN_F12R2_FB0                       (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F12R2_FB1                       (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F12R2_FB2                       (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F12R2_FB3                       (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F12R2_FB4                       (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F12R2_FB5                       (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F12R2_FB6                       (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F12R2_FB7                       (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F12R2_FB8                       (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F12R2_FB9                       (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F12R2_FB10                      (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F12R2_FB11                      (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F12R2_FB12                      (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F12R2_FB13                      (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F12R2_FB14                      (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F12R2_FB15                      (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F12R2_FB16                      (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F12R2_FB17                      (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F12R2_FB18                      (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F12R2_FB19                      (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F12R2_FB20                      (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F12R2_FB21                      (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F12R2_FB22                      (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F12R2_FB23                      (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F12R2_FB24                      (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F12R2_FB25                      (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F12R2_FB26                      (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F12R2_FB27                      (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F12R2_FB28                      (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F12R2_FB29                      (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F12R2_FB30                      (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F12R2_FB31                      (0x80000000)        	/*!<Filter bit 31 */

	/*******************  Bit definition for CAN_F13R2 register  ******************/
	#define  CAN_F13R2_FB0                       (0x00000001)        	/*!<Filter bit 0 */
	#define  CAN_F13R2_FB1                       (0x00000002)        	/*!<Filter bit 1 */
	#define  CAN_F13R2_FB2                       (0x00000004)        	/*!<Filter bit 2 */
	#define  CAN_F13R2_FB3                       (0x00000008)        	/*!<Filter bit 3 */
	#define  CAN_F13R2_FB4                       (0x00000010)        	/*!<Filter bit 4 */
	#define  CAN_F13R2_FB5                       (0x00000020)        	/*!<Filter bit 5 */
	#define  CAN_F13R2_FB6                       (0x00000040)        	/*!<Filter bit 6 */
	#define  CAN_F13R2_FB7                       (0x00000080)        	/*!<Filter bit 7 */
	#define  CAN_F13R2_FB8                       (0x00000100)        	/*!<Filter bit 8 */
	#define  CAN_F13R2_FB9                       (0x00000200)        	/*!<Filter bit 9 */
	#define  CAN_F13R2_FB10                      (0x00000400)        	/*!<Filter bit 10 */
	#define  CAN_F13R2_FB11                      (0x00000800)        	/*!<Filter bit 11 */
	#define  CAN_F13R2_FB12                      (0x00001000)        	/*!<Filter bit 12 */
	#define  CAN_F13R2_FB13                      (0x00002000)        	/*!<Filter bit 13 */
	#define  CAN_F13R2_FB14                      (0x00004000)        	/*!<Filter bit 14 */
	#define  CAN_F13R2_FB15                      (0x00008000)        	/*!<Filter bit 15 */
	#define  CAN_F13R2_FB16                      (0x00010000)        	/*!<Filter bit 16 */
	#define  CAN_F13R2_FB17                      (0x00020000)        	/*!<Filter bit 17 */
	#define  CAN_F13R2_FB18                      (0x00040000)        	/*!<Filter bit 18 */
	#define  CAN_F13R2_FB19                      (0x00080000)        	/*!<Filter bit 19 */
	#define  CAN_F13R2_FB20                      (0x00100000)        	/*!<Filter bit 20 */
	#define  CAN_F13R2_FB21                      (0x00200000)        	/*!<Filter bit 21 */
	#define  CAN_F13R2_FB22                      (0x00400000)        	/*!<Filter bit 22 */
	#define  CAN_F13R2_FB23                      (0x00800000)        	/*!<Filter bit 23 */
	#define  CAN_F13R2_FB24                      (0x01000000)        	/*!<Filter bit 24 */
	#define  CAN_F13R2_FB25                      (0x02000000)        	/*!<Filter bit 25 */
	#define  CAN_F13R2_FB26                      (0x04000000)        	/*!<Filter bit 26 */
	#define  CAN_F13R2_FB27                      (0x08000000)        	/*!<Filter bit 27 */
	#define  CAN_F13R2_FB28                      (0x10000000)        	/*!<Filter bit 28 */
	#define  CAN_F13R2_FB29                      (0x20000000)        	/*!<Filter bit 29 */
	#define  CAN_F13R2_FB30                      (0x40000000)        	/*!<Filter bit 30 */
	#define  CAN_F13R2_FB31                      (0x80000000)        	/*!<Filter bit 31 */

	/******************************************************************************/
	/*                                                                            */
	/*                        Serial Peripheral Interface                         */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for SPI_CR1 register  ********************/
	#define  SPI_CPHA                        	0            	/*!<Clock Phase */
	#define  SPI_CPOL                        	1            	/*!<Clock Polarity */
	#define  SPI_MSTR                        	2            	/*!<Master Selection */
	#define  SPI_BR                          	3            	/*!<BR[2:0] bits (Baud Rate Control) */
	#define  SPI_SPE                         	6            	/*!<SPI Enable */
	#define  SPI_LSBFIRST                    	7            	/*!<Frame Format */
	#define  SPI_SSI                         	8            	/*!<Internal slave select */
	#define  SPI_SSM                         	9            	/*!<Software slave management */
	#define  SPI_RXONLY                      	10            	/*!<Receive only */
	#define  SPI_DFF                         	11            	/*!<Data Frame Format */
	#define  SPI_CRCNEXT                     	12            	/*!<Transmit CRC next */
	#define  SPI_CRCEN                       	13            	/*!<Hardware CRC calculation enable */
	#define  SPI_BIDIOE                      	14            	/*!<Output enable in bidirectional mode */
	#define  SPI_BIDIMODE                    	15            	/*!<Bidirectional data mode enable */

	/*******************  Bit definition for SPI_CR2 register  ********************/
	#define  SPI_RXDMAEN                     	0               	/*!<Rx Buffer DMA Enable */
	#define  SPI_TXDMAEN                     	1               	/*!<Tx Buffer DMA Enable */
	#define  SPI_SSOE                        	2               	/*!<SS Output Enable */
	#define  SPI_ERRIE                       	5               	/*!<Error Interrupt Enable */
	#define  SPI_RXNEIE                      	6               	/*!<RX buffer Not Empty Interrupt Enable */
	#define  SPI_TXEIE                       	7               	/*!<Tx buffer Empty Interrupt Enable */

	/********************  Bit definition for SPI_SR register  ********************/
	#define  SPI_RXNE                         0               	/*!<Receive buffer Not Empty */
	#define  SPI_TXE                          1               	/*!<Transmit buffer Empty */
	#define  SPI_CHSIDE                       2               	/*!<Channel side */
	#define  SPI_UDR                          3               	/*!<Underrun flag */
	#define  SPI_CRCERR                       4               	/*!<CRC Error flag */
	#define  SPI_MODF                         5               	/*!<Mode fault */
	#define  SPI_OVR                          6               	/*!<Overrun flag */
	#define  SPI_BSY                          7               	/*!<Busy flag */

	/********************  Bit definition for SPI_DR register  ********************/
	#define  SPI_DR_DR                           ((uint16_t)0xFFFF)            	/*!<Data Register */

	/*******************  Bit definition for SPI_CRCPR register  ******************/
	#define  SPI_CRCPR_CRCPOLY                   ((uint16_t)0xFFFF)            	/*!<CRC polynomial register */

	/******************  Bit definition for SPI_RXCRCR register  ******************/
	#define  SPI_RXCRCR_RXCRC                    ((uint16_t)0xFFFF)            	/*!<Rx CRC Register */

	/******************  Bit definition for SPI_TXCRCR register  ******************/
	#define  SPI_TXCRCR_TXCRC                    ((uint16_t)0xFFFF)            	/*!<Tx CRC Register */

	/******************  Bit definition for SPI_I2SCFGR register  *****************/
	#define  SPI_I2SCFGR_CHLEN                   ((uint16_t)0x0001)            	/*!<Channel length (number of bits per audio channel) */

	#define  SPI_I2SCFGR_DATLEN                  ((uint16_t)0x0006)            	/*!<DATLEN[1:0] bits (Data length to be transferred) */
	#define  SPI_I2SCFGR_DATLEN_0                ((uint16_t)0x0002)            	/*!<Bit 0 */
	#define  SPI_I2SCFGR_DATLEN_1                ((uint16_t)0x0004)            	/*!<Bit 1 */

	#define  SPI_I2SCFGR_CKPOL                   ((uint16_t)0x0008)            	/*!<steady state clock polarity */

	#define  SPI_I2SCFGR_I2SSTD                  ((uint16_t)0x0030)            	/*!<I2SSTD[1:0] bits (I2S standard selection) */
	#define  SPI_I2SCFGR_I2SSTD_0                ((uint16_t)0x0010)            	/*!<Bit 0 */
	#define  SPI_I2SCFGR_I2SSTD_1                ((uint16_t)0x0020)            	/*!<Bit 1 */

	#define  SPI_I2SCFGR_PCMSYNC                 ((uint16_t)0x0080)            	/*!<PCM frame synchronization */

	#define  SPI_I2SCFGR_I2SCFG                  ((uint16_t)0x0300)            	/*!<I2SCFG[1:0] bits (I2S configuration mode) */
	#define  SPI_I2SCFGR_I2SCFG_0                ((uint16_t)0x0100)            	/*!<Bit 0 */
	#define  SPI_I2SCFGR_I2SCFG_1                ((uint16_t)0x0200)            	/*!<Bit 1 */

	#define  SPI_I2SCFGR_I2SE                    ((uint16_t)0x0400)            	/*!<I2S Enable */
	#define  SPI_I2SCFGR_I2SMOD                  ((uint16_t)0x0800)            	/*!<I2S mode selection */

	/******************  Bit definition for SPI_I2SPR register  *******************/
	#define  SPI_I2SPR_I2SDIV                    ((uint16_t)0x00FF)            	/*!<I2S Linear prescaler */
	#define  SPI_I2SPR_ODD                       ((uint16_t)0x0100)            	/*!<Odd factor for the prescaler */
	#define  SPI_I2SPR_MCKOE                     ((uint16_t)0x0200)            	/*!<Master Clock Output Enable */

	/******************************************************************************/
	/*                                                                            */
	/*                      Inter-integrated Circuit Interface                    */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for I2C_CR1 register  ********************/
	#define  I2C_CR1_PE                          ((uint16_t)0x0001)            	/*!<Peripheral Enable */
	#define  I2C_CR1_SMBUS                       ((uint16_t)0x0002)            	/*!<SMBus Mode */
	#define  I2C_CR1_SMBTYPE                     ((uint16_t)0x0008)            	/*!<SMBus Type */
	#define  I2C_CR1_ENARP                       ((uint16_t)0x0010)            	/*!<ARP Enable */
	#define  I2C_CR1_ENPEC                       ((uint16_t)0x0020)            	/*!<PEC Enable */
	#define  I2C_CR1_ENGC                        ((uint16_t)0x0040)            	/*!<General Call Enable */
	#define  I2C_CR1_NOSTRETCH                   ((uint16_t)0x0080)            	/*!<Clock Stretching Disable (Slave mode) */
	#define  I2C_CR1_START                       ((uint16_t)0x0100)            	/*!<Start Generation */
	#define  I2C_CR1_STOP                        ((uint16_t)0x0200)            	/*!<Stop Generation */
	#define  I2C_CR1_ACK                         ((uint16_t)0x0400)            	/*!<Acknowledge Enable */
	#define  I2C_CR1_POS                         ((uint16_t)0x0800)            	/*!<Acknowledge/PEC Position (for data reception) */
	#define  I2C_CR1_PEC                         ((uint16_t)0x1000)            	/*!<Packet Error Checking */
	#define  I2C_CR1_ALERT                       ((uint16_t)0x2000)            	/*!<SMBus Alert */
	#define  I2C_CR1_SWRST                       ((uint16_t)0x8000)            	/*!<Software Reset */

	/*******************  Bit definition for I2C_CR2 register  ********************/
	#define  I2C_CR2_FREQ                        ((uint16_t)0x003F)            	/*!<FREQ[5:0] bits (Peripheral Clock Frequency) */
	#define  I2C_CR2_FREQ_0                      ((uint16_t)0x0001)            	/*!<Bit 0 */
	#define  I2C_CR2_FREQ_1                      ((uint16_t)0x0002)            	/*!<Bit 1 */
	#define  I2C_CR2_FREQ_2                      ((uint16_t)0x0004)            	/*!<Bit 2 */
	#define  I2C_CR2_FREQ_3                      ((uint16_t)0x0008)            	/*!<Bit 3 */
	#define  I2C_CR2_FREQ_4                      ((uint16_t)0x0010)            	/*!<Bit 4 */
	#define  I2C_CR2_FREQ_5                      ((uint16_t)0x0020)            	/*!<Bit 5 */

	#define  I2C_CR2_ITERREN                     ((uint16_t)0x0100)            	/*!<Error Interrupt Enable */
	#define  I2C_CR2_ITEVTEN                     ((uint16_t)0x0200)            	/*!<Event Interrupt Enable */
	#define  I2C_CR2_ITBUFEN                     ((uint16_t)0x0400)            	/*!<Buffer Interrupt Enable */
	#define  I2C_CR2_DMAEN                       ((uint16_t)0x0800)            	/*!<DMA Requests Enable */
	#define  I2C_CR2_LAST                        ((uint16_t)0x1000)            	/*!<DMA Last Transfer */

	/*******************  Bit definition for I2C_OAR1 register  *******************/
	#define  I2C_OAR1_ADD1_7                     ((uint16_t)0x00FE)            	/*!<Interface Address */
	#define  I2C_OAR1_ADD8_9                     ((uint16_t)0x0300)            	/*!<Interface Address */

	#define  I2C_OAR1_ADD0                       ((uint16_t)0x0001)            	/*!<Bit 0 */
	#define  I2C_OAR1_ADD1                       ((uint16_t)0x0002)            	/*!<Bit 1 */
	#define  I2C_OAR1_ADD2                       ((uint16_t)0x0004)            	/*!<Bit 2 */
	#define  I2C_OAR1_ADD3                       ((uint16_t)0x0008)            	/*!<Bit 3 */
	#define  I2C_OAR1_ADD4                       ((uint16_t)0x0010)            	/*!<Bit 4 */
	#define  I2C_OAR1_ADD5                       ((uint16_t)0x0020)            	/*!<Bit 5 */
	#define  I2C_OAR1_ADD6                       ((uint16_t)0x0040)            	/*!<Bit 6 */
	#define  I2C_OAR1_ADD7                       ((uint16_t)0x0080)            	/*!<Bit 7 */
	#define  I2C_OAR1_ADD8                       ((uint16_t)0x0100)            	/*!<Bit 8 */
	#define  I2C_OAR1_ADD9                       ((uint16_t)0x0200)            	/*!<Bit 9 */

	#define  I2C_OAR1_ADDMODE                    ((uint16_t)0x8000)            	/*!<Addressing Mode (Slave mode) */

	/*******************  Bit definition for I2C_OAR2 register  *******************/
	#define  I2C_OAR2_ENDUAL                     ((uint8_t)0x01)               	/*!<Dual addressing mode enable */
	#define  I2C_OAR2_ADD2                       ((uint8_t)0xFE)               	/*!<Interface address */

	/********************  Bit definition for I2C_DR register  ********************/
	#define  I2C_DR_DR                           ((uint8_t)0xFF)               	/*!<8-bit Data Register */

	/*******************  Bit definition for I2C_SR1 register  ********************/
	#define  I2C_SR1_SB                          ((uint16_t)0x0001)            	/*!<Start Bit (Master mode) */
	#define  I2C_SR1_ADDR                        ((uint16_t)0x0002)            	/*!<Address sent (master mode)/matched (slave mode) */
	#define  I2C_SR1_BTF                         ((uint16_t)0x0004)            	/*!<Byte Transfer Finished */
	#define  I2C_SR1_ADD10                       ((uint16_t)0x0008)            	/*!<10-bit header sent (Master mode) */
	#define  I2C_SR1_STOPF                       ((uint16_t)0x0010)            	/*!<Stop detection (Slave mode) */
	#define  I2C_SR1_RXNE                        ((uint16_t)0x0040)            	/*!<Data Register not Empty (receivers) */
	#define  I2C_SR1_TXE                         ((uint16_t)0x0080)            	/*!<Data Register Empty (transmitters) */
	#define  I2C_SR1_BERR                        ((uint16_t)0x0100)            	/*!<Bus Error */
	#define  I2C_SR1_ARLO                        ((uint16_t)0x0200)            	/*!<Arbitration Lost (master mode) */
	#define  I2C_SR1_AF                          ((uint16_t)0x0400)            	/*!<Acknowledge Failure */
	#define  I2C_SR1_OVR                         ((uint16_t)0x0800)            	/*!<Overrun/Underrun */
	#define  I2C_SR1_PECERR                      ((uint16_t)0x1000)            	/*!<PEC Error in reception */
	#define  I2C_SR1_TIMEOUT                     ((uint16_t)0x4000)            	/*!<Timeout or Tlow Error */
	#define  I2C_SR1_SMBALERT                    ((uint16_t)0x8000)            	/*!<SMBus Alert */

	/*******************  Bit definition for I2C_SR2 register  ********************/
	#define  I2C_SR2_MSL                         ((uint16_t)0x0001)            	/*!<Master/Slave */
	#define  I2C_SR2_BUSY                        ((uint16_t)0x0002)            	/*!<Bus Busy */
	#define  I2C_SR2_TRA                         ((uint16_t)0x0004)            	/*!<Transmitter/Receiver */
	#define  I2C_SR2_GENCALL                     ((uint16_t)0x0010)            	/*!<General Call Address (Slave mode) */
	#define  I2C_SR2_SMBDEFAULT                  ((uint16_t)0x0020)            	/*!<SMBus Device Default Address (Slave mode) */
	#define  I2C_SR2_SMBHOST                     ((uint16_t)0x0040)            	/*!<SMBus Host Header (Slave mode) */
	#define  I2C_SR2_DUALF                       ((uint16_t)0x0080)            	/*!<Dual Flag (Slave mode) */
	#define  I2C_SR2_PEC                         ((uint16_t)0xFF00)            	/*!<Packet Error Checking Register */

	/*******************  Bit definition for I2C_CCR register  ********************/
	#define  I2C_CCR_CCR                         ((uint16_t)0x0FFF)            	/*!<Clock Control Register in Fast/Standard mode (Master mode) */
	#define  I2C_CCR_DUTY                        ((uint16_t)0x4000)            	/*!<Fast Mode Duty Cycle */
	#define  I2C_CCR_FS                          ((uint16_t)0x8000)            	/*!<I2C Master Mode Selection */

	/******************  Bit definition for I2C_TRISE register  *******************/
	#define  I2C_TRISE_TRISE                     ((uint8_t)0x3F)               	/*!<Maximum Rise Time in Fast/Standard mode (Master mode) */

	/******************************************************************************/
	/*                                                                            */
	/*         Universal Synchronous Asynchronous Receiver Transmitter            */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for USART_SR register  *******************/
	#define  USART_PE                         	0            	/*!<Parity Error */
	#define  USART_FE                         	1            	/*!<Framing Error */
	#define  USART_NE                         	2            	/*!<Noise Error Flag */
	#define  USART_ORE                        	3            	/*!<OverRun Error */
	#define  USART_IDLE                       	4            	/*!<IDLE line detected */
	#define  USART_RXNE                       	5            	/*!<Read Data Register Not Empty */
	#define  USART_TC                         	6            	/*!<Transmission Complete */
	#define  USART_TXE                        	7            	/*!<Transmit Data Register Empty */
	#define  USART_LBD                        	8            	/*!<LIN Break Detection Flag */
	#define  USART_CTS                        	9            	/*!<CTS Flag */

	/******************  Bit definition for USART_BRR register  *******************/
	#define  USART_Fraction              		0            	/*!<Fraction of USARTDIV */
	#define  USART_Mantissa              		4            	/*!<Mantissa of USARTDIV */

	/******************  Bit definition for USART_CR1 register  *******************/
	#define  USART_SBK                       	0            	/*!<Send Break */
	#define  USART_RWU                       	1            	/*!<Receiver wakeup */
	#define  USART_RE                        	2            	/*!<Receiver Enable */
	#define  USART_TE                        	3            	/*!<Transmitter Enable */
	#define  USART_IDLEIE                    	4            	/*!<IDLE Interrupt Enable */
	#define  USART_RXNEIE                    	5            	/*!<RXNE Interrupt Enable */
	#define  USART_TCIE                      	6            	/*!<Transmission Complete Interrupt Enable */
	#define  USART_TXEIE                     	7            	/*!<PE Interrupt Enable */
	#define  USART_PEIE                      	8            	/*!<PE Interrupt Enable */
	#define  USART_PS                        	9            	/*!<Parity Selection */
	#define  USART_PCE                       	10            	/*!<Parity Control Enable */
	#define  USART_WAKE                      	11            	/*!<Wakeup method */
	#define  USART_M                         	12            	/*!<Word length */
	#define  USART_UE                        	13            	/*!<USART Enable */

	/******************  Bit definition for USART_CR2 register  *******************/
	#define  USART_ADD                       	0            	/*!<Address of the USART node */
	#define  USART_LBDL                      	5            	/*!<LIN Break Detection Length */
	#define  USART_LBDIE                     	6            	/*!<LIN Break Detection Interrupt Enable */
	#define  USART_LBCL                      	8            	/*!<Last Bit Clock pulse */
	#define  USART_CPHA                      	9            	/*!<Clock Phase */
	#define  USART_CPOL                      	10            	/*!<Clock Polarity */
	#define  USART_CLKEN                     	11            	/*!<Clock Enable */
	#define  USART_STOP                      	12            	/*!<STOP[1:0] bits (STOP bits */
	#define  USART_LINEN                     	14            	/*!<LIN mode enable */

	/******************  Bit definition for USART_CR3 register  *******************/
	#define  USART_EIE                       	0            	/*!<Error Interrupt Enable */
	#define  USART_IREN                      	1            	/*!<IrDA mode Enable */
	#define  USART_IRLP                      	2            	/*!<IrDA Low-Power */
	#define  USART_HDSEL                     	3            	/*!<Half-Duplex Selection */
	#define  USART_NACK                      	4            	/*!<Smartcard NACK enable */
	#define  USART_SCEN                      	5            	/*!<Smartcard mode enable */
	#define  USART_DMAR                      	6            	/*!<DMA Enable Receiver */
	#define  USART_DMAT                      	7            	/*!<DMA Enable Transmitter */
	#define  USART_RTSE                      	8            	/*!<RTS Enable */
	#define  USART_CTSE                      	9            	/*!<CTS Enable */
	#define  USART_CTSIE                     	10            	/*!<CTS Interrupt Enable */

	/******************  Bit definition for USART_GTPR register  ******************/
	#define  USART_PSC                      	0            	/*!<PSC[7:0] bits (Prescaler value */
	#define  USART_GT                       	8            	/*!<Guard time value */

	/******************************************************************************/
	/*                                                                            */
	/*                                 Debug MCU                                  */
	/*                                                                            */
	/******************************************************************************/

	/****************  Bit definition for DBGMCU_IDCODE register  *****************/
	#define  DBGMCU_IDCODE_DEV_ID                (0x00000FFF)        	/*!<Device Identifier */

	#define  DBGMCU_IDCODE_REV_ID                (0xFFFF0000)        	/*!<REV_ID[15:0] bits (Revision Identifier) */
	#define  DBGMCU_IDCODE_REV_ID_0              (0x00010000)        	/*!<Bit 0 */
	#define  DBGMCU_IDCODE_REV_ID_1              (0x00020000)        	/*!<Bit 1 */
	#define  DBGMCU_IDCODE_REV_ID_2              (0x00040000)        	/*!<Bit 2 */
	#define  DBGMCU_IDCODE_REV_ID_3              (0x00080000)        	/*!<Bit 3 */
	#define  DBGMCU_IDCODE_REV_ID_4              (0x00100000)        	/*!<Bit 4 */
	#define  DBGMCU_IDCODE_REV_ID_5              (0x00200000)        	/*!<Bit 5 */
	#define  DBGMCU_IDCODE_REV_ID_6              (0x00400000)        	/*!<Bit 6 */
	#define  DBGMCU_IDCODE_REV_ID_7              (0x00800000)        	/*!<Bit 7 */
	#define  DBGMCU_IDCODE_REV_ID_8              (0x01000000)        	/*!<Bit 8 */
	#define  DBGMCU_IDCODE_REV_ID_9              (0x02000000)        	/*!<Bit 9 */
	#define  DBGMCU_IDCODE_REV_ID_10             (0x04000000)        	/*!<Bit 10 */
	#define  DBGMCU_IDCODE_REV_ID_11             (0x08000000)        	/*!<Bit 11 */
	#define  DBGMCU_IDCODE_REV_ID_12             (0x10000000)        	/*!<Bit 12 */
	#define  DBGMCU_IDCODE_REV_ID_13             (0x20000000)        	/*!<Bit 13 */
	#define  DBGMCU_IDCODE_REV_ID_14             (0x40000000)        	/*!<Bit 14 */
	#define  DBGMCU_IDCODE_REV_ID_15             (0x80000000)        	/*!<Bit 15 */

	/******************  Bit definition for DBGMCU_CR register  *******************/
	#define  DBGMCU_CR_DBG_SLEEP                 (0x00000001)        	/*!<Debug Sleep Mode */
	#define  DBGMCU_CR_DBG_STOP                  (0x00000002)        	/*!<Debug Stop Mode */
	#define  DBGMCU_CR_DBG_STANDBY               (0x00000004)        	/*!<Debug Standby mode */
	#define  DBGMCU_CR_TRACE_IOEN                (0x00000020)        	/*!<Trace Pin Assignment Control */

	#define  DBGMCU_CR_TRACE_MODE                (0x000000C0)        	/*!<TRACE_MODE[1:0] bits (Trace Pin Assignment Control) */
	#define  DBGMCU_CR_TRACE_MODE_0              (0x00000040)        	/*!<Bit 0 */
	#define  DBGMCU_CR_TRACE_MODE_1              (0x00000080)        	/*!<Bit 1 */

	#define  DBGMCU_CR_DBG_IWDG_STOP             (0x00000100)        	/*!<Debug Independent Watchdog stopped when Core is halted */
	#define  DBGMCU_CR_DBG_WWDG_STOP             (0x00000200)        	/*!<Debug Window Watchdog stopped when Core is halted */
	#define  DBGMCU_CR_DBG_TIM1_STOP             (0x00000400)        	/*!<TIM1 counter stopped when core is halted */
	#define  DBGMCU_CR_DBG_TIM2_STOP             (0x00000800)        	/*!<TIM2 counter stopped when core is halted */
	#define  DBGMCU_CR_DBG_TIM3_STOP             (0x00001000)        	/*!<TIM3 counter stopped when core is halted */
	#define  DBGMCU_CR_DBG_TIM4_STOP             (0x00002000)        	/*!<TIM4 counter stopped when core is halted */
	#define  DBGMCU_CR_DBG_CAN_STOP              (0x00004000)        	/*!<Debug CAN stopped when Core is halted */
	#define  DBGMCU_CR_DBG_I2C1_SMBUS_TIMEOUT    (0x00008000)        	/*!<SMBUS timeout mode stopped when Core is halted */
	#define  DBGMCU_CR_DBG_I2C2_SMBUS_TIMEOUT    (0x00010000)        	/*!<SMBUS timeout mode stopped when Core is halted */
	#define  DBGMCU_CR_DBG_TIM5_STOP             (0x00020000)        	/*!<TIM5 counter stopped when core is halted */
	#define  DBGMCU_CR_DBG_TIM6_STOP             (0x00040000)        	/*!<TIM6 counter stopped when core is halted */
	#define  DBGMCU_CR_DBG_TIM7_STOP             (0x00080000)        	/*!<TIM7 counter stopped when core is halted */
	#define  DBGMCU_CR_DBG_TIM8_STOP             (0x00100000)        	/*!<TIM8 counter stopped when core is halted */

	/******************************************************************************/
	/*                                                                            */
	/*                      FLASH and Option Bytes Registers                      */
	/*                                                                            */
	/******************************************************************************/

	/*******************  Bit definition for FLASH_ACR register  ******************/
	#define  FLASH_LATENCY                   	0					/*!<LATENCY[2:0] bits (Latency) */
	#define  FLASH_HLFCYA                    	3					/*!<Flash Half Cycle Access Enable */
	#define  FLASH_PRFTBE                    	4					/*!<Prefetch Buffer Enable */
	#define  FLASH_PRFTBS                    	5					/*!<Prefetch Buffer Status */

	
	/******************  Bit definition for FLASH_SR register  *******************/
	#define  FLASH_BSY                        	0               	/*!<Busy */
	#define  FLASH_PGERR                      	2               	/*!<Programming Error */
	#define  FLASH_WRPRTERR                   	4              	/*!<Write Protection Error */
	#define  FLASH_EOP                        	5               	/*!<End of operation */

	/*******************  Bit definition for FLASH_CR register  *******************/
	#define  FLASH_PG                         	0            	/*!<Programming */
	#define  FLASH_PER                        	1            	/*!<Page Erase */
	#define  FLASH_MER                        	2            	/*!<Mass Erase */
	#define  FLASH_OPTPG                      	4            	/*!<Option Byte Programming */
	#define  FLASH_OPTER                      	5            	/*!<Option Byte Erase */
	#define  FLASH_STRT                       	6            	/*!<Start */
	#define  FLASH_LOCK                       	7            	/*!<Lock */
	#define  FLASH_OPTWRE                     	9            	/*!<Option Bytes Write Enable */
	#define  FLASH_ERRIE                      	10            	/*!<Error Interrupt Enable */
	#define  FLASH_EOPIE                      	12            	/*!<End of operation interrupt enable */

	/******************  Bit definition for FLASH_OBR register  *******************/
	#define  FLASH_OPTERR                    	0            	/*!<Option Byte Error */
	#define  FLASH_RDPRT                     	1            	/*!<Read protection */
	#define  FLASH_WDG_SW                    	2            	/*!<WDG_SW */
	#define  FLASH_nRST_STOP                 	3            	/*!<nRST_STOP */
	#define  FLASH_nRST_STDBY                	4            	/*!<nRST_STDBY */
	

	/******************  Bit definition for FLASH_WRPR register  ******************/
	#define  FLASH_WRPR_WRP                        (0xFFFFFFFF)        	/*!<Write Protect */

	/*----------------------------------------------------------------------------*/

	/******************  Bit definition for FLASH_RDP register  *******************/
	#define  FLASH_RDP_RDP                       (0x000000FF)        	/*!<Read protection option byte */
	#define  FLASH_RDP_nRDP                      (0x0000FF00)        	/*!<Read protection complemented option byte */

	/******************  Bit definition for FLASH_USER register  ******************/
	#define  FLASH_USER_USER                     (0x00FF0000)        	/*!<User option byte */
	#define  FLASH_USER_nUSER                    (0xFF000000)        	/*!<User complemented option byte */

	/******************  Bit definition for FLASH_Data0 register  *****************/
	#define  FLASH_Data0_Data0                   (0x000000FF)        	/*!<User data storage option byte */
	#define  FLASH_Data0_nData0                  (0x0000FF00)        	/*!<User data storage complemented option byte */

	/******************  Bit definition for FLASH_Data1 register  *****************/
	#define  FLASH_Data1_Data1                   (0x00FF0000)        	/*!<User data storage option byte */
	#define  FLASH_Data1_nData1                  (0xFF000000)        	/*!<User data storage complemented option byte */

	/******************  Bit definition for FLASH_WRP0 register  ******************/
	#define  FLASH_WRP0_WRP0                     (0x000000FF)        	/*!<Flash memory write protection option bytes */
	#define  FLASH_WRP0_nWRP0                    (0x0000FF00)        	/*!<Flash memory write protection complemented option bytes */

	/******************  Bit definition for FLASH_WRP1 register  ******************/
	#define  FLASH_WRP1_WRP1                     (0x00FF0000)        	/*!<Flash memory write protection option bytes */
	#define  FLASH_WRP1_nWRP1                    (0xFF000000)        	/*!<Flash memory write protection complemented option bytes */

	/******************  Bit definition for FLASH_WRP2 register  ******************/
	#define  FLASH_WRP2_WRP2                     (0x000000FF)        	/*!<Flash memory write protection option bytes */
	#define  FLASH_WRP2_nWRP2                    (0x0000FF00)        	/*!<Flash memory write protection complemented option bytes */

	/******************  Bit definition for FLASH_WRP3 register  ******************/
	#define  FLASH_WRP3_WRP3                     (0x00FF0000)        	/*!<Flash memory write protection option bytes */
	#define  FLASH_WRP3_nWRP3                    (0xFF000000)        	/*!<Flash memory write protection complemented option bytes */


	/**
  * @}
  */

 	/**
  * @}
  */ 

	/** @addtogroup Exported_macro
  * @{
  */

	#define SET_BIT(REG, BIT)     ((REG) |= (BIT))

	#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(BIT))

	#define READ_BIT(REG, BIT)    ((REG) & (BIT))

	#define CLEAR_REG(REG)        ((REG) = (0x0))

	#define WRITE_REG(REG, VAL)   ((REG) = (VAL))

	#define READ_REG(REG)         ((REG))

	#define MODIFY_REG(REG, CLEARMASK, SETMASK)  WRITE_REG((REG), (((READ_REG(REG)) & (~CLEARMASK)) | (SETMASK)))

	/**
  * @}
  */

#endif 	/* __STM32F10x_H */


