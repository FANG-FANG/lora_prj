#include "boot.h"
#include "ModComAnaly.h"

#define ADDR_END		128
#define AddrMaxLen	100

extern 	BootVariable Bx;
extern void ModbusError(unsigned char ErrorCode);
extern unsigned char Write_CR(unsigned short CRNum,unsigned short Value);

BootRegSTRUCT BootReg;

/*
*返回：高4位bit15-12:错误码
*			 低11位bit10-0:回复数据长度
*/

//读多线圈/开关量
unsigned short ModReadBits(COMSTRUCT *ComBufP)
{
	unsigned short rec = 0;
	if(ComBufP->Buffer[0]){				//广播地址不处理
		if(ComBufP->Len != 8) rec |= 4<<12; //帧长度判断
		else{
			unsigned short addr_s_tmp,addr_e_tmp;
			addr_s_tmp = (ComBufP->Buffer[2]<<8) + ComBufP->Buffer[3];  //读寄存器起始地址
			addr_e_tmp = addr_s_tmp + ComBufP->Buffer[5] - 1;
			if(addr_e_tmp > ADDR_END)  rec |= 2<<12;   //超过法定地址,错误码2
			else{
				if(ComBufP->Buffer[4] != 0 || ComBufP->Buffer[5] == 0 || ComBufP->Buffer[5] > AddrMaxLen) //超过长度或者长度为0
					rec |= 3<<12;
				else{
					unsigned short i,temp1,temp2;
					temp1 = ComBufP->Buffer[5];    //读的长度
					ComBufP->Buffer[2] = temp1;    //回复的长度
					temp2 = ComBufP->Buffer[3];    //读的起始地址
					for(i=0;i<temp1;i++){
						if(Bx.CR[temp2] == 0) ComBufP->Buffer[i+3] = 0x00;
						else ComBufP->Buffer[i+3] = 0x01;
						temp2++;
					}
					rec = rec = (rec & 0xF000) | (temp1+3);
				}
			}
		}
	}
	
	return rec;
}

//读多寄存器
unsigned short ModReadRegs(COMSTRUCT *ComBufP)
{
	unsigned short rec = 0;
	if(ComBufP->Buffer[0]){				//广播地址不处理
		if(ComBufP->Len != 8) rec |= 4<<12; //帧长度判断
		else{
			unsigned short addr_s_tmp,addr_e_tmp;
			addr_s_tmp = (ComBufP->Buffer[2]<<8) + ComBufP->Buffer[3];  //读寄存器起始地址
			addr_e_tmp = addr_s_tmp + ComBufP->Buffer[5] - 1;
			if(addr_e_tmp > ADDR_END)  rec |= 2<<12;   //超过法定地址,错误码2
			else{
				if(ComBufP->Buffer[4] != 0 || ComBufP->Buffer[5] == 0 || ComBufP->Buffer[5] > AddrMaxLen) //超过长度或者长度为0
					rec |= 3<<12;
				else{
					unsigned short i,temp1,temp2;
					temp1 = 2*ComBufP->Buffer[5];    //读的长度（字节）
					ComBufP->Buffer[2] = temp1;
					temp2 = ComBufP->Buffer[3];
					for(i =0;i<temp1;i+=2){
						ComBufP->Buffer[i+3] = Bx.CR[temp2]>>8;
						ComBufP->Buffer[i+4] = Bx.CR[temp2];
						temp2++;
					}
					rec = (rec & 0xF000) | (temp1+3);
				}
			}
		}
	}
	return rec;
}

//写单线圈
unsigned short ModWriteSigleBit(COMSTRUCT *ComBufP)
{
	unsigned short rec = 0;
	if(ComBufP->Buffer[0]){				//广播地址不处理
		if(ComBufP->Len != 8) rec |= 4<<12; //帧长度判断
		else{
			if(ComBufP->Buffer[2]!=0 || ComBufP->Buffer[3]<0x10 || ComBufP->Buffer[3]>=0x90)//地址错误
				rec |= 2<<12;
			else{
				if(ComBufP->Buffer[5]!=0) rec |= 3<<12;
				else{
					if(ComBufP->Buffer[4]!=0xff && ComBufP->Buffer[4]!=0)
						rec |= 3<<12;
					else{
						u8 CR_Number = ComBufP->Buffer[3];
						if(CR_Number>=109){  //??
							if(ComBufP->Buffer[4]==0xFF) Bx.CR[CR_Number]=0x0001;
							else Bx.CR[CR_Number]=0;
						}
						rec = (rec & 0xF000) |6;
					}
				}
			}
		}
	}
	return rec;
}

//写单寄存器
unsigned short ModWriteSigleReg(COMSTRUCT *ComBufP)
{
	unsigned short rec = 0;
	if(ComBufP->Buffer[0]){				//广播地址不处理
		if(ComBufP->Len != 8) rec |= 4<<12; //帧长度判断
		else{
			unsigned short addr;
			addr = (ComBufP->Buffer[2]<<8)+ComBufP->Buffer[3];
			if(addr > ADDR_END) rec |= 2<<12;
			else{
				unsigned short i;
				unsigned char err = 0;
				i = (ComBufP->Buffer[4]<<8)+ComBufP->Buffer[5];
				
				err=Write_CR(ComBufP->Buffer[3],i);
				if(err == 3) rec |= err<<12;
				else rec = (rec & 0xF000) | 6;
			}
		}
	}
	return rec;
}

//
