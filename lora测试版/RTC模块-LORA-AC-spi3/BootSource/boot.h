#include "stm32f10x_type.h"
#include "stm32f10x_map.h"
#include "stm32f10x_nvic.h" 
typedef void (*Function)(void);
#include "sx1276.h"

//01 6c a8 b9 49 31
/*引导区版本信息***************************************************************/
#define BOOT_VERSION      (u8)0x40               //版本
#define BV_DATE           (u32)0X03090E          //版本生成的日月年，主机命令
/*程序参数*********************************************************************/
#define SysConstAdress    (u32)0x08003000
#define CRAdress          (u32)0x08003400
#define ManuConstAdress   (u32)0x08003800
#define AppAdress         (u32)0x08004000        //修改时需同时修改.sct文件中的地址
#define IndexAdress       (u32)(AppAdress+0x130)
#define AppSysConstAdr    (u32)(AppAdress+0x16c)
#define PageSize          (u16)0x400
#define ComMaxLen         (u16)127               //串口modbus能读的最大寄存器数量
#define PFtimes           (u16)3                 //并口数据滤波次数
#define CRC_RESET         (u16)0X01
#define BlinkTime         (u16)30                //link灯闪烁间隔，单位ms
#define RECEIVE           (u16)0
#define TRANSMIT          (u16)1
#define ENABLE            (u16)1
#define DISABLE           (u16)0  
#define IDSel             (u16)0

/*IWD*/
#define IWDG_RELOAD       (u16)0xAAAA
#define IWDG_ENABLE       (u16)0xCCCC
#define IWDG_WR_EN        (u16)0x5555
/*GPIO端口定义*****************************************************************/
#define sp485_DRE         (u16)0x0100 //PA8
#define TXD               (u16)0x0200 //PA9
#define RXD               (u16)0x0400 //PA10
#define ENIO              (u16)0x0800 //PA11
#define DSPCS             (u16)0x1000 //PA12
#define E_DI              (u16)0x0001 //PB0
#define eSEL              (u16)0x0002 //PB1
#define PwrDN             (u16)0x0004 //PB2
#define SDA               (u16)0x0020 //PB5
#define SCLK              (u16)0x0040 //PB6
#define QST               (u16)0x0080 //PB7
#define LinkGreen         (u16)0x0100 //PB8 //link green
#define LinkRed           (u16)0x0200 //PB9 //link red
#define LinkYellow        (u16)0x0300 //PB8&9 //link yellow 红色和绿色同时亮
#define E_ST              (u16)0x0400 //PB10
#define OutAddr           (u16)0x0800 //PB11
#define InAdd             (u16)0x1000 //PC12
#define IRQ               (u16)0x2000 //PC13 
#define ExPWR             (u16)0x4000 //PC14 
#define SWin              (u16)0x8000 //PC15
#define Vctr              (u16)0x0004 //PD2
#define EXTI10            (u16)0x0400 //外部中断10
#define EXTI2             (u16)0x0004 //外部中断2

#define SR_BUSY           (u16)0x0001  //Bit 0
#define SR_EOP            (u16)0x0020  //Bit 5
#define CR_PG             (u16)0x0001  //Bit 0
#define CR_OPTER          (u16)0x0020  //Bit 5
#define CR_STRT           (u16)0x0040  //Bit 6
#define CR_LOCK           (u16)0x0080  //Bit 7
#define CR_OPTWRE         (u16)0x0200  //Bit 9
#define CR_OPTPG          (u16)0x0010  //Bit 4
#define OBR_RDPRT         (u16)0x0002  //Bit 1

#define USART_SR_RXNE     (u16)0x0020  //Bit 5
#define USART_CR1_RXNEIE  (u16)0x0020  //Bit 5
#define USART_SR_TXE      (u16)0x0080  //Bit 7
#define USART_CR1_TXEIE   (u16)0x0080  //Bit 7
#define USART_CR1_TCIE    (u16)0x0040  //Bit 6
#define USART_SR_TC       (u16)0x0040  //Bit 6
#define USART_SR_PE       (u16)0x0001  //Bit 0 校验错误标志
#define USART_SR_FE       (u16)0x0002  //Bit 1 帧错误标志
#define USART_SR_NE       (u16)0x0004  //Bit 2 噪声错误标志
#define ASCII             (u8 )0x0001  //RtuOrAscii
#define RTU               (u8 )0x0000  //RtuOrAscii
#define TIM2_SR_UIF       (u16)0x0001

/*BitFlag**********************************************************************/
/*****共64位，前32位留给引导区用，后32位应用区用*****/
//第0位起 
//#define BITBANG(x)  *(vu8*)(0x22000000+4*x)
//#define AngOrDig     BITBANG(0)  //analog or digital开关量模拟量模块标志位,H,Dig,L:Ang
#define AngOrDig     *(vu8*)(0x22000000+4*0)  //analog or digital开关量模拟量模块标志位,H,Dig,L:Ang
#define ToBootApp    *(vu8*)(0x22000000+4*1)  //To boot or application 应用区引导区选择位
#define FlagParaIRQ  *(vu8*)(0x22000000+4*2)  //并口IRQ事件标志位
#define CRSaved      *(vu8*)(0x22000000+4*3)  //CR saved 掉电数据保存完成标志位
#define ModReFinsh   *(vu8*)(0x22000000+4*4)  //modbus receive finiah ;modbus接收数据完毕标志位
#define RtuOrAscii   *(vu8*)(0x22000000+4*5)  //rtu or ascii通信格式RTU,ASCii模式状态位
#define ProtocolChg  *(vu8*)(0x22000000+4*6)  //protocol changed通信格式protocol变化标志位
#define ParaSelReTx  *(vu8*)(0x22000000+4*7)  //parallel receive transmit select并口发送接收标志位
#define ParaUpdRe    *(vu8*)(0x22000000+4*8)  //parallel update data receive finish 并口升级帧接收完成标志位
#define ReLinkLedSig *(vu8*)(0x22000000+4*9)  //重闪link灯闪烁标志，1：3次闪烁后继续3次闪烁，0：3次后停止闪烁
#define LinkON       *(vu8*)(0x22000000+4*10) //
#define ExPower_t    *(vu8*)(0x22000000+4*11) //状态翻转turn记录1变，0不变
#define ExPower_s    *(vu8*)(0x22000000+4*12) //当前滤波状态status 0：有外部电源，1：无外部电源
 #define VALID_EXPOWER 0   //有外部电源
 #define NO_EXPOWER    1   //无外部电源
#define ExPower_last *(vu8*)(0x22000000+4*13) //上一次扫描状态
#define ParaReOk     *(vu8*)(0x22000000+4*14)
#define ModbusReOk   *(vu8*)(0x22000000+4*15)
#define PwrDN_t      *(vu8*)(0x22000000+4*16) //状态翻转turn记录1变，0不变
#define PwrDN_s      *(vu8*)(0x22000000+4*17) //当前滤波状态status 0：有外部电源，1：无外部电源
#define PwrDN_last   *(vu8*)(0x22000000+4*18) //上一次扫描状态
#define PowerSel     *(vu8*)(0x22000000+4*19) //模块电源选择：0:并口供电，1：端子上接24v供电
 #define SEL_EXTERNAL_POWER 1
 #define SEL_INTERNAL_POWER 0
#define bPwrDNErr    *(vu8*)(0x22000000+4*20) //PwrDN管脚标志，0正常，1异常。

//第32位起 见config_***.h(不同应用区不一样)

/*引导区变量*******************************************************************/
typedef struct
{ 
  /*系统位变量（共64位，前32位引导区用，后32位应用区用）*/
  vu32 BitFlag[2];          //位标志
  
  /*Modbus变量*/
  vu16 ModErr;              //帧接收过程错误号
  vu16 TXcount;
  vu16 TxAscCount;
  vu16 RXcount;
  vu16 RXAscCount;
  vu16 TXDaLen;             //待发送数据的字节长度
  
  /*并口通信变量*/
  vu32 ParallelAdr;         //并口地址
  vu32 ParaRxCount;         //并口通信已接收数据字节数
  vu32 ParaTxCount;         //并口通信已发送数据字节数
  vu32 ParaTXLen;
  
  /*应用程序升级变量*/
  vu16 FrameNumber;         //并口升级，升级数据帧帧号
  vu16 FrameAmount[5];      //段帧数
  vu32 SectionAd[5];        //段地址
    
  /*时间变量（共16个，用7个保留9个）*/
   //时基0.2ms
  vu32 TimeExPower;         //外部电源检测滤波时间
  vu32 TimePwrDN;           //掉电时间计时值
  vu32 Time1ms;             //1ms时基
   //时基1ms
  vu32 TimeLinkLed;         //link指示灯闪烁计时，时基1ms
  vu32 TimeIRQ;             //并口中断信号时间
  vu32 TimeErrLed;          //错误灯时间
  vu32 FilterTime;          //启动等待ms
  vu32 UpgradeTimeout;      //升级超时
	vu32 TimeCommEnd;					//通讯结束后计时
  vu32 TimResver[7];        //保留的时间变量
  
  /*其他*/
  //程序维护若需要增加系统变量可添加到此处，共64字节，用2字节预留62字节，
  vu8  UpgradeFlag;         //升级标志
	vu8  ManuCommID;					//厂商通讯代码
	vu8  ResverByte[62];      //预留变量
  
  /*********数组********/
  vu8  SysConst[16];        //系统参数缓存
  vu16 CR[256]; 
  vu8  ComBuffer[270];      //串口数据帧
  vu8  ParaBuffer[200];     //并口数据帧缓存
  vu8  UpdBuffer[PageSize]; //升级页数据缓存 
}BootVariable;

