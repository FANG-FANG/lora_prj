 #include  	"boot.h"
 
 

	/*******************  Bit definition for FLASH_ACR register  ******************/
	#define  FLASH_LATENCY                   	0					/*!<LATENCY[2:0] bits (Latency) */
	#define  FLASH_HLFCYA                    	3					/*!<Flash Half Cycle Access Enable */
	#define  FLASH_PRFTBE                    	4					/*!<Prefetch Buffer Enable */
	#define  FLASH_PRFTBS                    	5					/*!<Prefetch Buffer Status */

	
	
/*******************************************************************************
* 函数声明
*******************************************************************************/
void Boot(void);
void b_LinkLedControl(void);
u32  FirmwareStatus(void);
void b_ReadCR(void);
void b_PwrDNSample(void);
void b_ReloadIWDG(void);
void RefreshSysConst(void);
void RCCInitialize(void); 
void IWDGInitialize(void);
void NVICInitialize(void);
void NVIC_SetIRQEnable(u8 irq_channel,u8 action,u8 priority);
void GPIOInitialize(void);
void USART1_Initialize(void);
void TIM2Initialize(void);
void TIM3Initialize(void);  
void EXTIInitialize(void);
void b_Wipe(u32 adress,u8 Page_Num);       
void ProgrammeWord(u32 adress,u16 data);
void b_Shift595Data(u8 Rang);
void b_CRSave(void);
void b_ProtocolSet(u8 pset);
u16 b_CRC16( vu8 *CRCdata,vu16 CRCLen);
u32 GetCRC32(u8 *P, u32 Len);
void b_ExPowerQuery(void);
void b_PwrDNQuery(void);
void b_EnRdWrProtection(unsigned char action);
void UpgradeQurey(void);
extern void Start(void);  
extern void USART2_INIT(void);//zcf

            
/*******************************************************************************
* 变量声明
*******************************************************************************/
BootVariable __attribute__((section("vari")))Bx;
const u32 *const __attribute__((at(0x08000200)))FuncAdress[8]=
{(u32*)IndexAdress,(u32*)Boot,(u32*)(BOOT_VERSION|(BV_DATE<<8)),};
const u32 DefaultSysConst[4]={CRAdress,0x2A020110,0xC71D1056,0xfe050000,};  
/*******************************************************************************
* 主函数main,CPU上电后程序入口。
* 入口参数：无
* 出口参数：无
*******************************************************************************/
int main(void)
{
  u32 i;
  Bx.TimeLinkLed=180;
	Bx.TimeIRQ=501;
  Bx.ParallelAdr=0x0;
  Bx.BitFlag[0]=0;
  Bx.BitFlag[1]=0;
  //IWDGInitialize();//开启看门狗
  RCCInitialize();
  GPIOInitialize();
  TIM2Initialize();
  TIM3Initialize();
  //NVICInitialize();              
  PwrDN_s=1;
  Bx.FilterTime=150;//150*0.2ms=30ms
	Bx.TimeErrLed=0;
//  while(PwrDN_s)
//	{	
//		b_PwrDNSample();//PwrDN状态检测
//		if(Bx.TimeErrLed>=200)
//		{
//			Bx.CR[15]=(Bx.CR[15]&0xff00)|5;//PwrDN异常
//			bPwrDNErr=1;//掉电保持部分硬件电路异常
//			break;
//		}
//	}
  Bx.TimeErrLed=0;
  RCCInitialize();
  GPIOInitialize();
  USART1_Initialize();
  EXTIInitialize();
  TIM2Initialize();
  TIM3Initialize();  
  //NVICInitialize();      
  #ifdef BOOT_WRITEPROTECT //添加读写保护
  if((FLASH->WRPR&0X7)||(FLASH->OBR&0X02)==0)b_EnRdWrProtection(0XAA);
  #endif
  i=FirmwareStatus();//检查固件的完整性
	USART2_INIT();
  b_ReadCR();      //读CR，设置串口通信格式
	if((Bx.CR[15]>>8) ==0x00 || (Bx.CR[15]>>8) ==0xFF)
		Bx.CR[15] = 0xB900;
	else Bx.CR[15] &=0xff00;//20170620	
  Bx.CR[15]=(Bx.CR[15]&0xff00)|i;    
  ParaReOk=1;
  ModbusReOk=1; 
//SystemCoreClockUpdate();	
  Boot();  
}

/********************************************************************************
* App返回Boot区入口程序
* 程序入口：
* 程序出口：
* 说明：四组Led灯轮流刷新
********************************************************************************/
void Boot (void)       
{
  SCB->VTOR=0X0;//设置中断向量表地址 TABLEBASE(29)=0=FLASH,OFFSET(28-7)=0X00  
  Bx.FilterTime=25;//掉电检测滤波时间5ms
  GPIOD->BRR=Vctr;//设置为外部电源供电
  if(AngOrDig==0)GPIOC->CRH=(GPIOC->CRH&0X0000FFFF)|0X88880000;//配置switch为输入。
	
	(*(Function)Start)();//跳至应用区
	
}
/*******************************************************************************
*  重装载看门狗（喂狗）
* 
*******************************************************************************/
void b_ReloadIWDG(void)
{
	#ifdef USED_IWDG
  IWDG->KR = IWDG_RELOAD;//喂狗
  #endif
}
/*******************************************************************************
*  Link灯指示控制
*  0.5s亮红灯（有通信时闪红灯）0.5s暗
*******************************************************************************/
void b_LinkLedControl(void)
{  
  //正常
	if((Bx.CR[15]&0xff)==0)
	{
  	LinkON=1;
		GPIOB->BSRR=LinkRed;//灭红色
		if(Bx.TimeLinkLed>=(6*BlinkTime))
		{
    	if(Bx.ParallelAdr==0)GPIOB->BSRR=LinkGreen;//地址未分配，link指示灯暗
			else GPIOB->BRR=LinkGreen;  //并口地址已经分配，亮绿色灯。
		}
	}
  else if((Bx.CR[15]&0xff)==2)
  {
    GPIOB->BSRR=LinkGreen;//灭绿色灯
    if(Bx.TimeErrLed<500)
    {
      LinkON=1;
      if(Bx.TimeLinkLed>=(6*BlinkTime))GPIOB->BRR=LinkRed;
    }
    else if(Bx.TimeErrLed<1000)
    {
      LinkON=0;
      GPIOB->BSRR=LinkRed;  
    }
    else Bx.TimeErrLed=0;
  }
  //错误：有需要返厂的错误
  else if((Bx.CR[15]&0xff)==3 || (Bx.CR[15]&0xff)>=5)
  {  LinkON=1;
    GPIOB->BSRR=LinkGreen;//灭绿色灯
    if(Bx.TimeLinkLed>=(6*BlinkTime))GPIOB->BRR=LinkRed; //未通信亮红灯,灭绿灯.
  }  
}
/*******************************************************************************
* 固件firmware完整性判断
* 判断固件的完整性  应用区完整:ToBootApp=1,cr15=0;应用区不完整:ToBootApp=0,cr15=2;
* 更新系统参数sysconst
*******************************************************************************/
u32 FirmwareStatus(void)
{
  u32 i,j,cr15=0;
  //读sysconst
  i=SysConstAdress;
  for(i=0;i<16;i+=4)*(u32*)(Bx.SysConst+i)=*(u32*)(SysConstAdress+i);  
  
  /*****判断固件的完整性*****/
  ToBootApp=1;//默认为0，判断为完整则改为1 


  /*****sysconst合法性验证*****/
  if(b_CRC16(Bx.SysConst,16))
  {
    if(ToBootApp)RefreshSysConst();  
    else//备份区和应用区的系统参数都错误，报“需返厂错误”（错误3），同时把模块ID设置为0xff
    {
      u16 *p;
      cr15=3;
      b_Wipe(SysConstAdress,1);      
      p=(u16*)DefaultSysConst;
      for(i=0;i<16;i+=2)
      {
        j=*(u16*)p++;  
        *(u16*)(Bx.SysConst+i)=j;
        ProgrammeWord(SysConstAdress+i,j);
      }
    }
  }
  else
  {
    //若应用区固件完整，则更新Sysconst
    if(ToBootApp)
    {  for(i=0;i<(16-2);i+=2)//检测引导区参数与应用区参数的是否相同
      {  if(*(u16*)(Bx.SysConst+i)!=*(u16*)(AppSysConstAdr+i)) 
        {RefreshSysConst();break;}
      }
    }	
		else if(ToBootApp==0) Bx.SysConst[7]=0x2a;//当应用区不完整时，显示版本号为1.0 20140903	
  }
  return cr15;
}
/*******************************************************************************
 * sysconst擦写更新,
 *入口参数：updated_sig：升级标志，为1则置SysConst_ram[15]为1。出口参数：无
*******************************************************************************/
void RefreshSysConst(void)
{
  u32 i,j;
  for(i=0;i<14;i+=2)*(u16*)(Bx.SysConst+i)=*(u16*)(AppSysConstAdr+i);//读取应用区中SysConst
  i=b_CRC16(Bx.SysConst,14);  //CRC校验并添加校验结果
  Bx.SysConst[14]=i>>8;//先高字节后低字节
  Bx.SysConst[15]=i;
  b_Wipe(SysConstAdress,1);//擦除，
  for(i=0;i<16;i+=2) //重写
  {
    j=*(u16*)(Bx.SysConst+i);
    ProgrammeWord((SysConstAdress+i),(u16)j);
  }    
}
/*******************************************************************************
* 函数名    ：读CR
* 描述      ：读256个CR
* 出口      ：无
*******************************************************************************/
void b_ReadCR(void)
{
  u16 *cr;
  u32  i;  
  i=*(u32*)Bx.SysConst;
  cr=(u16*)i;
  for(i=0;i<256;i++)Bx.CR[i]=*cr++;  
  Bx.CR[0]=(Bx.SysConst[7]<<8)+Bx.SysConst[8];
  //串口地址和通信格式合法性验证
  i=Bx.SysConst[5];//地址偏移
  if(Bx.CR[i]>254)Bx.CR[i]=1;//非法地址，置硬地址
  i=Bx.SysConst[6];//通信格式偏移
  if((Bx.CR[i]&0xFFF0)>0x60||(Bx.CR[i]&0x0F)>0x6)Bx.CR[i]=0x30;//非法通信格式，置默认ox30
  b_ProtocolSet(Bx.CR[i]);
}
/*******************************************************************************
*说明：清除led和IO输出
*入口参数：无
*出口参数：无          
*******************************************************************************/
void GPIOInitialize(void)  
{
  AFIO->MAPR&=~0X04; //USART1不重映射USART1_REMAP(2)=0
  AFIO->EXTICR[0]|=0X0100;  //配置PB2为外部中断EXT2输入线，
  AFIO->EXTICR[2]|=0X0100;  //设置PB10(ST)为外部中断EXT10输入线
  GPIOC->BRR=0X0FFF;
  GPIOB->BRR=0XF000;    
  GPIOA->CRH=(GPIOA->CRH&0xffff0fff)|0X00008000;
  if(GPIOA->IDR&ENIO)
  {
    AngOrDig=1;//模块为24-64点开关量模块
    //PA口配置
    GPIOA->CRL=0x44444444;  //PA0-7（并口BD0-7）
    GPIOA->BSRR=ENIO|RXD|TXD|DSPCS;   
    GPIOA->BRR=sp485_DRE;      
    GPIOA->CRH=0X444338B3;   //PA8(485-DRE);PA9(TXD);PA10(485-RXD);PA11(ENIO);PA12(DSPCS);
    
    //PB口配置
    GPIOB->BSRR=E_DI|eSEL|PwrDN|LinkRed|LinkGreen|E_ST|OutAddr|SDA|QST|SCLK;
    GPIOB->CRL=0X33344834;  //PB0(E_DI);PB1(e_SEL);PB2(PwrDN);PB5(595-SDA);PB6(595-SCLK);PB7(595-QST);
    GPIOB->CRH=0X33333433;  //PB8(LinkG);PB9(LinkR);PB10(E_ST);PB11(outAdd);//PB12-15(IOD12-15);

    //PC口配置
    GPIOC->CRL=0X33333333;
    GPIOC->BRR=IRQ;
    GPIOC->BSRR=InAdd|ExPWR|SWin;
    GPIOC->CRH=0X88383333;  //PC8-11(IO8-11);PC12(InAdd);PC13(E_IRQ);PC14(ExPWR);PC15(Swin);

    //PD口配置
    GPIOD->BRR=Vctr;
    GPIOD->CRL=0x44444344;  //PD2(Vctr)
  }
  else
  {  AngOrDig=0;//模块为模拟量模块 或16点模块  
  
    //PA口配置
    GPIOA->CRL=0x44444444;  //PA0-7（并口BD0-7）
    GPIOA->BSRR=ENIO|RXD|TXD|DSPCS;   
    GPIOA->BRR=sp485_DRE;      
    GPIOA->CRH=0X444388B3;   //PA8(485-DRE);PA9(TXD);PA10(485-RXD);PA11(ENIO);PA12(DSPCS);
    
    //PB口配置
    GPIOB->BSRR=E_DI|eSEL|PwrDN|LinkRed|LinkGreen|E_ST|OutAddr;
    GPIOB->BRR=0XF0E0;
    GPIOB->CRL=0X33344834;  //PB0(E_DI);PB1(e_SEL);PB2(PwrDN);PB5(IOD12 | 悬空));PB6(IOD13 | SCL(I2C);PB7(IOD14 | SDA(I2C));
    GPIOB->CRH=0X33333433;  //PB8(Link);PB9(Err);PB10(E_ST);PB11(outAdd);PB12-15(IOD8-11);
  
    //PC口配置
    GPIOC->BRR=IRQ;
    GPIOC->BSRR=InAdd|ExPWR|SWin;
    GPIOC->BRR=0X8FFF;
    GPIOC->CRL=0X33333333;  //PC0-7(IOD00-07 | LD00-07)
    GPIOC->CRH=0X38388838;  //PC8-11(SW0-3);PC12(InAdd);PC13(E_IRQ);PC14(ExPWR);PC15(IOD15 | SHcp(ADUM1411))  
    //PD口配置
    GPIOD->BRR=Vctr;
    GPIOD->CRL=0x44444344;  //PD2(Vctr)
  }
}

/*******************************************************************************
* 函数名    ：引导区读写保护设置
* 描述      ：根据入口参数不同对模块上写保护或者去写保护
* 入口参数  ：action：0x55去写保护，0xaa：上写保护
*******************************************************************************/
void b_EnRdWrProtection(unsigned char action)
{
  while(FLASH->SR&SR_BUSY);
  if(FLASH->CR&CR_LOCK)
  {
    FLASH->KEYR=0X45670123;
    FLASH->KEYR=0xCDEF89AB;
  }
  while(FLASH->SR&SR_BUSY);
  FLASH->OPTKEYR=0X45670123;
  FLASH->OPTKEYR=0xCDEF89AB;
  while(FLASH->SR&SR_BUSY);
  FLASH->CR |= CR_OPTER;
  FLASH->CR |= CR_STRT;
  while(FLASH->SR&SR_BUSY);
  FLASH->CR &= ~(CR_OPTER|CR_STRT);
  FLASH->CR |= CR_OPTPG; //SET OPTPG(4)
  if(action==0x55)OB->RDP=0X00A5;//去写保护
  else if(action==0xaa)//上写保护
  {
    OB->RDP=0X0000;
    OB->WRP0=0x00F8;
  }
  while(FLASH->SR&SR_BUSY);
  FLASH->CR&=~(CR_OPTPG|CR_OPTWRE);
  FLASH->CR|=CR_LOCK;
  //SCB->AIRCR = 0x05FA0000 | (u32)0x04;
}
/*******************************************************************************
*说明：外部中断初始化，开启外部中断10和14，外部中断10设置为下降沿触发，外部中断14设置为上升沿
*入口参数：无
*出口参数：无
*******************************************************************************/
void EXTIInitialize(void)
{  
  EXTI->IMR|=EXTI10;//开放外部中断EXTi10
  EXTI->RTSR|=EXTI10;//上升沿触发
  EXTI->PR|=EXTI10;//清除挂起位
  //开放外部中断EXTi2  ，上升沿触发，清除挂起位  
  EXTI->IMR|=EXTI2;  
  EXTI->RTSR|=EXTI2;
  EXTI->PR|=EXTI2;
}
/*******************************************************************************
* 函数名    ：独立看门狗初始化
* 描述      ：设置看门都复位时间为
*******************************************************************************/
void IWDGInitialize(void)
{
  #ifdef USED_IWDG  
  IWDG->KR = IWDG_WR_EN;
  /*设置200ms喂狗时间*/
  IWDG->PR = 1;//40k的8分频   0.2s
  IWDG->RLR =1000;//重转载值
  IWDG->KR = IWDG_ENABLE;
  #endif
}
/*******************************************************************************
说明：中断向量表配置，设置中断向量表在0X08000000、使能系统中有用到的中断
入口：无
出口：无
*******************************************************************************/
void NVICInitialize(void)
{
  SCB->VTOR=0X0; //设置中断向量表地址 TABLEBASE(29)=0=FLASH,OFFSET(28-7)=0X00  
  SCB->AIRCR|=0x300;//设置中断优先级4bit抢占优先级，4Bit Sub优先级。 
  NVIC_SetIRQEnable(EXTI2_IRQChannel,ENABLE,0x12);     //开外部中断2，掉电检测2中断
  NVIC_SetIRQEnable(TIM2_IRQChannel,ENABLE,0x23);      //开TIM2中断
  NVIC_SetIRQEnable(TIM3_IRQChannel,ENABLE,0x22);      //开TIM3中断  
}
/*******************************************************************************
* 函数名    ：中断使能失能设置
* 描述      ：中断使能失能设置
* 入口参数  ：irq_channle:中断通道，action：使能or失能,
              priority:优先级、、前四位抢断优先级，后四位亚优先级
*******************************************************************************/
void NVIC_SetIRQEnable(u8 irq_channel,u8 action,u8 priority)
{
  u32 tmp,i;
  if(action==DISABLE)
  { 
    NVIC->ICER[irq_channel>>5]=((u32)1)<<(irq_channel&0x1f);
  }
  else if(action == ENABLE)
  {
    NVIC->ISER[irq_channel>>5]=((u32)1)<<(irq_channel&0x1f);
  }
  i=(irq_channel&0x3)<<3;
  tmp=NVIC->IPR[irq_channel>>2]&(~(0xf<<i));
  NVIC->IPR[irq_channel>>2] = tmp|(priority<<i);//设置优先级
} 
/*******************************************************************************
说明：RCC配置，选择HSE做时钟源，系统时钟设置为PLL，开启外设时钟
入口：无
出口：无
*******************************************************************************/
void  RCCInitialize(void)
{
  //开 HSE(16) HSI(0)
	FLASH->ACR=(1<<FLASH_PRFTBE)|(2<<FLASH_LATENCY); //使能预取缓冲区,等待时钟:2
  RCC->CR|=0x10001;
  //关PLLON(24)           
  RCC->CR&=~0X1000000;      
  //9倍频：PLLMUL[3:0](21:18)=0111；HSE不分频PLLXTPRE(17)=0;HSE时钟作为PLL输入时钟PLLSRC(16)=1
  RCC->CFGR|=0X1D0000;      
  RCC->CFGR&=~0X0220000;  
  //开PLLON(24)
  RCC->CR|=0X1000000;        
  //等待PLLRDY(25)就绪            
  while((RCC->CR&0X002000000)==0);  
  //切换至PLL SW(1:0)=10
  RCC->CFGR|=0X02;
  //AHB(4:7)=0X0不分频        
  RCC->CFGR&=~0XF0;
  //APB1时钟2分频：PPRE1(10:8)=100 PCLK1=36M
  RCC->CFGR&=~0X300;
  RCC->CFGR|=0X0400;
  //APB2时钟不分频：PPRE2(13:11)=000 PCLK2=72M
  RCC->CFGR&=~0X03100; 
  //开启CRC32时钟CRCEN(6)
  RCC->AHBENR|=0X40;
  //开启 TIME2/TIM3时钟 TIME2EN(0)=1;TIME3EN(1)=1
  RCC->APB1ENR|=0X003;
  //开PA、PB、PC、AFIO、USART1口时钟 USART1(14)IOPD(5)IOPC(4)IOPB(3)IOPA{2}AFIO(0)
  RCC->APB2ENR|=0X403D;
}
/*******************************************************************************
*说明：USART1初始化
*入口参数：无
*出口参数：无
*******************************************************************************/
void USART1_Initialize(void)
{
  u32 temp;
  //enable UE(13)
  USART1->CR1|=0X2000;
  //M(12)=0;PCE(10)=0;PS(9)=0;  TE(3)=1;RE(2)=1;
  temp=USART1->CR1&(~0X160C);
  USART1->CR1=temp|0X000C;
  USART1->CR3&=0XFCFF;//USART1->CR3:CTSE(9)=0 Disable,RTSE(8)=0 Disable
  //开中断 RXNEIE(5)=1   关中断 TXEIE(7)=0，TXCIE(6)=0
  USART1->CR1|=0X020;          
  USART1->CR1&=~(0X00C0);
  USART1->CR1|=0X000C;//enable USART1 UE(13) TE(3),RE(2)
  USART1->SR&=~0X40;//清除TC(6) 
}
/*******************************************************************************
*说明：Time2 初始化
*入口参数：无
*出口参数：无
*******************************************************************************/
void TIM2Initialize(void)
 {
  //自动重装载预装载允许ARPE(7）=0；CMS(6:5)=00;向上计数DIR(4)=0；OPM(3)=0
   //URS(2)=0；UDIS(1)=1；CEN(0)=0;CKD(1:0)=00
  TIM2->CR1=0x004;
  TIM2->SR&=~0X01;//清除更新中断标志UIF(0)=0
  TIM2->PSC=19;//预分频器
  TIM2->ARR=0x1482;//自动重装载寄存器
  TIM2->DIER|=0X01;//使能中断  
  TIM2->CR1&=~0x6;
  TIM2->EGR|=0x1;//触发更新  
 }
/*******************************************************************************
*说明：定时器TIM3初始化,定时200us
*入口参数：无
*出口参数：无
*******************************************************************************/
void TIM3Initialize(void)
{
  //自动重装载预装载允许ARPE(7）=0；CMS(6:5)=00;向上计数DIR(4)=0；OPM(3)=0
  //URS(2)=0；UDIS(1)=1；CEN(0)=0;CKD(1:0)=00
  TIM3->CR1=0x005;
  TIM3->SR&=~0X01;//清除更新中断标志UIF(0)=0
  TIM3->CNT=0;//计算器当前值清零
  TIM3->PSC=71;//预分频器
  TIM3->ARR=199; //自动重装载寄存器
  TIM3->DIER|=0X01;//使能中断
}
/*******************************************************************************
外部电源状态扫描
入口参数：无
出口参数：PwrDN状态:0和非0 
*******************************************************************************/
void  b_ExPowerQuery(void)
{  
  u8 temp1;
  
  /***外部电源检测采样***/
  temp1 = (GPIOC->IDR&ExPWR)>>14; //读取当前ExPwr状态
  if(temp1^ExPower_last)ExPower_t=1;
  ExPower_last=temp1;
  
  /***定时20ms到，更新采样结果***/
  if(Bx.TimeExPower>=100)
  {  
    Bx.TimeExPower=0; //重新计时
    if(ExPower_t==0)  //计时时间内，管脚状态未改变才更新检测结果
      ExPower_s=ExPower_last;
    ExPower_t=0;    //清翻转位
  } 
}
/*******************************************************************************
掉电状态扫描
入口参数：无
出口参数：PwrDN状态:0和非0 
*******************************************************************************/
void  b_PwrDNQuery(void)
{
	if(bPwrDNErr==0 && PwrDN_s)
	{
		if(CRSaved==0)
		{
			NVIC->ICER[1]=0X000100;//关并口中断EXIT10-15 Enable(40);
			b_CRSave();
			CRSaved=1;//写1，表示已经保存数据；
			Boot();
		}
	}
	if(bPwrDNErr && PwrDN_s==0)//上电检测PwrDN异常，但检测到30ms低电平
	{
		bPwrDNErr=0;//PwrDN正常。
	}
}
/*******************************************************************************
掉电状态扫描
入口参数：无
出口参数：PwrDN状态:0和非0 
*******************************************************************************/
void b_PwrDNSample(void)
{
	u8 temp1,temp2;
  if(GPIOB->IDR&PwrDN)temp1=1;
  else temp1=0;
  temp2=PwrDN_last;
  if(temp1^temp2)PwrDN_t=1;
  PwrDN_last=temp1;
  //定时到点采样结果更新
  if(Bx.TimePwrDN>=Bx.FilterTime)
  {
    Bx.TimePwrDN=0;
    if(PwrDN_t==0)PwrDN_s=PwrDN_last;//更新外部电源检测脚滤波结果
    PwrDN_t=0;    //清翻转位
  } 
}
/*******************************************************************************
* 函数名    ：页擦除Wipe
* 入口参数  ：页首地址adress ，页数：Page_Num
* 出口参数  ：无
*******************************************************************************/
void b_Wipe(u32 adress,u8 Page_Num)
{  
  if(adress>=0x08000000&&adress<=0x08010000)
  {
    //开flash锁  LOCK(7)bit   
    if(FLASH->CR&CR_LOCK)
    {                                   
      FLASH->KEYR=0X45670123;
      FLASH->KEYR=0xCDEF89AB;
    }
    //page erase
    while(Page_Num--)
    {
      FLASH->SR|=SR_EOP;  //reset EOP(5)
      FLASH->CR&=~CR_PG;  //Reset the bit PG(0),Reset MER(2)Bit
      FLASH->CR|=0X02;   //set PER(1)bit
      FLASH->AR=adress;    //Page choice
      FLASH->CR|=CR_STRT;   //set STRT(6)bit
      adress+=PageSize;
      while(FLASH->SR&SR_BUSY); //等待 BSY(0)=0 EOP(5)=1;
    }
    FLASH->CR&=~0X06;    //ResetPER(1) MER(2)BIT
  }
}
/*******************************************************************************
* 函数名    ：编程一个字
* 描述      ：向flash内写入一个字
* 入口参数  ：adress,要编程的flash地址。data:要写入的字。
*******************************************************************************/
void ProgrammeWord(u32 adress,u16 data)
{  
  while(FLASH->SR & SR_BUSY);//check the BSY(0)
  FLASH->SR|=SR_EOP;//清除 EOP(5)位
  FLASH->CR|=CR_PG;//set the bit PG(0)
  *(u16*)adress=data;
  while(FLASH->SR&SR_BUSY); //等待 BSY(0)=0 
  FLASH->CR&=~CR_PG;//Reset the bit PG(0)
}

/*******************************************************************************
  *  CR区数据保存到flash内。当外部检测到掉电调用此程序，把SRAM内的数据保存到falsh内 。
  *  入口参数：无
  *  出口参数：无
*******************************************************************************/
void b_CRSave(void)
{  
  u16 i,*rom_adr,k=0;
  u32 adress;
  adress=*(u32*)Bx.SysConst;  
  rom_adr=(u16*)adress;
  //CR内容是否改变
  for(i=0;i<256;i++)
  {  
    if(Bx.CR[i]!=*rom_adr++)
    {
      k=1;
      break;
    }  
  }
  if(k)
  {
    b_Wipe(CRAdress,1);
    FLASH->SR|=SR_EOP;//清除 EOP(5)位
    FLASH->CR|=CR_PG; //set the bit PG(0)
    rom_adr=(u16*)adress;
    for(i=0;i<256;i++)
    {  *rom_adr++=Bx.CR[i];   }
    while(FLASH->SR&SR_BUSY); //等待 BSY(0)=0 
    FLASH->CR&=~CR_PG; //Reset the bit PG(0)
    FLASH->CR|=CR_LOCK; //上锁  
  }
}
/*******************************************************************************
CRC32校验计算，入口参数：*p：校验数据指针。Len：校验数据的总字节长度。
返回参数：CRCresult32：32位crc32计算的结果。
*******************************************************************************/
u32 GetCRC32(u8 *P, u32 Len)
{ //对指定位置的数据进行指定长度的CRC32计算,计算长度：Byte
  u8 i,j;
  u32 CRCresult32;
  i=(u8)Len &0x03; //取出不满4字节的余数,并转换成bit数
  Len>>=2;
  CRC->CR=CRC_RESET; //复位硬件CRC32单元
  if (Len>0) 
  {    for (;Len>0;Len--)
    {    
      if((u32)P==(AppAdress+0x130)) CRC->DR=__rbit(0x2d696148);
      else
      {CRC->DR=__rbit(*(u32*)P);} //硬件计算CRC32
       //CRC->DR=__rbit(*(u32*)P);
      P+=4; //调整指针
    }
    CRCresult32=__rbit(CRC->DR);
  }
  else CRCresult32=0xffffffff;
  for(;i>0;i--) 
  {  CRCresult32^=*P++; //按字节取出不满双字的数据 
    for(j=8;j>0;j--) 
    {  if((CRCresult32)&0x01) 
      {  CRCresult32>>=1;
        CRCresult32^=0xedb88320; //与多项式异或
      }
      else CRCresult32>>=1;
    }
  }
  return CRCresult32^0xffffffff; //返回CRC32
} 
/*******************************************************************************
* 函数名    ：外部中断2
* 描述      ：有边缘的时候计时清零
*******************************************************************************/ 
void EXTI2_IRQHandler(void)
{  EXTI->PR|=EXTI2;
  Bx.TimePwrDN=0;
}











const u8 b_table_h[]={                    // crc高位字节值表
    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41,  
    0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 
    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1,0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41,  
    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1,0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41,  
    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40,  
    0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1,0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40,  
    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40,  
    0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40,  
    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 
    0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41,  
    0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40,  
    0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1,0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 
    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40  };

const u8 b_table_l[]={                    //crc低位字节值表
    0x00, 0xc0, 0xc1, 0x01, 0xc3, 0x03, 0x02, 0xc2, 0xc6, 0x06,0x07, 0xc7, 0x05, 0xc5, 0xc4, 0x04, 0xcc, 0x0c, 0x0d, 0xcd,
    0x0f, 0xcf, 0xce, 0x0e, 0x0a, 0xca, 0xcb, 0x0b, 0xc9, 0x09,0x08, 0xc8, 0xd8, 0x18, 0x19, 0xd9, 0x1b, 0xdb, 0xda, 0x1a,
    0x1e, 0xde, 0xdf, 0x1f, 0xdd, 0x1d, 0x1c, 0xdc, 0x14, 0xd4,0xd5, 0x15, 0xd7, 0x17, 0x16, 0xd6, 0xd2, 0x12, 0x13, 0xd3,  
    0x11, 0xd1, 0xd0, 0x10, 0xf0, 0x30, 0x31, 0xf1, 0x33, 0xf3,0xf2, 0x32, 0x36, 0xf6, 0xf7, 0x37, 0xf5, 0x35, 0x34, 0xf4,
    0x3c, 0xfc, 0xfd, 0x3d, 0xff, 0x3f, 0x3e, 0xfe, 0xfa, 0x3a,0x3b, 0xfb, 0x39, 0xf9, 0xf8, 0x38, 0x28, 0xe8, 0xe9, 0x29,  
    0xeb, 0x2b, 0x2a, 0xea, 0xee, 0x2e, 0x2f, 0xef, 0x2d, 0xed,0xec, 0x2c, 0xe4, 0x24, 0x25, 0xe5, 0x27, 0xe7, 0xe6, 0x26, 
    0x22, 0xe2, 0xe3, 0x23, 0xe1, 0x21, 0x20, 0xe0, 0xa0, 0x60,0x61, 0xa1, 0x63, 0xa3, 0xa2, 0x62, 0x66, 0xa6, 0xa7, 0x67,  
    0xa5, 0x65, 0x64, 0xa4, 0x6c, 0xac, 0xad, 0x6d, 0xaf, 0x6f,0x6e, 0xae, 0xaa, 0x6a, 0x6b, 0xab, 0x69, 0xa9, 0xa8, 0x68,  
    0x78, 0xb8, 0xb9, 0x79, 0xbb, 0x7b, 0x7a, 0xba, 0xbe, 0x7e,0x7f, 0xbf, 0x7d, 0xbd, 0xbc, 0x7c, 0xb4, 0x74, 0x75, 0xb5,  
    0x77, 0xb7, 0xb6, 0x76, 0x72, 0xb2, 0xb3, 0x73, 0xb1, 0x71,0x70, 0xb0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,  
    0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9c, 0x5c,0x5d, 0x9d, 0x5f, 0x9f, 0x9e, 0x5e, 0x5a, 0x9a, 0x9b, 0x5b,  
    0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4b, 0x8b,0x8a, 0x4a, 0x4e, 0x8e, 0x8f, 0x4f, 0x8d, 0x4d, 0x4c, 0x8c,  
    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42,0x43, 0x83, 0x41, 0x81, 0x80, 0x40  };

/*******************************************************************************
* 函数名    ：CRC16校验
* 描述      ：查表法CRC16校验
* 入口参数  ：*CRCdata数组指针，CRCLen：校验长度
* 出口参数  ：校验结果16bit，
*******************************************************************************/
u16 b_CRC16( vu8 *CRCdata,vu16 CRCLen)
{
  u8 CRCHi=0xff;
  u8 CRCLo=0xff;
  u8 CRCIndex;
  while(CRCLen--)
  {
    CRCIndex=CRCHi^*CRCdata++;
    CRCHi=CRCLo^b_table_h[CRCIndex];
    CRCLo=b_table_l[CRCIndex];
  }
  return (CRCHi<<8|CRCLo);
}

/*******************************************************************************
* 函数名    ：通讯格式设置
* 描述      ：设置串口通信的字符格式，波特率，结束延时，
* 入口参数  ：pset：通讯格式
*******************************************************************************/

const u16 b_Baudrate[7]={0x7530,0x3A98,0x1D4C,0x0EA6,0x753,0X04E2,0x0271};

//一个帧接收完成后延时3.5个字符时长。
const u16 b_FrameDealy[8]={1604,802,401,200,100,67,33,33};  


void b_ProtocolSet(u8 pset)
{  
  u32 i,j,m=0,n=0;
  i=pset&0x0f;//低半字节：帧字符格式
  j=pset>>4;//高半字节：波特率
  if(j<0x7&&i<0x7)
  {    
    if(i<3||i==6)RtuOrAscii = RTU;
    else  RtuOrAscii = ASCII;
    USART1->BRR = b_Baudrate[j];
		USART2->BRR = b_Baudrate[j];//zcf
    TIM2->ARR = b_FrameDealy[j];//帧结束3.5字节定时时间设置
    TIM2->PSC=719;
    TIM2->CR1&=~0x6;
    TIM2->EGR|=0x1;//触发更新各种寄存器

    //字长M(12) 奇偶校验pce(10)ps(9)停止位长度stop(12,13),CPOL(10)=1;
    switch(i)
    { 
      case 0://RTU  N,8,2
             n=0X2000;break;
      case 1://RTU  E,8,1
             m=0X1400;break;
      case 2://RTU  O,8,1
             m=0X1600;break;
      case 3://ASCII  N,7,2
                      break;
      case 4://ASCII  E,7,1
             m=0X0400;break;
      case 5://ASCII  O,7,1          
             m=0X0600;break;
      case 6://RTU N,8,1  
                      break;
      default:
                      break;
    }
    USART1->CR1=(~0X1600&USART1->CR1)|m;
    USART1->CR2=(~0X3000&USART1->CR2)|n;
		//zcf:
		USART2->CR1=(~0X1600&USART2->CR1)|m;
    USART2->CR2=(~0X3000&USART2->CR2)|n;
  }
}

/*******************************************************************************
* 函数名    ：TIM3事件中断
* 描述      ：为系统提供200us时基
*******************************************************************************/
//void TIM3_IRQHandler(void)
//{  
//  u32 i;
//  TIM3->SR&=~0X01;
//  Bx.TimeExPower++;
//  Bx.TimePwrDN++;
//  Bx.Time1ms++;    
//  if(Bx.Time1ms>=5)
//  {  
//    Bx.Time1ms=0;
//    if(Bx.TimeErrLed<1000)Bx.TimeErrLed++;
//    
//    /*IRQ周期500ms宽度1ms脉冲控制*/
//    if(Bx.TimeIRQ<=500)
//    { 
//      if(Bx.TimeIRQ == 0)GPIOC->BSRR=IRQ;
//      else if(Bx.TimeIRQ >= 1)GPIOC->BRR=IRQ;
//      Bx.TimeIRQ++;
//    }
//    else GPIOC->BRR=IRQ;

//    /*Link灯闪烁控制*/
//    i=Bx.TimeLinkLed;
//    if(i<(6*BlinkTime))
//    {
//      if(i==0)ReLinkLedSig=0;//清除重闪标志
//      if((i%BlinkTime)==0&&LinkON)
//      {
//        u32 j=0;
//        if((Bx.CR[15]&0xff)==2)j=LinkRed;
//        else if((Bx.CR[15]&0xff)==0)j=LinkGreen;         
//        else if((Bx.CR[15]&0xff)==4)j=LinkGreen|LinkRed;
//        else if((Bx.CR[15]&0xff)==3 || (Bx.CR[15]&0xff)>=5)j=LinkRed;
//        if(GPIOB->ODR&j)GPIOB->BRR=j;
//        else  GPIOB->BSRR=j;
//      }
//      i++;
//      //本次闪烁完成且需要重闪，则再次启动闪烁
//      if(i==(6*BlinkTime)&&ReLinkLedSig)i=0;
//      Bx.TimeLinkLed=i;
//    }

//    /*升级超时计时*/
//    if(Bx.UpgradeFlag && Bx.UpgradeTimeout < 5000)//在升级且未超时
//    { Bx.UpgradeTimeout++;}
//  }  
//}

