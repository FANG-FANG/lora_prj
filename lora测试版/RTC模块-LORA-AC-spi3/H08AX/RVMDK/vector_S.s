                             

                AREA    VECTOR2, DATA, READONLY
			;	IMPORT 	|Image$$LR_IROM2$$Lenght|
				IMPORT	__initial_sp
				IMPORT	Reset_Handler
				IMPORT  NMI_Handler                ; NMI Handler
                IMPORT  HardFault_Handler          ; Hard Fault Handler
                IMPORT  MemManage_Handler          ; MPU Fault Handler
                IMPORT  BusFault_Handler           ; Bus Fault Handler
                IMPORT  UsageFault_Handler         ; Usage Fault Handler
                IMPORT  SVC_Handler                ; SVCall Handler
                IMPORT  DebugMon_Handler           ; Debug Monitor Handler
                IMPORT  PendSV_Handler             ; PendSV Handler
                IMPORT  SysTick_Handler            ; SysTick Handler
				IMPORT  WWDG_IRQHandler            [WEAK]
                IMPORT  PVD_IRQHandler             [WEAK]
                IMPORT  TAMPER_IRQHandler          [WEAK]
                IMPORT  RTC_IRQHandler             [WEAK]
                IMPORT  FLASH_IRQHandler           [WEAK]
                IMPORT  RCC_IRQHandler             [WEAK]
                IMPORT  EXTI0_IRQHandler           [WEAK]
                IMPORT  EXTI1_IRQHandler           [WEAK]
                IMPORT  A_EXTI2_IRQHandler           [WEAK]
                IMPORT  EXTI3_IRQHandler           [WEAK]
                IMPORT  EXTI4_IRQHandler           [WEAK]
                IMPORT  DMA1_Channel1_IRQHandler   [WEAK]
                IMPORT  DMA1_Channel2_IRQHandler   [WEAK]
                IMPORT  DMA1_Channel3_IRQHandler   [WEAK]
                IMPORT  DMA1_Channel4_IRQHandler   [WEAK]
                IMPORT  DMA1_Channel5_IRQHandler   [WEAK]
                IMPORT  DMA1_Channel6_IRQHandler   [WEAK]
                IMPORT  DMA1_Channel7_IRQHandler   [WEAK]
                IMPORT  ADC1_2_IRQHandler          [WEAK]
                IMPORT  USB_HP_CAN1_TX_IRQHandler  [WEAK]
                IMPORT  USB_LP_CAN1_RX0_IRQHandler [WEAK]
                IMPORT  CAN1_RX1_IRQHandler        [WEAK]
                IMPORT  CAN1_SCE_IRQHandler        [WEAK]
                IMPORT  EXTI9_5_IRQHandler         [WEAK]
                IMPORT  TIM1_BRK_IRQHandler        [WEAK]
                IMPORT  TIM1_UP_IRQHandler         [WEAK]
                IMPORT  TIM1_TRG_COM_IRQHandler    [WEAK]
                IMPORT  TIM1_CC_IRQHandler         [WEAK]
                IMPORT  A_TIM2_IRQHandler          [WEAK]
                IMPORT  A_TIM3_IRQHandler            [WEAK]
                IMPORT  TIM4_IRQHandler            [WEAK]
                IMPORT  I2C1_EV_IRQHandler         [WEAK]
                IMPORT  I2C1_ER_IRQHandler         [WEAK]
                IMPORT  I2C2_EV_IRQHandler         [WEAK]
                IMPORT  I2C2_ER_IRQHandler         [WEAK]
                IMPORT  SPI1_IRQHandler            [WEAK]
                IMPORT  SPI2_IRQHandler            [WEAK]
                IMPORT  A_USART1_IRQHandler        [WEAK]
                IMPORT  USART2_IRQHandler      	   [WEAK]
                IMPORT  USART3_IRQHandler          [WEAK]
                IMPORT  A_EXTI15_10_IRQHandler     [WEAK]
                IMPORT  RTCAlarm_IRQHandler        [WEAK]
                IMPORT  USBWakeUp_IRQHandler       [WEAK]
                IMPORT  TIM8_BRK_IRQHandler        [WEAK]
                IMPORT  TIM8_UP_IRQHandler         [WEAK]
                IMPORT  TIM8_TRG_COM_IRQHandler    [WEAK]
                IMPORT  TIM8_CC_IRQHandler         [WEAK]
                IMPORT  ADC3_IRQHandler            [WEAK]
                IMPORT  FSMC_IRQHandler            [WEAK]
                IMPORT  SDIO_IRQHandler            [WEAK]
                IMPORT  TIM5_IRQHandler            [WEAK]
                IMPORT  SPI3_IRQHandler            [WEAK]
                IMPORT  UART4_IRQHandler           [WEAK]
                IMPORT  UART5_IRQHandler           [WEAK]
                IMPORT  TIM6_IRQHandler            [WEAK]
                IMPORT  TIM7_IRQHandler            [WEAK]
                IMPORT  DMA2_Channel1_IRQHandler   [WEAK]
                IMPORT  DMA2_Channel2_IRQHandler   [WEAK]
                IMPORT  DMA2_Channel3_IRQHandler   [WEAK]
                IMPORT  DMA2_Channel4_5_IRQHandler [WEAK]

                EXPORT  __Vectors2
                EXPORT  __Vectors2_End
               	EXPORT  __Vectors2_Size

__Vectors2      ;DCD		|Image$$LR_IROM2$$Lenght|
				DCD     __initial_sp               ; Top of Stack
                DCD     Reset_Handler              ; Reset Handler
                DCD     NMI_Handler                ; NMI Handler
                DCD     HardFault_Handler          ; Hard Fault Handler
                DCD     MemManage_Handler          ; MPU Fault Handler
                DCD     BusFault_Handler           ; Bus Fault Handler
                DCD     UsageFault_Handler         ; Usage Fault Handler
                DCD     0                          ; Reserved
                DCD     0                          ; Reserved
                DCD     0                          ; Reserved
                DCD     0                          ; Reserved
                DCD     SVC_Handler                ; SVCall Handler
                DCD     DebugMon_Handler           ; Debug Monitor Handler
                DCD     0                          ; Reserved
                DCD     PendSV_Handler             ; PendSV Handler
                DCD     SysTick_Handler            ; SysTick Handler

                ; External Interrupts
                DCD     WWDG_IRQHandler            ; Window Watchdog
                DCD     PVD_IRQHandler             ; PVD through EXTI Line detect
                DCD     TAMPER_IRQHandler          ; Tamper
                DCD     RTC_IRQHandler             ; RTC
                DCD     FLASH_IRQHandler           ; Flash
                DCD     RCC_IRQHandler             ; RCC
                DCD     EXTI0_IRQHandler           ; EXTI Line 0
                DCD     EXTI1_IRQHandler           ; EXTI Line 1
                DCD     A_EXTI2_IRQHandler           ; EXTI Line 2
                DCD     EXTI3_IRQHandler           ; EXTI Line 3
                DCD     EXTI4_IRQHandler           ; EXTI Line 4
                DCD     DMA1_Channel1_IRQHandler   ; DMA1 Channel 1
                DCD     DMA1_Channel2_IRQHandler   ; DMA1 Channel 2
                DCD     DMA1_Channel3_IRQHandler   ; DMA1 Channel 3
                DCD     DMA1_Channel4_IRQHandler   ; DMA1 Channel 4
                DCD     DMA1_Channel5_IRQHandler   ; DMA1 Channel 5
                DCD     DMA1_Channel6_IRQHandler   ; DMA1 Channel 6
                DCD     DMA1_Channel7_IRQHandler   ; DMA1 Channel 7
                DCD     ADC1_2_IRQHandler          ; ADC1 & ADC2
                DCD     USB_HP_CAN1_TX_IRQHandler  ; USB High Priority or CAN1 TX
                DCD     USB_LP_CAN1_RX0_IRQHandler ; USB Low  Priority or CAN1 RX0
                DCD     CAN1_RX1_IRQHandler        ; CAN1 RX1
                DCD     CAN1_SCE_IRQHandler        ; CAN1 SCE
                DCD     EXTI9_5_IRQHandler         ; EXTI Line 9..5
                DCD     TIM1_BRK_IRQHandler        ; TIM1 Break
                DCD     TIM1_UP_IRQHandler         ; TIM1 Update
                DCD     TIM1_TRG_COM_IRQHandler    ; TIM1 Trigger and Commutation
                DCD     TIM1_CC_IRQHandler         ; TIM1 Capture Compare
                DCD     A_TIM2_IRQHandler            ; TIM2
                DCD     A_TIM3_IRQHandler            ; TIM3
                DCD     TIM4_IRQHandler            ; TIM4
                DCD     I2C1_EV_IRQHandler         ; I2C1 Event
                DCD     I2C1_ER_IRQHandler         ; I2C1 Error
                DCD     I2C2_EV_IRQHandler         ; I2C2 Event
                DCD     I2C2_ER_IRQHandler         ; I2C2 Error
                DCD     SPI1_IRQHandler            ; SPI1
                DCD     SPI2_IRQHandler            ; SPI2
                DCD     A_USART1_IRQHandler          ; USART1
                DCD     USART2_IRQHandler          ; USART2
                DCD     USART3_IRQHandler          ; USART3
                DCD     A_EXTI15_10_IRQHandler       ; EXTI Line 15..10
                DCD     RTCAlarm_IRQHandler        ; RTC Alarm through EXTI Line
                DCD     USBWakeUp_IRQHandler       ; USB Wakeup from suspend
                DCD     TIM8_BRK_IRQHandler        ; TIM8 Break
                DCD     TIM8_UP_IRQHandler         ; TIM8 Update
                DCD     TIM8_TRG_COM_IRQHandler    ; TIM8 Trigger and Commutation
                DCD     TIM8_CC_IRQHandler         ; TIM8 Capture Compare
                DCD     ADC3_IRQHandler            ; ADC3
                DCD     FSMC_IRQHandler            ; FSMC
                DCD     SDIO_IRQHandler            ; SDIO
                DCD     TIM5_IRQHandler            ; TIM5
                DCD     SPI3_IRQHandler            ; SPI3
                DCD     UART4_IRQHandler           ; UART4
                DCD     UART5_IRQHandler           ; UART5
                DCD     TIM6_IRQHandler            ; TIM6
                DCD     TIM7_IRQHandler            ; TIM7
                DCD     DMA2_Channel1_IRQHandler   ; DMA2 Channel1
                DCD     DMA2_Channel2_IRQHandler   ; DMA2 Channel2
                DCD     DMA2_Channel3_IRQHandler   ; DMA2 Channel3
                DCD     DMA2_Channel4_5_IRQHandler ; DMA2 Channel4 & Channel5
__Vectors2_End

__Vectors2_Size EQU  __Vectors2_End - __Vectors2
				END
