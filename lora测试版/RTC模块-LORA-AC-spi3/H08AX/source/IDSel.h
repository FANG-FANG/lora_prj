#include "stm32f10x_type.h"

/*********************产品型号选择*****************************************************/

//  #define H04TC	//ID：157	CR数：80	
//	#define H08TC	//ID：158	CR数：80  	
//	#define H04RC	//ID：159	CR数：80	
//	#define H08RC	//ID：160	CR数：80
	#define A04TC	//ID:167	CR数：80
//	#define A08TC	//ID:168	CR数：80
//	#define A04RC	//ID:169	CR数：80

																		
/*调试选项*********************************************************************/
//#define SHOW_POWERSTATUS    //开启CR内电源控制和外部电源指示功能。
//#define ERR_RECORDE         //开启AI错误记录
//#define SAVE_CS5530DATA     //显示5530读到的数据，保存在Bx.CR[OFFLINE+3+i]中。
//#define SAVE_CALIB_RESULT     //显示自校准通道的校准后码值。

//#define ShowHardwareCode		//在CR180开始显示硬件码值		
//#define ShowLinearCode			//在CR210开始显示线性校准码值

/***************************************************************************************
生成Hex文件前需要确认的工作：
1，优化等级optimization是否已经为3级（最高）
2，是否已经启用看门狗和写保护，即取消注释USED_IWDG和BOOT_WRITEPROTECT(protect.h中)
3，是否已注释调试选项SHOW_POWERSTATUS ，ERR_RECORDE SHOW_CS5530DATA
4，a_constant中不能添加其他常数变量,
***************************************************************************************/
#if defined(S04XA) || defined(S04AI) || defined(S04AO) || defined(S08XA) || defined(S08AI) || defined(S08AO)
#define S04_08XA
#endif

#if defined(H04RC) || defined(H08RC) || defined(H04TC) || defined(H08TC)
#define H04_08RCTC
#endif

#if defined(A04TC) || defined(A08TC) || defined(A04RC)
#define A04_08RCTC
#endif

#ifdef S04AI	  
#define CpuVersion  0x2b
#define	ProductID	  151
#define CR_AMOUNT   48  
#define	AI_AMOUNT	  4
#define	AQ_AMOUNT	  0
#define ShiftNum		8
#define AICHOffset	0
#define AICSOffset	3
#define AIRGOffset	4
#define AQRGOffset	6  //未用
#define AQCSOffset	6  //未用
#endif

#ifdef S04AO	  
#define CpuVersion  0x2b
#define	ProductID	  152
#define CR_AMOUNT   48  
#define	AI_AMOUNT	  0
#define	AQ_AMOUNT	  4
#define ShiftNum		8
#define AQRGOffset	0
#define AQCSOffset	4  
#define AICHOffset	15
#define AICSOffset	15
#define AIRGOffset	15
#endif

#ifdef S04XA	  
#define CpuVersion  0x2b
#define	ProductID	  153
#define CR_AMOUNT   48  
#define	AI_AMOUNT	  2
#define	AQ_AMOUNT	  2
#define ShiftNum		8
#define AQRGOffset	0
#define AICHOffset	3
#define AQCSOffset	2
#define AICSOffset	4
#define AIRGOffset	5
#endif

#ifdef S08AI	  
#define CpuVersion  0x2b
#define	ProductID	  154
#define CR_AMOUNT   80
#define	AI_AMOUNT	  8
#define	AQ_AMOUNT	  0
#define ShiftNum		8
#define AICSOffset	0
#define AIRGOffset	1 
#define AICHOffset	3
#define AQRGOffset	15
#define AQCSOffset	15
#endif

#ifdef S08AO	  
#define CpuVersion  0x2b
#define	ProductID	  155
#define CR_AMOUNT   80
#define	AI_AMOUNT	  0
#define	AQ_AMOUNT	  8
#define ShiftNum		16
#define AQCSOffset	0
#define AQRGOffset	4
#define AICHOffset	15
#define AICSOffset	15
#define AIRGOffset	15
#endif

#ifdef S08XA	  
#define CpuVersion  0x2b
#define	ProductID	  156
#define CR_AMOUNT   80
#define	AI_AMOUNT	  4
#define	AQ_AMOUNT	  4
#define ShiftNum		16
#define AQCSOffset	0
#define AICSOffset	2
#define AQRGOffset	3
#define AIRGOffset	7
#define AICHOffset	9
#endif

#ifdef H04TC	  
#define CpuVersion  0x35
#define	ProductID	  157
#define CR_AMOUNT   48
#define	AI_AMOUNT	  4
#define	AQ_AMOUNT	  0
#define CH_AMOUNT   6
#define PCODE       50
#endif

#ifdef H08TC	  
#define CpuVersion  0x35
#define	ProductID	  158
#define CR_AMOUNT   80
#define	AI_AMOUNT	  8
#define	AQ_AMOUNT	  0
#define PCODE       80
#endif

#ifdef H04RC	  
#define CpuVersion  0x35
#define	ProductID	  159
#define CR_AMOUNT   48
#define	AI_AMOUNT	  4
#define	AQ_AMOUNT	  0
#define CH_AMOUNT   8
#define PCODE       50
#endif

#ifdef H08RC	  
#define CpuVersion  0x2b
#define	ProductID	  160
#define CR_AMOUNT   80
#define	AI_AMOUNT	  8
#define	AQ_AMOUNT	  0
#define CH_AMOUNT   16//通道数
#define PCODE       80
#endif

#ifdef  H04_08RCTC
#define ShiftNum		10
#define AICHOffset	0
#define AICSOffset	3
#define AIRGOffset	4 
#define INHOffset	  7
#define CDOffset_1  7
#define CDOffset_2	6
#define CDOffset_3	8
#define PDOffset 		9
#define SCR_CH1			0
#define SCR_CH2			1
#define SCR_CH3			2
#define AQRGOffset	15
#define AQCSOffset	15
#endif

#ifdef A04TC
#define CpuVersion  0x35
#define	ProductID	  167
#define CR_AMOUNT   80
#define	AI_AMOUNT	  4
#define	AQ_AMOUNT	  0
#define CH_AMOUNT   6
#define PCODE       50
#endif

#ifdef A08TC
#define CpuVersion  0x35
#define	ProductID	  168
#define CR_AMOUNT   80
#define	AI_AMOUNT	  8
#define	AQ_AMOUNT	  0
#define PCODE       80
#endif

#ifdef A04RC
#define CpuVersion  0x35
#define	ProductID	  169
#define CR_AMOUNT   80   //48
#define	AI_AMOUNT	  4
#define	AQ_AMOUNT	  0
#define CH_AMOUNT   8
#define PCODE       50
#endif

#ifdef  A04_08RCTC
#define ShiftNum		8
#define AICHOffset	0
#define AICSOffset	3
#define AIRGOffset	4 
#define INHOffset	  7
#define CDOffset_1  7
#define CDOffset_2	6
#define CDOffset_3	8
#define PDOffset 		9
#define SCR_CH1			0
#define SCR_CH2			1
#define SCR_CH3			2
#define AQRGOffset	15
#define AQCSOffset	15
#endif

/*********************CR偏移定义*******************************************************/
/*AI CR*/
#define AI_RANG_CR  (16+AI_AMOUNT)            //AI量程起始CR
#define AI_ENGCL_CR (16+AI_AMOUNT*2)          //AI工程量(GongChengLiang)使能CR
#define AI_DGCL_CR  (16+AI_AMOUNT*2+1)        //AI工程量down下限起始CR.
#define AI_UGCL_CR  (16+AI_AMOUNT*3+1)        //AI工程量UP上限起始CR.
#define AI_SMPL_CR  (16+AI_AMOUNT*4+1)        //AI采样次数起始CR
#define AI_0_CR     (16+AI_AMOUNT*5+1)
#define AI_OFFLINE  (16+AI_AMOUNT*6+1)        //Ai断线检测的CR号
/*AQ CR*/
#if AI_AMOUNT>0
  #define AQ_OFFSET   (16+AI_AMOUNT*6+2)        //AQ在CR中的偏移
#else
  #define AQ_OFFSET   16                        //AQ在CR中的偏移
#endif
#define AQ_CR       (AQ_OFFSET)
#define AQ_RANG_CR  (AQ_OFFSET+AQ_AMOUNT)      //AQ量程起始CR
#define AQ_ENGCL_CR (AQ_OFFSET+AQ_AMOUNT*2)    //AQ工程量(GongChengLiang)使能CR
#define AQ_DGCL_CR  (AQ_OFFSET+AQ_AMOUNT*2+1)  //AQ工程量down下限起始CR.
#define AQ_UGCL_CR  (AQ_OFFSET+AQ_AMOUNT*3+1)  //AQ工程量UP上限起始CR.
#define AQ_LED      (AQ_OFFSET+AQ_AMOUNT*4+1)  //AQ指示灯状态CR

