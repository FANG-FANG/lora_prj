#include "config_08AX.h"

/*******************************************************************************
* 函数声明
*******************************************************************************/
extern u8 Write_CR(u16 CRNum,u16 Value);
void Parallel_initial(void);
void ParallelQuery(void);
u8 ParaFliter(void);
void ParaDispose(void);
void ParaReply(u32 length);
void CRSave(void);
void a_ParaCmd0(void);
void a_ParaCmd1(void);
void a_ParaCmd2(void);
void a_ParaCmd3(void);
extern void a_ParaCmd4(void);
extern void a_ParaCmd5(void);
void a_ParaCmd6(void);
extern void a_ParaCmd7(void);
void a_ParaCmd8(void);
extern void a_ParaCmd9(void); 
extern void PGROM(u32 adress,u16 value);
extern u8 decode(u8 code,u32 positon); 
extern void ProtocolSet(u8 pset);
void EnRdWrProtection(unsigned char action);
extern void Wipe(u32 adress,u8 Page_Num);
extern void WriteSelfCaliData(void);

/******************************************************************************
* 变量声明
******************************************************************************/
vu8 	TimeIOLed;
vu16	TimeAiRefresh=0;
vu16	TimeAqRefresh=0;
vu16	RTCTimeout=0;
vu16  RCDelay=0;
vu32	SCDelay;
vu8		SCState;

extern  vu16  TimeRead24c08;//读24c02计时，超时则放弃读 
extern 	BootVariable Bx;
extern vu16 CaliData[];
u32 *const a_ParaCmd[16]=
{	(u32*)a_ParaCmd0,(u32*)a_ParaCmd1,(u32*)a_ParaCmd2,(u32*)a_ParaCmd3,
	(u32*)a_ParaCmd4,(u32*)a_ParaCmd5,(u32*)a_ParaCmd6,(u32*)a_ParaCmd7,
	(u32*)a_ParaCmd2,(u32*)a_ParaCmd9,(u32*)a_ParaCmd2,(u32*)a_ParaCmd2,
	(u32*)a_ParaCmd2,(u32*)a_ParaCmd2,(u32*)a_ParaCmd2,(u32*)a_ParaCmd2,};

/*******************************************************************************
* 函数名    ：并行通信查询
* 描述      ：查询InAddr电平状态和通讯地址
*******************************************************************************/
void ParallelQuery(void)
{
	if((GPIOC->IDR&InAdd)==0 && Bx.ParallelAdr==0)
  { 
    FlagParaIRQ=1; 
    if(Bx.TimeIRQ>=500)Bx.TimeIRQ=0;  
  }
  else
  {
    FlagParaIRQ=0;
    GPIOC->BRR=IRQ;
  }
}
/*******************************************************************************
* 函数名    ：外部中断10和14中断函数
* 描述      ：并口中断
*******************************************************************************/
void A_EXTI15_10_IRQHandler(void)
{  
  if((EXTI->PR&EXTI10)==EXTI10)//中断10挂起;进入中断处理 
  {  
    u32 para_data;
    EXTI->PR|=EXTI10;//清除挂起位  

    /*首字节(命令字)***********************************************************/ 
    if((GPIOB->IDR&E_DI)==0)
    {  
      GPIOA->CRL=0x44444444;
      para_data=ParaFliter();
      if((para_data&0xf0)==0||(para_data&0xf0)==Bx.ParallelAdr//自身地址或广播或
      || ((para_data&0x0f)==7 && (GPIOC->IDR&InAdd)==0))  //分配地址且地址线为低
      {  
        GPIOB->BRR=eSEL;          //拉低使选通线有效
        Bx.ParaBuffer[0]=para_data;
      } 
      else   GPIOB->BSRR=eSEL;      //拉高选通线不接收其他数据
      Bx.ParaRxCount=1;
      ParaSelReTx = RECEIVE;  
    }  
    else      
    {  
      /*发送并行数据***********************************************************/
      if(ParaSelReTx == TRANSMIT)
      {  
        para_data=Bx.ParaBuffer[Bx.ParaTxCount++];
        GPIOA->BSRR=(para_data|(para_data<<16))^0x00ff0000;
        if(Bx.ParaTxCount>=Bx.ParaTXLen)
        {
          GPIOA->CRL=0x44444444;
          ParaSelReTx=RECEIVE;
          Bx.ParaRxCount=0;
          ParaReOk=1;
        }
      } 
      /*接收并行数据***********************************************************/
      else
      {
        para_data=ParaFliter();
        Bx.ParaBuffer[Bx.ParaRxCount]=para_data;
        if(Bx.ParaRxCount<199)Bx.ParaRxCount++;   //限制最多接收200个并行数据
        if(Bx.ParaRxCount == (Bx.ParaBuffer[1]+3))  
        {             
          /*自身地址且非广播地址 或 命令7且本模块in地址线为低，置位8个数据线*/
          if(((Bx.ParaBuffer[0]&0xF0)!=0 && (Bx.ParaBuffer[0]&0xF0)==Bx.ParallelAdr)
              ||((para_data&0x0f)==7 && (GPIOC->IDR&InAdd)==0))           
          { GPIOA->BSRR=0XFF;  //置位总线上的数据，清除接收的末字节。
            GPIOA->CRL=0X33333333;
          }  
//          GPIOA->BSRR=ENIO;  
          ParaDispose();//长度相同判断帧结束
//          GPIOA->BRR=ENIO;
        }
      }      
    } 
  }
}
/*******************************************************************************
* 函数名    ：并行数据滤波  Parallel filter 
* 描述      ：并行数据滤波，连续三次采样到同一数据才是有效数据
* 返回值    ：滤波结果
*******************************************************************************/
u8 ParaFliter(void)
{
  u16 para_data,temp,k;
  k=PFtimes;//连续次数
  para_data = GPIOA->IDR&0xff;
  while(k)
  {  
    temp=GPIOA->IDR&0xff;
    if(para_data == temp) k--;
    else //重新计数
    {
      k=PFtimes;
      para_data = temp;
    }
  }
  return para_data;
}
/*******************************************************************************
* 函数名    ：并口回复，计算添加校验结果，启动led灯闪烁
* 入口参数  ：length待发送数据字节长度(不包括校验字节)
* 出口参数  ：无
*******************************************************************************/
void ParaReply(u32 length)
{  	
  u32 i=0,j=0;
	ParaSelReTx=1;
	GPIOA->CRL=0x33333333;
	Bx.ParaTXLen=length+2;
	Bx.ParaTxCount=0;
	for(;i<length;i++)j+=Bx.ParaBuffer[i];   //计算校验和
	Bx.ParaBuffer[length]=j&0xff;	
	if(Bx.TimeLinkLed>=(6*BlinkTime))Bx.TimeLinkLed=0;
	else ReLinkLedSig=1;
}
/*******************************************************************************
* 函数名    ：解释并口接收到的数据
* 描述      ：比较校验结果，跳转到对应的命令
* 入口参数  ：无
*******************************************************************************/
void ParaDispose(void)
{	u32 i=0,j=0,k;
	//判断校验	
	k=Bx.ParaBuffer[1]+2;		  	
	for(;i<k;i++)j+=Bx.ParaBuffer[i];   //计算校验和
	if((j&0xff)==Bx.ParaBuffer[k])
	{(*(Function)(a_ParaCmd[Bx.ParaBuffer[0]&0x0f]))();}
  else  GPIOA->CRL=0X44444444; //校验错误释放数据总线（设置为浮空输入）
} 
/*****00 读输入  *****/
void a_ParaCmd0(void)
{
	if(Bx.ParaBuffer[1]==0&&(Bx.ParaBuffer[0]&0xf0)) //非广播地址则回复
	{	
		u16 i=AI_AMOUNT;        
    while(i--)*(u16*)(Bx.ParaBuffer+3+2*i)=Bx.CR[16+i];
		Bx.ParaBuffer[0]=(Bx.ParallelAdr<<4)|0x0A;
		Bx.ParaBuffer[1]=Bx.CR[0]&0XFF;//产品ID
		Bx.ParaBuffer[2]=AI_AMOUNT*2;//回复字节数
		ParaReply(3+AI_AMOUNT*2);
	}	
}
/*01 写输出*******************************************************************/
void a_ParaCmd1(void)
{
	u16 i=AQ_AMOUNT;
  while(i--) Bx.CR[AQ_OFFSET+i] = *((u16*)(Bx.ParaBuffer+2+2*i));  		
	//非广播地址则回复
	if(Bx.ParaBuffer[0]&0xf0) 				
	{	Bx.ParaBuffer[0]=Bx.ParallelAdr|0x0A;
		Bx.ParaBuffer[1]=Bx.CR[0]&0XFF;
		Bx.ParaBuffer[2]=0;
		ParaReply(3);
	}
}
void a_ParaCmd2(void){GPIOA->CRL=0X44444444;}


/*03 读/写参数(From/To)********************************************************/
void a_ParaCmd3(void)
{	u32 i,j,k,m;
	u16 TmprCali;
	k=Bx.ParaBuffer[3];//起始CR号
	i=Bx.ParaBuffer[4];//CR数	
	//读(From指令)
	if(Bx.ParaBuffer[2]==0x01)
	{
  	if((i+k-1)<=CR_AMOUNT&&i<=80)//判断是否超出地址范围，最大读取80个
		{
    	if(Bx.ParaBuffer[0]&0xf0)
			{
      	i=2*i;
				for(j=0;j<i;j+=2)*(u16*)(Bx.ParaBuffer+3+j)=Bx.CR[k++];
				Bx.ParaBuffer[0]=Bx.ParallelAdr|0x0A;
				Bx.ParaBuffer[1]=Bx.CR[0]&0XFF;//产品ID
				Bx.ParaBuffer[2]=i;
				ParaReply(i+3);						
			}
		}
    else if(Bx.ParaBuffer[3]==0xF0 && Bx.ParaBuffer[4]==0X08)
    { 
      Bx.ParaBuffer[0] = Bx.ParallelAdr|0x0A;
      Bx.ParaBuffer[1]=Bx.CR[0]&0X0FF;
      Bx.ParaBuffer[2] = 8;
      *(u32*)(Bx.ParaBuffer+7) = Bx.ParallelAdr>>4;//先于下一条指令否则3级优化编译会出错      
      *(u32*)(Bx.ParaBuffer+3) = *(u32*)0x08000208;//版本信息     
      ParaReply(8+3);            
    }  
	}	 
	//写(To指令)
	else if(Bx.ParaBuffer[2]==0x02)
	{
		if(Bx.CR[79]!=0xA8B9)
		{
			if((i+k-1)<=CR_AMOUNT && i<=80)//判断是否超出地址范围，写最大个数80个
			{
				u32 err=0,cr_value;
				for(j=0;j<2*i;j+=2)
				{	//如果写波特率
					cr_value=*(u16*)(Bx.ParaBuffer+5+j);
					if(k==1 && cr_value==0)cr_value=1;
					else if(k==2)
					{
						if(Bx.CR[2] != cr_value)ProtocolSet(cr_value);
					}
					else if(k==79 && cr_value==0xA8B9)  //进入校准模式，清除校准数据
					{
						Wipe(CaliConstAdress,1);
						for(m=0;m<CALIDATA_NUM/2+2;m++)
							CaliData[m]=0;
						bCalibrateFlag=0;
					}
					err+=Write_CR(k,cr_value);							
					k++;
				}
				if(Bx.ParaBuffer[0]&0xf0)
				{
					if(err==0)Bx.ParaBuffer[0]=Bx.ParallelAdr|0x0A;
					else Bx.ParaBuffer[0]=Bx.ParallelAdr|0x05;
					Bx.ParaBuffer[1]=Bx.CR[0]&0XFF;//产品ID
					Bx.ParaBuffer[2]=0x00;
					ParaReply(3);
				}
			}
		}
		else if(Bx.CR[79]==0xA8B9)
		{
			u16 cr_value;
			if(k == 0)
			{
				cr_value=*(u16*)(Bx.ParaBuffer+5);//相当于第一个数据   CR0 代表 校准数据偏移地址量
				if(cr_value == 0xffff)
				{
					u16 * m16const;
					//Bx.CR[79]=0;
					
					/***保存校准数据***/
						Wipe(CaliConstAdress,1);
						FLASH->SR|=SR_EOP;//清除 EOP(5)位
						FLASH->CR|=CR_PG; //set the bit PG(0)			
						m16const=(u16*)CaliConstAdress;
						for(i=0;i<100;i+=1)
							*m16const++ = CaliData[i];					

						while(FLASH->SR&SR_BUSY); //等待 BSY(0)=0 
						FLASH->CR&=~CR_PG; //Reset the bit PG(0)
						FLASH->CR|=CR_LOCK; //上锁
						
					bCalibrateFlag=1;
				}
				else
				{
					u16 num = *(u16*)(Bx.ParaBuffer+7);//相当于第二个数据 CR1 代表 这批校准数据长度
					if ((cr_value+num)<100)
						for(i=0,j=0; i<num;i++,j+=2)
						{
							TmprCali= *(u16*)(Bx.ParaBuffer+9+j);
							TmprCali = (TmprCali>>8)|(TmprCali<<8); //高低字节互换
							CaliData[cr_value+i] = TmprCali;
						}		
					#if defined(A04TC)||defined(A08TC)
         if(CaliData[0]!=0) WriteSelfCaliData(); 	
          #endif						
				}
			}
			else if(k==79)
			{
				cr_value=*(u16*)(Bx.ParaBuffer+5);  //
				Write_CR(k,cr_value);
			}
			//else err =3;
			
			if(Bx.ParaBuffer[0]&0xf0)
			{
				Bx.ParaBuffer[0]=Bx.ParallelAdr|0x0A;
					//else Bx.ParaBuffer[0]=Bx.ParallelAdr|0x05;
				Bx.ParaBuffer[1]=Bx.CR[0]&0XFF;//产品ID
				Bx.ParaBuffer[2]=0x00;
				ParaReply(3);
			}
		}
	}
}

/*06 写硬件配置信息 ***********************************************************/ 
void a_ParaCmd6(void)
{	
  if(Bx.ParaBuffer[2] == (Bx.CR[0]&0XFF))  //对比ID号
  {
    u8 temp,i,j,k;

    /***AI参数配置***/
    #if AI_AMOUNT>0
    k = Bx.ParaBuffer[12];  //AI通道数
    for(i=0;i<k;i++)
    {   
      j=8*i;
      temp = Bx.ParaBuffer[18+j];
      //Bx.CR[AI_RANG_CR+i] = temp&0x0f;  //信号类型（量程）
			Write_CR(AI_RANG_CR+i, temp&0x0f);
			
    	if(temp & 0x10)  //使用工程量标志
    	{
    	  //Bx.CR[AI_ENGCL_CR] |= (0x01<<i); 
				Write_CR(AI_ENGCL_CR, Bx.CR[AI_ENGCL_CR] | (0x01<<i));
    	}
    	else
    	{
    	  //Bx.CR[AI_ENGCL_CR] &= ~(0x01<<i);
				Write_CR(AI_ENGCL_CR, Bx.CR[AI_ENGCL_CR] & ~(0x01<<i));
    	}
    	//Bx.CR[AI_SMPL_CR+i] = Bx.ParaBuffer[19+j];    //采样次数
			Write_CR(AI_SMPL_CR+i, Bx.ParaBuffer[19+j]);
			
    	//Bx.CR[AI_UGCL_CR+i] = *(u16*)(Bx.ParaBuffer+20+j);  //上限值
			Write_CR(AI_UGCL_CR+i, *(u16*)(Bx.ParaBuffer+20+j));
			
    	//Bx.CR[AI_DGCL_CR+i] = *(u16*)(Bx.ParaBuffer+22+j);  //下限值
			Write_CR(AI_DGCL_CR+i, *(u16*)(Bx.ParaBuffer+22+j));
			
    	//Bx.CR[AI_0_CR+i] = *(u16*)(Bx.ParaBuffer+24+j);  //零点修正值
			Write_CR(AI_0_CR+i, *(u16*)(Bx.ParaBuffer+24+j));
			
    }
    #endif
  
  	/***AQ参数配置***/
    #if AQ_AMOUNT>0
    k = Bx.ParaBuffer[15];  //AQ通道数
  	for(i=0;i<k;i++)
    {   
      #define N  (18+8*AI_AMOUNT)
      j=8*i;
      temp = Bx.ParaBuffer[N+j];
      //Bx.CR[AQ_RANG_CR+i] = temp&0x0f;  //信号类型（量程）
			Write_CR(AQ_RANG_CR+i, temp&0x0f;);
			
    	if(temp & 0x10)  //使用工程量标志
    	{
    	  //Bx.CR[AQ_ENGCL_CR] |= (0x01<<i); 
				Write_CR(AQ_ENGCL_CR, Bx.CR[AQ_ENGCL_CR] | (0x01<<i));				
    	}
    	else
    	{
    	  //Bx.CR[AQ_ENGCL_CR] &= ~(0x01<<i);
				Write_CR(AQ_ENGCL_CR, Bx.CR[AQ_ENGCL_CR] & ~(0x01<<i));
    	}
    	//Bx.CR[AQ_UGCL_CR+i] = *(u16*)(Bx.ParaBuffer+(N+2)+j);  //上限值
			Write_CR(AQ_UGCL_CR+i, *(u16*)(Bx.ParaBuffer+(N+2)+j));
			
    	//Bx.CR[AQ_DGCL_CR+i] = *(u16*)(Bx.ParaBuffer+(N+4)+j);  //下限值
			Write_CR(AQ_DGCL_CR+i, *(u16*)(Bx.ParaBuffer+(N+4)+j));
    }
    #endif
  	if(Bx.ParaBuffer[0]&0xf0) //非广播地址则回复
    { 
      Bx.ParaBuffer[0]=Bx.ParallelAdr|0x0A;
      Bx.ParaBuffer[1]=Bx.CR[0]&0X0FF;
      Bx.ParaBuffer[2]=0;
      ParaReply(3);	
    }
	}
}

/*******************************************************************************
* 函数名    ：引导区读写保护设置
* 描述      ：根据入口参数不同对模块上写保护或者去写保护
* 入口参数  ：action：0x55去写保护，0xaa：上写保护
*******************************************************************************/ 
void EnRdWrProtection(unsigned char action)
{
  while(FLASH->SR&SR_BUSY);
  if(FLASH->CR&CR_LOCK)
  {  FLASH->KEYR=0X45670123;
    FLASH->KEYR=0xCDEF89AB;
  }
  while(FLASH->SR&SR_BUSY);
  FLASH->OPTKEYR=0X45670123;
  FLASH->OPTKEYR=0xCDEF89AB;
  while(FLASH->SR&SR_BUSY);
  FLASH->CR |= CR_OPTER;
  FLASH->CR |= CR_STRT;
  while(FLASH->SR&SR_BUSY);
  FLASH->CR &= ~(CR_OPTER|CR_STRT);
  FLASH->CR |= CR_OPTPG; //SET OPTPG(4)
  if(action==0x55)OB->RDP=0X00A5;//去写保护
  else if(action==0xaa)//上写保护
  {  OB->RDP=0X0000;
    OB->WRP0=0x00F8;
  }
  while(FLASH->SR&SR_BUSY);
  FLASH->CR&=~(CR_OPTPG|CR_OPTWRE);
  FLASH->CR|=CR_LOCK;
 //SCB->AIRCR = 0x05FA0000 | (u32)0x04;
} 
/*******************************************************************************
* 函数名    ：TIM3事件中断
* 描述      ：为系统提供200us时基
*******************************************************************************/
void A_TIM3_IRQHandler(void)
{	u32 i;
	TIM3->SR&=~0X01;	
	Bx.TimeExPower++;
	Bx.TimePwrDN++;
	Bx.Time1ms++;		
	if(Bx.Time1ms>=5)//1ms的时钟
	{
  	Bx.Time1ms=0;
		if(Bx.TimeErrLed<1000)Bx.TimeErrLed++;
		TimeIOLed++; 
		// IRQ周期500ms宽度1ms脉冲控制
		if(Bx.TimeIRQ<=500)
		{
     	if(Bx.TimeIRQ==0)GPIOC->BSRR=IRQ;
			else if(Bx.TimeIRQ >=1)GPIOC->BRR=IRQ;
			Bx.TimeIRQ++;
		}
		//Link灯闪烁控制
		i=Bx.TimeLinkLed;
		if(i<(6*BlinkTime))
		{	if(i==0)ReLinkLedSig=0;
			if((i%BlinkTime)==0&&LinkON)
			{	u32 j=0;
				if(Bx.CR[15]==0)j=LinkGreen;
				else if(Bx.CR[15]==4)j=LinkGreen|LinkRed;
				else if(Bx.CR[15]==3||Bx.CR[15]>4)j=LinkRed;
				if(GPIOB->ODR&j)GPIOB->BRR=j;
				else	GPIOB->BSRR=j;                        				
			}
			i++;
			if(i==(6*BlinkTime)&&ReLinkLedSig)i=0;
			Bx.TimeLinkLed=i;
		}
    //modbus写校准数据30s超时 
    if(ModWrCali.Timeout<30000)ModWrCali.Timeout++;
    else ModWrCali.StartAdr=0;//超时地址清0 
    if(TimeRead24c08<300)TimeRead24c08++; 
    if(I2C.Time<TIME_5MS)I2C.Time++;
    #if AQ_AMOUNT>0
    if(TimeAqRefresh < 3)TimeAqRefresh++;//AQ间隔3ms更新一次  
    else
    {
      TimeAqRefresh=1;
      b_AqRefFlag=1;//置AQ更新标志为1
    }
    #endif
    #if defined(H04_08RCTC)||defined(A04_08RCTC)
    RTCTimeout++;
    #endif
	}
  #if AI_AMOUNT>0  && defined(S04_08XA)
	if(TimeAiRefresh<3)TimeAiRefresh++;//AI间隔3*0.2ms更新一次	
  else 
  {
    TimeAiRefresh=1;
    b_AiRefFlag=1;
  }
  #endif
	#if defined(H04_08RCTC)||defined(A04_08RCTC)
	RCDelay++;
	#endif
}
