#include "stm32f10x_type.h"
#include "stm32f10x_map.h"
#include "IDSel.h"
#include "boot.h"

#define CaliConstAdress	(u32)0x08003C00   //a系列校准数据存放地址

/********//********//********//********//********//********//********//********/
/*寄存器定义*******************************************************************/
#define SPI_TXE		    (u16)0x0002//
#define SPI_RXNE	    (u16)0x0001//
#define SPI_BUSY	    (u16)0x0080//
#define SPI_SPE 	    (u16)0x0040//SPI->CR1 Bit6
#define	SPI_TXEIE	    (u16)0x0080
#define	SPI_RXNEIE	  (u16)0x0040

/*程序参数定义*****************************************************************/
#define TIME_5MS        (u8)5	//计时时间5ms
/*校准数据字节数（不包括2个校验字节）,每通道4个量程，每个校准点2字节，还有一组基准点*/
#ifdef S04_08XA
#define	CALIDATA_NUM	  (u8)(3*2*(1+4*AI_AMOUNT+4*AQ_AMOUNT))
#endif
#if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC)
#define	CALIDATA_NUM	  (u8)(48+12*AI_AMOUNT)
#endif
#if defined(H04RC)||defined(H08RC)||defined(A04RC)
//#define	CALIDATA_NUM	  (u8)(24+24*AI_AMOUNT)
#define	CALIDATA_NUM	  (u8)(48+12*AI_AMOUNT)
#endif
/***********************端口定义*************************/
#define	STcp		      (u16)0x1000	//PB12	storage register clock input
#define SPICLK        (u16)0x2000 //PB13
#define	MISO		      (u16)0x4000	//PB14
#define	MOSI		      (u16)0x8000	//PB15
#define	SHcp		      (u16)0X8000	//PC15 	shift clock input	 调试值，正板中为PC15

/***************** spi_status宏定义***************/
#define SPI_IS_BUSY		(u16)0x0000 	 //spi_status,SPI正在发送or接收数据
#define	AQ_SPICMD_OK	(u16)0x0055	//AQ的spi命令发送完成
#define AI_SPICMD_OK	(u16)0x00aa	//AIspi命令发送完成
/************************宏定义*************************/
#define AL_BH	        (u16)0X8000//Achannel set Low,Bchannel set High
#define	BUFH	        (u16)0X4000//输入缓冲控制位，high有效
#define	GAL		        (u16)0x2000//输出增益控制位，low有效
#define	SHDNL	        (u16)0x1000//output powerdown contorl bit low有效
/*******************I2C寄存器宏定义*************************/
#define	ADR_24C08	    (u8 )0xA0//24c02 adress。高7位24C02地址，LSB：读/写选择位
#define I2C_START	    (u16)0x0100
#define I2C_STOP	    (u16)0x0200
#define I2C_ACK		    (u16)0x0400	//应答使能
#define I2C_PE		    (u16)0X0001	//I2C模块使能
#define I2C_FREQ_36M  (u16)0x0024
#define I2C_F_S		    (u16)0x8000	//标准（低有效）快速模式（高有效）选择位
#define I2C_SR_SB	    (u16)0x0001
#define I2C_SR_ADDR	  (u16)0x0002
#define I2C_BTF		    (u16)0x0004//CR2 bit2
#define I2C_TXE		    (u16)0x0080
#define I2C_RXNE	    (u16)0x0040
#define	I2C_BUSY	    (u16)0x0002//SR2 bit 1
#define	I2C_ITEVTEN	  (u16)0x0400	//外部事件中断使能
#define	I2C_ITBUFEN   (u16)0x0200 //缓冲器中断使能
#define	RCC_I2C1EN	  (u32)0x200000	//Enable I2C1时钟 I2C1EN(21)
#define	RCC_SPI2EN	  (u32)0x004000	//Enable SPI2时钟 SPI2EN(14)
/*******************BitFlag**************************/
/*****共64位，前32位留给引导区用，后32位应用区用*****/
//第0位起 见config.h

//第32位起
#define	bCaliDataErr	  *(vu8*)(0x22000000+4*32)//
#define b_AqRefFlag		  *(vu8*)(0x22000000+4*33)//AQ刷新标志
#define b_AiRefFlag		  *(vu8*)(0x22000000+4*34)//AQ刷新标志
#define bCalibrateFlag  *(vu8*)(0x22000000+4*35)//校准标志，0不校准；1校准

extern struct SPIAttributeType
{
  vu8 RxCount;
  vu8 TxCount;
  vu8 TxLen;
  vu8 RxData[5];
  vu8 TxData[5];
}SPICom;
extern struct MCUTempSensorType
{
  u16 Temperature;//当前温度
  u16 Sum;    //
  u16 SmplCount;
}MCUTemp;

extern const struct  CRAttributeType
{
 u8 Limit;    //限制属性
 u8 Rang;     //限制范围
 u8 PDSave;   //PoweDown Save，掉电是否保持。
}CR_Att[CR_AMOUNT];

#if AI_AMOUNT>0          //测试
extern struct FilterType
{
  u16 Count;
  u16 Times;//滤波次数
  signed long Max;  
  signed long Min;  
  signed long Sum;    
}AiFilter[AI_AMOUNT];
#endif
#if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC)
extern struct S_FilterType
{
	s32 Sum1;
	s32 Sum2;
	s16 TCu50;
	s16 T1047[2];
	u8	DiscardChe;
}S_AiFilter;
#endif
extern struct I2CType
{
  vu16 Cmd;         //I2c控制命令
  vu16 StartAdress;  //欲读24c08的起始地址
  vu8  *CaliDataAdr;//CaliData地址字节偏移，读时为存地址，写时为取地址
  vu16 Lenght;      //读/写长度
  vu16 TxCount;     //发送数据计数
  vu16 RxCount;     //接收数据计数
  vu16 Time;        //通信间隔计时每单位0.2ms。计时时间26*0.2ms=5.2ms
}I2C;
extern struct ModWrCaliType
{
  u16 StartAdr;     //起始地址
  u16  Timeout;      //超时（10s超时）
}ModWrCali;         //modbus写校准数据
