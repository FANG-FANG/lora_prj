#include "config_08AX.h"
extern NMI_Handler(void);
extern u32  Load$$LR$$LR_IROM2$$Length;
extern u32  Load$$LR$$LR_IROM3$$Length;
//  #define __initial_sp 0x20005000
extern void  Reset_Handler(void);
extern int   NMI_Handler                (void);
extern void  HardFault_Handler          (void);
extern void  MemManage_Handler          (void);
extern void  BusFault_Handler           (void);
extern void  UsageFault_Handler         (void);
extern void  SVC_Handler                (void);
extern void  DebugMon_Handler           (void);
extern void  PendSV_Handler             (void);
extern void  SysTick_Handler            (void);
extern void  A_EXTI2_IRQHandler         (void);
extern void  A_TIM2_IRQHandler          (void);
extern void  A_TIM3_IRQHandler          (void);
extern void  A_USART1_IRQHandler        (void);
extern void  A_EXTI15_10_IRQHandler     (void);
extern void  A_I2C1_EV_IRQHandler       (void);
extern void  DMA2_Channel1_IRQHandler   (void);
extern void  EXTI9_5_IRQHandler         (void);
extern void  L_TIM4_IRQHandler          (void);
extern void  L_USART2_IRQHandler        (void);

/*******************************************************************************
* 应用区中断向量表(0x130个字节 )
*******************************************************************************/
u32 *const AppVectTable[0x130/4] __attribute__((used))=
{
  (u32*)  0x20005000,         //0
  (u32*)  Reset_Handler,      //4
  (u32*)  NMI_Handler,        //8 NMI Handler
  (u32*)  HardFault_Handler,  //c Hard Fault Handler
  (u32*)  MemManage_Handler,  //10 MPU Fault Handler
  (u32*)  BusFault_Handler,   //14 Bus Fault Handler
  (u32*)  UsageFault_Handler, //18 Usage Fault Handler
  0,0,0,0,                    //保留
  (u32*)  SVC_Handler,        //2c SVCall Handler
  (u32*)  DebugMon_Handler,   //30 Debug Monitor Handler
  0,                          //保留
  (u32*)  PendSV_Handler,     //34 PendSV Handler
  (u32*)  SysTick_Handler,    //38 SysTick Handler
  (u32*)HardFault_Handler,    //3c WWDG_IRQHandler            
  (u32*)HardFault_Handler,    //40 PVD_IRQHandler             
  (u32*)HardFault_Handler,    //44 TAMPER_IRQHandler          
  (u32*)HardFault_Handler,    //48 RTC_IRQHandler           
  (u32*)HardFault_Handler,    //4c FLASH_IRQHandler         
  (u32*)HardFault_Handler,    //RCC_IRQHandler             
  (u32*)HardFault_Handler,    //EXTI0_IRQHandler           
  (u32*)HardFault_Handler,    //EXTI1_IRQHandler           
  (u32*)A_EXTI2_IRQHandler,   //外部中断2
  (u32*)HardFault_Handler,    //EXTI3_IRQHandler           
  (u32*)HardFault_Handler,    //EXTI4_IRQHandler           
  (u32*)HardFault_Handler,    //DMA1_Channel1_IRQHandler   
  (u32*)HardFault_Handler,    //DMA1_Channel2_IRQHandler   
  (u32*)HardFault_Handler,    //DMA1_Channel3_IRQHandler   
  (u32*)HardFault_Handler,    //DMA1_Channel4_IRQHandler   
  (u32*)HardFault_Handler,    //DMA1_Channel5_IRQHandler   
  (u32*)HardFault_Handler,    //DMA1_Channel6_IRQHandler   
  (u32*)HardFault_Handler,    //DMA1_Channel7_IRQHandler   
  (u32*)HardFault_Handler,    //ADC1_2_IRQHandler          
  (u32*)HardFault_Handler,    //USB_HP_CAN1_TX_IRQHandler  
  (u32*)HardFault_Handler,    //USB_LP_CAN1_RX0_IRQHandler 
  (u32*)HardFault_Handler,    //CAN1_RX1_IRQHandler        
  (u32*)HardFault_Handler,    //CAN1_SCE_IRQHandler        
  (u32*)EXTI9_5_IRQHandler,    //EXTI9_5_IRQHandler         
  (u32*)HardFault_Handler,    //TIM1_BRK_IRQHandler        
  (u32*)HardFault_Handler,    //TIM1_UP_IRQHandler         
  (u32*)HardFault_Handler,    //TIM1_TRG_COM_IRQHandler    
  (u32*)HardFault_Handler,    //TIM1_CC_IRQHandler         
  (u32*)A_TIM2_IRQHandler,    //(28)定时器2
  (u32*)A_TIM3_IRQHandler,    //(29)定时器3
  (u32*)L_TIM4_IRQHandler,    //TIM4_IRQHandler            
  (u32*)A_I2C1_EV_IRQHandler,                              
  (u32*)HardFault_Handler,    //I2C1_ER_IRQHandler         
  (u32*)HardFault_Handler,    //I2C2_EV_IRQHandler         
  (u32*)HardFault_Handler,    //I2C2_ER_IRQHandler         
  (u32*)HardFault_Handler,    //SPI1_IRQHandler            
  (u32*)HardFault_Handler,      //SPI2_IRQHandler            
  (u32*)A_USART1_IRQHandler,  //(37)USART1
  (u32*)L_USART2_IRQHandler,    //USART2_IRQHandler           
  (u32*)HardFault_Handler,    //USART3_IRQHandler          
  (u32*)A_EXTI15_10_IRQHandler,//(40)外部中断10        
  (u32*)HardFault_Handler,    //RTCAlarm_IRQHandler        
  (u32*)HardFault_Handler,    //USBWakeUp_IRQHandler       
  (u32*)HardFault_Handler,    //TIM8_BRK_IRQHandler        
  (u32*)HardFault_Handler,    //TIM8_UP_IRQHandler         
  (u32*)HardFault_Handler,    //TIM8_TRG_COM_IRQHandler    
  (u32*)HardFault_Handler,    //TIM8_CC_IRQHandler         
  (u32*)HardFault_Handler,    //ADC3_IRQHandler            
  (u32*)HardFault_Handler,    //FSMC_IRQHandler            
  (u32*)HardFault_Handler,    //SDIO_IRQHandler            
  (u32*)HardFault_Handler,    //TIM5_IRQHandler            
  (u32*)HardFault_Handler,    //SPI3_IRQHandler            
  (u32*)HardFault_Handler,    //UART4_IRQHandler           
  (u32*)HardFault_Handler,    //UART5_IRQHandler           
  (u32*)HardFault_Handler,    //TIM6_IRQHandler            
  (u32*)HardFault_Handler,    //TIM7_IRQHandler            
  (u32*)DMA2_Channel1_IRQHandler,    //DMA2_Channel1_IRQHandler   
  (u32*)HardFault_Handler,    //DMA2_Channel2_IRQHandler   
  (u32*)HardFault_Handler,    //DMA2_Channel3_IRQHandler   
  (u32*)HardFault_Handler,    //DMA2_Channel4_5_IRQHandler 
};
/*******************************************************************************
* 索引表(60个字节 )
*******************************************************************************/
u32  *const IndexTable[15] __attribute__((used))= 
{
  (u32*)(('H')|('a'<<8)|('i'<<16)|('-')<<24),  //hai-
  (u32*)(('w')|('e'<<8)|('l'<<16)|('l'<<24)),  //well
  (u32*)0X20130127,  
  (u32*)(CpuVersion>>5),           //低字节CPUID（1:ARM or 0:AVR），高3字节保留
  (u32*)((0x10<<24)|(0x11<<16)|    //Bit24-31：CPU2段起始；Bit16-23：CPU索引；
        (((((CpuVersion&0x1f)/10)<<4)|((CpuVersion&0x1f)%10))<<8)|//产品版本；
        ProductID ),//产品ID；
  (u32*)AppAdress,              //段1起始地址
  (u32*)(&Load$$LR$$LR_IROM3$$Length+(u32)0x01),  //段1字节数
  (u32*)0,  //段2起始地址
  (u32*)0,  //段2字节数
  (u32*)0,  //段3起始地址                  
  (u32*)0,  //段3字节数                
  (u32*)0,  //段4起始地址                  
  (u32*)0,  //段4字节数
  (u32*)0,  //段5起始地址
  (u32*)0,  //段5字节数
};

const   u8   SysConst_app[64] __attribute__((used)) //__attribute__((section("sysconst_app_sct")))
  ={(u8)CRAdress,(u8)(CRAdress>>8),(u8)(CRAdress>>16),(u8)(CRAdress>>24),//CR地址
   0x10,//搜索回复帧长度
   0x01,//串口地址在CR中的偏移位置
   0x02,//通信格式在CR中的偏移位置
   CpuVersion,//CPU版本
   ProductID,//产品ID
   AI_AMOUNT,//DI点数
   AQ_AMOUNT,//do点数
   };
const   u8 __attribute__((used))  process_end[8] __attribute__((section("p_end")))="hai-well"; 
