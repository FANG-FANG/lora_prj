#include "stm32f10x_lib.h"
#include "stm32f10x_i2c.h"
#include "config_08AX.h"
#include "Protect.h"
#define READ24C08_TIMEOVER  20//读24c08超时时间20ms

/*******************************************************************************                        
* 函数声明
*******************************************************************************/
void Start(void) __attribute__((section("Start"))) ;
void AppInitial(void);
#ifdef H04_08RCTC
void I2cInit(void);
void Read24C08(u16 start_adr,u16 lenght, u8 *calidata_adr);
void Write24C08(u16 cmd,u16 start_adr,u16 lenght,u8 *calidata_adr);
void LaunchI2C(void);
void I2CQuery(void);
void ReadCalidata(void);
#endif
#ifdef A04_08RCTC
void ReadCalidata_A(void);
#endif
void ReloadIWDG(void);
void Shift595(u16 data);
void delay(u32 times);
void PowerQuery(void);  
void LinkLedControl(void);       
void QuerySPI(void);
void LedRefresh(void);   
void SwitchScan(void);
void ReadCR(void);
void ReadManuCommID(void);
void Wipe(u32 adress,u8 Page_Num);
void ExPowerQuery(void);
void PwrDNScan(void) ;
void SaveCR(void);
extern void SelfCaliCalc(void);
extern void RTCmain(void);
extern void ProtocolSet(u8 pset);
extern void ModbusQuery(void);
extern void ParallelQuery(void);
extern void ADCInit(void);
extern void QureyADC(void);
extern u16 CRC16(vu8 *CRCdata,u16 CRCLen);
void a_NVIC_SetIRQEnable(u8 irq_channel,u8 action,u8 priority);
extern void EXTI9_5_IRQHandler_lora(void);
extern void DMA2_Channel1_IRQHandler_lora(void);

/*******************************************************************************                        
* 变量声明
*******************************************************************************/
struct I2CType I2C;
vu16 TimeRead24c08;//读24c08计时，超时则放弃读 
vu16 TimeErrLed;
vu16 CaliData[100/*CALIDATA_NUM/2+2*/];

//ZCF:
//#define zz
#ifdef A04TC
const u16 CalidataValue[100] = {0x13BD,0X62AF,0XD91B,0X13BD,0X62AF,0XD91B,\
0X12BF,0X641C,0XDAF1,0X12FB,0X6143,0XD582,\
0X12BF,0X641C,0XDAF1,0X12FB,0X6143,0XD582,\
0X12BF,0X641C,0XDAF1,0X12FB,0X6143,0XD582,\
0X12BF,0X641C,0XDAF1,0X12FB,0X6143,0XD582,\
0x13BD,0X62AF,0XD91B,0X13BD,0X62AF,0XD91B,\
0X12BF,0X641C,0XDAF1,0X12FB,0X6143,0XD582,\
0X0026,0X0072,0X000A,0x0,0x35d4,0xecca\
};//0x6ba9,0x6ba90xee22,
#endif
#ifdef A08TC
//const u16 CalidataValue[100] = {0x13BD,0X62AF,0XD91B,0X13BD,0X62AF,0XD91B,\
//0X13e2,0X62fe,0Xd9df,0X1356,0X60fc,0Xd550,\
//0X13e2,0X62fe,0Xd9df,0X1356,0X60fc,0Xd550,\
//0X13e2,0X62fe,0Xd9df,0X1356,0X60fc,0Xd550,\
//0X13e2,0X62fe,0Xd9df,0X1356,0X60fc,0Xd550,\
//0X13e2,0X62fe,0Xd9df,0X1356,0X60fc,0Xd550,\
//0X13e2,0X62fe,0Xd9df,0X1356,0X60fc,0Xd550,\
//0X13e2,0X62fe,0Xd9df,0X1356,0X60fc,0Xd550,\
//0X13e2,0X62fe,0Xd9df,0X1356,0X60fc,0Xd550,\
//0x13BD,0X62AF,0XD91B,0X13BD,0X62AF,0XD91B,\
//0X13e2,0X62fe,0Xd9df,0X1356,0X60fc,0Xd550,\
//0X0026,0X0072,0X000A,0x0,0x3616,0xee22,\
//0x6ba9,0x6ba9,};
const u16 CalidataValue[100] = {0x13BD,0X62AF,0XD91B,0X13BD,0X62AF,0XD91B,\
0X1309,0X63B8,0XDA71,0X1321,0X60B6,0XD48C,\
0X1309,0X63B8,0XDA71,0X1321,0X60B6,0XD48C,\
0X1309,0X63B8,0XDA71,0X1321,0X60B6,0XD48C,\
0X1309,0X63B8,0XDA71,0X1321,0X60B6,0XD48C,\
0X1309,0X63B8,0XDA71,0X1321,0X60B6,0XD48C,\
0X1309,0X63B8,0XDA71,0X1321,0X60B6,0XD48C,\
0X1309,0X63B8,0XDA71,0X1321,0X60B6,0XD48C,\
0X1309,0X63B8,0XDA71,0X1321,0X60B6,0XD48C,\
0x13BD,0X62AF,0XDA71,0X13BD,0X62AF,0XD91B,\
0X1309,0X63B8,0XDA71,0X1321,0X60B6,0XD48C,\
0X0026,0X0072,0X000A,0x0,0x3616,0xee22,\
0x6ba9,0x6ba9,};
#endif

/***外部变量***/
extern u8 AIMessage;
extern u8 CSMessage;
u8 AiRang[AI_AMOUNT];//ai量程 
struct FilterType AiFilter[AI_AMOUNT];

extern vu16 RTCTimeout;
vu16 Dout595;  //
extern u32 FuncAdress[];
extern vu8 TimeIOLed;
extern BootVariable Bx;
extern u32 SCDelay;
extern u8 RangeType; 
extern vu8 SCState;
#if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC) 
extern u8 ErrCount;
extern struct S_FilterType S_AiFilter;
u8 ledstate=0;
#endif
#if defined(H04RC)||defined(H08RC)||defined(A04RC)
extern vu8 DispCode;
extern vu8 AiChannel;	
extern vu8 ScCH;
extern vu8 LastCH;
extern u32 WireR[AI_AMOUNT*2];
extern u8	FIFOCount[AI_AMOUNT];
extern vu8	StartupFlag;
#endif

//lora
extern void SX1276AppInit(void);
extern void LoRa_Init(void);
extern void modbus_handle(void);
extern void USART2_TX(uint8_t *data,uint16_t len);

u8 SendBuf[256] = {0x01,0x03,0x0,0x10,0x0,0x3,0x4,0xe,};//modbus command3,cr16,num3
//u8 SendBuf[64] = {0x01,0x06,0x0,0x18,0x0,0x2,0x88,0xc,};//modbus command6,cr16,
//u8 SendBuf[64] = {0x01,0x11,0x01,0x11,0x13,0x14,0x15,016,};
//u8 SendBuf[64] = {0x02,0x22,0x01,0x12,0x23,0x24,0x25,0x26,};

extern DeviceModeSetting LoraMasSla;
u32 count=0;	
u8 testlen=20;
extern RadioLoRaSettings_t LoRa;
/*******************************************************************************                        
* 函数名    ：入口Start()函数
* 描述      ：
*******************************************************************************/                        
void Start(void)
{ 
  u16 sendlen;

	SCB->VTOR=AppAdress&0Xfffff;//设置中断向量表地址 
  /* Configure the system clock */
  //RCCInitialize();
	a_NVIC_SetIRQEnable(TIM2_IRQChannel,ENABLE,0x20);
  a_NVIC_SetIRQEnable(USART2_IRQChannel,ENABLE,0x10);
	LoRa_Init();
  SX1276AppInit();
	
  ReadCR();
	ReadManuCommID();
//for(sendlen=0;sendlen<256;sendlen++)SendBuf[sendlen]=sendlen;
//	LoraMasSla.LoraMS=Master;//测试，主机模式
  while (1)
  {
		modbus_handle();
		count++;
//	
		if(count>9000000)
		{	
			count = 0;
		//	 USART2_TX(SendBuf,8);
	//		if((LoraMasSla.LoraMS==Master)&&(LoraMasSla.MasterState==MIdle))
	//		SX1276Send(SendBuf, testlen);//测试，主机发送数据
//			
     } 
		if((LoraMasSla.LoraMS==Master)&&(LoraMasSla.MasterState==MDateWaitHandle))//接收到数据等待处理
		{ /* 处理完数据修改LoraMasSla.MasterState，将lora设置为相应状态，LoraMasSla.MasterBuffer为接收数据，LoraMasSla.MasterSize为接收长度*/
      
			    /*此添加数据处理*/ 
			 LoraMasSla.MasterState=MIdle;
			 if(LoraMasSla.MasterState!=MDateWaitHandle)
			 {	
				 //LoRa.RxContinuous = true; 
				 SX1276SetRx(0);//数据处理完进入相应状态
			 }
     }
  }

//	u8 i,data=0x20;
//  AppInitial();
//  GPIOD->BSRR=Vctr;//测试控制为并口供电
//#ifdef H04_08RCTC	
//  I2cInit();
//  ReadCalidata();   //HS系列从24C08读校准数据
//	
//#endif
//#ifdef A04_08RCTC
//	ReadCalidata_A(); //A系列从flash读校准数据
//#endif	
//  ReadCR();
//	ReadManuCommID(); 
//   
//  SPI2->CR1 |= SPI_SPE;//开启SPI
//	
//	ModWrCali.StartAdr=0;	//20140528	
//  TimeIOLed=0; 
//  TimeRead24c08=0; 
//	
////	Bx.SysConst[7] = 0x2a;	
////	Bx.CR[0]=Bx.SysConst[7]*256+Bx.SysConst[8];
//		
//  while(1) 
//  {
//    ReloadIWDG();
//    PwrDNScan(); 
//			
//    if(bPwrDNErr==1 ||(bPwrDNErr==0 && PwrDN_s==0))
//    {  
//      if(ModbusReOk&&ParaReOk&&ToBootApp==0)//判断升级
//      {         
//        GPIOA->BSRR=DSPCS;            //关led
//        a_NVIC_SetIRQEnable(I2C1_EV_IRQChannel,DISABLE,0x30);
//				Bx.CR[0] &= 0xE0FF; Bx.CR[0] |= 0x0a00;//版本号改为1.0
//        (*(Function)FuncAdress[1])(); //跳到引导区boot子函数入口      
//      }
////      NVIC->ISER[EXTI15_10_IRQChannel>>5]=((u32)1)<<(EXTI15_10_IRQChannel&0x1f);//使能中断  

//			if(SCState>=1)
//				SelfCaliCalc();		
//			else 
//        RTCmain();
//      if(TimeIOLed>=3)//3ms更新一次led灯
//      {																            
//        TimeIOLed=0;   
//        SwitchScan();
//        LedRefresh();
//      }																													
//      PowerQuery();
//      ParallelQuery();
//      LinkLedControl(); 
//     	ledstate = GPIOC->IDR;
//		#ifdef H04_08RCTC 
//			ModbusQuery();
//      I2CQuery();
//		#endif
//    }
//  }
}  

/*******************************************************************************                        
* 函数名    ：清除IO口LED指示灯，清除IO输出值。
* 描述      ：
*******************************************************************************/                        

void AppInitial(void)     
{  //设置中断向量表地址 TABLEBASE(29)=0=FLASH,OFFSET(28-7)=0X00        
  SCB->VTOR=AppAdress&0Xfffff;
	SCB->AIRCR = 0x05FA0000 | 0x0300;//设置中断优先级4bit抢占优先级
  a_NVIC_SetIRQEnable(I2C1_EV_IRQChannel,ENABLE,0x30);
  a_NVIC_SetIRQEnable(TIM2_IRQChannel,ENABLE,0x20);
  a_NVIC_SetIRQEnable(TIM3_IRQChannel,ENABLE,0x20);
  a_NVIC_SetIRQEnable(USART1_IRQChannel,ENABLE,0x10);
  a_NVIC_SetIRQEnable(EXTI2_IRQChannel,ENABLE,0x00);
  a_NVIC_SetIRQEnable(EXTI15_10_IRQChannel,ENABLE,0x10);

  RCC->APB1ENR|=RCC_SPI2EN;//使能SPI2时钟
  Bx.CR[0]=Bx.SysConst[7]*256+Bx.SysConst[8];
	Bx.CR[15]=0;

  GPIOC->BRR=0X8000;
  GPIOC->BSRR=0X0FFF;
/****区分H系列与A系列：引脚定义略有不同**************************************************/
#if defined(H04TC)||defined(H08TC)||defined(H04RC)||defined(H08RC)
  GPIOC->CRL=0X33333333;  //PC0-7(LD00-07)
  GPIOC->CRH=0X38388888;  //PC8-11(SW0-3)PC15(SHcp)
  //拉低DSPCS,使能led灯指示
  GPIOA->BRR=DSPCS;
  GPIOA->CRH=(GPIOA->CRH&0XFFF0FFFF)|0X30000;//原
#endif
#if defined(A04TC)||defined(A08TC)||defined(A04RC)
	GPIOC->CRL=0X33333333;
	GPIOC->CRH=0X38383333;
	GPIOA->CRH=(GPIOA->CRH&0xFFF0FF00)|0x00030033;
	//拉低DSPCS,使能led灯指示
	GPIOA->BRR=DSPCS;
#endif

  //关AICS和AQCS
  Dout595 = (((0xFF>>(8-AQ_AMOUNT/2))<<AQCSOffset)) |(1<<AICSOffset);
  #if defined(H04RC)||defined(H08RC)||defined(A04RC)	
	Dout595 |= 1<<CDOffset_1;
	Dout595 |= 1<<CDOffset_2;
	Dout595 |= 1<<CDOffset_3;//RC片选全关
	#endif	
  Shift595(Dout595);
	
  //SPI口初始化
  GPIOB->BSRR=0xe000;
  GPIOB->CRH=(GPIOB->CRH&0X0000FFFF)|0XB8B30000;
  while(SPI2->SR&0x80);//查询busy位
  #ifdef  S04_08XA
  SPI2->CR1=0X0327; 
  #endif 
  #if defined(H04_08RCTC)||defined(A04_08RCTC)
  SPI2->CR1=0X0324; 
  #endif
  SPI2->SR=0X00;
  TIM2->CR1&=~0x6;
  TIM2->EGR|=0x1;//触发更新   

  #if defined(H04_08RCTC)||defined(A04_08RCTC)
	/* RTC变量初值 */		
  AIMessage=0;//0=sDELAY;				
	CSMessage=0;									
  RTCTimeout=0;									
	RangeType=0;
	SCState=1;//上电先执行自校计算
  #if defined(H04RC)||defined(H08RC)||defined(A04RC)
	DispCode=0;
	ScCH=0;
	LastCH=0;
	AiChannel=AI_AMOUNT;
	StartupFlag=0;
	{	u8 i; 
		for(i=AI_AMOUNT;i<2*AI_AMOUNT;i++)
			WireR[i]=0;
		for(i=0;i<AI_AMOUNT;i++)
			FIFOCount[i]=0;
	}		
	#endif
  #if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC)
	ErrCount=0;							 
	S_AiFilter.TCu50 =0x7fff;	
	S_AiFilter.T1047[0] =0x7fff;
	S_AiFilter.T1047[1] =0x7fff;
	S_AiFilter.DiscardChe=0; 	
	Bx.CR[102]=0;
	#endif									 
  #endif	
	
}
/*******************************************************************************
* 函数名    ：中断使能失能设置
* 描述      ：中断使能失能设置
* 入口参数  ：irq_channle:中断通道，action：使能or失能,priority:优先级
*******************************************************************************/
void a_NVIC_SetIRQEnable(u8 irq_channel,u8 action,u8 priority)
{
  u32 tmp,i;
  if(action==DISABLE)
  { 
    NVIC->ICER[irq_channel>>5]=((u32)1)<<(irq_channel&0x1f);
  }
  else if(action == ENABLE)
  {
    NVIC->ISER[irq_channel>>5]=((u32)1)<<(irq_channel&0x1f);
  }
  i=(irq_channel&0x3)<<3;
  tmp=NVIC->IPR[irq_channel>>2]&(~(0xff<<i));
  NVIC->IPR[irq_channel>>2] = tmp|(priority<<i);
}
/*******************************************************************************
*  重装载看门狗（喂狗）
* 
*******************************************************************************/
void ReloadIWDG(void)
{
	#ifdef USED_IWDG
  IWDG->KR = IWDG_RELOAD;//喂狗
  #endif
}
#ifdef H04_08RCTC
/*******************************************************************************                        
* 函数名    ：I2C初始化
* 描述      ：
*******************************************************************************/ 
#define I2C_SWRST (u16)0x8000;
void I2cInit(void)
{
  GPIOB->BSRR=0XC0;
  RCC->APB1ENR|=RCC_I2C1EN;//使能I2C1时钟
  GPIOB->CRL=(GPIOB->CRL & 0x00ffffff) | 0xFF000000;//SDA SCLK IO脚配置为复用开漏输出
  I2C1->CR1|=I2C_SWRST;    //复位I2C1
  I2C1->CR1&=~I2C_SWRST;
  I2C1->CR2 = I2C_FREQ_36M;//时钟配置为2m
  I2C1->CCR = I2C_F_S|0x1E;//快速模式时钟400kHz
  I2C1->TRISE = 10;//3个Tpclk上升时间
  I2C1->CR1 |= I2C_PE;//开启I2C 
  I2C1->CR2 |= I2C_ITEVTEN | I2C_ITBUFEN;//开启中断
}
/*******************************************************************************                        
* 函数名    ：操作24c08
* 描述      ：
*******************************************************************************/                        
void I2CQuery(void)
{
  if(I2C.Time >= TIME_5MS)
  {
    if (I2C.Cmd==1)
    {
      if(I2C.Lenght)LaunchI2C();
      else I2C.Cmd=0;              
    }
    else if(I2C.Cmd == 2) 
    { 
      if(I2C.Lenght)LaunchI2C();
      else Write24C08(1,16,(CALIDATA_NUM+4),(u8*)CaliData);
    }
    I2C.Time=0;          
  }
}
/*******************************************************************************                        
* 函数名    ：读校准数据
* 描述      ：读校准数据，并验证校准数据的可靠性。修复24c08中的数据。
*******************************************************************************/                        
void ReadCalidata(void)
{
//	u8 i;
//	for(i=0;i<100;i++) 
//	  CaliData[i] = CalidataValue[i];//ADD BY ZCF
//	bCalibrateFlag=1;
//	
//	if(0)
//	{
  u16 crc1,crc2,tmp;
	
  Read24C08(16,(CALIDATA_NUM+4),(u8*)CaliData); 
  if(Bx.CR[15]!=3)//CR15=3表示读24c02失败
  {
    crc1=CRC16((u8*)CaliData,(CALIDATA_NUM+2)); //读主校准数据
    tmp=*(CaliData+CALIDATA_NUM/2);//保存主校验数据区校验结果
    for(crc2=0;crc2<(CALIDATA_NUM/2+1);crc2++)*(CaliData+crc2)=0;//清除第一次读的数据
    Read24C08(528,(CALIDATA_NUM+2),(u8*)CaliData);//读备份组校准数据    
    crc2=CRC16((u8*)CaliData,(CALIDATA_NUM+2));
    if(crc1==0 && crc2==0)
    {
      crc1=tmp;//主校验数据区校验结果
      crc2=*(CaliData+CALIDATA_NUM/2);//备份校验数据区校验结果
      tmp=*(CaliData+CALIDATA_NUM/2+1);
      if(crc1 == crc2){}
      else if(tmp==crc1 && tmp!= crc2)
      {
        Read24C08(16,(CALIDATA_NUM+4),(u8*)CaliData);
        Write24C08(1,528,(CALIDATA_NUM+2),(u8*)CaliData);
      }
      else if(tmp!=crc1 && tmp== crc2)
      {
        *(CaliData+CALIDATA_NUM/2+1)=tmp;
        Write24C08(1,16,(CALIDATA_NUM+4),(u8*)CaliData);
      }
      else if(tmp!=crc1 && tmp!= crc2)
      { 
        Read24C08(16,(CALIDATA_NUM+4),(u8*)CaliData);
        Write24C08(1,528,(CALIDATA_NUM+2),(u8*)CaliData);
      }
    }
    else if(crc1!=0 && crc2!=0)Bx.CR[15]=3;//两个校准数据存储区都校验都错误，报错误3
    else if(crc1!=0 && crc2==0)
    { 
      *(CaliData+CALIDATA_NUM/2+1)=tmp;
      Write24C08(1,16,(CALIDATA_NUM+4),(u8*)CaliData);
    }  
    else if(crc1==0 && crc2!=0)
    {
      Read24C08(16,(CALIDATA_NUM+4),(u8*)CaliData);       
      Write24C08(1,528,(CALIDATA_NUM+2),(u8*)CaliData);
    }
    /*校准标志设置*/
		
    if(CaliData[0]==0xffff && CaliData[1]==0xffff && CaliData[2]==0xffff)//不校准
      bCalibrateFlag=0;
    else bCalibrateFlag=1;//置位校准标志
  }
	if(Bx.CR[15]==3)bCaliDataErr=1;//校准数据错误标志写1
//}
}

/*******************************************************************************                        
* 函数名    ：操作24c08
* 描述      ：
*******************************************************************************/                        
void Write24C08(u16 cmd,u16 start_adr,u16 lenght, u8 *calidata_adr)
{
  I2C.Cmd = cmd; //Read
  I2C.Time =0;
  I2C.StartAdress=start_adr;
  I2C.CaliDataAdr=calidata_adr;
  I2C.Lenght=lenght;
  LaunchI2C();
}
/*******************************************************************************                        
* 函数名    ：操作24c08
* 描述      ：
*******************************************************************************/                        
void Read24C08(u16 start_adr,u16 lenght, u8 *calidata_adr)
{
  I2C.Cmd = 0; //Read
  I2C.StartAdress=start_adr;
  I2C.CaliDataAdr=calidata_adr;
  I2C.Lenght=lenght;
  LaunchI2C();
  TimeRead24c08 = 0;//计时清零
  while(I2C.RxCount < I2C.Lenght)          //等待读24c08结束
  {
    if(TimeRead24c08 > READ24C08_TIMEOVER)//判断是否超时，超时则报错跳出
    {
      Bx.CR[15]=3;//报错误3
      I2C1->CR1 &= ~I2C_PE ;//关I2C
      break;
    }
  }
}
/*******************************************************************************                        
* 函数名    ：操作24c08
* 描述      ：
*******************************************************************************/                        
void LaunchI2C(void)
{
  I2C.TxCount=0;
  I2C.RxCount = 0; 
  I2C1->CR1 |= I2C_PE ;
  I2C1->CR1 &= ~I2C_STOP;
  I2C1->CR2 |= I2C_ITEVTEN | I2C_ITBUFEN; //开启中断
  if(I2C1->SR2&I2C_BUSY)
		I2cInit();
  I2C1->CR1 |= (I2C_START | I2C_ACK);//发送start条件
}
#endif
#ifdef A04_08RCTC
/********************************************************************************
*函数：A系列读校准数据
*描述：A系列从单片机flash读校准数据
********************************************************************************/
void ReadCalidata_A(void)
{ 
	u8 i;
	u16 * Caliconst;
	Caliconst=(u16*)CaliConstAdress;
	for(i=0;i<100;i++)	//CaliData[i] = CalidataValue[i];//ZCF
    CaliData[i] = *Caliconst++;
	if(CaliData[0]!=0) 
		bCalibrateFlag =1;
} 
#endif
/*******************************************************************************                        
* 函数名  ：读CR,
* 描述    ：前16个保持boot区中读到的值，输入输出CR不从rom中读取而是直接清0
*******************************************************************************/                        
void ReadCR(void)
{ 
  u16 i;
  if(Bx.CR[1]>254)
	{
		Bx.CR[1]=1; //非法地址，置硬地址
		CRSaved =1;
	}
  if((Bx.CR[2]&0xf0)>0x60||(Bx.CR[2]&0x0f)>0x6)
  {
    Bx.CR[2]=0x30;            //非法通信格式，置默认0x30
		CRSaved =1;
  }
  for(i=0;i<CR_AMOUNT;i++)
  {
    if(CR_Att[i].PDSave==0)
    {Bx.CR[i]=0;}
  }
  #if AI_AMOUNT>0 
  Bx.CR[AI_OFFLINE]=0XFF>>(8-AI_AMOUNT);//初始化断线标志位
  for(i=0;i<AI_AMOUNT;i++)
  {    
    if(Bx.CR[AI_RANG_CR+i]>CR_Att[AI_RANG_CR].Rang)//检验信号类型合法性 
    {
      Bx.CR[AI_RANG_CR+i]=3;//设置成默认值
      Bx.CR[15]=6;
			CRSaved =1;
    }
    AiRang[i]=Bx.CR[AI_RANG_CR + i];
    if(Bx.CR[AI_SMPL_CR+i]>CR_Att[AI_SMPL_CR].Rang)//检验采样次数合法性
    {
      Bx.CR[AI_SMPL_CR+i]=5;//设置成默认值
      Bx.CR[15]=6;
			CRSaved =1;
    }
    #ifdef S04_08XA
    AiFilter[i].Times = 2<<Bx.CR[AI_SMPL_CR+i];
    #endif    
    if((signed short)Bx.CR[AI_DGCL_CR+i]>=(signed short)Bx.CR[AI_UGCL_CR+i])//检验上下限的合法性
    {      
      Bx.CR[AI_DGCL_CR+i]=0;//设置成默认值
      Bx.CR[AI_UGCL_CR+i]=32000;
      Bx.CR[15]=6;
			CRSaved =1;
    }
  }
  for(i=0;i<AI_AMOUNT;i++)AiFilter[i].Count=0; //清除已采样次数计数
  #endif
  #if AQ_AMOUNT>0
  for(i=0;i<AQ_AMOUNT;i++)
  {
    if(Bx.CR[AQ_RANG_CR+i]>5)//检验信号类型合法性  
    {
      Bx.CR[AQ_RANG_CR+i]=5;//设置成默认值  
      Bx.CR[15]=6;
			CRSaved =1;
    }
    if(Bx.CR[AQ_DGCL_CR+i]>Bx.CR[AQ_UGCL_CR+i])//检验上下限的合法性
    {
      Bx.CR[AQ_DGCL_CR+i]=0;
      Bx.CR[AQ_UGCL_CR+i]=1000;
      Bx.CR[15]=6;
			CRSaved =1;
    }
  }
  #endif
}

/*******************************************************************************
* 函数名    ：读厂商通讯代码
* 描述      ：读并校验
* 出口      ：无
*******************************************************************************/
void ReadManuCommID(void)
{
	u8 * manuconst;	
	u8 buff[6];
	u16 i;
	
	manuconst=(u8*)ManuConstAdress;
	for(i=0;i<6;i++)	
		buff[i] = *manuconst++;
	
	//计算crc16
	i= CRC16(buff,3);
	if(i==0 && buff[0]!=0x00 && buff[0]!=0xff)
	{
		Bx.ManuCommID = buff[0];
	}
	else
	{
		u16 * m16const;	
		i= CRC16((buff+3),3);
		if(i==0 && buff[3]!=0x00 && buff[3]!=0xff)
		{
			buff[0]=buff[3];
			buff[1]=buff[4];
			buff[2]=buff[5];
			
			//擦除重写
			Wipe(ManuConstAdress,1);
			FLASH->SR|=SR_EOP;//清除 EOP(5)位
			FLASH->CR|=CR_PG; //set the bit PG(0)			
			
			m16const=(u16*)ManuConstAdress;
			for(i=0;i<6;i+=2)			
				*m16const++ = *(u16*)(buff+i);

			while(FLASH->SR&SR_BUSY); //等待 BSY(0)=0 
			FLASH->CR&=~CR_PG; //Reset the bit PG(0)
			FLASH->CR|=CR_LOCK; //上锁
			
			Bx.ManuCommID = buff[0];
			
		}
		else 
		{	
			Bx.ManuCommID = 0xb9;
			//Bx.CR[16] = 1;//报错厂商代码错误
		}
	}
}

/*******************************************************************************
* 函数名    ：页擦除Wipe
* 入口参数  ：页首地址adress ，页数：Page_Num
* 出口参数  ：无
*******************************************************************************/
void Wipe(u32 adress,u8 Page_Num)
{  
  if(adress>=0x08000000&&adress<=0x08010000)
  {
    //开flash锁  LOCK(7)bit   
    if(FLASH->CR&CR_LOCK)
    {                                   
      FLASH->KEYR=0X45670123;
      FLASH->KEYR=0xCDEF89AB;
    }
    //page erase
    while(Page_Num--)
    {
      FLASH->SR|=SR_EOP;  //reset EOP(5)
      FLASH->CR&=~CR_PG;  //Reset the bit PG(0),Reset MER(2)Bit
      FLASH->CR|=0X02;   //set PER(1)bit
      FLASH->AR=adress;    //Page choice
      FLASH->CR|=CR_STRT;   //set STRT(6)bit
      adress+=PageSize;
      while(FLASH->SR&SR_BUSY); //等待 BSY(0)=0 EOP(5)=1;
    }
    FLASH->CR&=~0X06;    //ResetPER(1) MER(2)BIT
  }
}
/*******************************************************************************                        
* 函数名    ：Link灯指示控制
* 描述      ：分三种情况控制：正常，外部电源错误，需返厂的错误。
*******************************************************************************/                        
void LinkLedControl(void)
{  
  /*正常***********************************************************************/
  if(Bx.CR[15]==0)
  {  LinkON=1;
    GPIOB->BSRR=LinkRed;//灭红色
    if(Bx.TimeLinkLed>=(6*BlinkTime))
    {  if(Bx.ParallelAdr==0)GPIOB->BSRR=LinkGreen;//地址未分配，link指示灯暗
      else GPIOB->BRR=LinkGreen;//并口地址已经分配，亮绿色灯。
    }
  }

  /*错误4：配置外部电源硬件中无外部电源****************************************/
  else if(Bx.CR[15]==4)
  {
    if(Bx.TimeErrLed<500)
    {  LinkON=1;
      if(Bx.TimeLinkLed>=(6*BlinkTime))GPIOB->BRR=LinkRed|LinkGreen;
    }
    else if(Bx.TimeErrLed<1000)
    {  LinkON=0;
      GPIOB->BSRR=LinkRed|LinkGreen;  
    }
    else Bx.TimeErrLed=0;
  }
  /*错误3：有需要返厂的错误****************************************************/
  else if(Bx.CR[15]==3 || Bx.CR[15]>=5)
  {
    LinkON=1;
    GPIOB->BSRR=LinkGreen;//灭绿色灯
    if(Bx.TimeLinkLed>=(6*BlinkTime))GPIOB->BRR=LinkRed; //未通信亮红灯
  }  
}
/*******************************************************************************                        
* 函数名    ：LED刷新显示
* 描述      ：
*******************************************************************************/                   
void LedRefresh(void)
{ 
#if defined(H04TC)||defined(H08TC)||defined(H04RC)||defined(H08RC)
  #define AI_BIT  (u8)(0xFF>>(8-AI_AMOUNT))
  #define AQ_BIT  (u8)((0xFF>>(8-AQ_AMOUNT))<<AI_AMOUNT)
  u32 i=0;//j;
  if(PowerSel == SEL_INTERNAL_POWER || ExPower_s==0)//并口供电或存在外部电源
  {
    #if AI_AMOUNT>0
    i=Bx.CR[AI_OFFLINE]^AI_BIT;//亮AI灯，断线取反
    #endif
		
    #if AQ_AMOUNT>0     
    i|=AQ_BIT; //亮AQ灯
    #endif
  }
  GPIOC->BSRR = ((i<<16)|i) ^ (AQ_BIT | AI_BIT);
#endif
#if	defined(A04TC)||defined(A08TC)||defined(A04RC)
	if(PowerSel == SEL_INTERNAL_POWER || ExPower_s==0)//并口供电或存在外部电源
	{
		#if defined(A04RC)||defined(A04TC)
			if(Bx.CR[AI_OFFLINE]&(1<<0)) GPIOA->BSRR=1<<9;//PA9
			else GPIOA->BRR=1<<9;
			if(Bx.CR[AI_OFFLINE]&(1<<1)) GPIOA->BSRR=1<<8;//PA8
			else GPIOA->BRR=1<<8;
			if(Bx.CR[AI_OFFLINE]&(1<<2)) GPIOC->BSRR=1<<9;//PC9
			else GPIOC->BRR=1<<9;
			if(Bx.CR[AI_OFFLINE]&(1<<3)) GPIOC->BSRR=1<<8;//PC8
			else GPIOC->BRR=1<<8;
			GPIOC->BSRR=0x33<<6;//PC6\7\10\11不亮
		#endif
		#ifdef A08TC
			if(Bx.CR[AI_OFFLINE]&(1<<0)) GPIOA->BSRR=1<<9;//PA9
			else GPIOA->BRR=1<<9;
			if(Bx.CR[AI_OFFLINE]&(1<<1)) GPIOA->BSRR=1<<8;//PA8
			else GPIOA->BRR=1<<8;
			if(Bx.CR[AI_OFFLINE]&(1<<2)) GPIOC->BSRR=1<<9;//PC9
			else GPIOC->BRR=1<<9;
			if(Bx.CR[AI_OFFLINE]&(1<<3)) GPIOC->BSRR=1<<8;//PC8
			else GPIOC->BRR=1<<8;
			if(Bx.CR[AI_OFFLINE]&(1<<4)) GPIOC->BSRR=1<<10;//PC10
			else GPIOC->BRR=1<<10;
			if(Bx.CR[AI_OFFLINE]&(1<<5)) GPIOC->BSRR=1<<11;//PC11
			else GPIOC->BRR=1<<11;
			if(Bx.CR[AI_OFFLINE]&(1<<6)) GPIOC->BSRR=1<<7;//PC7
			else GPIOC->BRR=1<<7;
			if(Bx.CR[AI_OFFLINE]&(1<<7)) GPIOC->BSRR=1<<6;//PC6
			else GPIOC->BRR=1<<6;
		#endif
	}
		
#endif
}		
/*******************************************************************************                        
* 函数名    ：拨码地址状态扫描，一次扫描薄码开关的一个位，结果更新于CR01H内
* 描述      ：
*******************************************************************************/                        
void SwitchScan(void)
{
  #if (AI_AMOUNT+AQ_AMOUNT)==8
  u8 i;
  i=((~(GPIOC->IDR>>8))&0x0F);
	#if defined(H08RC)
  if(Bx.CR[1]<16)Bx.CR[1]=i;
	#endif
  #endif
  if(Bx.CR[1]==0)Bx.CR[1]=1;
}
/*******************************************************************************                        
* 函数名  ：外部电源检测
* 描述    ：
*******************************************************************************/                        
void PowerQuery(void)
{	
  u8 temp1; 

  /*模块供电电源选择，调试用***************************************************/
  #ifdef SHOW_POWERSTATUS
  if(Bx.CR[CR_AMOUNT-1]==1)PowerSel = SEL_EXTERNAL_POWER;
  else if(Bx.CR[CR_AMOUNT-1]==0)PowerSel = SEL_INTERNAL_POWER;
  #endif

  /*模块电源选择***************************************************************/
  if(PowerSel == SEL_EXTERNAL_POWER)GPIOD->BRR=Vctr;   //控制为外部24v供电
	else GPIOD->BSRR=Vctr;           //控制为并口供电

  /*外部电源检测采样***********************************************************/
  temp1 = (GPIOC->IDR&ExPWR)>>14; //读取当前ExPwr状态
	if(temp1^ExPower_last)
  {
    ExPower_t=1;
  }																																							
	ExPower_last=temp1;																													
																																							
  /*定时20ms到，更新采样结果***************************************************/
	if(Bx.TimeExPower>=100)																											
	{																																						
    Bx.TimeExPower=0; //重新计时																								
		if(ExPower_t==0)  //计时时间内，管脚状态未改变才更新检测结果
		{	
      //调试用，在CR145中显示ExPower_last
      #ifdef SHOW_POWERSTATUS
			Bx.CR[CR_AMOUNT-2]=ExPower_last^0x01; 			
      #endif
      
      ExPower_s=ExPower_last;      
			//若控制为外部电源，但未检测到外部电源，错误码写4			   
			if(PowerSel == SEL_EXTERNAL_POWER && ExPower_s == NO_EXPOWER)  
			{				
				if(Bx.CR[15]==0)Bx.CR[15]=4;//不覆盖其他的错误
			}
			else
			{
				if(Bx.CR[15]==4)Bx.CR[15]=0;//不覆盖清除其他的错误
			}
		}
		ExPower_t=0;	  //清翻转位
	}
}
/*******************************************************************************                        
* 函数名    ：掉电状态扫描
* 描述      ：PwrDN状态:0和非0
*******************************************************************************/                        
void PwrDNScan(void)
{
  u8 temp1,temp2;
  if(GPIOB->IDR&PwrDN)temp1=1;//掉电
  else temp1=0;//供电正常
  temp2=PwrDN_last;
  if(temp1^temp2)PwrDN_t=1;//状态发生变化
  PwrDN_last=temp1;//保存上次采样结果
  //定时到点采样结果更新
  if(Bx.TimePwrDN>=25)
  {
    Bx.TimePwrDN=0;
    if(PwrDN_t==0)PwrDN_s=PwrDN_last;//更新外部电源检测脚滤波结果
    PwrDN_t=0;//清翻转位
  } 	
//  if(bPwrDNErr==0 && PwrDN_s)//PwrDN_s==1 ，即temp1=1 掉电 ，
//  {  

//    if(CRSaved==0)
//    {
//			NVIC->ICER[EXTI15_10_IRQChannel>>5]=((u32)1)<<(EXTI15_10_IRQChannel&0x1f);//关中断使能
//			GPIOD->BRR=Vctr;            //设置为外部电源供电			
//      SaveCR();
//      CRSaved=1;                    //写1，表示已经保存数据；
//      Start();
//    }
//  }
//	else
//	{
//		if(CRSaved)
//		{
//			NVIC->ISER[EXTI15_10_IRQChannel>>5]=((u32)1)<<(EXTI15_10_IRQChannel&0x1f);//使能中断
//			GPIOD->BSRR=Vctr;            //设置为内部电源供电		
//			CRSaved=0;
//		}
//	}
	if(bPwrDNErr && PwrDN_s==0)bPwrDNErr=0;//上电检测PwrDN异常，但检测到30ms低电平		
	if(bPwrDNErr && (Bx.CR[15] &0x00ff)==0) Bx.CR[15]=(Bx.CR[15] &0xff00) |5;//PwrDN报错
	if((Bx.CR[15] &0x00ff)==5 && PwrDN_s==0)Bx.CR[15] &= 0xff00;//boot报错未清，在应用清除
}
/*******************************************************************************                        
* 函数名  ：保存CR数据
* 描述    ：CR区数据保存到flash内。当外部检测到掉电调用此程序，把SRAM内的数据
             保存到falsh内 。
*******************************************************************************/                        

void SaveCR(void)
{  
  u16 *rom_adr,i,k=0;    
  /*CR内容是否改变*/
  rom_adr=(u16*)CRAdress;
  for(i=0;i<CR_AMOUNT;i++)
  {  
    if(CR_Att[i].PDSave==1 && Bx.CR[i]!=*(rom_adr+i))//要掉电保持的数据的值是否变化
    {
      k=1;
      break;
    }
  } 
  if(k)
  {
    //开flash锁  LOCK(7)bit   
    if(FLASH->CR&CR_LOCK)
    {
      FLASH->KEYR=0X45670123;
      FLASH->KEYR=0xCDEF89AB;
    }
    /*页擦除*/
    FLASH->SR|=SR_EOP;    //reset EOP(5)
    FLASH->CR&=~CR_PG;    //Reset the bit PG(0),Reset MER(2)Bit
    FLASH->CR|=0X02;      //set PER(1)bit
    FLASH->AR=CRAdress;   //Page choice
    FLASH->CR|=CR_STRT;   //set STRT(6)bit
    while(FLASH->SR&SR_BUSY); //等待 BSY(0)=0 EOP(5)=1;
//    if(FLASH->CR&0X06)
    FLASH->CR&=~0X06;     //ResetPER(1) MER(2)BIT
    
    /***重写***/
    while(FLASH->SR&SR_BUSY);//check the BSY(0)
    FLASH->SR|=SR_EOP;    //清除 EOP(5)位
    FLASH->CR&=~0X06;     //ResetPER(1) MER(2)BIT
    FLASH->CR|=CR_PG;     //set the bit PG(0)
    rom_adr=(u16*)(CRAdress);
    for(i=0;i<CR_AMOUNT;i++)
    {
      if(CR_Att[i].PDSave==1)*rom_adr = Bx.CR[i];
      else if(CR_Att[i].PDSave==0) *rom_adr = 0;//不需要掉电保持的清0
      rom_adr++;
    }
    while(FLASH->SR&SR_BUSY); //Wait BSY(0)to be reset
    FLASH->CR&=~CR_PG;      //Reset the bit PG(0)    
    FLASH->CR|=CR_LOCK;       //上锁  
  }
}
/*******************************************************************************                        
* 函数名  ：外部中断2
* 描述    ：
*******************************************************************************/                        
void A_EXTI2_IRQHandler(void)
{
  EXTI->PR|=EXTI2;	
	if(CRSaved==0)return;	
	
	GPIOD->BRR=Vctr;
		
	SaveCR();
	CRSaved=0;
	
	GPIOD->BSRR=Vctr;
  
}

/*******************************************************************************                        
* 函数名   ：双74HC595移位
* 描述     ：双74HC595移位，SHcp和STcp上升沿有效，SCLK时钟周期为1us
* 入口参数 ：待移位的16位数data
*******************************************************************************/                        
void Shift595(u16 data)
{
  u8 i;  
  GPIOB->CRH=(GPIOB->CRH&0X00FFFFFF)|0X38000000;//PB14输入，PB15输出
  GPIOB->BRR=STcp;   //PB12=0
  for(i=0;i<ShiftNum;i++)
  {
    if(data&(1<<(ShiftNum-1)))GPIOB->BSRR=MOSI; //PB15=1
    else GPIOB->BRR=MOSI;												//PB15=0
    delay(3);//延时使时钟周期为1us
    GPIOC->BRR=SHcp;    //PC15=0
    data=data<<1;
    delay(5);
    GPIOC->BSRR=SHcp;
  }  
  GPIOB->BSRR=STcp;
  GPIOB->CRH=(GPIOB->CRH&0X00FFFFFF)|0Xb8000000;
}
/*******************************************************************************
* 函数名  ：延时
* 描述    ：延时times个机器周期,延时时间T=84*Times+42（ns）
*******************************************************************************/
void delay(u32 times)
{
  while(times--);
}
/*******************************************************************************                        
* 函数名  ：I2C中断处理程序
* 描述    ：
*******************************************************************************/                        

void A_I2C1_EV_IRQHandler(void)
{
  u16 SR1;
  SR1=I2C1->SR1;
  
  /************读数据************/
  if(I2C.Cmd == 0)
  {
    if(SR1 & I2C_SR_SB)
    {
      u8 i;
      i=(I2C.StartAdress>>7)&0x6;//页地址高2位  
      if(I2C.TxCount == 0)I2C1->DR = (ADR_24C08|i)&0xFE;//写24c08地址，LSB置低进入发送器模式
      else I2C1->DR = (ADR_24C08|i)|0x01;//第二个START,写24c08地址，LSB置高进入接收器模式
    }
    if(SR1&I2C_SR_ADDR)
    {
      Bx.UpdBuffer[0]=I2C1->SR2;//读SR2清除ADDR中断
    }
    
    if(SR1&I2C_TXE)
    {
      if(I2C.TxCount == 0)I2C1->DR=I2C.StartAdress;//24C08数据的起始地址
      else if(I2C.TxCount == 1)I2C1->CR1 |= I2C_START;
      I2C.TxCount++;      
    }
    if(SR1 & I2C_RXNE)
    { 
      *I2C.CaliDataAdr++ = I2C1->DR ;
      I2C.RxCount++; 
      //收到的最后第二个字节后写入STOP为1，收到最后一个字节后产生停止条件，
      if(I2C.RxCount==(I2C.Lenght-1))
      {  
        I2C1->CR1 &= ~I2C_ACK ;
        I2C1->CR1 |=  I2C_STOP;          
      }
      else if(I2C.RxCount >= I2C.Lenght)
      {  
        I2C1->CR1 &= ~I2C_PE ;  
        I2C1->CR2 &= ~(I2C_ITEVTEN | I2C_ITBUFEN); //关中断     
      }
    }
  }
  /*************写数据*****************/
  else if(I2C.Cmd == 1 || I2C.Cmd == 2)
  {    
    if(SR1 & I2C_SR_SB)
    {
      u8 i;
      i=(I2C.StartAdress>>7)&0x6;//页地址高2位
      I2C1->DR = (ADR_24C08|i)&0xFE;//写24c08地址，LSB拉低进入发送器模式
    }
    if(SR1&I2C_SR_ADDR)
    {
      Bx.UpdBuffer[0]=I2C1->SR2;//读SR2清除ADDR中断
    }
    if(SR1 & I2C_TXE )
    {
      if(I2C.TxCount == 0)I2C1->DR=I2C.StartAdress;//起始地址             
      else if (I2C.TxCount < 17) I2C1->DR = *I2C.CaliDataAdr++;
      else
      {
        I2C1->CR1 |= I2C_STOP;//写STOP位，STOP条件将在当前字节发送结束后送出。
        I2C1->CR2 &= ~(I2C_ITEVTEN | I2C_ITBUFEN); //关中断     
        I2C1->CR1 &= ~I2C_PE ;//关I2C
        if(I2C.Lenght >=16)I2C.Lenght -=16;
        else I2C.Lenght =0;
        I2C.StartAdress+=16;
        I2C.Time=0;
      }
      I2C.TxCount++;  
    }
//    if(SR1&0X400)Bx.CR[15]=3;//查询应答错误
  }
}

void EXTI9_5_IRQHandler(void)
{
	EXTI9_5_IRQHandler_lora();

}
void DMA2_Channel1_IRQHandler(void)
{
  DMA2_Channel1_IRQHandler_lora();
}
