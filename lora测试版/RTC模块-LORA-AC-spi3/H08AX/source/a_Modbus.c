#include "config_08AX.h"

/*******************************************************************************
* 函数声明 main
*******************************************************************************/
u8  HexToAsc(u8 HexDa);
u8  AscToHex(u8 AscDa);
u16 CRC16(vu8 *CRCdata,u16 CRCLen);
u8  LRC(vu8 *LRCdata,u16 LRCLen);  
u8  Write_CR(u16 CRNum,u16 Value); 
void USART1_TX(u8 com_adress);
void TXend(void);
void ProtocolSet(u8 pset);
void ModbusError(u8 ErrorCode);
void ModbusDispose(void);
void FunCode01(void);
void FunCode03(void);
void FunCode05(void);
void FunCode06(void);
void FunCode15(void);
void FunCode16(void);
extern void FunCode47(void);
extern void FunCode66(void);
extern void FunCode6A(void);
extern void FunCode6E(void);
extern void FunCode69(void);
void WriteCaliConst(void);
#ifdef H04_08RCTC 
extern void Write24C08(u16 cmd,u16 start_adr,u16 lenght, u8 *calidata_adr);
#endif
extern void EnRdWrProtection(unsigned char action);
extern s32 Linear(s32 x,s32 x0,s32 x1, s32 y0,s32 y1);
extern void Wipe(u32 adress,u8 Page_Num);
/******************************************************************************
* 变量声明
******************************************************************************/
const  u16 Baudrate[7]={0x7530,0x3A98,0x1D4C,0x0EA6,0x753,0X04E2,0x0271};
const  u16 FrameDealy[8]={1604,802,401,200,100,67,33,33};  //一个帧接收完成后延时3.5个字符时长。
extern BootVariable Bx;
struct ModWrCaliType ModWrCali;
extern vu16 CaliData[];
#if defined(H04RC)||defined(H08RC)||defined(A04RC)
extern vu8 DispCode;
#endif

void Radio_TX(u8 com_adress);
///*******************************************************************************
//* 函数名    ：Modbus查询处理
//* 描述      ：若USART1帧接收完成，检验数据帧的错误，无误则进入帧解析
//*******************************************************************************/
//void ModbusQuery(void)
//{
//  if(ModReFinsh)
//  {
//    u8 com_adr = Bx.ComBuffer[0];
//    /*只接收处理三类地址：自身地址，广播地址(0x00,0xff)*/
//    if(com_adr == Bx.CR[1] || com_adr == 0xFF || com_adr == 0x00)  
//    {
//      /*接收字节过程中的报错*/ 
//      if(Bx.ModErr)ModbusError(Bx.ModErr);
//      else
//      {
//        /*长度错误*/
//        if(Bx.RXcount<4||Bx.RXcount>(ComMaxLen*2+9))ModbusError(4);
//        else
//        {  
//          /*RTU帧*/
//          if(RtuOrAscii==RTU)
//          { 
//            if(CRC16(Bx.ComBuffer,Bx.RXcount))ModbusError(9);//CRC校验
//            else ModbusDispose();//进入Modbus功能码解释
//          }

//          /*ASCII帧*/
//          else
//          {
//            u16 lrc_result;            
//            lrc_result=LRC(Bx.ComBuffer,Bx.RXcount-2);
//            if(lrc_result!=Bx.ComBuffer[Bx.RXcount-2]) ModbusError(9);
//            else ModbusDispose();  
//          }
//        }   
//      }
//    }  
//    Bx.RXcount=0;
//    Bx.RXAscCount=0;
//    Bx.ModErr=0;
//    ModReFinsh=0;
//  }
//}
///*******************************************************************************
//* 函数名    ：启动Modbus发送
//* 描述      ：USART1 启动发送,添加校验结果，发送第一字节数据。设置comd_adress是
//              因为如果CR内地址被修改的情况，当次回复仍然回复修改前的地址。
//* 入口参数  ：com_adress：回复的地址
//*******************************************************************************/
void USART1_TX(u8 com_adress)
{
//  u32 i;
//  Bx.ComBuffer[0]=com_adress;
//  GPIOA->BSRR=sp485_DRE; //切换至发送PA8

//  /*RTU帧模式******************************************************************/
//  if(RtuOrAscii==RTU)
//  {
//    /*添加CRC校验结果*/
//    i=CRC16(Bx.ComBuffer,Bx.TXDaLen);
//    Bx.ComBuffer[Bx.TXDaLen++]=(i>>8);
//    Bx.ComBuffer[Bx.TXDaLen++]=i&0x00ff;

//    //发送第一个字节
//    USART1->DR=Bx.ComBuffer[0];
//    Bx.TXcount=1;
//  }

//  /*ASCII帧模式****************************************************************/
//  else
//  {
//    /*添加LRC校验结果*/
//    i=LRC(Bx.ComBuffer,Bx.TXDaLen);
//    Bx.ComBuffer[Bx.TXDaLen]=i;
//    Bx.TXDaLen++;
//    //发送首字节0X3A&0x80
//    USART1->DR=0xBA;
//    Bx.TxAscCount=1;
//    Bx.TXcount=0;
//  }
//  //开发送TXEIE中断，关接收中断  
//  USART1->CR1=(USART1->CR1&(~USART_CR1_RXNEIE)) | USART_CR1_TXEIE;
//  //通信指示灯闪烁
//  if(Bx.TimeLinkLed>=(6*BlinkTime))Bx.TimeLinkLed=0;
 // else ReLinkLedSig=1;
}
/*******************************************************************************
* 函数名    ：通讯格式设置
* 描述      ：设置串口通信的字符格式，波特率，结束延时，
* 入口参数  ：pset：通讯格式
*******************************************************************************/

void ProtocolSet(u8 pset)
{  
//  u32 i,j,m=0,n=0;
//  i=pset&0x0f;//低半字节：帧字符格式
//  j=pset>>4;//高半字节：波特率
//  if(j<0x7&&i<0x7)
//  {    
//    if(i<3||i==6)RtuOrAscii = RTU;
//    else  RtuOrAscii = ASCII;
//    USART1->BRR = Baudrate[j];
//    TIM2->ARR = FrameDealy[j];//帧结束3.5字节定时时间设置
//    TIM2->PSC=719;
//    TIM2->CR1&=~0x6;
//    TIM2->EGR|=0x1; //触发更新

//    //字长M(12) 奇偶校验pce(10)ps(9)停止位长度stop(12,13),CPOL(10)=1;
//    switch(i)
//    { 
//      case 0://RTU  N,8,2
//             n=0X2000;break;
//      case 1://RTU  E,8,1
//             m=0X1400;break;
//      case 2://RTU  O,8,1
//             m=0X1600;break;
//      case 3://ASCII  N,7,2
//                      break;
//      case 4://ASCII  E,7,1
//             m=0X0400;break;
//      case 5://ASCII  O,7,1          
//             m=0X0600;break;
//      case 6://RTU N,8,1  
//                      break;
//      default:
//                      break;
//    }
//    USART1->CR1=(~0X1600&USART1->CR1)|m;
//    USART1->CR2=(~0X3000&USART1->CR2)|n;
//  }
}
/*******************************************************************************
* 函数名    ：定时器TIME2 中断函数
* 描述      ：3.5个字节延时：modbus通信一帧数据接收完毕后延时。
              2400-115200分别为：16.04ms，8.02ms，4.01ms，2ms，1ms，668us，334us
              AscII模式中帧超时计时
* 入口参数  ：无
*******************************************************************************/
void A_TIM2_IRQHandler(void)
{ 
  if(TIM2->SR&TIM2_SR_UIF)
  {
    ModReFinsh=1;
    TIM2->CR1&=~0X01;//关定时器
    TIM2->DIER&=~0X01;//关更新中断UIE(0)
    TIM2->SR&=~0X01;//清除中断标志UIF(0)=0  
    if(RtuOrAscii && TIM2->ARR==10000)Bx.ModErr=0x07;//错误7 ASCii帧接收超时1s 
  }
}
/******************************************************************************
* 函数名    ：USART1中断事件
* 描述      ：USART1中断处理函数
******************************************************************************/
void A_USART1_IRQHandler(void)
{
//  u32 USART1_SR,temp0,rx_data;
//  USART1_SR=USART1->SR;

//  /**************接收中断******************************************************/
//  if ((USART1_SR&USART_SR_RXNE) && (USART1->CR1&USART_CR1_RXNEIE))
//  {
//    rx_data=USART1->DR;//读DR可清除错误标志位，
//    //1，上次接收数据已处理；2，无以下错误：校验错误;噪声错误;帧错误(已删此判断)
//    if(ModReFinsh==0 && (USART1_SR&(USART_SR_PE|USART_SR_NE))==0)
//    {
//      /*RTU帧模式***************************************************************/
//      if(RtuOrAscii==0)
//      { 
//        Bx.ComBuffer[Bx.RXcount]=rx_data;           
//        //超过127字节不再接收数据，并报错 
//        if(Bx.RXcount<=(ComMaxLen*2+9))Bx.RXcount++;          
//        else Bx.ModErr=0x04;
//      }

//      /*ASCII帧模式*************************************************************/
//      else
//      {
//        rx_data=rx_data&0x7f;//对数据最高位清零
//        //判断首字节3A
//        if(Bx.RXAscCount==0&&rx_data==0x3a)
//        {
//            Bx.RXAscCount=1; 
//            Bx.RXcount=0;      
//        }
//        else
//        {  
//          TIM2->ARR=10000;//超时延时10us*10000=100ms
//          if(rx_data == 0x0d) Bx.ComBuffer[Bx.RXcount++]=rx_data; //判断结束符0x0D
//          else if(rx_data==0x0a)//判断结束字符0x0A
//          {
//            Bx.ComBuffer[Bx.RXcount]=rx_data;
//            //判断结束符0D OA
//            if(Bx.ComBuffer[Bx.RXcount-1]==0x0D)
//            {
//              temp0=(Bx.CR[2]>>4)&0x07;
//              TIM2->ARR=FrameDealy[temp0];//延时3.5字符时长后进入帧解析
//            }
//            else Bx.ModErr=0x05;              
//          }
//          else
//          {
//            //超出最大字节
//            if(Bx.RXcount>(ComMaxLen*2+9))  Bx.ModErr=0x04;
//            else
//            {
//              /*合法ascii不处理，非法ASCCII码报5号错*/
//              if(  (rx_data >= 0x30 && rx_data <=0x39)     //"0"-"9"
//                || (rx_data >= 0x41 && rx_data <=0x46)     //"A"-"F"
//                || (rx_data >= 0x61 && rx_data <=0x66)){}  //"a"-"f"
//              else Bx.ModErr=0x05;              
//              
//              /*接收ASCCII字低半字节*/
//              if(Bx.RXAscCount&0x01)Bx.ComBuffer[Bx.RXcount]=AscToHex(rx_data);
//              
//              /*接收ASCCII字高半字节*/
//              else
//              {
//                rx_data=AscToHex(rx_data);
//                temp0=Bx.ComBuffer[Bx.RXcount]<<4;
//                Bx.ComBuffer[Bx.RXcount]=rx_data|temp0;
//                Bx.RXcount++;
//              }
//            }
//          }
//          Bx.RXAscCount++;
//        }
//      } 
//    }
//    /*重置计时器*/
//    TIM2->CNT=0X00;//清除当前值         
//    TIM2->SR=0X0;  //清除中断标志UIF(0)=0
//    TIM2->DIER|=0X01;//使能更新中断UIE(0)
//    TIM2->CR1|=0X01;//使能TIM2定时器器CEN(0)=1;
//  }  

//  /*发送中断*******************************************************************/
//  if((USART1_SR&USART_SR_TXE)&&(USART1->CR1&USART_CR1_TXEIE))
//  {

//    /*RTU帧模式****************************************************************/
//    if(RtuOrAscii==RTU)
//    {
//      USART1->DR = Bx.ComBuffer[Bx.TXcount++]; 
//      //最后一字节,关缓存空中断
//      if(Bx.TXcount == Bx.TXDaLen) 
//      {
//        USART1->CR1 = (USART1->CR1 & (~USART_CR1_TXEIE)) | USART_CR1_TCIE;
//      }
//    } 
//    
//    /*ASCII帧模式 *************************************************************/
//    else
//    {
//      /*ASCII帧的高位发送*/
//      if(Bx.TxAscCount&0x01)
//      {
//        if(Bx.TXcount==Bx.TXDaLen)USART1->DR=0X8D;//发送结束符0D,最高位置位故为8D    
//        else  USART1->DR=0x80|(HexToAsc(Bx.ComBuffer[Bx.TXcount]>>4));//发送高四位
//      }

//      /*ASCII帧低位发送*/
//      else
//      {
//        if(Bx.TXcount == Bx.TXDaLen)
//        {
//          USART1->DR=0X8A;//发送结束符“0A”最高位置位故为8A
//          USART1->CR1 = (USART1->CR1 & (~USART_CR1_TXEIE)) | USART_CR1_TCIE;
//        }
//        else
//        {
//          temp0=HexToAsc(0x0f&Bx.ComBuffer[Bx.TXcount]);
//          USART1->DR=temp0|0x80;
//          Bx.TXcount++;
//        }
//      }
//      Bx.TxAscCount++;
//    } 
//  }
//  /*发送结束中断***************************************************************/
//  if((USART1_SR&USART_SR_TC)&&(USART1->CR1&USART_CR1_TCIE))
//  {
//    //关发送完成中断TXCIE(6)。开接收中断RXEIE(5)
//    USART1->CR1=(USART1->CR1 & (~USART_CR1_TCIE) ) | USART_CR1_RXNEIE;
//    GPIOA->BRR=sp485_DRE;//设置PA8为RX状态

//    //如果波特率被修改则修改波特率
//    if(ProtocolChg)
//    {
//      ProtocolSet(Bx.CR[2]);
//      ProtocolChg=0;
//    }
//    ModbusReOk=1;//帧发送完成，置允许跳转位
//  }
}  
///*******************************************************************************
//* 函数名    ：把十六进制转为ASCII
//* 描述      ：把十六进制转为ASCII
//* 入口参数  ：hex_data：十六进制数
//* 出口参数  ：hex_data：转换后的ASCII结果
//*******************************************************************************/
//u8 HexToAsc(u8 hex_data)
//{
//  if(hex_data<10)hex_data+=0x30;
//  else    hex_data+=0x37;
//  return hex_data;
//}

///*******************************************************************************
//* 函数名    ：把ASCII码转换为十六进制
//* 描述      ：把ASCII码转换为十六进制
//* 入口参数  ：asc_data：ASCII数
//* 出口参数  ：asc_data：转换后的十六进制结果
//*******************************************************************************/
//u8 AscToHex(u8 asc_data)
//{
//  if(asc_data<0X40)      asc_data-=0x30;//'0'-'9'
//  else if(asc_data<0x47) asc_data-=0x37;//'A'-'F'
//  else                asc_data-=0x57;//'a'-'f'
//  return asc_data;
//}

//const u8 table_h[]={                    // crc高位字节值表
//    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41,  
//    0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 
//    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1,0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41,  
//    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1,0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41,  
//    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40,  
//    0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1,0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40,  
//    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40,  
//    0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40,  
//    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 
//    0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41,  
//    0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40,  
//    0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1,0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 
//    0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,0x80, 0x41, 0x00, 0xc1, 0x81, 0x40  };

//const u8 table_l[]={                    //crc低位字节值表
//    0x00, 0xc0, 0xc1, 0x01, 0xc3, 0x03, 0x02, 0xc2, 0xc6, 0x06,0x07, 0xc7, 0x05, 0xc5, 0xc4, 0x04, 0xcc, 0x0c, 0x0d, 0xcd,
//    0x0f, 0xcf, 0xce, 0x0e, 0x0a, 0xca, 0xcb, 0x0b, 0xc9, 0x09,0x08, 0xc8, 0xd8, 0x18, 0x19, 0xd9, 0x1b, 0xdb, 0xda, 0x1a,
//    0x1e, 0xde, 0xdf, 0x1f, 0xdd, 0x1d, 0x1c, 0xdc, 0x14, 0xd4,0xd5, 0x15, 0xd7, 0x17, 0x16, 0xd6, 0xd2, 0x12, 0x13, 0xd3,  
//    0x11, 0xd1, 0xd0, 0x10, 0xf0, 0x30, 0x31, 0xf1, 0x33, 0xf3,0xf2, 0x32, 0x36, 0xf6, 0xf7, 0x37, 0xf5, 0x35, 0x34, 0xf4,
//    0x3c, 0xfc, 0xfd, 0x3d, 0xff, 0x3f, 0x3e, 0xfe, 0xfa, 0x3a,0x3b, 0xfb, 0x39, 0xf9, 0xf8, 0x38, 0x28, 0xe8, 0xe9, 0x29,  
//    0xeb, 0x2b, 0x2a, 0xea, 0xee, 0x2e, 0x2f, 0xef, 0x2d, 0xed,0xec, 0x2c, 0xe4, 0x24, 0x25, 0xe5, 0x27, 0xe7, 0xe6, 0x26, 
//    0x22, 0xe2, 0xe3, 0x23, 0xe1, 0x21, 0x20, 0xe0, 0xa0, 0x60,0x61, 0xa1, 0x63, 0xa3, 0xa2, 0x62, 0x66, 0xa6, 0xa7, 0x67,  
//    0xa5, 0x65, 0x64, 0xa4, 0x6c, 0xac, 0xad, 0x6d, 0xaf, 0x6f,0x6e, 0xae, 0xaa, 0x6a, 0x6b, 0xab, 0x69, 0xa9, 0xa8, 0x68,  
//    0x78, 0xb8, 0xb9, 0x79, 0xbb, 0x7b, 0x7a, 0xba, 0xbe, 0x7e,0x7f, 0xbf, 0x7d, 0xbd, 0xbc, 0x7c, 0xb4, 0x74, 0x75, 0xb5,  
//    0x77, 0xb7, 0xb6, 0x76, 0x72, 0xb2, 0xb3, 0x73, 0xb1, 0x71,0x70, 0xb0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,  
//    0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9c, 0x5c,0x5d, 0x9d, 0x5f, 0x9f, 0x9e, 0x5e, 0x5a, 0x9a, 0x9b, 0x5b,  
//    0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4b, 0x8b,0x8a, 0x4a, 0x4e, 0x8e, 0x8f, 0x4f, 0x8d, 0x4d, 0x4c, 0x8c,  
//    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42,0x43, 0x83, 0x41, 0x81, 0x80, 0x40  };

///*******************************************************************************
//* 函数名    ：CRC16校验
//* 描述      ：查表法CRC16校验
//* 入口参数  ：*CRCdata数组指针，CRCLen：校验长度
//* 出口参数  ：校验结果16bit，
//*******************************************************************************/
//u16 CRC16( vu8 *CRCdata,vu16 CRCLen)
//{
//  u8 CRCHi=0xff;
//  u8 CRCLo=0xff;
//  u8 CRCIndex;
//  while(CRCLen--)
//  {
//    CRCIndex=CRCHi^*CRCdata++;
//    CRCHi=CRCLo^table_h[CRCIndex];
//    CRCLo=table_l[CRCIndex];
//  }
//  return (CRCHi<<8|CRCLo);
//}
///*******************************************************************************
//* 函数名    ：LRC校验
//* 描述      ：LRC校验
//* 入口参数  ：*LRCdata数组指针，LRCLen：校验长度
//* 出口参数  ：LRCresult校验结果8bit，
//*******************************************************************************/
//u8 LRC(vu8 *LRCdata,vu16 LRCLen)
//{
//  u8 LRCresult=0;
//  u16 i;
//  for(i=0;i<LRCLen;i++)LRCresult+=LRCdata[i];
//  LRCresult=~LRCresult;
//  LRCresult++;
//  return LRCresult;
//}

///*******************************************************************************
//* 函数名    ：modbus错误码回复函数
//* 描述      ：LRC校验
//* 入口参数  ：Errorcode错误号
//* 出口参数  ：无
//*******************************************************************************/
//void ModbusError(u8 ErrorCode)
//{ 
//  //若地址为0不回复
//  if(Bx.ComBuffer[0])
//  {  
//    Bx.ComBuffer[1]+=0x80;
//    Bx.ComBuffer[2]=ErrorCode; 
//    Bx.TXDaLen=3;    
//    Radio_TX(Bx.CR[1]);//USART1_TX(Bx.CR[1]);
//  }
//}
///*******************************************************************************
//* 函数名    ：Modbus功能码解释
//* 描述      ：功能码跳转
//*******************************************************************************/
//void ModbusDispose(void)
//{ 
//   switch(Bx.ComBuffer[1])
//   {//case 0x01:  FunCode01();  break;
//    //case 0x02:  FunCode01();  break;
//    case 0x03:  FunCode03();  break;
//    case 0x04:  FunCode03();  break;
//    //case 0x05:  FunCode05();  break;
//    case 0x06:  FunCode06();  break;
//    //case 0x0f:  FunCode15();  break;
//    case 0x10:  FunCode16();  break;
//    case 0x47:  FunCode47();  break;
//    case 0x66:  FunCode66();  break;
//    case 0x69:  FunCode69();  break;
//    case 0x6c:  
//                WriteCaliConst();//写校准参数 
//                break;
//    case 0x6A:  FunCode6A();    break; 
//    case 0x6E:  FunCode6E();    break; 		
//    default :  ModbusError(1);//功能码错误
//    }
// }
///*******************************************************************************
// *  功能码01
// *  读位状态:功能码01：(0)地址+(1)功能码+(2)地址H+(3)地址L+(4)数量h+(5)数量l+CRC_h+CRC_l  
// *  回复：站号+功能码+字节数+字节1+字节2+...+校验  
// *  入口参数：无  
// *  出口参数：无  
//********************************************************************************/
//void FunCode01(void)
//{
////广播地址0不处理
//if(Bx.ComBuffer[0])
//{  if(Bx.RXcount!=8) ModbusError(4);  //长度错误
//  else             
//  {  if(Bx.ComBuffer[2]!=0||Bx.ComBuffer[3]<0x10)ModbusError(2); //地址错误
//    else
//    {  u16 temp1;    
//      temp1=Bx.ComBuffer[4]*256+Bx.ComBuffer[5];
//      if(temp1>128||temp1==0)  ModbusError(3);  //数值错误
//      else
//      {    u16 temp0;
//        temp0=temp1+Bx.ComBuffer[3]-1;
//        if(temp0>=0X90)  ModbusError(2);   //地址超出
//        else
//        {  u8 a,c,d,i,j;
//          c=Bx.ComBuffer[3];
//          d=3;
//          //填写需回复的全部字节
//          a=temp1/8;
//          if(temp1%8)a++;
//          for(i=0;i<a;i++)
//          {  u8 data=0;
//            for(j=0;j<8;j++)data+=Bx.CR[c++]<<j;
//            Bx.ComBuffer[d++]=data;
//          }
//          //清除除多余位
//          if(temp1%8)
//          {  a=temp1%8;
//            c=0xff<<a;
//            Bx.ComBuffer[d-1]&=~c;
//          }  
//           Bx.ComBuffer[2]=d-3;
//          //回复帧长度(不含CRC)
//          Bx.TXDaLen=d;
//          Radio_TX(Bx.CR[1]);//USART1_TX(Bx.CR[1]);            
//        }  
//      }  
//    }    
//  }  
//}
//}
///*******************************************************************************
// *  功能码03
// *  读多寄存器：站号+功能码+寄存器地址*2+00+寄存器数量+检验位*2  
// *  回复：站号+功能码+返回的字节数+第一个值高位+第一个值低位+......+CRC校验*2
// *  入口参数：无  
// *  出口参数：无  
//********************************************************************************/
//void FunCode03(void)
//{    
//if(Bx.ComBuffer[0])  //广播地址0x00不处理
//{  //判断帧长度错误
//  if(Bx.RXcount!=8)ModbusError(4);
//  else
//  {  u16 temp1,temp0;
//    temp0=(Bx.ComBuffer[2]<<8)+Bx.ComBuffer[3]; 
//    temp1=temp0+Bx.ComBuffer[5]-1;
//    if(temp1 >= CR_AMOUNT)ModbusError(2);
//    else
//    {
//      if(Bx.ComBuffer[4]!=0||Bx.ComBuffer[5]==0||Bx.ComBuffer[5]>ComMaxLen)ModbusError(3);
//      else
//      {  u16 i;
//        //回复指令第2个字
//        temp1=2*Bx.ComBuffer[5];
//        Bx.ComBuffer[2]=temp1;
//        temp0=Bx.ComBuffer[3];
//        for(i=0;i<temp1;i+=2)
//        {  Bx.ComBuffer[i+3]=Bx.CR[temp0]>>8;
//          Bx.ComBuffer[i+4]=Bx.CR[temp0];
//          temp0++;
//        }
//        Bx.TXDaLen=temp1+3;   
//        Radio_TX(Bx.CR[1]);//USART1_TX(Bx.CR[1]);  
//      }       
//    }
//  }
//}  
//}
///*******************************************************************************
// *  功能码05---写单线圈
// *  接收：站号+功能码+线圈地址*2+线圈状态(00/FF)+00+CRC校验值*2  
// *  回复：同接收
// *  入口参数：无  
// *  出口参数：无  
//*******************************************************************************/                        
//void FunCode05(void)
//{
//  if(Bx.RXcount!=8){ModbusError(4);  }       //数据长度不为8
//  else
//  {
//    if(Bx.ComBuffer[2]!=0||Bx.ComBuffer[3]<0x10||Bx.ComBuffer[3]>=0x90){ModbusError(2);}//地址错误
//    else
//    {
//      if(Bx.ComBuffer[5]!=0){ModbusError(3);}
//      else
//      {  if(Bx.ComBuffer[4]!=0xff&&Bx.ComBuffer[4]!=0){ModbusError(3);}
//        else
//        {  u8 CR_number=Bx.ComBuffer[3];
//          if(CR_number>=0x50)
//          {  if(Bx.ComBuffer[4]==0xff)  Bx.CR[CR_number]=0x0001;  
//            else  Bx.CR[CR_number]=0x0000;  
//          }
//          //广播地址不回复
//          if(Bx.ComBuffer[0])
//          {  Bx.TXDaLen=6;
//            Radio_TX(Bx.CR[1]);//USART1_TX(Bx.CR[1]);
//          }
//        }
//      }
//    }
//  }
//}
///*******************************************************************************
//*  功能码06---写单寄存器
//*  接收：地址+功能码+寄存器地址H+寄存器地址H+预写值H+预写值L+校验值*2  
//*  回复：同接收
//*  入口参数：无  
//*  出口参数：无  
//*******************************************************************************/                        
//void FunCode06(void)
//{
//  if(Bx.RXcount!=8){  ModbusError(4);}
//  else
//  {
//    u16 temp0;
//    temp0=Bx.ComBuffer[2]*256+Bx.ComBuffer[3];
//    if(temp0>CR_AMOUNT){ModbusError(2);}
//    else
//    {
//      u16 i;
//      u8 err=0,original_adress=Bx.CR[1];
//      i=Bx.ComBuffer[4]*256+Bx.ComBuffer[5];
//      if(Bx.ComBuffer[3]==2 && i!=Bx.CR[2])//修改波特率
//        ProtocolChg=1;//置位Protocol已修改标志
//      else if(Bx.ComBuffer[3]==1 && i==0)i=1;
//      err=Write_CR(Bx.ComBuffer[3],i);          
//      if(Bx.ComBuffer[0])//广播地址0不回复
//      {
//        if(err==3)ModbusError(err);
//        else 
//        {
//          Bx.TXDaLen=6;
//          Radio_TX(original_adress);//USART1_TX(original_adress);
//        }
//      }
//    } 
//  }
//}
///*******************************************************************************                        
// *  功能码15---写多线圈
// *  接收：站号+功能码+起始地址H+起始地址L+线圈数H+线圈数L+字节数+线圈值*n+......+校验值*2
// *      线圈值n：一字节内线圈地址：HiBit+......+LoBit,下一字节从高地址往低地址排。剩余位补0
// *  回复：站号+功能码+起始地址H+起始地址L+已写线圈数H+已写线圈数L+校验值*2
// *  入口参数：无  
// *  出口参数：无  
//*******************************************************************************/                        
//void FunCode15(void)
//{
//  if(Bx.ComBuffer[2]!=0x0||Bx.ComBuffer[3]<0x10)ModbusError(2);    //地址错误
//  else
//  {  u16 temp0,temp1;
//    temp0=Bx.ComBuffer[4]*256+Bx.ComBuffer[5];
//    if(temp0>256||temp0==0)  ModbusError(3);
//    else
//    {  temp1=temp0+Bx.ComBuffer[3];
//      //地址超出范围
//      if(temp1>0x90)ModbusError(2);
//      else
//      {  if(Bx.RXcount!=(Bx.ComBuffer[6]+9))ModbusError(4);
//        else
//        {  u8 byte_amount;
//          byte_amount=temp0/8;
//          if(temp0%8)byte_amount++;
//          if(byte_amount!=Bx.ComBuffer[6])ModbusError(3);
//          else
//          {  u8 a,b=7,c,d=0,i;
//            c=Bx.ComBuffer[7];
//            a=Bx.ComBuffer[3];
//            for(i=a;i<temp1;i++)
//            {  if(a>=0x50)
//              {  if(c&0x01)Bx.CR[a]=0x0001;
//                else Bx.CR[a]=0;
//              }
//              c=c>>1;  
//              d++;
//              a++;
//              if(d==8)
//              {  d=0;
//                b++;
//                c=Bx.ComBuffer[b];
//              }  
//            }
//            //广播地址不回复
//            if(Bx.ComBuffer[0])
//            {  Bx.TXDaLen=6;
//             Radio_TX(Bx.CR[1]);// USART1_TX(Bx.CR[1]);
//            }
//          }
//        }
//      }
//    }
//  }
//}
// 
///******************************************************************************                        
// *  功能码16---  写多寄存器
// *  接收：站号+功能码+起始地址*2+00+寄存器总数+寄存器总字节+寄存器高字节+寄存器低字节+......+CRC校验值*2
// *  回复：站号+功能码+起始地址*2+00+寄存器总数+CRC校验*2
// *  入口参数：无  
// *  出口参数：无  
//*******************************************************************************/                        
//void FunCode16(void)
//{  
//  if(Bx.ComBuffer[4]!=0||Bx.ComBuffer[5]==0||Bx.ComBuffer[5]>ComMaxLen){ModbusError(3);}
//  else
//  {  u16 cr_max;
//    cr_max=Bx.ComBuffer[3]+Bx.ComBuffer[2]*256+Bx.ComBuffer[5]-1;
//		#if defined(H04TC) || defined(H08TC)||defined(A04TC)||defined(A08TC)
//		if(cr_max>=CR_AMOUNT && Bx.ComBuffer[3] !=102)  ModbusError(2);
//		else if(Bx.ComBuffer[3] == 102)
//		{
//			if(Bx.ComBuffer[5]==2 && Bx.ComBuffer[6]==4 && Bx.ComBuffer[7]==0xA6 && Bx.ComBuffer[8]==0xB9)
//			{
//				Bx.CR[103] = (Bx.ComBuffer[9]<<8) | Bx.ComBuffer[10];
//				if(Bx.CR[103] == 0x6666)
//					Bx.CR[102] =0;				
//				else
//					Bx.CR[102] = 0xA6B9;		
//				Bx.TXDaLen=6;
//				Radio_TX(Bx.CR[1]);//USART1_TX(Bx.CR[1]);
//			}
//			else ModbusError(2);
//		}
//		#else
//		if(cr_max>=CR_AMOUNT)  ModbusError(2);
//		#endif
//    else
//    {
//        if((Bx.ComBuffer[5]*2)!=Bx.ComBuffer[6])  ModbusError(3);
//        else
//        {  if((Bx.ComBuffer[6]+9)!=Bx.RXcount)  ModbusError(4);
//        else
//        {
//          u8 i=Bx.ComBuffer[3];
//          u8 err=0,err_temp=0,n=7;
//          u8 original_adress=Bx.CR[1];//若地址被改，回复仍然回复修改前的地址
//          for(;i<=cr_max;i++)
//          {  //如果写波特率
//            u16 cr_value;
//            cr_value=Bx.ComBuffer[n]*256+Bx.ComBuffer[n+1];
//            if(i==2 && cr_value!=Bx.CR[2])//改变波特率
//              ProtocolChg=1;//置位波特率改变位
//            else if(i==1 && cr_value==0)cr_value=1;//写地址0则地址设置为默认值。            
//            err_temp=Write_CR(i,cr_value);
//            //即有写成功就不回复错误。没有写成功回复错误
//            if(err!=1)
//            {  if(err_temp==1||err_temp==3)err=err_temp;}
//            n+=2;
//          }
//          if(Bx.ComBuffer[0])//广播地址不回复        
//          {
//            if(err==3)ModbusError(err);
//            else
//            {
//              Bx.TXDaLen=6;
//              Radio_TX(original_adress);//USART1_TX(original_adress);
//            }
//          }
//        }
//      }  
//    }
//  }  
//}

///******************************************************************************                        
//*  函数名  ：写校准数据
//*  描述    ：
//*******************************************************************************/                        
//void WriteCaliConst(void)
//{
//  u16 tmpr,key;
//  u8 length;
//  
//  //验证命令是否为0xb9a8
//  tmpr = *(u16*)(Bx.ComBuffer+2);
//  if(tmpr == 0xb9a8)
//  {
//    //验证key是否正确
//    key=Bx.ComBuffer[4];
//    key = (key^2)^0xb9a8;
//    tmpr = *(u16*)(Bx.ComBuffer+5); 
//    if(tmpr == key)//1，key相同
//    { 
//      tmpr=(Bx.ComBuffer[7]<<8)+Bx.ComBuffer[8];//写校准数据起始地址
//      if(tmpr==ModWrCali.StartAdr)
//      { 
//        length=*(Bx.ComBuffer+9);//校准数据字节长度  
//				if((length+12)==Bx.RXcount)//判断欲发送数据长度和实收长度是否相同
//				{
//					/***更新校准数据***/
//					u8 *adr1,*adr2,i;
//					adr1 = (u8*)CaliData +ModWrCali.StartAdr;//保存校准数据结构体的偏移
//					adr2 = (u8*)Bx.ComBuffer+10;//数据开始的地址
//					for(i=length;i>0;i--)
//						*adr1++ = *adr2++;//拷贝
//					ModWrCali.StartAdr+=length;//保存校准数据结构体的偏移递增
//					if(ModWrCali.StartAdr== CALIDATA_NUM)//写满
//					{      
//						/*校准标志设置*/
//						if(CaliData[0]==0xffff && CaliData[1]==0xffff && CaliData[2]==0xffff)//不校准
//							bCalibrateFlag=0;
//						else
//						{
//							#if defined(H04RC)||defined(H08RC)||defined(A04RC)
//							DispCode=1;
//							#endif
//							#if defined (H08TC) || defined (H04TC)||defined(A04TC)||defined(A08TC)
//							//线性校准完再保存
//							s32 Val[2];
//							u8 t=6+6*0+3; //校准数据偏移		
//							s32 x0,x1,y0,y1;
//							Val[0]=Bx.CR[100];
//							Val[1]=Bx.CR[101];
//							x0=x1=y0=y1=0;
//							x0 =~(u32)CaliData[t]+1;//第一个校准数据是负数
//							y0 =~CaliData[3]+1;//
//							x1 =CaliData[t+1];  
//							y1 =CaliData[4];//	
//							Val[0]=Linear(Val[0],x0,x1,y0,y1);
//							
//							x0 =(u32)CaliData[t+1];//
//							y0 = CaliData[4];//
//							x1 =CaliData[t+2];  
//							y1 =CaliData[5];//
//							Val[1]=Linear(Val[1],x0,x1,y0,y1);		

//							*(CaliData+CALIDATA_NUM/2-2)=Val[0];
//							*(CaliData+CALIDATA_NUM/2-1)=Val[1];

//							#endif
//							bCalibrateFlag=1;//置位校准标志 
//				    }
//						/****添加CRC16校验****/	//保存校准数据结构体末尾字节增加CRC
//						tmpr = CRC16((u8*)CaliData,CALIDATA_NUM);
//						tmpr = (tmpr>>8) | (tmpr<<8);//高低字节互换
//						*(CaliData+CALIDATA_NUM/2) = tmpr;
//						*(CaliData+CALIDATA_NUM/2+1) = tmpr;
//						
//						/****把校准数据写入24c08******/
//					#ifdef H04_08RCTC	
//						Write24C08(2,528,CALIDATA_NUM+2,(u8*)CaliData);
//					#endif	
//						if(Bx.CR[15]==3)Bx.CR[15] = 0;//去除需要返厂的错误。
//						bCaliDataErr=0;
//						ModWrCali.StartAdr=0;
//						Bx.ComBuffer[2]=0;//收到完整数据
//					} 
//					else	Bx.ComBuffer[2]=1;//还有数据未接收
//					ModWrCali.Timeout=0; 
//					/***地址非0则回复***/
//					if(Bx.ComBuffer[0])
//					{ 
//						Bx.TXDaLen=3;
//						Radio_TX(Bx.CR[1]);//USART1_TX(Bx.CR[1]);
//					}
//				}
//				else ModbusError(4);//数据长度错误
//      }
//      else ModbusError(2);//地址错误      
//    }
//  }
//  else if(tmpr==0xa8b9 && Bx.RXcount==8)
//  {
//    tmpr=Bx.ComBuffer [4];//起始地址
//		length = Bx.ComBuffer[5];//读校准数据长度
//		if((tmpr+length)>CALIDATA_NUM || length==0)
//			ModbusError(2); // adress/length error;
//		else
//		{
//			for(key=0;key<length;key++)
//			{ 
//				Bx.ComBuffer[2+key]=*(u8*)((u8*)CaliData+tmpr+key);
//			} 
////			Bx.TXDaLen=CALIDATA_NUM+2;//读8点校正数据异常
//				Bx.TXDaLen=length+2;//2014.05.13
//			
//			Radio_TX(Bx.CR[1]);//USART1_TX(Bx.CR[1]);
//		}
//  }
//}
/******************************************************************************                        
 *  写CR寄存器,读CR属性和取值范围，如果写入值符合条件则写入否则返回错误码
 *  入口参数：CRnum:CR号。Value：预写值  
 *  出口参数：value_error：错误号。0写成功；3预写值不在范围内。1写只读或不可写
*******************************************************************************/                        
u8 Write_CR(u16 CRNum,u16 Value)
{
  u32 rang,en_wr=3; //en_wr:3值域不对不可写;0可写；1只读
  rang=CR_Att[CRNum].Rang;
  switch(CR_Att[CRNum].Limit)
  {
    case 0:  //只读
           en_wr=1;break;
    case 1://任意写  
           en_wr=0;break;
    case 2://小于等于  
           if(Value<=rang)en_wr=0;  break;
    case 3://比较cr小于等于  
           if((signed short)Value<=(signed short)Bx.CR[rang])en_wr=0;  break;
    case 4:  //比较cr大于等于
           if((signed short)Value>=(signed short)Bx.CR[rang])  en_wr=0;break;
    case 5://通信格式  
           if((Value&0xf000)<=(rang&0xf000) && (Value&0xf00)<=(rang&0xf00) 
           && (Value&0xf0)<=(rang&0xf0) && (Value&0x0f)<=(rang&0x0f))
           en_wr=0;break;
    case 6:  
           if(Value<=rang&&Value!=0)en_wr=0;break;
    default:
           en_wr=3;break;
  }
  if(en_wr==0)
	{
		if(Bx.CR[CRNum] != Value && CR_Att[CRNum].PDSave==1)
			CRSaved =1;		
		Bx.CR[CRNum]=Value;
	}
  return (u8)en_wr;
}
///*
// *modbus for lora
//*/
//int LR_Recieve_Handler(u8* RxBuffer,u8 PacketSize)
//{
//  u8 i;
//	u16 lrc_result; 
//	u32 rx_data,temp0;
//  Bx.RXcount = PacketSize;
//  memcpy( Bx.ComBuffer, RxBuffer, Bx.RXcount );
//	/*只接收处理三类地址：自身地址，广播地址(0x00,0xff)*///注：对于ascii来说Bx.ComBuffer[0]为0x3a?
//	if(Bx.ComBuffer[0] == Bx.CR[1] || Bx.ComBuffer[0] == 0xFF || Bx.ComBuffer[0] == 0x00)
//	{ 
//		/*长度错误*/
//		if(Bx.RXcount<4||Bx.RXcount>(ComMaxLen*2+9))ModbusError(4);
//		else
//		{  
//			/*RTU帧*/
//			if(RtuOrAscii==RTU)
//			{
//			if(CRC16(Bx.ComBuffer,Bx.RXcount))ModbusError(9);//CRC校验
//			else ModbusDispose();//进入Modbus功能码解释}

//			}
//			/*ASCII帧*/
//			else
//			{
//				//need to judge the end of 0x0d 0x0a? ???
//				for(i=2;i<Bx.RXcount-2 ;i++)
//				{
//					rx_data =Bx.ComBuffer[i];
//					rx_data=rx_data&0x7f;//对数据最高位清零
//					/*合法ascii不处理，非法ASCCII码报5号错*/
//					if(  (rx_data >= 0x30 && rx_data <=0x39)     //"0"-"9"
//					|| (rx_data >= 0x41 && rx_data <=0x46)     //"A"-"F"
//					|| (rx_data >= 0x61 && rx_data <=0x66)){}  //"a"-"f"
//					else 
//						{
//							ModbusError(5);//Bx.ModErr=0x05;   
//              return 1;
//						}							

//					/*接收ASCCII字低半字节*/
//					if(i&0x01)Bx.ComBuffer[i]=AscToHex(rx_data);

//					/*接收ASCCII字高半字节*/
//					else
//					{
//					rx_data=AscToHex(rx_data);
//					temp0=Bx.ComBuffer[i]<<4;
//					Bx.ComBuffer[i]=rx_data|temp0;
//					}
//				}
//         
//				lrc_result=LRC(Bx.ComBuffer,Bx.RXcount-2);
//				if(lrc_result!=Bx.ComBuffer[Bx.RXcount-2]) ModbusError(9);
//				else ModbusDispose();  
//			}
//		}
//	//	return 1;
//	}
//  return 0;
//}
///*******************************************************************************
//* 函数名    ：启动Modbus发送
//* 描述      ：radio启动发送,添加校验结果，发送第一字节数据。设置comd_adress是
//              因为如果CR内地址被修改的情况，当次回复仍然回复修改前的地址。
//* 入口参数  ：com_adress：回复的地址
//*******************************************************************************/
//void Radio_TX(u8 com_adress)
//{
//  u32 i;
//  Bx.ComBuffer[0]=com_adress;
////Bx.ComBuffer[0]=0x01;
////Bx.ComBuffer[1]=0x10;
////Bx.ComBuffer[2]=0;
////Bx.ComBuffer[3]=26;	
////	Bx.ComBuffer[4]=00;
////	Bx.ComBuffer[5]=1;
////	Bx.ComBuffer[6]=0;
////	Bx.ComBuffer[7]=16;
////	Bx.TXDaLen=8;
//  /*RTU帧模式******************************************************************/
//  if(RtuOrAscii==RTU)
//  {
//    /*添加CRC校验结果*/
//    i=CRC16(Bx.ComBuffer,Bx.TXDaLen);
//    Bx.ComBuffer[Bx.TXDaLen++]=(i>>8);
//    Bx.ComBuffer[Bx.TXDaLen++]=i&0x00ff;

//    //发送第一个字节
////    USART1->DR=Bx.ComBuffer[0];
//    Bx.TXcount=1;
//  }

//  /*ASCII帧模式****************************************************************/
//  else
//  {
//    /*添加LRC校验结果*/
//    i=LRC(Bx.ComBuffer,Bx.TXDaLen);
//    Bx.ComBuffer[Bx.TXDaLen]=i;
//    Bx.TXDaLen++;
//    //发送首字节0X3A&0x80
// //   USART1->DR=0xBA;
//    Bx.TxAscCount=1;
//    Bx.TXcount=0;
//  }
//  //开发送TXEIE中断，关接收中断  
////  USART1->CR1=(USART1->CR1&(~USART_CR1_RXNEIE)) | USART_CR1_TXEIE;
//  //通信指示灯闪烁
////delayms(12000);
////	USER_UART1_SendData(Bx.ComBuffer,Bx.TXDaLen);
//	SX1276Send(Bx.ComBuffer,Bx.TXDaLen);//Radio.Send(Bx.ComBuffer,Bx.TXDaLen);
//  if(Bx.TimeLinkLed>=(6*BlinkTime))Bx.TimeLinkLed=0;
//  else ReLinkLedSig=1;	
////	delayms(300);
//}
unsigned char RX_485[271];
unsigned short RX_CNT;
u16 TX_CNT;
unsigned char Radio_485[271];
u16 Radiolen;
void USART2_TX(uint8_t *data,uint16_t len)
{
	u8 i, t;
  GPIOA->BSRR=(1<<1); //切换至发送PA1
//  USART2->CR1=(USART2->CR1&(~USART_CR1_RXNEIE));
//	for(t=0;t<len;t++)
//	{ 
//		while((USART2->SR&0X40)==0);//等待发送结束
//		USART2->DR=data[t];
//	
//	}
//	while((USART2->SR&0X40)==0);
////	USART2->CR1=(USART2->CR1|(USART_CR1_RXNEIE));
//	GPIOA->BRR=(1<<1); //切换至接收PA1
	Radiolen=len;
for(t=0;t<len;t++)
  Radio_485[t]=data[t];
    
    //发送第一个字节
    USART1->DR=data[0];
    TX_CNT=0;

  //开发送TXEIE中断，关接收中断  
  USART2->CR1=(USART2->CR1&(~USART_CR1_RXNEIE)) | USART_CR1_TXEIE;
//  //通信指示灯闪烁
//  if(Bx.TimeLinkLed>=(6*BlinkTime))Bx.TimeLinkLed=0;

}
//extern u8 SendBuf[64];
void L_USART2_IRQHandler(void)
{
  u32 USART2_SR,temp0,rx_data;
  USART2_SR=USART2->SR;

  /**************接收中断******************************************************/
  if ((USART2_SR&USART_SR_RXNE) && (USART2->CR1&USART_CR1_RXNEIE))
  {
    rx_data=USART2->DR;//读DR可清除错误标志位，
    //1，上次接收数据已处理；2，无以下错误：校验错误;噪声错误;帧错误(已删此判断)
    if(ModReFinsh==0 && (USART2_SR&(USART_SR_PE|USART_SR_NE))==0)
    {
      /*RTU帧模式***************************************************************/
      if(RtuOrAscii==0)
      { 
        RX_485[RX_CNT]=rx_data;           
        //超过127字节不再接收数据，并报错 
        if(RX_CNT<=(ComMaxLen*2+9))RX_CNT++;          
        else Bx.ModErr=0x04;
      }
    }
    /*重置计时器*/
    TIM2->CNT=0X00;//清除当前值         
    TIM2->SR=0X0;  //清除中断标志UIF(0)=0
    TIM2->DIER|=0X01;//使能更新中断UIE(0)
    TIM2->CR1|=0X01;//使能TIM2定时器器CEN(0)=1;
  }  

  /*发送中断*******************************************************************/
  if((USART2_SR&USART_SR_TXE)&&(USART2->CR1&USART_CR1_TXEIE))
  {

    /*RTU帧模式****************************************************************/
    if(RtuOrAscii==RTU)
    {
      USART2->DR = Radio_485[TX_CNT++]; 
      //最后一字节,关缓存空中断
      if(TX_CNT == Radiolen) 
      {
        USART2->CR1 = (USART2->CR1 & (~USART_CR1_TXEIE)) | USART_CR1_TCIE;
      }
    } 
    
  }
  /*发送结束中断***************************************************************/
  if((USART2_SR&USART_SR_TC)&&(USART2->CR1&USART_CR1_TCIE))
  {
    //关发送完成中断TXCIE(6)。开接收中断RXEIE(5)
    USART2->CR1=(USART2->CR1 & (~USART_CR1_TCIE) ) | USART_CR1_RXNEIE;
    GPIOA->BRR=(1<<1);//设置PA1为RX状态

    //如果波特率被修改则修改波特率
    if(ProtocolChg)
    {
      ProtocolSet(Bx.CR[2]);
      ProtocolChg=0;
    }
    ModbusReOk=1;//帧发送完成，置允许跳转位
  }
}  
/*******************************************************************************
* 函数名    ：把十六进制转为ASCII
* 描述      ：把十六进制转为ASCII
* 入口参数  ：hex_data：十六进制数
* 出口参数  ：hex_data：转换后的ASCII结果
*******************************************************************************/
u8 HexToAsc(u8 hex_data)
{
  if(hex_data<10)hex_data+=0x30;
  else    hex_data+=0x37;
  return hex_data;
}
/*******************************************************************************
* 函数名    ：把ASCII码转换为十六进制
* 描述      ：把ASCII码转换为十六进制
* 入口参数  ：asc_data：ASCII数
* 出口参数  ：asc_data：转换后的十六进制结果
*******************************************************************************/
u8 AscToHex(u8 asc_data)
{
  if(asc_data<0X40)      asc_data-=0x30;//'0'-'9'
  else if(asc_data<0x47) asc_data-=0x37;//'A'-'F'
  else                asc_data-=0x57;//'a'-'f'
  return asc_data;
}
//extern void SX1276SetMaxPayloadLength(uint8_t max );
void modbus_handle(void)
{
  if(ModReFinsh)
	{
    if((RX_CNT>0)&&(Bx.ModErr==0)&&(RX_485[0]!=0))  
    {
//			if(RX_485[0]==0X23) 
//				SX1276SetMaxPayloadLength(RX_485[1]);
//			else
			  SX1276Send(RX_485,RX_CNT);
		}
    ModReFinsh=0;
		Bx.ModErr=0;
		RX_CNT=0;
  }
}
