#include "config_08AX.h"
#include "RTC.h"
//函数声明
void RTCmain(void);
void SelfCaliCalc(void);

#if defined(H04_08RCTC)||defined(A04_08RCTC)
  s32 CalibrateCode(s32 data);
  u32 Div64bitBy31bit(u32 datal,u32 datah,u32 divisor);
	s32 ConvertTempElectric(u8 flag,u16 rang,s32 data);
	s32 Linear(s32 x,s32 x0,s32 x1, s32 y0,s32 y1);			 	
	static void Sort(s32 *Array,u8 data);	
	static s32 AverMid(s32 *Array,u8 data);	
  static u8 ComSpiByte(u8 data);
  static u32 ComSpi40Bit(u8 cmd,u32 data);
  static void CheckOffline(u32 data);
	static void GetAiValue(s32 data); 
	static void SetAiOffline(void);
	static void RTCResCrosst(void);																																						
	static s32 Calibrate_IO_Code(s32 data);
  extern void Shift595(u16 data);					
  extern void LedRefresh(void);
	static void CalSCData(s32 *Array);	
	static s32  GetTempration( s32 data);
  static void SwitchRtcChannel(void);	
	static void SwitchRtcChannel_SC(u8 data);
  void sort(u32 *Array,u8 data);
/*全局变量*/     
  u8 AIMessage;//SPI读AI的进度信息。                 
	u8 CSMessage;																			
  u8 DiscardCnt;//丢弃AI结果数												
	u8 RangeType;//大小量程控制位					
	static u8 Count;//计数
	static u32 lastADvalue[30];
  vu8 LastCH;		//													
  u16 AiOnline;	
	u32 MedianValue;	//采集中位值
	s32 y;            //储存采集次数
	
  #if defined(H04RC)||defined(H08RC)||defined(A04RC)
  u32 WireR[AI_AMOUNT*2];//线阻，单位毫欧
	vu8 DispCode;//显示线性校准后码值
	vu8 ScCH;
	u8  OffCount[AI_AMOUNT];//RC断线次数计数
	vu8	StartupFlag;//上电标志
	s32 RcValue[AI_AMOUNT][4];//保存+端数据与M端数据，各两个
	u8	FIFOCount[AI_AMOUNT];//队列存储计数
	u8	FIFOErrCount[AI_AMOUNT];//线阻计算无效计数
	u16 WireRFIFO[AI_AMOUNT][FIFO_AMOUNT];//线阻滤波队列
	const u16	MinR[4]={2500,25000,6000,11000};//四种热电阻测量的最低电阻值对应码值
	u32 data1;		//测量用的变量
  #endif
  #if defined(H04TC)||defined(H08TC) ||defined(A04TC)||defined(A08TC)
  s16 TempCu50, Temp1047[2];//外补温度,内补温度
	u8 ErrCount;//TC读内补温度错误计数	
	struct S_FilterType S_AiFilter;//滤波	
	#endif		
  struct SCType
	{
		s32 HC0[SC_NUM];//HardwareCode 硬件码值
		s32 HC1[SC_NUM];
		s32 HC2[SC_NUM];
		s32 HC3[SC_NUM];
		s32 HC4[SC_NUM];
		s32 HC5[SC_NUM];
		s32 FC0[SC_NUM];//FilterCode 滤波码值  
		s32 FC1[SC_NUM];
		s32 FC2[SC_NUM];
		s32 FC3[SC_NUM];
		s32 FC4[SC_NUM];
		s32 FC5[SC_NUM];
		s32 LC[6];//Linear Code 线性校准码值
		u16	Num;
		u16 NumX;
		float a, c,b, d;//自校计算参数
		u8 Exc;
	}SC;//Self calibration																																
/*外部变量*/																																							
  const u8 TCRANG[13]={0,1,0,1,1,0,1,0,1,1,0,1,1};//热点偶0-12信号类型对应的量程	20140522
  extern vu16 CaliData[];																																
  vu8 AiChannel;																																	
  extern u8 AiRang[];//ai量程 																													
  extern vu16 RTCTimeout;																																
	extern vu16 RCDelay;																														
  extern vu16 Dout595;																																	
  extern BootVariable Bx;
	extern vu8 SCState;
#endif
/*******************************************************************************
* 函数名  ：查询CS5530
* 描述    ：
*******************************************************************************/																																						
void RTCmain(void)  										
{
  #if defined(H04_08RCTC)||defined(A04_08RCTC) 	
	u32 i;

  if(AIMessage == sDELAY)//上电延时 				
  {
    if(RTCTimeout >= 100)//延时300ms
      AIMessage = sSYNC; 
  }																		
  else if(AIMessage == sSYNC)                          
  {
    Dout595 &= ~(1<<AICSOffset);//拉低CS
    Shift595(Dout595);
    for(i=0;i<30;i++)
    ComSpiByte(SYNC1); 
    ComSpiByte(SYNC0);
    AIMessage = sSET_RS;
		Count=0;//计数复位RS失败次数
  }
  else if(AIMessage == sSET_RS)																																									
  {
    ComSpi40Bit(Write_CONFIG,SET_RS);//复位RS		
    i=ComSpi40Bit(Read_CONFIG,0);//读配置寄存器
    if((i&CONFIG_RV))//查询RV=1  
		{
      AIMessage = sRST_RV;
			Count=0;//计数清除Rv失败次数
		}
		else 
		{			
			if(Bx.CR[15]==0)Bx.CR[15]=16;//报错，复位CS5530错误
			SetAiOffline();			
			Count++;
			if(Count>3)AIMessage=sDELAY;
		}
  }
  else if(AIMessage == sRST_RV)                       
  {
    ComSpi40Bit(Write_CONFIG,0);//清除RV
    i=ComSpi40Bit(Read_CONFIG,0);//读配置寄存器
    if((i&CONFIG_RV)==0)//查询RV=0
      AIMessage = sWR_CONFIG;                        
		else												 
		{																													
			if(Bx.CR[15]==0)Bx.CR[15]=16;//报错，复位CS5530错误
			SetAiOffline();																		
			Count++;																					
			if(Count>3)AIMessage=sDELAY;		    
 		}
  }
  else if(AIMessage == sWR_CONFIG)
  {	
		#if defined(H04RC) || defined(H08RC)||defined(A04RC)
    ComSpi40Bit(Write_CONFIG,(0x02000000|(1<<11)));//测试写配置：参考电压<2.5,双极性模式，字速率60Hz
		#endif
		#if defined(H04TC) || defined(H08TC)||defined(A04TC)||defined(A08TC)
    ComSpi40Bit(Write_CONFIG,(0x02000000|(2<<11)));//测试写配置：参考电压<2.5,双极性模式，字速率30Hz
		#endif	 

		SwitchRtcChannel();	
    AIMessage = sCONVERT;								
  }															
  else if(AIMessage == sCONVERT)
  {
    ComSpiByte(START_CONTINUOUS);           
    AIMessage=sQUERY_SDO;
    DiscardCnt=0;//读计数
    RTCTimeout=0;									
  }																
  else if(AIMessage == sQUERY_SDO)//查询SDO线
  {
    if((GPIOB->IDR&MISO) == 0)//SDO被拉低
    {
			#if defined(H04TC)||defined(H04RC)||defined(A04RC)||defined(A04TC)
			if((s8)AiChannel>=0&&(s8)AiChannel<=3)
				y=7/*Bx.CR[AiChannel+33]*/;
			else y=7;
			#endif			
			#if defined(H08TC)||defined(H08RC)||defined(A08TC)
			if((s8)AiChannel>=0&&(s8)AiChannel<=7)
				y=7/*Bx.CR[AiChannel+49]*/;
			else y=7;
			#endif

			for(i=y-1;i>0;i--)
				lastADvalue[i]=lastADvalue[i-1];//采集多次
			lastADvalue[0]=ComSpi40Bit(0,0);//读转换结果
			
      DiscardCnt++;//采集数自加1
      RTCTimeout=0;
      if(DiscardCnt>=y)//采集数满y个后     
        AIMessage=sVALID_AI;//进入读有效的AD转换结果数据
    } 
    else
    {
      if(RTCTimeout>1000)																 
      {																										
        RTCTimeout=0;																			
        AIMessage=sDELAY;															
      }																									
    }																										
  }																										  	
  else if(AIMessage == sVALID_AI)//读有效的AD转换结果数据
  {						
		s32 j,j1,j2,j3,j4/*,j5*/;
    u32 data;
		u32 lastdata,templastdata;
    data = ComSpi40Bit(0xFF,0);//读AD转换结果

/*		if(AiChannel==9)
		{Bx.CR[66]=data;
		Bx.CR[67]=data>>15;}
		if(Bx.CR[67]==0)
			Bx.CR[68]++;*/		
		lastdata = lastADvalue[0];
		templastdata=lastdata>>15;
		if(templastdata&0x10000) templastdata|=0xffff0000;
		
		for(i=y;i>0;i--)
		{
			lastADvalue[i-1]=lastADvalue[i-1]>>15;
			if(lastADvalue[i-1]&0x10000) lastADvalue[i-1]|=0xffff0000;
		}
/*		sort(lastADvalue,y);		//排序
		//求中位值
		if(y%2==1)
			MedianValue=lastADvalue[(y-1)/2];
		else
			MedianValue=lastADvalue[y/2]/2+lastADvalue[y/2-1]/2;
		
		j=MedianValue-templastdata;*/
		j=lastADvalue[1]-lastADvalue[0];
		j1=lastADvalue[2]-lastADvalue[1];
		j2=lastADvalue[3]-lastADvalue[2];
		j3=lastADvalue[4]-lastADvalue[3];
		j4=lastADvalue[5]-lastADvalue[4];
//		j5=lastADvalue[6]-lastADvalue[5]; //丢掉前面两个的比较值
		
		if( j>50 || j<-50||j1>50||j1<-50||j2>50||j2<-50||j3>50||j3<-50||j4>50||j4<-50|| data != lastdata) 
		{
			RTCResCrosst();						//RTC抗通道串扰
			SwitchRtcChannel();				//RCTC通道切换
			RTCTimeout=0;					
			AIMessage=sCONVERT;
		}		
    else if((data&0xff)==0 || (data&0xff)==0x04)//如果数据有效                        
    {	
			if(Bx.CR[15]==16)Bx.CR[15]=0;//报错，复位CS5530错误
      CheckOffline(data);//断线检查										        
			if(bCaliDataErr==0)//校准数据正常                                                    
				data = CalibrateCode(data);//校准CS5530读到的码值               
			#ifdef SAVE_CALIB_RESULT			                                                   
				Bx.CR[AI_OFFLINE+1+AiChannel]=data; //在CR中显示校准后的码值
			#endif																											 

			if(bCalibrateFlag)                    											       
			{																									
				#if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC)					
  			data=GetTempration(data);//电信号转温度					
				GetAiValue(data);						
				#endif																					
				#if defined(H04RC)||defined(H08RC)||defined(A04RC)					
				u16 tmpr;																				
				if(DispCode==1)																	
				{																								
					i=AiChannel;																	
					if(data&0x80000000)tmpr=(~data)+1;//读到负电压
					else tmpr=data;										
					if(i<AI_AMOUNT)Bx.CR[16+i]=tmpr;
				}
				else
				{
					data=GetTempration(data);//电信号转温度
						GetAiValue(data);
				}
				#endif
			}	 	
			RTCResCrosst();//RTC抗通道串扰 		
			SwitchRtcChannel();					
			AIMessage = sCONVERT;				
    }
    else 
    {
      AIMessage = sDELAY;//无效数据，延时重新初始化CS5530
      RTCTimeout=0;
    }
  }
	#endif
}
#if defined(H04_08RCTC)||defined(A04_08RCTC)  
/*******************************************************************************
* 函数名  ：发一字节命令，同时读写一个字（32位）
* 描述    ：
*******************************************************************************/
static u32 ComSpi40Bit(u8 cmd,u32 data)																					
{   																																						
  u32 i,j;																																			
  ComSpiByte(cmd);																															
  i = ComSpiByte(data>>24); //发送接收高8位																			
  j = i<<24;																																		
  i = ComSpiByte(data>>16);																											
  j |= i<<16;																																		
  i = ComSpiByte(data>>8);																											
  j |= i<<8;																																		
  i = ComSpiByte(data);//发送接收低8位																						
  j |= i;																																				
  return j;																																			
}																																																														
/*******************************************************************************
* 函数名  ：SPI读写一个字节（8位）
* 描述    ：
*******************************************************************************/
static u8 ComSpiByte(u8 data)																										
{																																								   
  u8 i;																																					
  if(SPI2->SR&SPI_RXNE)i = SPI2->DR;//清除RXNE																	
  while((SPI2->SR&SPI_TXE)==0);//等待TXE，DR为空																	
  SPI2->DR = data;																															
  while((SPI2->SR&SPI_RXNE)==0);//等待RXNE,DR为非空															
  i = SPI2->DR;																																	
  return i;																																			
}
/*******************************************************************************                        
* 函数名   ：设置断线标志
* 描述     ：
* 入口参数 ：
*******************************************************************************/                        
static void CheckOffline(u32 data)
{
  s16 i=AiChannel,tmpr;	
  tmpr=data>>16; //tmpr为data的高16位，最高位为符号位
	#if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC)
	if(S_AiFilter.DiscardChe<DISCHE_Num)
	{
		tmpr=32001;
		S_AiFilter.DiscardChe++;
	}
	#endif
  if(tmpr<-32000 || tmpr>32000 || data&4)
    AiOnline &= ~(1<<i);//断线				
  else AiOnline |= 1<<i;//在线
  #if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC)
  Bx.CR[AI_OFFLINE] = (0XFF^AiOnline)&(0XFF>>(8-AI_AMOUNT));
  #endif
  #if defined(H04RC)||defined(H08RC)||defined(A04RC)
	if(i<AI_AMOUNT)
	{	
		if((AiOnline&(1<<i))==0 )
		{	
			OffCount[i]++;
			if(OffCount[i]>=3){				
				Bx.CR[AI_OFFLINE] |= 1<<i; //通道断线
				WireR[AI_AMOUNT+i]=0;//线阻清零
				FIFOCount[i]=0;//队列计数清0
				FIFOErrCount[i]=0;//出错计数清0				
			}
		}
		else 
		{
			Bx.CR[AI_OFFLINE] &= ~(1<<i);//在线
			OffCount[i]=0;		
		}
	}
  #endif
}																						
/*******************************************************************************                        
* 函数名   ：
* 描述     ：
* 入口参数 ：
*******************************************************************************/                        
s32 CalibrateCode( s32 data)
{
  u16 tmpr,i=AiChannel,k;
  s32 x0,x1,y0,y1;
	x0 = x1 =y0 =y1 =0;
  data=data>>15;
  if(data&0x10000) //读到负电压
  {
  	data|=0xffff0000;
    tmpr=(~data)+1;
  }
  else tmpr=data;	
	#ifdef ShowHardwareCode
	Bx.CR[180+i]=tmpr;
	#endif
	/* TC自校通道数据存储与计算	 **********************************************/
 #if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC)
	if(i==AI_AMOUNT+3)SC.HC0[SC.Num]=data;
	else if(i==AI_AMOUNT+3+1)SC.HC1[SC.Num++]=data;
	if(SC.Num>=SC_NUM)	
	{
		s32 Val[2];		
		SC.Num=0;//清零
		Sort(SC.HC0,SC_NUM);Sort(SC.HC1,SC_NUM);		
		SC.FC0[SC.NumX]=SC.HC0[SC_NUM/2];
		SC.FC1[SC.NumX++]=SC.HC1[SC_NUM/2];
		if(SC.NumX>=SC_NUM)
		{
			SC.NumX=0;
			Val[0]=AverMid(SC.FC0,SC_NUM);
			Val[1]=AverMid(SC.FC1,SC_NUM);
			CalSCData(Val);
		}
	}		
 #endif
 /* RC自校通道数据存储与计算	 **********************************************/	
 #if defined(H04RC)||defined(H08RC)	||defined(A04RC)
  if		 (i==AI_AMOUNT*2)	 SC.HC0[SC.Num]=data;
	else if(i==AI_AMOUNT*2+1)SC.HC1[SC.Num]=data;
	else if(i==AI_AMOUNT*2+2)SC.HC2[SC.Num]=data;
	else if(i==AI_AMOUNT*2+3)SC.HC3[SC.Num]=data;
	else if(i==AI_AMOUNT*2+4)SC.HC4[SC.Num]=data;
	else if(i==AI_AMOUNT*2+5)SC.HC5[SC.Num++]=data;		
  if(SC.Num>=SC_NUM)//
	{
		s32 Val[6];
		SC.Num=0;//清零
		Sort(SC.HC0,SC_NUM);Sort(SC.HC1,SC_NUM);Sort(SC.HC2,SC_NUM);
		Sort(SC.HC3,SC_NUM);Sort(SC.HC4,SC_NUM);Sort(SC.HC5,SC_NUM);//排序
		SC.FC0[SC.NumX]=SC.HC0[SC_NUM/2];SC.FC1[SC.NumX]=SC.HC1[SC_NUM/2];
		SC.FC2[SC.NumX]=SC.HC2[SC_NUM/2];SC.FC3[SC.NumX]=SC.HC3[SC_NUM/2];
		SC.FC4[SC.NumX]=SC.HC4[SC_NUM/2];SC.FC5[SC.NumX++]=SC.HC5[SC_NUM/2];
	  if(SC.NumX>=SC_NUM)
		{
			SC.NumX=0;
			Val[0]=AverMid(SC.FC0,SC_NUM);Val[1]=AverMid(SC.FC1,SC_NUM);
			Val[2]=AverMid(SC.FC2,SC_NUM);Val[3]=AverMid(SC.FC3,SC_NUM);
			Val[4]=AverMid(SC.FC4,SC_NUM);Val[5]=AverMid(SC.FC5,SC_NUM);			
			CalSCData(Val);
		}												
	}																																			
 #endif																																																	

  /* 校准数据读取 *******************************************************/	
  if(bCalibrateFlag==0 )//不校准			
  {																
    if(i<AI_AMOUNT)
			Bx.CR[16+i]=tmpr;
		else if(i<2*AI_AMOUNT)
      Bx.CR[AI_OFFLINE+1+LastCH]=tmpr;																	 
  }	
	/* 硬件码值线性校准 ***************************************************/		
  else	
  {				
    if(i<AI_AMOUNT)//码值滤波
    {
      k=AiFilter[i].Times; //滤波系数k%,刚上电为0
      data = (AiFilter[i].Sum*k +data*(100-k))/100;//一阶滞后滤波
      AiFilter[i].Sum = data;
      AiFilter[i].Times =Bx.CR[AI_SMPL_CR+i]*7;//更新滤波参数
    } 			
    /*TC模块电压校准*/
    #if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC)
    if(i<AI_AMOUNT)//热端温度测量
    {
      tmpr = TCRANG[AiRang[i]]*3;//x偏移
      k = 6+6*i+tmpr;//y偏移
      if(data < CaliData[k+1])//取前两个校准点
      {
        x0 =~(u32)CaliData[k]+1;
        x1 =CaliData[k+1];
        y0 =~(u32)CaliData[tmpr]+1;
        y1 =CaliData[tmpr+1];
      }
      else//取后两个较准点
      {
        x0 =CaliData[k+1];
        x1 =CaliData[k+2];
        y0 =CaliData[tmpr+1];
        y1 =CaliData[tmpr+2];
      }
    }            
    else //冷端温度测量
    {
				if(LastCH==0)//热电阻Cu50电压校准 
					k=7+AI_AMOUNT*6;
				else if(LastCH==1 || LastCH==2)  //TS1047校准
					k=7+AI_AMOUNT*6+3;
				else k=7+AI_AMOUNT*6+3;//自校通道2
				y0 =CaliData[k];
				y1 =CaliData[k+1];
				x0 =CaliData[k+6];
				x1 =CaliData[k+7];
				if(LastCH==3)//自校通道1 
				{
					k = 6+3;
					x0 =~(u32)CaliData[k]+1;
					x1 =CaliData[k+1];								
					y0 =~(u32)CaliData[3]+1;					
					y1 =CaliData[3+1];									
				}																									
				if(data<0)data=0;												
    }		
    data=Linear(data,x0,x1,y0,y1);
		//bCalibrateFlag=0;//调试			
		//Scc_Exc=0;//调试			
		if(bCalibrateFlag && (SC.Exc==1))data = Calibrate_IO_Code(data);//自校准		
		
    if(data>64000)data=64000; 
    #endif 
    /*RC模块电压校准*/     
    #if defined(H04RC)||defined(H08RC)||defined(A04RC)
    if(AiRang[i&(AI_AMOUNT-1)]==1)k=3;//选择大量程 ,只有Pt1000是大量程
    else k=0;
    if(data>CaliData[k+1])k+=1;//码值大于中间较准点选择后两个基准点
		#if defined(H04RC)||defined(H08RC)
    tmpr=6+6*(i&(AI_AMOUNT-1))+k;
		#endif
		#ifdef A04RC
		tmpr=6+6*i+k;
		#endif
    y0 =CaliData[k];
    y1 =CaliData[k+1];
    x0 =CaliData[tmpr];
    x1 =CaliData[tmpr+1];	
																															 
    data=Linear(data,x0,x1,y0,y1); //U=(y1-y0)(x-x0)/(x1-x0)+y0  
    if(data>64000)data=64000;    	
    #endif                                               
  }
	#ifdef ShowLinearCode
	Bx.CR[210+i]=data;
	#endif
  return data;
}
/*******************************************************************************                        
* 函数名   ：计算电阻值
* 描述     ：电阻R=rpu*data/(64*2500000/range - data)；Um为RcM测得码值。
* 入口参数 ：data:码值，rang：码值64000对应电压/mv，rpu上拉电阻值/R
*******************************************************************************/                        
u32 GetResistance(u32 data,u16 range,u16 rpu)																								
{																																														
  u32 tmpr;																																									
  tmpr = 64*2500000/range- data;																														
  data = rpu*data;																																					 
  data = Div64bitBy31bit(((data<<10)&0xfffffc00),((data>>22)&0x3ff),tmpr); //value放大1024倍
  data = (data*125)>>7;//等效于data*1000/1024																								
  return data;//返回电阻值单位mR																															
}	
/*******************************************************************************                        
* 函数名   ：
* 描述     ：
* 入口参数 ：	
*******************************************************************************/                        
s32 GetTempration( s32 data)
{
  u16 i=AiChannel;
  /*热电阻-阻值拟合成温度*****************************************************/
  #if defined(H04RC)||defined(H08RC)||defined(A04RC)
	u16 R1,R2,R3;
	#ifdef H04_08RCTC
	R1 = CaliData[18+6*AI_AMOUNT];//50
	R2 = CaliData[19+6*AI_AMOUNT];//383
	R3 = CaliData[20+6*AI_AMOUNT];//12000
	#endif
	#ifdef A04_08RCTC
	R1 = CaliData[6+6*AI_AMOUNT*2];//30欧
	R2 = CaliData[7+6*AI_AMOUNT*2];//383欧
	R3 = CaliData[8+6*AI_AMOUNT*2];//12000欧
	#endif
  if(i<AI_AMOUNT)	
  {		
		if(LastCH==i && (Bx.CR[AI_OFFLINE] & (1<<i))==0)//本轮M端刚取到值,且在线
		{
			s32 k,j;
			//保存+端值，用于线阻计算			
			RcValue[i][0]=RcValue[i][1];
			RcValue[i][1]=data;	
			j=RcValue[i][1]-RcValue[i][0];			
			k=RcValue[i][3]-RcValue[i][2];
			j=j-k;
			k=MinR[AiRang[i]];
			//计算线阻条件：+端码值变化量与M端变化量相差在20以内，
			//	+端码值>M端码值且线阻码值<1000 (线阻限制最大约6~7欧姆),
			//	M端码值>本通道量程类型测量的最小电阻对应码值。
			if(j<20 && j>-20 && RcValue[i][1]>=RcValue[i][3] && RcValue[i][1]-RcValue[i][3]<1000 
				&& RcValue[i][3]>k )
			{
				FIFOErrCount[i]=0;
				k=RcValue[i][1]-RcValue[i][3];//线阻
				//FIFO移入
				for(j=0;j<FIFO_AMOUNT-1;j++)
					WireRFIFO[i][j] = WireRFIFO[i][j+1];
				WireRFIFO[i][FIFO_AMOUNT-1]=k;				
				if(FIFOCount[i]<FIFO_AMOUNT)FIFOCount[i]++;
				if(FIFOCount[i]>=FIFO_AMOUNT)
				{
					k=0;
					//更新线阻
					for(j=0;j<FIFO_AMOUNT;j++)
						k+=WireRFIFO[i][j];
					k/=FIFO_AMOUNT;
					WireR[AI_AMOUNT+i]=k;
				}
			}
			else{
				FIFOCount[i]=0;//
				if(++FIFOErrCount[i]>=4)
				{
					//WireR[AI_AMOUNT+i]=0;//不清掉线阻
					FIFOErrCount[i]=0;
				}
			}
		}
		//计算温度
		if(data<64000)
		{		
			if(AiRang[i&(AI_AMOUNT-1)]==1)
			{
				data= data - SC.LC[2];
				data= data-2*WireR[AI_AMOUNT+i];
				data= R3 * data/(SC.b-data)*((R2+R1)/(SC.a+R1))*1000;
				//WireR[i] = data;
				data=ConvertTempElectric(1,AiRang[i],data);//电阻-温度折线拟合
				data1=data;
			}																																					 
			else																																		
			{
				data= data - SC.LC[5];
				data= data-2*WireR[AI_AMOUNT+i];				
				data= R3 * data/(SC.d-data)*((R2+R1)/(SC.c+R1))*1000;
				//WireR[i] = data;
				data=ConvertTempElectric(1,AiRang[i],data);//电阻-温度折线拟合
				data1=data;
			}																																				
		}																																						
  }		 																																
  else if(i>=AI_AMOUNT && i<AI_AMOUNT*2) //M端
  {	
			i=i-AI_AMOUNT;
			RcValue[i][2]=RcValue[i][3];
			RcValue[i][3]=data;		
	}																																					
  #endif //对应#if defined(H04RC)||defined(H08RC)||defined(A04RC)

  /*热电耦-电压拟合温度*****************************************************/
  #if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC)
	u16 rpu;
	u16 v[2]={0,0};	
  v[0]=CaliData[18+6*AI_AMOUNT];//小的量程时64000码值对应电压
  v[1]=CaliData[19+6*AI_AMOUNT];//大的量程时64000码值对应电压
  rpu=CaliData[20+6*AI_AMOUNT]*1000;//上拉电阻10M
  if(i<AI_AMOUNT && AiRang[i]<10)
  {
    if(Bx.CR[AI_OFFLINE]&(1<<i))//断线显示最大值
      data=RCTC_TEMP[ TEMP_VOL_OFFSET[AiRang[i]]+TEMP_VOL_NUM[AiRang[i]]-1];
    else
    {
      s32 vol;
			u8 j;
			#if defined(H08TC)||defined(A08TC)
			if(i<AI_AMOUNT/2)j=0;//使用内补1
			else j=1;//使用内补2					
			#endif
			#if defined(H04TC)||defined(A04TC)
			j=0;
			#endif
			
      if(TempCu50 > -500 && TempCu50 < 1500)//Cu50热电阻有效
			{
				if(Bx.CR[102] == 0xA6B9)//测试机流程
				{
					vol=Bx.CR[103];
					vol = ConvertTempElectric(0,AiRang[i],vol); 
				}
				else
				{
					if(Bx.CR[AI_0_CR+i]<=750)
						vol = TempCu50 + Bx.CR[AI_0_CR+i];//零点修正;
					else vol = TempCu50 - (65536-Bx.CR[AI_0_CR+i]);//零点修正;
					vol = ConvertTempElectric(0,AiRang[i],vol); 
				}
			}
      else if(Temp1047[j] > -400 && Temp1047[j] <1250)//1047温度传感器有效
			{
				if(Bx.CR[AI_0_CR+i]<=750)
					vol = Temp1047[j] + Bx.CR[AI_0_CR+i] ;
				else vol = Temp1047[j] - (65536-Bx.CR[AI_0_CR+i]) ;
        vol = ConvertTempElectric(0,AiRang[i],vol); //温度转电信号
      }
			else vol=0;//Cu50和1047数据都无效        
      data = data + vol;//热端电压+冷端电压
      data=ConvertTempElectric(1,AiRang[i],data);//把电压值拟合成温度  	
    }
  }
  else if(i >= AI_AMOUNT && (i<AI_AMOUNT+3))
  {
    if(LastCH==0)//读取Cu50温度值
    {
      data=GetResistance(data,v[0],rpu);//电压对应码值，由码值转电阻值
      TempCu50 = ConvertTempElectric(1,13,data);//电阻转温度，温度对应工程量
			if(TempCu50> -500 && TempCu50< 1500)//滤波
			{
				if(S_AiFilter.TCu50<0x7fff)
				{
					TempCu50 = (TempCu50*70+S_AiFilter.TCu50*30)/100;
					S_AiFilter.TCu50=TempCu50;
				}
				else S_AiFilter.TCu50=TempCu50;																					
			}
			//Bx.CR[AI_OFFLINE+1]=TempCu50;//显示外补温度
    }
    else if(LastCH==1 || LastCH==2)//读取1047温度	OK
    {
			u8 j;
			if(LastCH==1)j=0;//内补1
			else j=1;//内补2
			
      data=data*v[1]*23/64000;//计算电压值
      if(data>1750)data=1750;
      else if(data<100)data=100;
      Temp1047[j]=data-500; 

			if(Temp1047[j]> -400 && Temp1047[j]< 1250)//滤波
			{
				if(S_AiFilter.T1047[j]<0x7fff)
				{
					Temp1047[j] = (Temp1047[j]*70+S_AiFilter.T1047[j]*30)/100;
					S_AiFilter.T1047[j]=Temp1047[j];
				}
				else S_AiFilter.T1047[j]=Temp1047[j];	
			}																	
			//Bx.CR[AI_OFFLINE+2+j]=Temp1047[j];//显示内补温度				
			if((TempCu50<=-500 || TempCu50>=1500) && (Temp1047[j]<=-400 || Temp1047[j]>=1250))
			{
				ErrCount++;																		
				if(ErrCount>3 && Bx.CR[15]==0) Bx.CR[15]=17;//报错冷端异常		
	
			}
			else 
			{	
				if( Bx.CR[15]==17) Bx.CR[15]=0;//清除冷端异常报错
				ErrCount=0;
			}	
    }	
  }
  #endif //对应#if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC) 	
	return data;
}
/******************************************************************************                        
* 函数名   ：保存AI值
* 描述     ：根据工程量的设置保存AI值到CR上
* 入口参数 ：
*******************************************************************************/ 
void GetAiValue(s32 data)
{
  u32 tmpr,i=AiChannel;
	if(i<AI_AMOUNT)
  {		
   	if(Bx.CR[AI_OFFLINE]&(1<<i) )//断线
		{	
			
			if((Bx.CR[AI_ENGCL_CR]&(1<<i))==0)data=32000;//不使用工程量
			else
			{
				tmpr = Bx.CR[AI_RANG_CR+i];//更新信号类型
				AiRang[i] = tmpr;
				if(tmpr<10)data=RCTC_TEMP[ TEMP_VOL_OFFSET[tmpr]+TEMP_VOL_NUM[tmpr]-1];//上限温度
				else data =Bx.CR[AI_UGCL_CR+i];//工程量上限																			
			}				
		}
		else//未断线
		{											 
			signed short up,down;
			if(AiRang[i]<10) 		 															
			{										 														  
				if((Bx.CR[AI_ENGCL_CR]&(1<<i))==0)//不使能工程量 
				{     																					 																			
					down=RCTC_TEMP[ TEMP_VOL_OFFSET[AiRang[i]]];//下限温度
					up=RCTC_TEMP[ TEMP_VOL_OFFSET[AiRang[i]]+TEMP_VOL_NUM[AiRang[i]]-1];//上限温度
					data = (32000*(data-down))/(up-down);
				}																			 		 
			}																				 		 
			else//TC的(0,20)mv,(0,50)mv,(0,100)mv三个量程 
			{																						 
				tmpr=RCTC_VOL[ TEMP_VOL_OFFSET[AiRang[i]]];//量程的最大码值
				//if(data>tmpr)data=tmpr;		20140522			 
				//else if(data<0)data=0;//负数设为0				 
				if(data<0)data=0;													 
				else if(data>tmpr)data=tmpr;							 
				if((Bx.CR[AI_ENGCL_CR]&(1<<i))==0)//不使能工程量
					data=(32000*data)/tmpr;
				else
				{            
					down=Bx.CR[AI_DGCL_CR+i];//工程量下限		
					up=Bx.CR[AI_UGCL_CR+i];//工程量上限
					data = ((up-down)*data)/tmpr+down;
				}
			}			 
		}
		#if defined(H04RC)||defined(H08RC)||defined(A04RC)
		Bx.CR[16+i]=data+Bx.CR[AI_0_CR+i];//零点偏移
		#endif														
		#if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC) 
		Bx.CR[16+i]=data;
		#endif			
  }
}
/******************************************************************************
* 函数名   :设置所有通道断线，AI数值显示为最大值
* 描述     ：
* 入口参数 ：
*******************************************************************************/
void SetAiOffline(void)
{
	u32 i,data,k;
	Bx.CR[AI_OFFLINE]=0xff>>(8-AI_AMOUNT);//设置所有通道断线。
	for(i=0;i<AI_AMOUNT;i++)
	{
		if((Bx.CR[AI_ENGCL_CR]&(1<<i))==0)//不使用工程量
			data=32000;
		else
		{
			k = Bx.CR[AI_RANG_CR+i];//更新信号类型
			AiRang[i] = k;
			if(k<10)data=RCTC_TEMP[ TEMP_VOL_OFFSET[k]+TEMP_VOL_NUM[k]-1];//上限温度
			else data =Bx.CR[AI_UGCL_CR+i];//工程量上限
		}
		Bx.CR[16+i]=data;
	}	
}
/******************************************************************************                        
* 函数名   ：RCTC通道切换
* 描述     ：
* 入口参数 ：
*******************************************************************************/
static void SwitchRtcChannel(void)
{							
  u8 i,j=0,k;
			
	/* 轮询通道切换 *****************************************************/	
	#if defined (H08TC) || defined (H04TC)||defined(A04TC)||defined(A08TC)//TC轮询方式	
	
  AiChannel++;
  if(AiChannel==AI_AMOUNT)
  {    
		LastCH++; 
		if(LastCH>=5)LastCH=0;//外补cu50 | 内补1 | 内补2 | 自校1 |自校2
    AiChannel=AI_AMOUNT+LastCH;		
  }
  else if(AiChannel>AI_AMOUNT)AiChannel=0;	
	#endif	
	
	#if defined (H08RC) || defined (H04RC) ||defined(A04RC)//RC轮询方式		
	if(StartupFlag>=2){
		if(AiChannel>=2*AI_AMOUNT) AiChannel =AI_AMOUNT;
		else if(AiChannel>=AI_AMOUNT && AiChannel<2*AI_AMOUNT) AiChannel -=AI_AMOUNT;
		else{													
			AiChannel++;									
			if(AiChannel == LastCH) AiChannel +=AI_AMOUNT;
			else if(AiChannel>=AI_AMOUNT)	{
				LastCH++;							 //上次M端通道号 	
				if(LastCH>=AI_AMOUNT)	{			
					LastCH=0;									
					AiChannel=2*AI_AMOUNT + ScCH;   //
			 		if(++ScCH >=6)						
						ScCH=0;									
				}														
			 	else AiChannel =0;			
			}
		}	 
	}		 
	else{
		if(AiChannel>=AI_AMOUNT && AiChannel<2*AI_AMOUNT) AiChannel -=AI_AMOUNT;
		else if(LastCH<AI_AMOUNT-1) AiChannel = ++LastCH +AI_AMOUNT;
		else {
			LastCH=0;
			AiChannel = AI_AMOUNT;		
			StartupFlag++ ;
			if(StartupFlag>=2){ //上电两轮计算线阻
				//计算线阻
				for(i=0;i<AI_AMOUNT;i++){
					if(FIFOCount[i]>=1)WireR[AI_AMOUNT+i]=WireRFIFO[i][FIFO_AMOUNT-1];
				}
			}
		}
	} 
	#endif		
	
  i=AiChannel;
  if(i<AI_AMOUNT)AiRang[i] = Bx.CR[AI_RANG_CR+i];//更新信号类型
  k=CH_SW[i];//bit3为INH,bit0-2为4051的ABC
	/* 配置片选 *****************************************************/
	#if defined(H04TC)||defined(H08TC)||defined(A04TC)||defined(A08TC)
  if(k&0x8)Dout595 |= 1<<INHOffset;//CD4051-2选通
  else  Dout595 &= ~(1<<INHOffset);//CD4051-1选通
	#endif
	#if defined(H04RC)||defined(H08RC)||defined(A04RC)
	if((k>>3)==1)
	{
		Dout595 &= ~(1<<CDOffset_1);		//开Q17
		Dout595 |= 1<<CDOffset_2;
		Dout595 |= 1<<CDOffset_3;
	}
	else if((k>>3)==2)
	{
		Dout595 |= 1<<CDOffset_1;
		Dout595 &= ~(1<<CDOffset_2);    //开Q16
		Dout595 |= 1<<CDOffset_3;
	}
	else 
	{
		Dout595 |= 1<<CDOffset_1;
		Dout595 |= 1<<CDOffset_2;
		Dout595 &= ~(1<<CDOffset_3);		//开Q20
	}
	Dout595 &= ~(1<<PDOffset);//释放ADC
	#endif
  Dout595 = (Dout595&(~(7<<AICHOffset)))|((k&7)<<AICHOffset);//CD4051的ABC选通位  Q10、Q11、Q12
	/* 配置量程 *****************************************************/
  #if defined (H08TC) || defined (H04TC)||defined(A04TC)||defined(A08TC)
  if(i>=AI_AMOUNT)
  { 
		if(LastCH==0)j=1;//热电阻
		else j=2;
  }
  else j = 1 + TCRANG[AiRang[i]];//j=1:0-38mv,j=2:0-114mv
  #endif
  #if defined (H08RC) || defined (H04RC) ||defined(A04RC)
	if(i<AI_AMOUNT*2)//140714
	{	
		if(AiRang[i&(AI_AMOUNT-1)]==1)j=2;//量程0-114mv
		else j=1;//量程(0,38)mv
	}
	else if(i>=AI_AMOUNT*2 && i<(AI_AMOUNT*2+3) )j=2;//RC校准输入通道前3个使用大量程
	else j=1;
  #endif    
  Dout595 =(Dout595&(~(3<<AIRGOffset)))|(j<<AIRGOffset);//Q14、Q15
  Dout595 &= (~(1<<AICSOffset));//拉低CS   Q13
  Shift595(Dout595);
}
/******************************************************************************                        
* 函数名   ：自校准用通道切换
* 描述     ：自校准参数计算时调用
* 入口参数 ：
*******************************************************************************/
static void SwitchRtcChannel_SC(u8 data)
{
	u8 k;
	#if defined(H08TC)||defined(A08TC)
	k=CH_SW[AI_AMOUNT+3+data];//
	#endif
	#if defined(H04TC)
	k=CH_SW[AI_AMOUNT+3+data];//
	#endif
	#if defined(A04TC)
	k=CH_SW[AI_AMOUNT+2+data];//
	#endif
	#ifdef H08RC
	k=CH_SW[AI_AMOUNT*2+data];
	#endif
	#if defined(H04RC)||defined(A04RC)
	k=CH_SW[AI_AMOUNT*2+data];
	#endif
	#if defined (H08TC) || defined (H04TC)||defined(A04TC)||defined(A08TC)
	if(k&0x8)Dout595 |= 1<<INHOffset;//CD4051-2
	else  Dout595 &= ~(1<<INHOffset);//CD4051-1
  #endif
	#if defined (H08RC) || defined (H04RC) ||defined(A04RC)
	if((k>>3)==1)	
	{																																										
		Dout595 &= ~(1<<CDOffset_1);
		Dout595 |= 1<<CDOffset_2;
		Dout595 |= 1<<CDOffset_3;
	}
	else if((k>>3)==2)
	{
		Dout595 |= 1<<CDOffset_1;
		Dout595 &= ~(1<<CDOffset_2);
		Dout595 |= 1<<CDOffset_3;
	}	
	else 
	{
		Dout595 |= 1<<CDOffset_1;
		Dout595 |= 1<<CDOffset_2;
		Dout595 &= ~(1<<CDOffset_3);
	}
	Dout595 &= ~(1<<PDOffset);//释放ADC
	#endif
	Dout595 = (Dout595&(~(7<<AICHOffset)))|((k&7)<<AICHOffset);
	#if defined (H08TC) || defined (H04TC)||defined(A04TC)||defined(A08TC)
	Dout595 =(Dout595&(~(3<<AIRGOffset)))|(2<<AIRGOffset);//TC校准通道使用大量程
	#endif
	#if defined (H08RC) || defined (H04RC)||defined(A04RC)
	if(RangeType==0)Dout595=(Dout595&(~(0x3<<AIRGOffset)))|(2<<AIRGOffset);//大量程
	else Dout595=(Dout595&(~(0x3<<AIRGOffset)))|(1<<AIRGOffset);//小量程
	#endif	
	Dout595 &= (~(1<<AICSOffset));	
	Shift595(Dout595);	
}

/******************************************************************************                        
* 函数名   ：计算自校准参数
* 描述     ：排序，取中间值平均滤波，计算
* 入口参数 ：
*******************************************************************************/
static void CalSCData(s32 *Val)
{
  s32 x0,x1,y0,y1;
	x0=x1=y0=y1=0;
	
	#if defined (H08TC) || defined (H04TC)||defined(A04TC)||defined(A08TC)
	Bx.CR[100]=Val[0];//在写入校准数据时一起保存
	Bx.CR[101]=Val[1];
	#endif
	
	if(bCalibrateFlag)				//bCalibrateFlag校准标志
	{
		#if defined (H08TC) || defined (H04TC)||defined(A04TC)||defined(A08TC)
		u8 t=6+6*0+3; //校准数据偏移			
		if((Val[0]>16000)||(Val[0]<11000)||(Val[1]>63000)||(Val[1]<58000))
		{
			SC.Exc=0;//自校准不开启
			if(Bx.CR[15]==0) Bx.CR[15]=18;//报错		
		}
		else
		{	
			if(Bx.CR[15]==18) Bx.CR[15]=0;//清除报错
			x0 =~(u32)CaliData[t]+1;//第一个校准数据是负数
			y0 =~CaliData[3]+1;//
			x1 =CaliData[t+1];  
			y1 =CaliData[4];//	
			Val[0]=Linear(Val[0],x0,x1,y0,y1);
			if(SCState==1 || S_AiFilter.Sum1==0)S_AiFilter.Sum1=Val[0];
			else{
				Val[0]=(S_AiFilter.Sum1*70+Val[0]*(100-70))/100;
				S_AiFilter.Sum1=Val[0];
			}
			x0 =(u32)CaliData[t+1];//
			y0 = CaliData[4];//
			x1 =CaliData[t+2];  
			y1 =CaliData[5];//
			Val[1]=Linear(Val[1],x0,x1,y0,y1);	
			if(SCState==1 || S_AiFilter.Sum2==0)S_AiFilter.Sum2=Val[1];
			else{
				Val[1]=(S_AiFilter.Sum2*70+Val[1]*(100-70))/100;
				S_AiFilter.Sum2=Val[1];
			}
			
			//Bx.CR[AI_OFFLINE+4]=Val[0];//显示自校值
			//Bx.CR[AI_OFFLINE+5]=Val[1];//
			//Val[0]是x0 ,Val[1]是x1 ; 那么y0是*(CaliData+CALIDATA_NUM/2-2)，y1是*(CaliData+CALIDATA_NUM/2-1)
			//a * x + b =y
			
			y0= *(CaliData+CALIDATA_NUM/2-2);
			y1= *(CaliData+CALIDATA_NUM/2-1);
			
			SC.Exc=1;//开自校准			
			SC.b = (float)(y0 * Val[1]-y1 *Val[0] )/(Val[1] - Val[0]);
			SC.a = (float)(y1-SC.b)/Val[1];																			
			SC.c = SC.a;																											
			SC.d = SC.b;			  																							
		}		
		#endif
		#if defined (H08RC) || defined (H04RC)||defined(A04RC)
			u8 i,k,tmpr;	
			u16 R1,R3;
		#ifdef H04_08RCTC
			R1 = CaliData[18+6*AI_AMOUNT];//r1 50
			R3 = CaliData[20+6*AI_AMOUNT];//R3 12000
		#endif	
		#ifdef A04_08RCTC
			R1 = CaliData[6+6*AI_AMOUNT*2]; //
			R3 = CaliData[8+6*AI_AMOUNT*2];
		#endif
			for(i=0;i<6;i++)
			{
				if(i>2) k=0;
				else k=3;
				if(i%3==0) k+=1;
				tmpr = 6+k;
				x0 = CaliData[tmpr];
				x1 = CaliData[tmpr+1];
				y0 = CaliData[k];
				y1 = CaliData[k+1];
				SC.LC[i]=Linear(Val[i],x0,x1,y0,y1);				
			}																																											
			SC.LC[0]-=SC.LC[2];SC.LC[1]-=SC.LC[2];SC.LC[3]-=SC.LC[5];SC.LC[4]-=SC.LC[5];
			SC.a = ((float)SC.LC[0]/SC.LC[1]-1)*R1;//大量程	R2
			SC.b = (float)SC.LC[1]/R1*(R1+R3+SC.a);//大量程VRef			
			SC.c = ((float)SC.LC[3]/SC.LC[4]-1)*R1;//小量程	R2
			SC.d = (float)SC.LC[4]/R1*(R1+R3+SC.c);//小量程VRef				
		#endif
	}
}
/******************************************************************************                        
* 函数名   ：码值的自校准参数修正
* 描述     ：将线性码值进行自校准参数修正
* 入口参数 ：
*******************************************************************************/
static s32 Calibrate_IO_Code(s32 data)
{	
		u16 i = AiChannel;
    #if defined (H08TC) || defined (H04TC) ||defined(A04TC) ||defined(A08TC)
		float vdata=data;	
		if(i<AI_AMOUNT)
		{
			vdata = SC.a * data + SC.b ;			
		}		
		else
		{	//冷端
			if(LastCH==0 || LastCH==1 || LastCH==2)//内外补
			{
				vdata = SC.c * data + SC.d ;
			}
		}	
		data =vdata;
		#endif
		return data;
}
/******************************************************************************                        
* 函数名   ：写自校准数据
* 描述     ：
*******************************************************************************/
void WriteSelfCaliData(void)
{
		s32 Val[2];
		u8 t=6+6*0+3; //校准数据偏移		
		s32 x0,x1,y0,y1;
		Val[0]=Bx.CR[100];
		Val[1]=Bx.CR[101];
		x0=x1=y0=y1=0;
		x0 =~(u32)CaliData[t]+1;//第一个校准数据是负数
		y0 =~CaliData[3]+1;//
		x1 =CaliData[t+1];  
		y1 =CaliData[4];//	
		Val[0]=Linear(Val[0],x0,x1,y0,y1);

		x0 =(u32)CaliData[t+1];//
		y0 = CaliData[4];//
		x1 =CaliData[t+2];  
		y1 =CaliData[5];//
		Val[1]=Linear(Val[1],x0,x1,y0,y1);		

		*(CaliData+CALIDATA_NUM/2-2)=Val[0];
		*(CaliData+CALIDATA_NUM/2-1)=Val[1];
}
/*******************************************************************************                        
* 函数名   ：线性校准公式
* 描述     ：公式：y=k*(x-x0)+y0   其中k=(y1-y0)/(x1-x0) 
*******************************************************************************/                        
s32 Linear(s32 x,s32 x0,s32 x1, s32 y0,s32 y1)
{
  s32 tmpr;
  tmpr = (y1-y0)*(x-x0);
  tmpr = tmpr/(x1-x0)+y0;
  return tmpr;
}
/******************************************************************************                        
* 函数名   ：64位数除法
* 描述     ：被除数64位，除数31为，结果32位
* 入口参数 ：
*******************************************************************************/                        
u32 Div64bitBy31bit(u32 datal,u32 datah,u32 divisor)
{
  u8 a;
  u32 tmpr,result,data;
  if(divisor==0)divisor=0;
  if(divisor>0x7fffffff) divisor=0x7fffffff;
  tmpr=0;
  data=datah;
  result=0;
  for(a=0;a<64;a++)
  {
    tmpr=tmpr<<1;
    if(data&0x80000000)tmpr |= 1;
    data=data<<1;     
    result=result<<1;
    if(tmpr>=divisor)
    {
      tmpr=tmpr-divisor;
      result|=1;
    }
    if(a==31)data=datal;
  }
  return result;
}
/******************************************************************************                        
* 函数名   ：电压码值计算电阻值
* 描述     ：根据47k两端电压求电流，电压除电流得电阻，
* 入口参数 ：rang：量程，data0：47k上拉电阻下端电压码值，data：待测电阻两端电压码值
* 返回值   ：待测电阻值：单位毫欧
*******************************************************************************/                        
u32 Code2Resistance(u8 rang,u32 data0,u32 data)
{
  u32 tmpr;
  if(rang==1)//(0,114mv)
    tmpr=114;//k为满量程电压值/mv
  else tmpr=38;
  tmpr = 64*2500000/tmpr - data0;
  data = 47000*data; 
  data = Div64bitBy31bit(((data<<10)&0xfffffc00),((data>>22)&0x3ff),tmpr); //放大1024倍再除
  data = data*1000/1024;
  return data;
}
/******************************************************************************                        
* 函数名   ：电信号和温度值转换
* 描述     ：通过对比确定data数据在数组RCTC_VOL[]中的大小区间范围
* 入口参数 ：flag:为1时：电信号转温度;data：数据。rang：信号类型编号
* 出口参数 ：返回n，
*******************************************************************************/  
s32 ConvertTempElectric(u8 flag,u16 rang,s32 data) 
{  
  s32 *p,*t,a,b,c,d;
  if(flag)//电信号转温度
  {
    p=(s32 *)RCTC_VOL;  
    t=(s32 *)RCTC_TEMP; 
  }
  else//温度转电信号 
  {
    p=(s32 *)RCTC_TEMP;
    t=(s32 *)RCTC_VOL;
  }
  a = TEMP_VOL_NUM[rang];//拟合参数个数               
  b = TEMP_VOL_OFFSET[rang];//拟合参数起始偏移         
	
  if(data < *(p+b))//小于最小值
    data = *(p+b);
  if(data > *(p+b+a-1))//大于最大值
    data = *(p+b+a-1);
	
  b=1;
  while(b<a)b=b<<1;//大于等于j最小的2的次幂。
  c = TEMP_VOL_OFFSET[rang]+(b>>1);
  b =b>>2;
  d = TEMP_VOL_NUM[rang]+TEMP_VOL_OFFSET[rang]-1;//最后一个参数的偏移 
  while(b)
  {        
    if(data >= *(p+c)) c+=b;
    else c -= b;         
    if(c > d )c = d;        
    b=b>>1;
  }
  if(data <= *(p+c))c--;//n的含义为：RCTC_VOL[n]<data<RCTC_VOL[n+1]
  rang=c;
  a = *(p+rang);
  b = *(p+rang+1);  
  c = *(t+rang);
  d = *(t+rang+1);
  data = (d - c)*(data - a);
  data = data/(b-a);
  data = data + c; 
  return data;
}
/*******************************************************************************                        
* 函数名  ：排序
* 描述    ：
*******************************************************************************/ 
static void Sort(s32 *Array,u8 data)
{	
	u8 p,q;
	s32 temp;
	for(p=1;p<data;p++)
		for(q=0;q<data-p;q++)
		{
			if( Array[q]>Array[q+1] )
			{
				temp = Array[q];
				Array[q] = Array[q+1];
				Array[q+1] = temp;
			}
		}
}
/*******************************************************************************                        
* 函数名  ：平均
* 描述    ：舍掉头尾
*******************************************************************************/ 
static s32 AverMid(s32 *Array,u8 data)
{
	s32 p=0;
	u8 temp;
	for(temp=1;temp<(data-1);temp++)//舍弃2个	
		p = p + Array[temp];												
	p = p/(data-2); 	
	return p;
}
/*******************************************************************************                        
* 函数名  ：RTC抗通道串扰
* 描述    ：
*******************************************************************************/ 
static void RTCResCrosst(void)
{
		#if defined(H04RC) || defined(H08RC)||defined(A04RC)
//		ComSpiByte(START_CONTINUOUS); 			    
		Dout595 |= 1<<CDOffset_1; Dout595 |= 1<<CDOffset_2; Dout595 |= 1<<CDOffset_3;//片选全关
		Shift595(Dout595);			
		RCDelay=0;
		while(RCDelay<2);//延时200~400us																																																		
		Dout595 |= 1<<PDOffset;//拉低ADC
		Shift595(Dout595);								 
		RCDelay=0;												 
		while(RCDelay<10);//    15				 								
		Dout595 &= ~(1<<PDOffset);//释放ADC
		Shift595(Dout595);								 
		RCDelay=0;												 
		while(RCDelay<2);//延时200~400us	 																			
//		ComSpi40Bit(0xFF,0);//读AD转换结果
		#endif
		#if defined (H08TC) || defined (H04TC) ||defined(A04TC)||defined(A08TC)
		if(AiChannel == AI_AMOUNT)
		{
			Dout595 |= 1<<INHOffset;//CD4051-2选通
			Dout595 = (Dout595&(~(7<<AICHOffset)))|(1<<AICHOffset);//ABC通道位
			Dout595 &= (~(1<<AICSOffset));//拉低AICS
			Shift595(Dout595);//切换到接地的通道
			RCDelay=0;
			ComSpiByte(START_CONTINUOUS);
			while(RCDelay<250);//延时50ms
			ComSpi40Bit(0xFF,0);//读AD转换结果
		}				
		#endif
}			
#endif
/*******************************************************************************                        
* 函数名  ：上电自校准参数计算
* 描述    ：1、切换通道2、读码值3、调用码值线性校准与计算自校准参数函数
*******************************************************************************/ 
void SelfCaliCalc(void)
{												
#if defined(H04_08RCTC)||defined(A04_08RCTC) 											
	u32 i;	
	if(CSMessage == sDELAY){
		if(RTCTimeout >= 100){ //延时100ms			
			CSMessage = sSYNC;
			SC.Exc=0;	
			SC.Num=0;	
			SC.NumX=0;
			#if defined(H04RC)||defined(H08RC)
			Dout595 |= 1<<CDOffset_1;
			Dout595 |= 1<<CDOffset_2;
			Dout595 &= ~(1<<CDOffset_3);
			#endif
			#if defined(A04RC)
			Dout595 |= 1<<CDOffset_1;
			Dout595 &= ~(1<<CDOffset_2);
			#endif
		}												
	}
	if(CSMessage == sSYNC){
		Dout595 &= ~(1<<AICSOffset);//拉低CS  
		Shift595(Dout595);
		for(i=0;i<30;i++)
		ComSpiByte(SYNC1); 
		ComSpiByte(SYNC0);	
		CSMessage = sSET_RS;
		Count=0;//计数复位RS失败次数
	}
	if(CSMessage == sSET_RS){
		ComSpi40Bit(Write_CONFIG,SET_RS);//复位RS
		i=ComSpi40Bit(Read_CONFIG,0);//读配置寄存器
		if((i&CONFIG_RV)){ //查询RV=1  
			CSMessage = sRST_RV;
			Count=0;//计数清除Rv失败次数
		}
		else{
			if(Bx.CR[15]==0)Bx.CR[15]=16;//报错，复位CS5530错误
			SetAiOffline();	
			Count++;
			if(Count>3)CSMessage=sDELAY;	
		}
	}
	if(CSMessage == sRST_RV){
		ComSpi40Bit(Write_CONFIG,0);//清除RV
		i=ComSpi40Bit(Read_CONFIG,0);//读配置寄存器
		if((i&CONFIG_RV)==0)//查询RV=0
			 CSMessage = sWR_CONFIG;
		else{
			if(Bx.CR[15]==0)Bx.CR[15]=16;//报错，复位CS5530错误
			SetAiOffline();
			Count++;
			if(Count>3)CSMessage=sDELAY;	
		}
  }	
	if(CSMessage == sWR_CONFIG){
		ComSpi40Bit(Write_CONFIG,(0x02000000|(1<<11)));//测试写配置：参考电压<2.5,双极性模式，字速率30Hz
		Dout595 |= (1<<AICSOffset);//拉高CS	
		Shift595(Dout595);								
		Dout595 &= (~(1<<AICSOffset));//拉低AICS
		Shift595(Dout595);
		CSMessage = sCONVERT;																																											
	}	
	if(CSMessage == sCONVERT){
		SwitchRtcChannel_SC(SCR_CH1);//切换到通道
		ComSpiByte(START_CONTINUOUS);//开启连续转换  
		CSMessage=sQUERY_SDO;
    DiscardCnt=0;//读计数
		Count=0;
    RTCTimeout=0;
	}
	/* 读取自校通道1码值 *****************************************************/
	if(CSMessage == sQUERY_SDO){
		u32 udata;	s32 sdata;
		if((GPIOB->IDR&MISO) == 0){	//SDO被拉低																		
			if(DiscardCnt>=3){																		
				udata=ComSpi40Bit(0,0);//读转换结果
				if((udata&0xff)==0 || (udata&0xff)==0x04)//如果数据有效  
				{																				
					sdata=udata>>15;								
					if(sdata&0x10000) sdata|=0xffff0000;//读到负电压
					if(RangeType==0) SC.HC0[Count]=sdata;//大量程 
					else SC.HC3[Count]=sdata;//小量程					
					Count++;												
					RTCTimeout=0;										
					if(Count>=1){	//只取一次		 															
						CSMessage = sQUERY_SDO2;			
						SwitchRtcChannel_SC(SCR_CH2);//切换到通道
						Count=0;										
						DiscardCnt=0;//读计数				
						RTCTimeout=0;								
					}																
				}																 
				else{	//无效数据														
					CSMessage = sDELAY;				
					RTCTimeout=0;					
				}
			}
			else {
				ComSpi40Bit(0,0);//丢弃
				DiscardCnt++;
				RTCTimeout=0;
			}
		}
		else if(RTCTimeout>1000){ //限时到
			 CSMessage = sDELAY;
			 RTCTimeout=0;			
		}
	}
	/* 读取自校通道2码值 *****************************************************/
	if(CSMessage == sQUERY_SDO2){
		u32 udata;	s32 sdata;
		if((GPIOB->IDR&MISO) == 0){ //SDO被拉低
			if(DiscardCnt>=3){
				if(Count>=1){
					#if defined(H04RC) || defined(H08RC)||defined(A04RC)
					CSMessage = sQUERY_SDO3;
					SwitchRtcChannel_SC(SCR_CH3);//切换到通道
					Count=0;
					DiscardCnt=0;//读计数
					RTCTimeout=0;
					#endif
					#if defined(H04TC) || defined(H08TC)||defined(A04TC)||defined(A08TC)
					s32 Val[2];
					ComSpi40Bit(0xff,0);//停止ADC转换	
					CSMessage=sDELAY;
					AIMessage=sDELAY;
//					Sort(SC.HC0,SC_NUM);Sort(SC.HC1,SC_NUM);
//					Val[0]=AverMid(SC.HC0,SC_NUM);Val[1]=AverMid(SC.HC1,SC_NUM);
					Val[0]=SC.HC0[0];Val[1]=SC.HC1[0];
					CalSCData(Val);				
					SCState=0;//自校参数计算完成	
					Count=0;RTCTimeout=0;Dout595=0;						
					#endif
				}																
				else{
					udata=ComSpi40Bit(0x00,0);//读转换结果
					if((udata&0xff)==0 || (udata&0xff)==0x04){ //如果数据有效  				
						sdata=udata>>15;							
						if(sdata&0x10000) sdata|=0xffff0000;//读到负电压
						if(RangeType==0) SC.HC1[Count]=sdata; 
						else SC.HC4[Count]=sdata;			  
						Count++;											 
					}
					else { //无效数据
						CSMessage = sDELAY;
						RTCTimeout=0;
					}			
				} 		
			}
			else{
				ComSpi40Bit(0,0);//丢弃
				DiscardCnt++;
			}
			RTCTimeout=0;		
		}
		else if(RTCTimeout>1000){ //限时到
			CSMessage = sDELAY;
			RTCTimeout=0;		
		}		
	}
	/* 读取自校通道3码值 *****************************************************/
	if(CSMessage == sQUERY_SDO3){
		u32 udata;	s32 sdata;
		if((GPIOB->IDR&MISO) == 0){ //SDO被拉低
			if(DiscardCnt>=3){
				if(Count>=1){
					s32 Val[6];
					ComSpi40Bit(0xff,0);//停止ADC转换			
					CSMessage=sSYNC;
					AIMessage=sDELAY;
					if(RangeType!=0){	
//						Sort(SC.HC0,SC_NUM);Sort(SC.HC1,SC_NUM);Sort(SC.HC2,SC_NUM);
//						Sort(SC.HC3,SC_NUM);Sort(SC.HC4,SC_NUM);Sort(SC.HC5,SC_NUM);//排序数组
//						Val[0]=AverMid(SC.HC0,SC_NUM);Val[3]=AverMid(SC.HC3,SC_NUM);
//						Val[1]=AverMid(SC.HC1,SC_NUM);Val[4]=AverMid(SC.HC4,SC_NUM);
//						Val[2]=AverMid(SC.HC2,SC_NUM);Val[5]=AverMid(SC.HC5,SC_NUM);//平均
						Val[0]=SC.HC0[0];Val[3]=SC.HC3[0];
						Val[1]=SC.HC1[0];Val[4]=SC.HC4[0];
						Val[2]=SC.HC2[0];Val[5]=SC.HC5[0];						
						CalSCData(Val);						
						SCState=0;//自校参数计算完成				 
					}	
					Count=0;			  	
					RTCTimeout=0;				
					RangeType=1;						
				}
				else {
					udata=ComSpi40Bit(0x00,0);//读转换结果
					if((udata&0xff)==0 || (udata&0xff)==0x04){ //如果数据有效  
						sdata=udata>>15;										
						if(sdata&0x10000) sdata|=0xffff0000;//读到负电压
						if(RangeType==0) SC.HC2[Count]=sdata; 
						else SC.HC5[Count]=sdata;			  
						Count++;							
					}
					else{ //无效数据
						CSMessage = sDELAY;
						RTCTimeout=0;
					}	
				}
			}
			else{
				ComSpi40Bit(0,0);//丢弃
				DiscardCnt++;
			}
			RTCTimeout=0;		
		}
		else if(RTCTimeout>1000){ //限时到
			CSMessage = sDELAY;
			RTCTimeout=0;				
		}	
	}
#endif	
}																																							
/*************************************************************************
*函数：排序
*入口参数：数组Array、排序数量data
**************************************************************************/
void sort(u32 *Array,u8 data)
{
	u8 p,q;
	u32 temp;
	for(p=1;p<data;p++)
		for(q=0;q<data-p;q++)
		{
			if((s32)Array[q]>(s32)Array[q+1])
			{
				temp = Array[q];
				Array[q] = Array[q+1];
				Array[q+1] = temp;
			}
		}
}
